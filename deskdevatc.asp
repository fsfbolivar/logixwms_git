 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%
//Se valida que aun la session sea válida
ValidarSession();

Session("DEV")="";

if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
  
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Ordenar(tipo,dir,lugar)
{
	document.getElementById("BUSQ_ORD").action+="?tipo="+tipo+"&dir="+dir+"&lug="+lugar;
	document.getElementById("BUSQ_ORD").submit();
}
function BusqDesk()
{
if(document.getElementById("nro_ord").value=="" && document.getElementById("nb_clte").value=="" && document.getElementById("fecomp").value=="" && document.getElementById("fecomp2").value=="" )
{
	alert("Debe indicar alguno de los campos para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_DESK").submit();
}
}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
function ImpOrd()
{
var ordenes=document.getElementsByName("IMPORD");
var hay=false;
for(var i=0;i<ordenes.length && !hay;i++)
{
	if(ordenes.item(i).checked)
		hay=true;
}
if(!hay)
	alert("Indique las ordenes que desea imprimir");
else
	document.getElementById("ordenes").submit();

}
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a), <%=Request.Cookies("usr")("nombre")%> </td>
  </tr>
  <tr>
    <td width="115"></td>
    <td width="631"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
	
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="6" class="tituloblanco"><div align="center">B�squeda de devoluciones en elaboraci&oacute;n </div></td>
        </tr>
        <tr class="textoformulario">
          <td width="20%" class="textoformulario">Nro devoluci&oacute;n </td>
          <td width="18%">Cliente</td>
         
          <td>Fecha de devoluci&oacute;n(desde)</td>
          <td>Fecha de devoluci&oacute;n(hasta)</td>
          <td>&nbsp;</td>
        </tr>
        <form id="FRM_DESK" name="FRM_DESK" action='deskdevatc.asp?bsq=1' method="post">
        <tr class="">
          <td height="19" class="textoformulario"><input name="nro_ord" id="nro_ord" type="text" class="texto" size="15" maxlength="15"></td>
          <td class="textoformulario">
          <input type='text' class='texto' id='nb_clte' name='nb_clte' size='15' maxlength='60'>
          </td>
          <td width="25%" class="textoformulario"><table border=0 cellspacing=1 cellpadding=0><tr><td><input name="fecomp" id="fecomp" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fecomp","-1");'></td><td><A HREF='JavaScript:IniciarCalendario("fecomp");'><img border='0' src='images/cal.gif'></A></td></tr></table></td>
        <td width="22%" class="textoformulario"><table border=0 cellspacing=1 cellpadding=0>
          <tr>
            <td><input name="fecomp2" id="fecomp2" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fecomp2","-1");'></td>
            <td><A HREF='JavaScript:IniciarCalendario("fecomp2");'><img border='0' src='images/cal.gif'></A></td>
          </tr>
        </table></td>
        <td width="15%" class="textoformulario"><input name="Submit" onClick="JavaScript:BusqDesk();" type="button" class="texto" value="Buscar"></td>
        </tr>
        </form>
</table>
      <p class="textonegrita">Devoluciones en elaboraci&oacute;n </p>
      <table width="100%"  border="0" cellpadding="1" cellspacing="1" id="DET_ORD">
      <form id='ordenes' name='ordenes' action='ImpOrd.asp' method='post' target="_blank">
        <tr class="tituloblanco">
          <td width="6%">&nbsp;</td>
          <td width="25%">Nro de devoluci&oacute;n</td>
          <td width="41%">Cliente</td>
          <td width="28%">Fecha de devoluci&oacute;n </td>
          </tr>
        <%
        strSQL="SELECT ID_DEV, COD_DEV, NB_CLTE_DEV, CONVERT(VARCHAR(10),FE_REG,103) FE_REG FROM T_SANT_DEV WHERE ID_ST=0 AND ID_CHEQ="+intIdVend;
		if(Request.Form.Count>0)
		{
			if(Request.QueryString("bsq")=="1")
			{
				var nro_ord=Request.Form("nro_ord")+"";
				var nb_clte=Request.Form("nb_clte")+"";
				var fecomp=Request.Form("fecomp")+"";
				var fecomp2=Request.Form("fecomp2")+"";
				
				if(nro_ord.length>0)
				{
					strSQL+=" AND COD_DEV = '"+nro_ord+"'";
				}
				if(nb_clte.length>0)
				{
					strSQL+=" AND NB_CLTE_DEV LIKE '%"+nb_clte+"%'";
				}
				
				if(fecomp.length>0)
				{
					strSQL+=" AND CONVERT(VARCHAR(12),FE_REG,112)>= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fecomp+"',103),112) ";
				}
				if(fecomp2.length>0)
				{
					strSQL+=" AND CONVERT(VARCHAR(12),FE_REG,112)<= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fecomp2+"' ,103),112)";
				}				
			
				
			}		
		}
		strSQL+=" ";
		Rs=Ejecutar(strSQL,2);
		if(Rs.EOF)
		{%>
        <tr class="textoformulario">
          <td colspan=4>No devoluciones en elaboraci&oacute;n </td>
        </tr>
		<%
		}
		var edit=false;
		while(!Rs.EOF)
		{
		%>
        <tr class="textoformulario">
          <td><input type='checkbox' NAME='IMPORD' ID='IMPORD' VALUE='<%=Rs("COD_DEV")%>'></td>
          <td><% Response.Write("<a href='ordendevatc.asp?iddev="+Rs("ID_DEV")+"'>"+Rs("COD_DEV")+"</a>");
          %></td>
          <td><%=Rs("NB_CLTE_DEV")%></td>
          <td><%=Rs("FE_REG")%></td>
          </tr>
       <%
       Rs.MoveNext();
       }
       Rs.Close();
       %>
       </form>
      </table>      
      <p>&nbsp; </p>
    <p>&nbsp;</p></td>
  </tr>
</table>

</body>
</html>
<%
Desconectar();
%>