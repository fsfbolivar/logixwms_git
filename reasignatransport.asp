<%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>GRUPO SANTILLANA</title>
		<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
	</head>
	<% 
		ValidarSession();
		
		if(Conectar()!=0)
			Response.Redirect("index.asp?error=1");
			
		var sel_trans=Request.Form("sel_trans")+"";
	%>
	<script language="javascript" type="text/javascript">
	
	<%
		if(sel_trans!="" && sel_trans!="undefined"){ //se reasigna el transportista
			strSQL="UPDATE T_SANT_TRANSP SET STATUS_TRANSP=2 WHERE CI_TRANSP='"+sel_trans+"'";
			Ejecutar(strSQL,1);
			strSQL="UPDATE T_SANT_GUIA_CARGA SET ID_TRANSP='"+sel_trans+"' WHERE ID_GUIA_CARGA="+Request.QueryString("idg")+" AND ID_TRANSP="+Request.QueryString("transp")+"";
			Ejecutar(strSQL,1);
			strSQL="UPDATE T_SANT_TRANSP SET STATUS_TRANSP=1 WHERE CI_TRANSP='"+Request.QueryString("transp")+"'";
			Ejecutar(strSQL,1);
			Response.Write("window.opener.location.href='Guia_Carga.asp?transp="+sel_trans+"&idg="+Request.QueryString("idg")+"';");
			Response.Write("window.close();");
		}
	%>
	
		resizeTo(400,150);
		function validar(){
			return (document.getElementById('sel_trans').selectedIndex!=0 && document.getElementById('sel_trans').selectedIndex!=-1);
		}
	</script>
	<body>
		<form name="asignar" method="post" action="reasignatransport.asp?idg=<%=Request.QueryString("idg")%>&transp=<%=Request.QueryString("transp")%>" onsubmit="javascript: return(validar());">
			<table width="100%" border="0" align="center" cellpadding="2" cellspacing="1" class="tablamenu">
				<tr class="tituloblanco">
					<td>Transportista</td>
				</tr>
				<tr class="textoformulario">
					<td>
						<select name="sel_trans" id="sel_trans" class="texto">
							<option value=""></option>
							<%
								strSQL="SELECT *  FROM T_Sant_Transp WHERE Status_Transp = 1 and IN_ST=1 ORDER BY NB_Transp";
								Rs2=Ejecutar(strSQL,2);
								while(!Rs2.EOF){
									Response.Write("<option value='"+ Rs2("CI_Transp").Value +"'>"+ Rs2("NB_Transp").Value +"&nbsp;"+ Rs2("AP_Transp").Value +"</option>");
									Rs2.MoveNext();
								}
								Rs2.Close();
							%>
						</select>
					</td>
				</tr>
			</table>
			<br />
			<div align="right">
			<input class="texto" type="submit" name="asignar" id="asignar" value="Asignar" />
			</div>
		</form>
	</body>
</html>
<%
	Desconectar();
%>