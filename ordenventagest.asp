<%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var cantSol=0;
var cantEmb=0;
var cantDif=0;
var txOrd="";
var nbCliente="";
var dirClte="";
var tpoOrd="";
var telOrd="";
var ciuOrd="";
var txObs="";
var feEmb="";
var feAtc="";
var nbUsrAtc="";

var perf=parseFloat(Request.Cookies("usr")("cr"));
//**************************************************************************************
//	Marco que usuario de atenci�n al cliente la abri� por primera vez
//**************************************************************************************
if(parseFloat(perf)==5)
{
	strSQL="UPDATE T_SANT_ORD SET ID_USR_ATC="+intIdVend+", FE_USR_ATC=GETDATE() WHERE ID_ORD="+idOrd+" AND ID_USR_ATC IS NULL";
	Rs=Ejecutar(strSQL,2);
}

strSQL="SELECT CONVERT(VARCHAR(12),A.FE_USR_ATC,103) FE_USR_ATC , B.NB_USR, A.CIU_ENT, A.DIR_ENT,A.TEL_CLTE1, A.TEL_CLTE2, A.ID_ST_ORD, A.ID_ORD, A.ID_ORD_GP, CASE A.ID_TPO_ORD WHEN 0 THEN 'Venta' ELSE 'Consignaci�n' END ID_TPO_ORD, ";
strSQL+=" A.NB_CLTE, CONVERT(VARCHAR(12),A.FE_EMB_ORD,103) FE_EMB_ORD, CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD, 'Por embalar' ST, isnull(A.OBS,'') obs";
strSQL+=" FROM T_SANT_ORD A , T_SANT_USR B WHERE A.ID_ORD="+idOrd+"  AND A.ID_USR_ATC=B.ID_USR ";

Rs=Ejecutar(strSQL,2);
feEmb=Rs("FE_EMB_ORD").Value
txOrd=Rs("ID_ORD_GP").Value;
nbCliente=Rs("NB_CLTE").Value;
dirClte=Rs("DIR_ENT").Value;
tpoOrd=Rs("ID_TPO_ORD").Value;
telOrd=Rs("TEL_CLTE1").Value+" "+Rs("TEL_CLTE2").Value;
ciuOrd=Rs("CIU_ENT").Value+"";
txObs=Rs("OBS").Value+"";
nbUsrAtc=Rs("NB_USR").Value+"";
feAtc=Rs("FE_USR_ATC").Value+"";

Rs.Close();

%>

 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
var libPend=0;

function Guardar(tpo)
{

	var obs=document.getElementById("obs").value+"";
	if(obs.length==0)
	{
		alert("Debe indicar las observaciones");
		return(false);
	}

	if(parseFloat(tpo)==1)
	{
		if(window.confirm("�Est� seguro de cerrar esta orden?"))
		{
			obs="[ORDEN CERRADA] "+obs;
		}
		else
			return(false);
	}
	var idOrd=document.getElementById("ID_ORD").value;
	var url="ProcesarGest.asp?tpo="+tpo+"&idOrd="+idOrd+"&obs="+escape(obs);
	document.location.href=url;

}
var http=null;
function IniciarHTTP()
{
try{
	if (window.ActiveXObject)
    {
        // pero si es IE
        try 
        {
            http = new ActiveXObject ("Msxml2.XMLHTTP");
        }
        catch (e)
        {
            // en caso que sea una versi�n antigua
            try
            {
                http = new ActiveXObject ("Microsoft.XMLHTTP");
            }
            catch (e)
            {
            }
        }
    }
    else
    {
		http=null;
		alert("Aplicacion dise�ada solo para Internet Explorer");
    }
 }
 catch(e)
 {
	http=null;
 }
}


function CerrarCaja()
{
cajaEnProceso=false;
}
function RegSerial(serial,pos)
{
try{
	var idOrd=document.getElementById("ID_ORD").value;
	var Cant=parseFloat(document.getElementById("cant_serial").value);
	
	var url="RegSerial.asp?idOrd="+idOrd+"&ser="+serial+"&cant="+Cant;
	StSerial("Registrando codigo");
	document.getElementById("SERIAL").disabled=true;
	
	if(!http)
		IniciarHTTP();
		
	if(!http)
	{
		StSerial("Error registrando el codigo");
		return(false);
	}
	
	http.open ('GET', url, false); // asignamos los m�todos open y send
    http.send (null);	
    var retorno=http.responseText;
    //alert("ret:"+retorno.toString());
    
    var idResp=retorno.split(',')[0];
    var msgResp=retorno.split(',')[1];
    
    if(parseFloat(idResp)!="0")
	{
		if(parseFloat(idResp)==5)
			document.getElementById("Submit2").disabled=true;

		alert(msgResp);
		return(false);
	}
	
	if(true)
	{
	
	for(var i=0;i<arrProd.length;i++)
	{
		document.getElementById("FILA_"+i).className='textoformulario';
	}
	
		document.getElementById("FILA_"+pos).className='textoformularioRes';
		
		document.getElementById("CANT_EMB_"+pos).innerHTML=eval(parseFloat(document.getElementById("CANT_EMB_"+pos).innerHTML)+Cant);
		document.getElementById("CANT_DIF_"+pos).innerHTML=eval(parseFloat(document.getElementById("CANT_DIF_"+pos).innerHTML)-Cant);
		document.getElementById("CANT_DIF").innerHTML=eval(parseFloat(document.getElementById("CANT_DIF").innerHTML)-Cant);
		document.getElementById("CANT_EMB").innerHTML=eval(parseFloat(document.getElementById("CANT_EMB").innerHTML)+Cant);
		document.getElementById("cant_serial").value=1;
		cajaEnProceso=true;
		if(libPend==10 || Cant>=10)
		{
			alert("Recuerde que debe imprimir las etiquetas");
			libPend=0;
		}
		else
			libPend++;
	}
}
catch(e)
{
	alert(e.description);
}
finally
{
	StSerial("");
	document.getElementById("SERIAL").disabled=false;
}
}
var arrSer=new Array(Array(),Array()); //id producto, codigo barras
var arrProd=new Array();

function IniciarProducto(codigo) //Registro la posicion del producto para luego actualizar los valores
{
	arrProd[arrProd.length]=codigo;
}

function IniciarSerial(codigo,serial)//codigo producto (gp), codigo barras
{
	var i=arrSer[0].length;
	arrSer[0][i]=codigo;
	arrSer[1][i]=serial;
}


function StSerial(texto)
{
	document.getElementById("ST_SERIAL").innerHTML=texto;
}
function ImpEtiqueta()
{
if(window.confirm("�Est� seguro que desea imprimir la etiqueta?"))
{
	var idOrd=document.getElementById("ID_ORD").value;
	window.open("ImpEtiqueta.asp?idOrd="+idOrd,"impetiqueta","width=700,height=500,modal=yes,status=no, menubar=no, scroll=1, scrollbars=1");
}
}
function IngresoSerial(serial)
{

	var valido=false;
	var codigo='';
	var pos=-1;
	
	if(event.keyCode==13 || event.keyCode==8  || event.keyCode==27  )
		return(false);
	if(serial.length==13)
	{
		StSerial("Validando codigo");		
		for(var i=0;i<arrSer[0].length && !valido;i++)
		{
			if(serial==arrSer[1][i])
			{
				codigo=arrSer[0][i];
				valido=true;
			}
		}
	if(valido)
	{
		for(var i=0;i<arrProd.length;i++)
		{
			if(arrProd[i]==codigo)
			{
				pos=i;
				break;
			}
				
		}
		if((parseFloat(document.getElementById("CANT_DIF_"+pos).innerHTML)-parseFloat(document.getElementById("cant_serial").value))<0)
		{
			alert("Esta sobrepasando la cantidad de libros necesarios");
			StSerial("Cantidad excedida");		
		}
		else
		{
			RegSerial(serial,pos);
			
		}
	}
	else
	{		
		alert("El codigo introducido no se encuentra dentro de los solicitados en este orden de venta.");
		StSerial("Codigo invalido");	
	}
	
	document.getElementById("cant_serial").value=1;
	document.getElementById("serial").value="";
	document.getElementById("serial").focus();
	
	
	}
	
}
//////////////////////////////////////////////////////////////////////////////////////////////
function ImpEtiquetas(tpo)	{ 																//																					//
	var idOrd=document.getElementById("ID_ORD").value;										//
window.open("ImpProcesar1.asp?tipo="+tpo+"&idOrd="+idOrd,"impetiqueta",					"width=800,height=600,modal=yes,status=no, menubar=no,scrollbars=1,scroll=1,scrollbar=1");	//
}
////////////////////////////////////////////////////////////////////////////////////////////
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
var cajaEnProceso=false;
</script>
<input type='hidden' name='ID_ORD' id='ID_ORD' value='<%=idOrd%>'>

<table width="758" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="6" class="textonegrita" ><p><img src="images/logo.png" width="257" height="53"></p>
        <p>&nbsp;</p></td>
  </tr>
</table>
<table width="867" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="108"></td>
    <td width="701"></td>
  </tr>
  <tr>
    <td height="605" valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
    <%
    if(Request.QueryString("msg").Count>0)
    {%>
    <div class='texto_rojo' align='center'><%=Request.QueryString("msg")%></div><br>
    <%}%>
      <table width="100%"  border="0" cellpadding="2" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="2" class="tituloblanco"><div align="center">Datos del pedido </div></td>
        </tr>
        <tr class="textoformulario">
          <td width="22%" class="textoformulario">Nro pedido </td>
          <td width="78%"><%=txOrd%>
          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td><%=nbCliente%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Direcci&oacute;n</td>
          <td><%=dirClte%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Ciudad</td>
          <td><%=ciuOrd%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Tipo</td>
          <td class="textoformulario"><%=tpoOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Telefonos</td>
          <td class="textoformulario"><%=telOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Fecha embalaje</td>
          <td class="textoformulario"><%=feEmb%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Visualizado primera vez por </td>
          <td class="textoformulario"><%=nbUsrAtc%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Fecha visualizaci&oacute;n </td>
          <td class="textoformulario"><%=feAtc%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Fecha cierre </td>
          <td class="textoformulario">&nbsp;</td>
        </tr>
	</table>
<br>
      <br>    
      <table width="758" border="0" cellpadding="2" cellspacing="1" class="tablamenu">
        <tr class="">
          <td colspan="6" class="tituloblanco">Datos del pedido </td>
        </tr>
        <tr class="titulo_subgrupo">
          <td width="172" class=""><div align="center">C&oacute;digo del producto </div></td>
          <td width="384"><div align="center">Descripci&oacute;n</div></td>
          <td width="57"><div align="center">Cantidad solicitada</div></td>
          <td width="59"><div align="center">Cantidad embalada </div></td>
          <td width="60"><div align="center">Cantidad pendiente </div></td>
        </tr>
        <%
        strSQL="SELECT A.SER_PROD,LTRIM(RTRIM(A.ID_PROD)) ID_PROD, A.DESC_PROD, A.CANT_ORD, sum(ISNULL(B.CANT_PROD,0)) EMB, A.CANT_ORD-SUM(ISNULL(B.CANT_PROD,0)) DIF ";
		strSQL+=" FROM T_SANT_DET_ORD A LEFT JOIN T_SANT_DET_CAJA_ORD B ";
		strSQL+=" ON A.ID_ORD=B.ID_ORD AND A.ID_PROD=B.ID_PROD ";
		strSQL+=" WHERE A.ID_ORD="+idOrd;
		strSQL+=" GROUP BY A.SER_PROD, A.ID_PROD, A.DESC_PROD, A.CANT_ORD";
		//Response.Write(strSQL);
		Response.Flush();
		Rs=Ejecutar(strSQL,2);
		var posicion=0;
		while(!Rs.EOF)
		{
        %>        
        <tr class="<%if(parseFloat(Rs("DIF"))>0) {Response.Write("textoformularioFalta");} else {Response.Write("textoformulario");} %>" id="FILA_<%=posicion%>">
          <td><div align="center"><%=Rs("ID_PROD")%></div></td>
          <td><div align="center" ><%=Rs("DESC_PROD")%> </div></td>
          <td><div align="center" id="CANT_ORD_<%=posicion%>" name="CANT_ORD_<%=posicion%>" ><%=Rs("CANT_ORD")%></div></td>
          <td><div align="center" id="CANT_EMB_<%=posicion%>" name="CANT_EMB_<%=posicion%>"><%=Rs("EMB")%></div></td>
          <td><div align="center" id="CANT_DIF_<%=posicion%>" name="CANT_DIF_<%=posicion%>"><%=Rs("DIF")%></div>
          <% Response.Write("<script>IniciarProducto('"+Rs("ID_PROD")+"');</script>"); %>
          </td>
        </tr>
       <%
       cantSol+=parseFloat(Rs("CANT_ORD"));
       cantEmb+=parseFloat(Rs("EMB"));
       cantDif+=parseFloat(Rs("DIF"));
       posicion++;
       Rs.MoveNext();
       }
       Rs.Close();
       %>
        <tr class="titulo_subgrupo">
          <td class="">&nbsp;</td>
          <td><div align="right">Total </div></td>
          <td><div align="center" id="CANT_SOL" name="CANT_SOL"><%=cantSol%></div></td>
          <td><div align="center"  id="CANT_EMB" name="CANT_EMB"><%=cantEmb%></div></td>
          <td><div align="center"  id="CANT_DIF" name="CANT_DIF"><%=cantDif%></div></td>
        </tr>
      </table>
      <p>&nbsp;</p>
      <table width="100%"  border="0" cellpadding="2" cellspacing="1" class="tablamenu">
        <tr class="tituloblanco">
          <td colspan="3" class="tituloblanco">Datos de la gesti&oacute;n </td>
        </tr>
        <tr class="titulo_subgrupo">
          <td width="23%" class="titulo_subgrupo"><div align="center">Usuario</div></td>
          <td width="20%"><div align="center">Fecha de gesti&oacute;n </div></td>
          <td width="57%"><div align="center">Observaciones</div></td>
        </tr>
        <%
        strSQL="SELECT CONVERT(VARCHAR(10), A.FE_GEST, 103) FE_GEST, A.OBS_GEST, B.NB_USR FROM T_SANT_USR B, T_SANT_GEST_ORD A WHERE A.ID_USR_ATC=B.ID_USR AND A.ID_ORD="+idOrd+" ORDER BY FE_GEST";
        Rs=Ejecutar(strSQL,2);
        while(!Rs.EOF)
        {
        %>
        <tr class="textoformulario">
          <td><%=Rs("NB_USR")%></td>
          <td><%=Rs("FE_GEST")%></td>
          <td><%=Rs("OBS_GEST")%></td>
        </tr>
        <%
			Rs.MoveNext();
        }
        Rs.Close();
        %>

      </table>      <p>&nbsp;</p>
      <div align="right">
        <table width="758" border="0" align="left" cellpadding="0" cellspacing="0" class="tabla_borde_negro">
          <tr class="">
            <td height="21" colspan="7" class="tituloblanco">Observaciones</td>
          </tr>
          <tr class="titulo_subgrupo">
            <td colspan="7" class=""><div align="center"></div>              </td>
          </tr>
          <tr class="textoformulario">
            <td colspan="7" class="textoformulario"><div align="center">
             
              <textarea  cols="90" rows="6" id='obs' name="obs"></textarea>
              
                          </div>              </td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <p align="justify">&nbsp;</p>
      </div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <table width="324" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="149" height="41">
            <div align="center">
              <input type="button" name="Submit" value="Guardar" onClick="JavaScript:Guardar(0);">
            </div>

          <span class="textoformulario">        </span></td>
        <td width="167">
          <div align="center">
            <input type="button" name="Submit2" id="Submit2" value="Cerrar" onClick="JavaScript:Guardar(1);">
          </div>
         </td>
      </tr>
    </table>    
    <span class="textoformulario">    </span>    <p align="center">&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>

<%

Desconectar();
%>