 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var tipo=Request.QueryString("tipo")+"";
var texto=(parseFloat(tipo)==0?" orden ":" devoluci�n ");
var idg=Request.QueryString("idg")+"";
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script>
window.resizeTo(800,500);

function Buscar(tipo)
{

	if(document.getElementById("txCliente").value=="" && document.getElementById("nroItem").value=="" )
	{
		alert("Debe indicar el nombre del cliente o el n�mero para buscar la "+tipo+" a agregar");
	}
	else
	{
		document.getElementById("buscaritem").submit();	
	}

}

function Asignar(tipo)
{
 var solic=document.getElementsByName("solic") ;
 var hay=false;
 
 for (var i=0;i<solic.length && !hay;i++)
 {
	if(solic[i].checked)
		hay=true;
 }

if(!hay)
{
	alert("Indique la(s) "+tipo+"(es) a asignar");
}
else
{
	document.getElementById("solicitudes").submit();
}
}
</script>

<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<div align="center">
	<form id='buscaritem' name='buscaritem' target="_top" action='AgregarItemGuia.asp?idg=<%=Request.QueryString("idg")%>&tipo=<%=Request.QueryString("tipo")%>' method='post'>
  <table width="90%"  border="0" cellpadding="2" cellspacing="1" class="tablamenu">
    <tr class="tituloblanco">
      <td colspan="5">Buscar <%=texto%></td>
    </tr>
    <tr class="textoformulario">
      <td width="8%">Cliente</td>
      <td width="19%"><input name="txCliente" id="txCliente" type="text" class="textonegrita"></td>
      <td width="12%">N&uacute;mero de <%=texto%></td>
      <td width="43%"><input name="nroItem" id="nroItem" type="text" class="textonegrita"></td>
      <td width="18%"><input name="Submit" type="button" class="textonegrita" value="Buscar" onClick="JavaScript:Buscar('<%=texto%>');"></td>
    </tr>
  </table>
  </form>
</div>
<p align="center">
<%
if(Request.QueryString("msg")=="1")
{
	Response.Write(" <span class='texto_verde'>Las "+texto+"(es) han sido asociadas a la guia de carga</span>");
	Response.Write("<script>window.opener.location.reload();</script>");
}
%>
</p>
<% if(Request.Form.Count>0)
{
tiempo = new Date();
Response.Write(tiempo);
%>
<div align="center">
  <table width="90%"  border="0" cellpadding="2" cellspacing="1" class="tablamenu">
  <form id='solicitudes' name='solicitudes' action='AgregarItemGuiaF.asp?idg=<%=Request.QueryString("idg")%>&tipo=<%=Request.QueryString("tipo")%>' method='POST'>
    <tr class="tituloblanco">
      <td colspan="4">Resultado</td>
    </tr>
    <tr class="titulo_subgrupo">
      <td width="10%">&nbsp;</td>
      <td width="18%">Cliente</td>
      <td width="29%">N&uacute;mero de <%=texto%></td>
      <td width="43%">Direcci&oacute;n</td>
    </tr>
    <%
    if(parseFloat(tipo)==0)//Orden
    {
		strSQL="SELECT ID_ORD, CASE A.ID_ST_ORD WHEN 0 THEN 'Nueva' WHEN 2 THEN 'Embalada' WHEN 3 THEN 'Despachada' WHEN 1 THEN 'En proceso' END STATUS, a.id_ord, A.NB_CLTE, A.ID_ORD_GP,A.CIU_ENT,A.DIR_ENT, CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD,  CONVERT(VARCHAR(12),A.FE_DESP_ORD,103) FE_DESP_ORD ";
		strSQL+=" FROM T_SANT_ORD A	WITH (NOLOCK)	WHERE ID_ORD=ID_ORD and ID_ST_ORD in ('7')  ";
		
		if(Request.Form("txCliente")+""!="" && Request.Form("txCliente")+""!="undefined")
			strSQL+=" AND A.NB_CLTE LIKE '%"+Request.Form("txCliente")+"%' ";	

		if(Request.Form("nroItem")+""!="" && Request.Form("nroItem")+""!="undefined")
			strSQL+=" AND A.ID_ORD_GP = '"+Request.Form("nroItem")+"' ";	
		
		strSQL+=" AND ID_ORD NOT IN (SELECT ID_ORD FROM T_SANT_ORD WITH (NOLOCK) WHERE ID_GUIA="+idg+") AND ID_GUIA IS  NULL";
		strSQL+=" ORDER BY ID_ORD "	
			
    } 
    else //Devolucion
    {
		strSQL="SELECT ID_DEV ID_ORD, COD_DEV ID_ORD_GP, NB_CLTE_DEV NB_CLTE, DIR_DEV DIR_ENT FROM T_SANT_DEV WITH (NOLOCK) WHERE IN_ATC=1 AND ID_ST=1 "
		if(Request.Form("txCliente")+""!="" && Request.Form("txCliente")+""!="undefined")
			strSQL+=" AND NB_CLTE_DEV LIKE '%"+Request.Form("txCliente")+"%' ";	

		if(Request.Form("nroItem")+""!="" && Request.Form("nroItem")+""!="undefined")
			strSQL+=" AND COD_DEV = '"+Request.Form("nroItem")+"' ";

		strSQL+=" AND ID_DEV NOT IN (SELECT ID_DEV FROM T_SANT_DEV WITH (NOLOCK) WHERE ID_GUIA="+idg+")  AND ID_GUIA IS  NULL";
				
		strSQL+=" ORDER BY ID_DEV "	
    }
//Response.Write(strSQL);
    Rs=Ejecutar(strSQL,2);
    while(!Rs.EOF)
    {
		%>
		<tr class="textoformulario">
		  <td><input type="checkbox" name="solic" value="<%=Rs("ID_ORD")%>"></td>
		  <td><%=Rs("NB_CLTE")%></td>
		  <td><%=Rs("ID_ORD_GP")%></td>
		  <td><%=Rs("DIR_ENT")%></td>
	    </tr>
		<%
		Rs.MoveNext();
    }
    Rs.Close();
    %>
    <tr class="textoformulario">
      <td colspan="4" class="titulo_subgrupo">&nbsp;</td>
    </tr>
    </form>
  </table>
  <%
tiempo = new Date();
Response.Write(tiempo);
%>
</div>
<p align="center">
  <input name="Submit2" type="button" class="textonegrita" value="Agregar" onClick="JavaScript:Asignar('<%=texto%>');">
</p>
<%
}
%>
</body>
</html>

<%
Desconectar();
%>