 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var cantSol=0;
var cantEmb=0;
var cantDif=0;
var txOrd="";
var nbCliente="";
var dirClte="";
var tpoOrd="";
var telOrd="";
var ciuOrd="";

        strSQL="SELECT CIU_ENT, DIR_ENT,TEL_CLTE1, TEL_CLTE2, ID_ST_ORD, ID_ORD, ID_ORD_GP, CASE ID_TPO_ORD WHEN 0 THEN 'Venta' ELSE 'Consignación' END ID_TPO_ORD, ";
		strSQL+=" NB_CLTE, CONVERT(VARCHAR(12),FE_ING_ORD,103) FE_ING_ORD, 'Por embalar' ST";
		strSQL+=" FROM T_SANT_ORD WHERE ID_ORD="+idOrd;

Rs=Ejecutar(strSQL,2);

txOrd=Rs("ID_ORD_GP").Value;
nbCliente=Rs("NB_CLTE").Value;
dirClte=Rs("DIR_ENT").Value;
tpoOrd=Rs("ID_TPO_ORD").Value;
telOrd=Rs("TEL_CLTE1").Value+" "+Rs("TEL_CLTE2").Value;
ciuOrd=Rs("CIU_ENT").Value
Rs.Close();

var idCaja=0;
strSQL="SELECT ID_CAJA FROM T_SANT_CAJA_ORD WHERE ID_ORD="+idOrd+" AND ID_ST_CAJA=0";
Rs=Ejecutar(strSQL,2);
if(!Rs.EOF)
{
idCaja=Rs(0).Value;
Rs.Close();


	  var mes = new Array("01","02","03","04","05","06","07","08","09","10","11","12");
				a=new Date();
				m=a.getMonth();
				e=a.getYear();
				h=a.getHours();
				mi=a.getMinutes();
				s=a.getSeconds() ;
				var fecha=a.getDate()+"-"+mes[m]+"-"+e;
	
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">

</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
</script>

<table width="303"  border="0" align="center" cellpadding="1" cellspacing="0">
  <tr>
    <td width="282">
		<table width="93%" border="0" cellpadding="0" cellspacing="0" >
      		<tr>
       
       		  <td width="259" class="textonegrita" ><div align="center"><font size=6> Caja <%=idCaja%> </font></div></td>
			 
      		</tr>
			
   	  </table>
		
	</td>
  </tr>
  
  <tr>
    <td class="textonegrita"><div align="center">Chequeado por:<%=Request.Cookies("usr")("nombre")%> </div></td>
  </tr>
  <tr>
    <td height="98" valign="top" nowrap>
      <table width="106%" border="0" cellpadding="0" cellspacing="0" >
	  	
        <tr>
          <td colspan="2"><div align="center" class="textoformulariocentrado">Datos del pedido </div></td>
        </tr>
		
        <tr class="textonegrita">
          <td colspan="2"><div align="center"><%=fecha%></div></td>
        </tr>
        <tr class="textonegrita">
          <td width="30%">Nro pedido : </td>
          <td width="70%"><font size=3><b><%=txOrd%></b></font>          </td>
        </tr>
        <tr class="textonegrita">
          <td height="26" class="textonegrita">Nombre del cliente: </td>
          <td><b><%=nbCliente%></td>
        </tr>
        <tr class="textonegrita">
          <td height="20" class="textonegrita">Direcci&oacute;n:</td>
          <td><%=ciuOrd%></td>
        </tr>
        <tr class="textonegrita">
          <td height="20" class="textonegrita">Ciudad:</td>
          <td><%=dirClte%></td>
        </tr>
	</table>
	
	    
    </td>
  </tr>
</table>
<input type='hidden' name='ID_ORD' id='ID_ORD' value='<%=idOrd%>'>
</body>
</html>
<script>window.print();
window.opener.CerrarCaja();
</script>
<%
}
else
{
Rs.Close();
%>
<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<div class='texto'> No hay cajas en proceso si desea re-imprimir una etiqueta dirijase al menu Despacho / Etiquetas</div>
<%
}
strSQL="EXEC SP_SANT_CERRAR_CAJA "+idOrd+","+idCaja;
Ejecutar(strSQL,1);
Desconectar();
%>