if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_actualiza_asignado]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SNT_actualiza_asignado]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_AsignarUnidadesConsig]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_AsignarUnidadesConsig]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_BorrarConsignacion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_BorrarConsignacion]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_Consig_Act_AsigXorden]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_Consig_Act_AsigXorden]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_Consignar]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_Consignar]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_DevolverConsignacion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_DevolverConsignacion]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_GenerarProximoNumero]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_GenerarProximoNumero]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_LiquidarConsignacion]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_LiquidarConsignacion]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_ModifConsigeventual]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_ModifConsigeventual]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_SalvarLineaConsig]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_SalvarLineaConsig]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_concilia_Cnsg]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_concilia_Cnsg]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Snt_devoluciones_batch]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Snt_devoluciones_batch]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_CONSIG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_CONSIG]
GO

/****** Object:  View dbo.snt_devolver_vw    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_devolver_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[snt_devolver_vw]
GO

/****** Object:  View dbo.snt_liquidar_0_vw    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_liquidar_0_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[snt_liquidar_0_vw]
GO

/****** Object:  View dbo.snt_liquidar_vw    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_liquidar_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[snt_liquidar_vw]
GO

/****** Object:  View dbo.snt_saldoHkits_0_vw    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_saldoHkits_0_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[snt_saldoHkits_0_vw]
GO

/****** Object:  View dbo.snt_saldoHkits_vw    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_saldoHkits_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[snt_saldoHkits_vw]
GO

/****** Object:  View dbo.SNTCONSIGHDRHST_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNTCONSIGHDRHST_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNTCONSIGHDRHST_VW]
GO

/****** Object:  View dbo.SNT_IV00102_Cnsl_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_IV00102_Cnsl_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNT_IV00102_Cnsl_VW]
GO

/****** Object:  View dbo.SNT_STOCKOSXLS_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_STOCKOSXLS_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNT_STOCKOSXLS_VW]
GO

/****** Object:  View dbo.SNTfacturado_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNTfacturado_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNTfacturado_VW]
GO

/****** Object:  View dbo.SNTultimoregistro_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNTultimoregistro_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNTultimoregistro_VW]
GO

/****** Object:  View dbo.V_SANT_REP_CONSIGNAHST    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[V_SANT_REP_CONSIGNAHST]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[V_SANT_REP_CONSIGNAHST]
GO

/****** Object:  View dbo.V_SANT_REP_DEV    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[V_SANT_REP_DEV]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[V_SANT_REP_DEV]
GO

/****** Object:  View dbo.snt_cngsaldoini2006_vw    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_cngsaldoini2006_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[snt_cngsaldoini2006_vw]
GO

/****** Object:  View dbo.snt_consigdethst_vw    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_consigdethst_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[snt_consigdethst_vw]
GO

/****** Object:  View dbo.SNTCONSIGNADO_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNTCONSIGNADO_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNTCONSIGNADO_VW]
GO

/****** Object:  View dbo.SNTVW_CALCTOTCNSG    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNTVW_CALCTOTCNSG]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNTVW_CALCTOTCNSG]
GO

/****** Object:  View dbo.SNTVW_CALCTOTCNSGHST    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNTVW_CALCTOTCNSGHST]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNTVW_CALCTOTCNSGHST]
GO

/****** Object:  View dbo.SNT_IV00102_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_IV00102_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNT_IV00102_VW]
GO

/****** Object:  View dbo.SNT_STOCKOSIV_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_STOCKOSIV_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNT_STOCKOSIV_VW]
GO

/****** Object:  View dbo.SNT_STOCKOS_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_STOCKOS_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNT_STOCKOS_VW]
GO

/****** Object:  View dbo.sntconsigitems_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sntconsigitems_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[sntconsigitems_VW]
GO

/****** Object:  View dbo.ZZ_sop30300_undZero_cnsg_vw    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZZ_sop30300_undZero_cnsg_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[ZZ_sop30300_undZero_cnsg_vw]
GO

/****** Object:  Trigger dbo.TR_SANT_CONSIG_H    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TR_SANT_CONSIG_H]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[TR_SANT_CONSIG_H]
GO

/****** Object:  Table [dbo].[SNT_KITS_HIJOS]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_KITS_HIJOS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SNT_KITS_HIJOS]
GO

/****** Object:  Table [dbo].[SNT_KITS_HIJOS_F]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_KITS_HIJOS_F]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SNT_KITS_HIJOS_F]
GO

/****** Object:  Table [dbo].[SNT_STOCKOSIV]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_STOCKOSIV]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SNT_STOCKOSIV]
GO

/****** Object:  Table [dbo].[SNT_STOCKOSIV_F2006]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNT_STOCKOSIV_F2006]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SNT_STOCKOSIV_F2006]
GO

/****** Object:  Table [dbo].[SntCnsgDetH_RgInsDev20061217]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntCnsgDetH_RgInsDev20061217]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntCnsgDetH_RgInsDev20061217]
GO

/****** Object:  Table [dbo].[SntConsigCfg]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigCfg]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigCfg]
GO

/****** Object:  Table [dbo].[SntConsigDet]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigDet]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigDet]
GO

/****** Object:  Table [dbo].[SntConsigDetHst]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigDetHst]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigDetHst]
GO

/****** Object:  Table [dbo].[SntConsigDetHst_Devol20061217]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigDetHst_Devol20061217]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigDetHst_Devol20061217]
GO

/****** Object:  Table [dbo].[SntConsigHdr]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigHdr]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigHdr]
GO

/****** Object:  Table [dbo].[SntConsigHdrHst]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigHdrHst]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigHdrHst]
GO

/****** Object:  Table [dbo].[SntConsigLoc]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigLoc]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigLoc]
GO

/****** Object:  Table [dbo].[SntConsig_only_n_SOP]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsig_only_n_SOP]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsig_only_n_SOP]
GO

/****** Object:  Table [dbo].[SntDescCategoria]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntDescCategoria]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntDescCategoria]
GO

/****** Object:  Table [dbo].[SntVentas2005]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntVentas2005]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntVentas2005]
GO

/****** Object:  Table [dbo].[SntpptoSello]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntpptoSello]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntpptoSello]
GO

/****** Object:  Table [dbo].[SntpptoSello07]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntpptoSello07]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntpptoSello07]
GO

/****** Object:  Table [dbo].[SntpptoSellos]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntpptoSellos]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntpptoSellos]
GO

/****** Object:  Table [dbo].[snt_TodosCustItem]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_TodosCustItem]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[snt_TodosCustItem]
GO

/****** Object:  Table [dbo].[snt_statusDev]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_statusDev]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[snt_statusDev]
GO

/****** Object:  Table [dbo].[snt_statusliq]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_statusliq]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[snt_statusliq]
GO

/****** Object:  Table [dbo].[snt_stockos_iniciales]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_stockos_iniciales]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[snt_stockos_iniciales]
GO

/****** Object:  Table [dbo].[snt_tmp_liq]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_tmp_liq]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[snt_tmp_liq]
GO

/****** Object:  Table [dbo].[snt_tmp_liq01]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_tmp_liq01]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[snt_tmp_liq01]
GO

/****** Object:  Table [dbo].[sntppto2006]    Script Date: 12/27/2007 6:44:02 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[sntppto2006]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[sntppto2006]
GO

--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[RM_NationalAccounts_MSTR_FKC]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
--ALTER TABLE [dbo].[RM00105] DROP CONSTRAINT RM_NationalAccounts_MSTR_FKC
--GO

--/****** Object:  Trigger dbo.tr_SVC_Item_Ext_U    Script Date: 12/27/2007 6:54:27 PM ******/
--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tr_SVC_Item_Ext_U]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
--drop trigger [dbo].[tr_SVC_Item_Ext_U]
--GO

--/****** Object:  Trigger dbo.tr_SVC_IV00101_D    Script Date: 12/27/2007 6:54:27 PM ******/
--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tr_SVC_IV00101_D]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
--drop trigger [dbo].[tr_SVC_IV00101_D]
--GO

--/****** Object:  Trigger dbo.tr_SVC_IV00101_U    Script Date: 12/27/2007 6:54:27 PM ******/
--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tr_SVC_IV00101_U]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
--drop trigger [dbo].[tr_SVC_IV00101_U]
--GO

--/****** Object:  Trigger dbo.tr_SVC_Item_Site_Ext_U    Script Date: 12/27/2007 6:54:27 PM ******/
--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tr_SVC_Item_Site_Ext_U]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
--drop trigger [dbo].[tr_SVC_Item_Site_Ext_U]
--GO

--/****** Object:  Trigger dbo.tr_SVC_IV00102_D    Script Date: 12/27/2007 6:54:27 PM ******/
--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tr_SVC_IV00102_D]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
--drop trigger [dbo].[tr_SVC_IV00102_D]
--GO

--/****** Object:  Trigger dbo.tr_SVC_IV00102_U    Script Date: 12/27/2007 6:54:27 PM ******/
--if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[tr_SVC_IV00102_U]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
--drop trigger [dbo].[tr_SVC_IV00102_U]
--GO

/****** Object:  View dbo.SNTCONSIGHDRHST_ADR_VW    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SNTCONSIGHDRHST_ADR_VW]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[SNTCONSIGHDRHST_ADR_VW]
GO

/****** Object:  Table [dbo].[snt_statusDev]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[snt_statusDev]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[snt_statusDev]
GO

/****** Object:  Table [dbo].[SntConsigDetHst]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigDetHst]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigDetHst]
GO

/****** Object:  Table [dbo].[SntConsigHdrHst]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SntConsigHdrHst]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[SntConsigHdrHst]
GO

/****** Object:  Table [dbo].[T_SANT_DET_CAJA_DEV]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_DET_CAJA_DEV]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_DET_CAJA_DEV]
GO

/****** Object:  Table [dbo].[T_SANT_DET_DEV_PRE]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_DET_DEV_PRE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_DET_DEV_PRE]
GO

/****** Object:  Table [dbo].[T_SANT_DEV]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_DEV]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_DEV]
GO

/****** Object:  Table [dbo].[T_SANT_ST_DEV]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_ST_DEV]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_ST_DEV]
GO

/****** Object:  Table [dbo].[ZZ_sop30300_undZero]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ZZ_sop30300_undZero]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ZZ_sop30300_undZero]
GO

/****** Object:  Table [dbo].[IV30300_COSTO]    Script Date: 12/27/2007 6:54:27 PM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[IV30300_COSTO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[IV30300_COSTO]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_SANT_DET_CAJA_ORD]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[T_SANT_DET_CAJA_ORD] DROP CONSTRAINT FK_SANT_DET_CAJA_ORD
GO

/****** Object:  Trigger dbo.TR_SANT_ORD    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TR_SANT_ORD]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[TR_SANT_ORD]
GO

/****** Object:  Trigger dbo.TR_SANT_CONSIG_H    Script Date: 12/27/2007 11:14:27 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TR_SANT_CONSIG_H]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[TR_SANT_CONSIG_H]
go

/****** Object:  Trigger dbo.TR_SANT_10104_LOG    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TR_SANT_10104_LOG]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[TR_SANT_10104_LOG]
GO

/****** Object:  Trigger dbo.TR_SANT_ORD_LOG    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[TR_SANT_ORD_LOG]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[TR_SANT_ORD_LOG]
GO

/****** Object:  Trigger dbo.T_SANT_ORD_1    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_ORD_1]') and OBJECTPROPERTY(id, N'IsTrigger') = 1)
drop trigger [dbo].[T_SANT_ORD_1]
GO

/****** Object:  View dbo.Transportista    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Transportista]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[Transportista]
GO

/****** Object:  View dbo.v_sant_direcciones_vw    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[v_sant_direcciones_vw]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[v_sant_direcciones_vw]
GO

/****** Object:  View dbo.v_sant_direcciones_vw2    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[v_sant_direcciones_vw2]') and OBJECTPROPERTY(id, N'IsView') = 1)
drop view [dbo].[v_sant_direcciones_vw2]
GO

/****** Object:  Table [dbo].[T_SANT_DET_CAJA_ORD]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_DET_CAJA_ORD]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_DET_CAJA_ORD]
GO

/****** Object:  Table [dbo].[T_SANT_CAJA_DEV]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_CAJA_DEV]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_CAJA_DEV]
GO

/****** Object:  Table [dbo].[T_SANT_CAJA_ORD]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_CAJA_ORD]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_CAJA_ORD]
GO

/****** Object:  Table [dbo].[T_SANT_COD_PROD]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_COD_PROD]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_COD_PROD]
GO

/****** Object:  Table [dbo].[T_SANT_DET_CAJA_DEV]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_DET_CAJA_DEV]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_DET_CAJA_DEV]
GO

/****** Object:  Table [dbo].[T_SANT_DET_DEV_PRE]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_DET_DEV_PRE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_DET_DEV_PRE]
GO

/****** Object:  Table [dbo].[T_SANT_DET_ORD]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_DET_ORD]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_DET_ORD]
GO

/****** Object:  Table [dbo].[T_SANT_DEV]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_DEV]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_DEV]
GO

/****** Object:  Table [dbo].[T_SANT_GEST_ORD]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_GEST_ORD]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_GEST_ORD]
GO

/****** Object:  Table [dbo].[T_SANT_GUIA_CARGA]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_GUIA_CARGA]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_GUIA_CARGA]
GO

/****** Object:  Table [dbo].[T_SANT_MEMO_GUIA]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_MEMO_GUIA]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_MEMO_GUIA]
GO

/****** Object:  Table [dbo].[T_SANT_ORD]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_ORD]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_ORD]
GO

/****** Object:  Table [dbo].[T_SANT_ORD_TRANSPORTE]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_ORD_TRANSPORTE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_ORD_TRANSPORTE]
GO

/****** Object:  Table [dbo].[T_SANT_ST_DEV]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_ST_DEV]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_ST_DEV]
GO

/****** Object:  Table [dbo].[T_SANT_ST_ENT]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_ST_ENT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_ST_ENT]
GO

/****** Object:  Table [dbo].[T_SANT_ST_ORD]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_ST_ORD]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_ST_ORD]
GO

/****** Object:  Table [dbo].[T_SANT_USR]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_SANT_USR]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_SANT_USR]
GO

/****** Object:  Table [dbo].[T_Sant_Transp]    Script Date: 12/27/2007 11:12:21 AM ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[T_Sant_Transp]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[T_Sant_Transp]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_ACT_10104]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_ACT_10104]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_ACT_ORD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_ACT_ORD]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_AGREGAR_ITEM_GUIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_AGREGAR_ITEM_GUIA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_APROB_LOTE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_APROB_LOTE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_CERRAR_CAJA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_CERRAR_CAJA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_CONSIG]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_CONSIG]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_DAT_GUIA_CARGA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_DAT_GUIA_CARGA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_ELIMINAR_ITEM_GUIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_ELIMINAR_ITEM_GUIA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_GEST_ORD]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_GEST_ORD]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_MEMO_GUIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_MEMO_GUIA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_OBT_DAT_USR]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_OBT_DAT_USR]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REG_DEV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REG_DEV]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REG_GUIA_CARGA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REG_GUIA_CARGA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REG_SERIAL]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REG_SERIAL]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REG_SERIAL_DEV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REG_SERIAL_DEV]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REG_SERIAL_DEV_PRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REG_SERIAL_DEV_PRE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REP20MAS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REP20MAS]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE NAME = 'SP_SANT_QUITAR_SUSPENSION' AND TYPE = 'P')
	DROP PROCEDURE SP_SANT_QUITAR_SUSPENSION
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE NAME = 'FN_SANT_SUSPENDIDO_EN_GP' AND TYPE = 'FN')
	DROP FUNCTION FN_SANT_SUSPENDIDO_EN_GP
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REPCONSIGNAHST]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REPCONSIGNAHST]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REPFACDEV]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REPFACDEV]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REPFACDEV2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REPFACDEV2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REPFACDEV3]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REPFACDEV3]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_REPMASVENDIDOS2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_REPMASVENDIDOS2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_ST_ITEM_GUIA]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_ST_ITEM_GUIA]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_UPDATE_ADDRESS]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_UPDATE_ADDRESS]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_UPDATE_ADDRESS2]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_UPDATE_ADDRESS2]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_UPD_SERIAL_DEV_PRE]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_UPD_SERIAL_DEV_PRE]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[SP_SANT_VALIDAUSUARIO]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[SP_SANT_VALIDAUSUARIO]
GO
