
-- sntconsighdr: encabezado de consignaciones
update sntconsighdr set
SUBTOTAL=round((SUBTOTAL/1000),2),
TRDISAMT=round((TRDISAMT/1000),2),
FRTAMNT=round((FRTAMNT/1000),2),
MISCAMNT=round((MISCAMNT/1000),2),
TAXAMNT=round((TAXAMNT/1000),2),
DOCAMNT=round((DOCAMNT/1000),2)

-- sntconsigdet: detalle de consignaciones
update sntconsigdet set
UNITPRCE=round((UNITPRCE/1000),2),
UNITCOST=round((UNITCOST/1000),2),
XTNDPRCE = QUANTITY_SHP*round((UNITPRCE/1000),2)

-- sntconsighdrhst: encabezado de consignaciones historicas
update sntconsighdrhst set
SUBTOTAL=round((SUBTOTAL/1000),2),
TRDISAMT=round((TRDISAMT/1000),2),
FRTAMNT=round((FRTAMNT/1000),2),
MISCAMNT=round((MISCAMNT/1000),2),
TAXAMNT=round((TAXAMNT/1000),2),
DOCAMNT=round((DOCAMNT/1000),2)

-- sntconsigdethst: detalle de consignaciones historicas
update sntconsigdethst set
UNITPRCE=round((UNITPRCE/1000),2),
UNITCOST=round((UNITCOST/1000),2),
XTNDPRCE = QUANTITY_SHP * round((UNITPRCE/1000),2)
