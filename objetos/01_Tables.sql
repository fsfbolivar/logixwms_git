/****** Object:  Table [dbo].[SNT_KITS_HIJOS]    Script Date: 12/27/2007 6:44:59 PM ******/
CREATE TABLE [dbo].[SNT_KITS_HIJOS] (
	[NoAplica_P] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR_P] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTNMBR_P] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Saldo_P] [numeric](19, 5) NULL ,
	[DESCUENTO_P] [numeric](5, 2) NULL ,
	[NoAplica_H] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR_H] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTNMBR_H] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Saldo_H] [numeric](19, 5) NULL ,
	[TIPO] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SEQUENCE] [int] NULL ,
	[DIF_SALDO] [numeric](19, 5) NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SNT_KITS_HIJOS_F]    Script Date: 12/27/2007 6:45:01 PM ******/
CREATE TABLE [dbo].[SNT_KITS_HIJOS_F] (
	[NOCONSIG_P] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NoAplica_P] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR_P] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTNMBR_P] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Saldo_P] [numeric](19, 5) NULL ,
	[DESCUENTO_P] [numeric](5, 2) NULL ,
	[NOCONSIG_H] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NoAplica_H] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR_H] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTNMBR_H] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Saldo_H] [numeric](19, 5) NULL ,
	[TIPO] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SEQUENCE] [int] NULL ,
	[DIF_SALDO] [numeric](19, 5) NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SNT_STOCKOSIV]    Script Date: 12/27/2007 6:45:01 PM ******/
CREATE TABLE [dbo].[SNT_STOCKOSIV] (
	[DOCTYPE] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TRXQTY] [numeric](19, 5) NOT NULL ,
	[TRFINQTY] [numeric](19, 5) NOT NULL ,
	[TRFOUTQTY] [numeric](19, 5) NOT NULL ,
	[UNITCOST] [numeric](19, 5) NOT NULL ,
	[EXTDCOST] [numeric](19, 5) NOT NULL ,
	[TRXLOCTN] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TRNSTLOC] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SNT_STOCKOSIV_F2006]    Script Date: 12/27/2007 6:45:02 PM ******/
CREATE TABLE [dbo].[SNT_STOCKOSIV_F2006] (
	[DOCTYPE] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TRXQTY] [numeric](19, 5) NOT NULL ,
	[TRFINQTY] [numeric](19, 5) NOT NULL ,
	[TRFOUTQTY] [numeric](19, 5) NOT NULL ,
	[UNITCOST] [numeric](19, 5) NOT NULL ,
	[EXTDCOST] [numeric](19, 5) NOT NULL ,
	[TRXLOCTN] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TRNSTLOC] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntCnsgDetH_RgInsDev20061217]    Script Date: 12/27/2007 6:45:03 PM ******/
CREATE TABLE [dbo].[SntCnsgDetH_RgInsDev20061217] (
	[NOCONSIG] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NOAPLICA] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCTYPE] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SEQUENCE] [int] NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [char] (51) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UOFM] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UNITPRCE] [decimal](19, 5) NULL ,
	[UNITCOST] [decimal](19, 5) NULL ,
	[XTNDPRCE] [decimal](19, 5) NULL ,
	[QUANTITY_ORD] [decimal](19, 5) NULL ,
	[QUANTITY_SHP] [decimal](19, 5) NULL ,
	[REMANENTE] [decimal](19, 5) NULL ,
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PRICELVL] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DESCUENTO] [decimal](5, 2) NULL ,
	[IMPUESTO] [decimal](19, 5) NULL ,
	[DEX_ROW_ID] [int] IDENTITY (1, 1) NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntConsigCfg]    Script Date: 12/27/2007 6:45:04 PM ******/
CREATE TABLE [dbo].[SntConsigCfg] (
	[CONSNUMB] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DEVONUMB] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCID] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MONEDA] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NotaEntrega] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[actindx] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntConsigDet]    Script Date: 12/27/2007 6:45:04 PM ******/
CREATE TABLE [dbo].[SntConsigDet] (
	[NOCONSIG] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SEQUENCE] [int] NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [char] (101) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UOFM] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UNITPRCE] [decimal](19, 5) NULL ,
	[UNITCOST] [decimal](19, 5) NULL ,
	[XTNDPRCE] [decimal](19, 5) NULL ,
	[QUANTITY_ORD] [decimal](19, 5) NULL ,
	[QUANTITY_SHP] [decimal](19, 5) NULL ,
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PRICELVL] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DESCUENTO] [decimal](5, 2) NULL ,
	[IMPUESTO] [decimal](19, 5) NULL ,
	[TIPO] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntConsigDetHst]    Script Date: 12/27/2007 6:45:04 PM ******/
CREATE TABLE [dbo].[SntConsigDetHst] (
	[NOCONSIG] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NOAPLICA] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCTYPE] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SEQUENCE] [int] NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [char] (101) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UOFM] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UNITPRCE] [decimal](19, 5) NULL ,
	[UNITCOST] [decimal](19, 5) NULL ,
	[XTNDPRCE] [decimal](19, 5) NULL ,
	[QUANTITY_ORD] [decimal](19, 5) NULL ,
	[QUANTITY_SHP] [decimal](19, 5) NULL ,
	[REMANENTE] [decimal](19, 5) NULL ,
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PRICELVL] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DESCUENTO] [decimal](5, 2) NULL ,
	[IMPUESTO] [decimal](19, 5) NULL ,
	[TIPO] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntConsigDetHst_Devol20061217]    Script Date: 12/27/2007 6:45:05 PM ******/
CREATE TABLE [dbo].[SntConsigDetHst_Devol20061217] (
	[NOCONSIG] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NOAPLICA] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCTYPE] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SEQUENCE] [int] NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [char] (51) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UOFM] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[UNITPRCE] [decimal](19, 5) NULL ,
	[UNITCOST] [decimal](19, 5) NULL ,
	[XTNDPRCE] [decimal](19, 5) NULL ,
	[QUANTITY_ORD] [decimal](19, 5) NULL ,
	[QUANTITY_SHP] [decimal](19, 5) NULL ,
	[REMANENTE] [decimal](19, 5) NULL ,
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PRICELVL] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DESCUENTO] [decimal](5, 2) NULL ,
	[IMPUESTO] [decimal](19, 5) NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntConsigHdr]    Script Date: 12/27/2007 6:45:05 PM ******/
CREATE TABLE [dbo].[SntConsigHdr] (
	[NOCONSIG] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTNMBR] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CNTCPRSN] [char] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CONTACT] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADRSCODE] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS1] [char] (63) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS2] [char] (63) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CITY] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[STATE] [char] (29) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZIPCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COUNTRY] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PHONE1] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PHONE2] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FAX] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CSTPONBR] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SLPRSNID] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCDATE] [datetime] NULL ,
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PRCLEVEL] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TaxSchId] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SUBTOTAL] [decimal](19, 2) NULL ,
	[TRDISAMT] [decimal](19, 2) NULL ,
	[DESCPRC] [decimal](19, 2) NULL ,
	[FRTAMNT] [decimal](19, 2) NULL ,
	[MISCAMNT] [decimal](19, 2) NULL ,
	[TAXAMNT] [decimal](19, 2) NULL ,
	[DOCAMNT] [decimal](19, 2) NULL ,
	[STATUS] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MONEDA] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COMENT1] [char] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COMENT2] [char] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LOCNConsig] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NoControl] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITMCLSCD] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Retenido] [smallint] NULL ,
	[RetenidoCredito] [smallint] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntConsigHdrHst]    Script Date: 12/27/2007 6:45:06 PM ******/
CREATE TABLE [dbo].[SntConsigHdrHst] (
	[NOCONSIG] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NOAPLICA] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCTYPE] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTNMBR] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CNTCPRSN] [char] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CONTACT] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADRSCODE] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS1] [char] (63) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ADDRESS2] [char] (63) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CITY] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[STATE] [char] (29) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZIPCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COUNTRY] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PHONE1] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PHONE2] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FAX] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CSTPONBR] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SLPRSNID] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCDATE] [datetime] NULL ,
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PRCLEVEL] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TaxSchId] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SUBTOTAL] [decimal](19, 2) NULL ,
	[TRDISAMT] [decimal](19, 2) NULL ,
	[DESCPRC] [decimal](19, 2) NULL ,
	[FRTAMNT] [decimal](19, 2) NULL ,
	[MISCAMNT] [decimal](19, 2) NULL ,
	[TAXAMNT] [decimal](19, 2) NULL ,
	[DOCAMNT] [decimal](19, 2) NULL ,
	[USUERACT] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FECHAACT] [datetime] NULL ,
	[MONEDA] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COMENT1] [char] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COMENT2] [char] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LOCNConsig] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NoControl] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITMCLSCD] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntConsigLoc]    Script Date: 12/27/2007 6:45:06 PM ******/
CREATE TABLE [dbo].[SntConsigLoc] (
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LOCNDSCR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntConsig_only_n_SOP]    Script Date: 12/27/2007 6:45:07 PM ******/
CREATE TABLE [dbo].[SntConsig_only_n_SOP] (
	[SOPNUMBE] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR] [varchar] (18) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[QTYTOINV] [int] NULL ,
	[QUANTITY_SHP] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntDescCategoria]    Script Date: 12/27/2007 6:45:08 PM ******/
CREATE TABLE [dbo].[SntDescCategoria] (
	[CUSTNMBR] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITMCLSCD] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DiscountPrc] [decimal](5, 2) NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntVentas2005]    Script Date: 12/27/2007 6:45:08 PM ******/
CREATE TABLE [dbo].[SntVentas2005] (
	[SOPTYPE] [smallint] NULL ,
	[DOCID] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SOPNUMBE] [nvarchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCDATE] [datetime] NULL ,
	[CUSTCLAS] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SHRTNAME] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTNMBR] [nvarchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SLPRSNID] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SALSTERR] [nvarchar] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[STATE] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZONA] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITMCLSCD] [nvarchar] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[QUANTITY] [float] NULL ,
	[UNITPRCE] [float] NULL ,
	[MRKDNAMT] [float] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntpptoSello]    Script Date: 12/27/2007 6:45:09 PM ******/
CREATE TABLE [dbo].[SntpptoSello] (
	[USCATVLS_1] [nvarchar] (155) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ENE] [int] NULL ,
	[FEB] [int] NULL ,
	[MAR] [int] NULL ,
	[ABR] [int] NULL ,
	[MAY] [int] NULL ,
	[JUN] [int] NULL ,
	[JUL] [int] NULL ,
	[AGO] [int] NULL ,
	[SEP] [int] NULL ,
	[OCT] [int] NULL ,
	[NOV] [int] NULL ,
	[DIC] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntpptoSello07]    Script Date: 12/27/2007 6:45:09 PM ******/
CREATE TABLE [dbo].[SntpptoSello07] (
	[USCATVLS_1] [nvarchar] (155) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MESES] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CANT] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[SntpptoSellos]    Script Date: 12/27/2007 6:45:10 PM ******/
CREATE TABLE [dbo].[SntpptoSellos] (
	[USCATVLS_1] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MESES] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MES] [float] NULL ,
	[CANT] [float] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[snt_TodosCustItem]    Script Date: 12/27/2007 6:45:10 PM ******/
CREATE TABLE [dbo].[snt_TodosCustItem] (
	[CUSTITEM] [varchar] (46) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[snt_statusDev]    Script Date: 12/27/2007 6:45:10 PM ******/
CREATE TABLE [dbo].[snt_statusDev] (
	[NoAplica] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COD_DEV] [char] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [char] (51) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Saldo] [numeric](19, 5) NULL ,
	[Devuelto] [numeric](19, 5) NULL ,
	[difer_dev] [numeric](19, 5) NULL ,
	[Devlt_Def] [numeric](19, 5) NULL ,
	[Danado] [numeric](19, 5) NULL ,
	[Consecutivo] [int] NULL ,
	[Tipo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[snt_statusliq]    Script Date: 12/27/2007 6:45:11 PM ******/
CREATE TABLE [dbo].[snt_statusliq] (
	[NoAplica] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COD_LIQ] [char] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [char] (51) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Saldo] [numeric](19, 5) NULL ,
	[liquidado] [numeric](19, 5) NULL ,
	[difer_liq] [numeric](19, 5) NULL ,
	[liqud_Def] [numeric](19, 5) NULL ,
	[Danado] [numeric](19, 5) NULL ,
	[Consecutivo] [int] NULL ,
	[Tipo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[snt_stockos_iniciales]    Script Date: 12/27/2007 6:45:11 PM ******/
CREATE TABLE [dbo].[snt_stockos_iniciales] (
	[YEAR] [int] NULL ,
	[CODIGO] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TITULO] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ALMACEN] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CANTIDAD] [float] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[snt_tmp_liq]    Script Date: 12/27/2007 6:45:12 PM ******/
CREATE TABLE [dbo].[snt_tmp_liq] (
	[custnmbr] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Clase] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NoAplica] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [char] (101) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Saldo] [numeric](19, 5) NULL ,
	[Liquidar] [numeric](19, 5) NULL ,
	[unitprce] [numeric](19, 5) NULL ,
	[Newunitprce] [numeric](19, 5) NULL ,
	[difer_liq] [numeric](19, 5) NULL ,
	[Devlt_liq] [numeric](19, 5) NULL ,
	[Danado] [numeric](19, 5) NULL ,
	[Tipo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Consecutivo] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[snt_tmp_liq01]    Script Date: 12/27/2007 6:45:12 PM ******/
CREATE TABLE [dbo].[snt_tmp_liq01] (
	[custnmbr] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Clase] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NoAplica] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMDESC] [char] (101) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Saldo] [numeric](19, 5) NULL ,
	[Liquidar] [numeric](19, 5) NULL ,
	[unitprce] [numeric](19, 5) NULL ,
	[Newunitprce] [numeric](19, 5) NULL ,
	[difer_liq] [numeric](19, 5) NULL ,
	[Devlt_liq] [numeric](19, 5) NULL ,
	[Danado] [numeric](19, 5) NULL ,
	[Tipo] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Consecutivo] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [IV30300_COSTO] (
	[COSTO_PROM] [numeric](19, 5) NULL ,
	[EXISTENCIA] [numeric](19, 5) NULL ,
	[TRXSORCE] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCTYPE] [smallint] NULL ,
	[DOCNUMBR] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DOCDATE] [datetime] NULL ,
	[HSTMODUL] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CUSTNMBR] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LNSEQNBR] [numeric](19, 5) NULL ,
	[UOFM] [char] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TRXQTY] [numeric](19, 5) NULL ,
	[UNITCOST] [numeric](19, 5) NULL ,
	[EXTDCOST] [numeric](19, 5) NULL ,
	[TRXLOCTN] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TRNSTLOC] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TRFQTYTY] [smallint] NULL ,
	[TRTQTYTY] [smallint] NULL ,
	[IVIVINDX] [int] NULL ,
	[IVIVOFIX] [int] NULL ,
	[DECPLCUR] [smallint] NULL ,
	[DECPLQTY] [smallint] NULL ,
	[QTYBSUOM] [numeric](19, 5) NULL ,
	[DEX_ROW_ID] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [ZZ_sop30300_undZero] (
	[SOPTYPE] [smallint] NOT NULL ,
	[SOPNUMBE] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LNITMSEQ] [int] NOT NULL ,
	[CMPNTSEQ] [int] NOT NULL ,
	[ITEMNMBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ITEMDESC] [char] (101) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NONINVEN] [smallint] NOT NULL ,
	[DROPSHIP] [smallint] NOT NULL ,
	[UOFM] [char] (9) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LOCNCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[UNITCOST] [numeric](19, 5) NOT NULL ,
	[ORUNTCST] [numeric](19, 5) NOT NULL ,
	[UNITPRCE] [numeric](19, 5) NOT NULL ,
	[ORUNTPRC] [numeric](19, 5) NOT NULL ,
	[XTNDPRCE] [numeric](19, 5) NOT NULL ,
	[OXTNDPRC] [numeric](19, 5) NOT NULL ,
	[REMPRICE] [numeric](19, 5) NOT NULL ,
	[OREPRICE] [numeric](19, 5) NOT NULL ,
	[EXTDCOST] [numeric](19, 5) NOT NULL ,
	[OREXTCST] [numeric](19, 5) NOT NULL ,
	[MRKDNAMT] [numeric](19, 5) NOT NULL ,
	[ORMRKDAM] [numeric](19, 5) NOT NULL ,
	[MRKDNPCT] [smallint] NOT NULL ,
	[MRKDNTYP] [smallint] NOT NULL ,
	[INVINDX] [int] NOT NULL ,
	[CSLSINDX] [int] NOT NULL ,
	[SLSINDX] [int] NOT NULL ,
	[MKDNINDX] [int] NOT NULL ,
	[RTNSINDX] [int] NOT NULL ,
	[INUSINDX] [int] NOT NULL ,
	[INSRINDX] [int] NOT NULL ,
	[DMGDINDX] [int] NOT NULL ,
	[ITMTSHID] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[IVITMTXB] [smallint] NOT NULL ,
	[BKTSLSAM] [numeric](19, 5) NOT NULL ,
	[ORBKTSLS] [numeric](19, 5) NOT NULL ,
	[TAXAMNT] [numeric](19, 5) NOT NULL ,
	[ORTAXAMT] [numeric](19, 5) NOT NULL ,
	[TXBTXAMT] [numeric](19, 5) NOT NULL ,
	[OTAXTAMT] [numeric](19, 5) NOT NULL ,
	[BSIVCTTL] [tinyint] NOT NULL ,
	[TRDISAMT] [numeric](19, 5) NOT NULL ,
	[ORTDISAM] [numeric](19, 5) NOT NULL ,
	[DISCSALE] [numeric](19, 5) NOT NULL ,
	[ORDAVSLS] [numeric](19, 5) NOT NULL ,
	[QUANTITY] [numeric](19, 5) NOT NULL ,
	[ATYALLOC] [numeric](19, 5) NOT NULL ,
	[QTYINSVC] [numeric](19, 5) NOT NULL ,
	[QTYINUSE] [numeric](19, 5) NOT NULL ,
	[QTYDMGED] [numeric](19, 5) NOT NULL ,
	[QTYRTRND] [numeric](19, 5) NOT NULL ,
	[QTYONHND] [numeric](19, 5) NOT NULL ,
	[QTYCANCE] [numeric](19, 5) NOT NULL ,
	[QTYCANOT] [numeric](19, 5) NOT NULL ,
	[QTYORDER] [numeric](19, 5) NOT NULL ,
	[QTYPRBAC] [numeric](19, 5) NOT NULL ,
	[QTYPRBOO] [numeric](19, 5) NOT NULL ,
	[QTYPRINV] [numeric](19, 5) NOT NULL ,
	[QTYPRORD] [numeric](19, 5) NOT NULL ,
	[QTYPRVRECVD] [numeric](19, 5) NOT NULL ,
	[QTYRECVD] [numeric](19, 5) NOT NULL ,
	[QTYREMAI] [numeric](19, 5) NOT NULL ,
	[QTYREMBO] [numeric](19, 5) NOT NULL ,
	[QTYTBAOR] [numeric](19, 5) NOT NULL ,
	[QTYTOINV] [numeric](19, 5) NOT NULL ,
	[QTYTORDR] [numeric](19, 5) NOT NULL ,
	[QTYFULFI] [numeric](19, 5) NOT NULL ,
	[QTYSLCTD] [numeric](19, 5) NOT NULL ,
	[QTYBSUOM] [numeric](19, 5) NOT NULL ,
	[EXTQTYAL] [numeric](19, 5) NOT NULL ,
	[EXTQTYSEL] [numeric](19, 5) NOT NULL ,
	[ReqShipDate] [datetime] NOT NULL ,
	[FUFILDAT] [datetime] NOT NULL ,
	[ACTLSHIP] [datetime] NOT NULL ,
	[SHIPMTHD] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SALSTERR] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SLPRSNID] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PRCLEVEL] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[COMMNTID] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[BRKFLD1] [smallint] NOT NULL ,
	[BRKFLD2] [smallint] NOT NULL ,
	[BRKFLD3] [smallint] NOT NULL ,
	[CURRNIDX] [smallint] NOT NULL ,
	[TRXSORCE] [char] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[SOPLNERR] [binary] (4) NOT NULL ,
	[DOCNCORR] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ORGSEQNM] [int] NOT NULL ,
	[ITEMCODE] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PURCHSTAT] [smallint] NOT NULL ,
	[DECPLQTY] [smallint] NOT NULL ,
	[DECPLCUR] [smallint] NOT NULL ,
	[ODECPLCU] [smallint] NOT NULL ,
	[EXCEPTIONALDEMAND] [tinyint] NOT NULL ,
	[TAXSCHID] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TXSCHSRC] [smallint] NOT NULL ,
	[PRSTADCD] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ShipToName] [char] (65) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CNTCPRSN] [char] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ADDRESS1] [char] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ADDRESS2] [char] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ADDRESS3] [char] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CITY] [char] (35) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[STATE] [char] (29) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ZIPCODE] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CCode] [char] (7) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[COUNTRY] [char] (61) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PHONE1] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PHONE2] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PHONE3] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[FAXNUMBR] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Flags] [smallint] NOT NULL ,
	[CONTNBR] [char] (11) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CONTLNSEQNBR] [numeric](19, 5) NOT NULL ,
	[CONTSTARTDTE] [datetime] NOT NULL ,
	[CONTENDDTE] [datetime] NOT NULL ,
	[CONTITEMNBR] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CONTSERIALNBR] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ISLINEINTRA] [tinyint] NOT NULL ,
	[DEX_ROW_ID] [int] NULL ,
	CONSTRAINT [PKZZ_sop30300_undZero] PRIMARY KEY  NONCLUSTERED 
	(
		[SOPNUMBE],
		[SOPTYPE],
		[CMPNTSEQ],
		[LNITMSEQ]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	 CHECK (datepart(hour,[ACTLSHIP]) = 0 and datepart(minute,[ACTLSHIP]) = 0 and datepart(second,[ACTLSHIP]) = 0 and datepart(millisecond,[ACTLSHIP]) = 0),
	 CHECK (datepart(hour,[CONTENDDTE]) = 0 and datepart(minute,[CONTENDDTE]) = 0 and datepart(second,[CONTENDDTE]) = 0 and datepart(millisecond,[CONTENDDTE]) = 0),
	 CHECK (datepart(hour,[CONTSTARTDTE]) = 0 and datepart(minute,[CONTSTARTDTE]) = 0 and datepart(second,[CONTSTARTDTE]) = 0 and datepart(millisecond,[CONTSTARTDTE]) = 0),
	 CHECK (datepart(hour,[FUFILDAT]) = 0 and datepart(minute,[FUFILDAT]) = 0 and datepart(second,[FUFILDAT]) = 0 and datepart(millisecond,[FUFILDAT]) = 0),
	 CHECK (datepart(hour,[ReqShipDate]) = 0 and datepart(minute,[ReqShipDate]) = 0 and datepart(second,[ReqShipDate]) = 0 and datepart(millisecond,[ReqShipDate]) = 0)
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[sntppto2006]    Script Date: 12/27/2007 6:45:13 PM ******/
CREATE TABLE [dbo].[sntppto2006] (
	[CLASE] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CODIGO] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TITULO] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PPTO2006] [float] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SNT_STOCKOSIV] WITH NOCHECK ADD 
	CONSTRAINT [DF__SNT_STOCK__TRXQT__63AC8108] DEFAULT (0) FOR [TRXQTY],
	CONSTRAINT [DF__SNT_STOCK__TRFIN__64A0A541] DEFAULT (0) FOR [TRFINQTY],
	CONSTRAINT [DF__SNT_STOCK__TRFOU__6594C97A] DEFAULT (0) FOR [TRFOUTQTY],
	CONSTRAINT [DF__SNT_STOCK__UNITC__6688EDB3] DEFAULT (0) FOR [UNITCOST],
	CONSTRAINT [DF__SNT_STOCK__EXTDC__677D11EC] DEFAULT (0) FOR [EXTDCOST],
	CONSTRAINT [DF__SNT_STOCK__TRXLO__68713625] DEFAULT ('') FOR [TRXLOCTN],
	CONSTRAINT [DF__SNT_STOCK__TRNST__69655A5E] DEFAULT ('') FOR [TRNSTLOC]
GO

ALTER TABLE [dbo].[SNT_STOCKOSIV_F2006] WITH NOCHECK ADD 
	CONSTRAINT [DF__SNT_STOCK__TRXQT__4527F9E8] DEFAULT (0) FOR [TRXQTY],
	CONSTRAINT [DF__SNT_STOCK__TRFIN__461C1E21] DEFAULT (0) FOR [TRFINQTY],
	CONSTRAINT [DF__SNT_STOCK__TRFOU__4710425A] DEFAULT (0) FOR [TRFOUTQTY],
	CONSTRAINT [DF__SNT_STOCK__UNITC__48046693] DEFAULT (0) FOR [UNITCOST],
	CONSTRAINT [DF__SNT_STOCK__EXTDC__48F88ACC] DEFAULT (0) FOR [EXTDCOST],
	CONSTRAINT [DF__SNT_STOCK__TRXLO__49ECAF05] DEFAULT ('') FOR [TRXLOCTN],
	CONSTRAINT [DF__SNT_STOCK__TRNST__4AE0D33E] DEFAULT ('') FOR [TRNSTLOC]
GO

ALTER TABLE [dbo].[SntConsigDetHst] WITH NOCHECK ADD 
	CONSTRAINT [CK_SntConsigDetHst] CHECK ([REMANENTE] >= 0)
GO

ALTER TABLE [dbo].[SntConsigHdr] WITH NOCHECK ADD 
	CONSTRAINT [DF__SntConsig__CSTPO__2ACFFDAC] DEFAULT ('') FOR [CSTPONBR],
	CONSTRAINT [DF__SntConsig__SLPRS__2BC421E5] DEFAULT ('') FOR [SLPRSNID],
	CONSTRAINT [DF__SntConsig__LOCNC__2CB8461E] DEFAULT ('') FOR [LOCNCODE],
	CONSTRAINT [DF__SntConsig__PRCLE__2DAC6A57] DEFAULT ('') FOR [PRCLEVEL],
	CONSTRAINT [DF__SntConsig__TaxSc__2EA08E90] DEFAULT ('') FOR [TaxSchId],
	CONSTRAINT [DF__SntConsig__SUBTO__2F94B2C9] DEFAULT (0) FOR [SUBTOTAL],
	CONSTRAINT [DF__SntConsig__TRDIS__3088D702] DEFAULT (0) FOR [TRDISAMT],
	CONSTRAINT [DF__SntConsig__DESCP__317CFB3B] DEFAULT (0) FOR [DESCPRC],
	CONSTRAINT [DF__SntConsig__FRTAM__32711F74] DEFAULT (0) FOR [FRTAMNT],
	CONSTRAINT [DF__SntConsig__MISCA__336543AD] DEFAULT (0) FOR [MISCAMNT],
	CONSTRAINT [DF__SntConsig__TAXAM__345967E6] DEFAULT (0) FOR [TAXAMNT],
	CONSTRAINT [DF__SntConsig__DOCAM__354D8C1F] DEFAULT (0) FOR [DOCAMNT],
	CONSTRAINT [DF__SntConsig__STATU__3641B058] DEFAULT ('N') FOR [STATUS],
	CONSTRAINT [DF__SntConsig__COMEN__3735D491] DEFAULT ('') FOR [COMENT1],
	CONSTRAINT [DF__SntConsig__COMEN__3829F8CA] DEFAULT ('') FOR [COMENT2],
	CONSTRAINT [DF__SntConsig__NoCon__391E1D03] DEFAULT ('') FOR [NoControl],
	CONSTRAINT [DF__SntConsig__ITMCL__3A12413C] DEFAULT ('') FOR [ITMCLSCD],
	CONSTRAINT [DF__SntConsig__Reten__3B066575] DEFAULT (0) FOR [Retenido],
	CONSTRAINT [DF__SntConsig__Reten__3BFA89AE] DEFAULT (0) FOR [RetenidoCredito]
GO

ALTER TABLE [dbo].[SntConsigHdrHst] WITH NOCHECK ADD 
	CONSTRAINT [DF__SntConsig__NoCon__3ED6F659] DEFAULT ('') FOR [NoControl],
	CONSTRAINT [DF__SntConsig__ITMCL__3FCB1A92] DEFAULT ('') FOR [ITMCLSCD]
GO

 CREATE  UNIQUE  INDEX [SntConsigDetI0] ON [dbo].[SntConsigDet]([NOCONSIG], [SEQUENCE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  UNIQUE  INDEX [SntConsigDetI1] ON [dbo].[SntConsigDet]([NOCONSIG], [ITEMNMBR]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  UNIQUE  INDEX [SntConsigDetHstI0] ON [dbo].[SntConsigDetHst]([NOCONSIG], [NOAPLICA], [DOCTYPE], [SEQUENCE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [SntConsigDetHstI1] ON [dbo].[SntConsigDetHst]([ITEMNMBR]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  UNIQUE  INDEX [SntConsigDetHstI2] ON [dbo].[SntConsigDetHst]([NOCONSIG], [NOAPLICA], [DOCTYPE], [ITEMNMBR], [SEQUENCE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  UNIQUE  INDEX [SntConsigHdrI0] ON [dbo].[SntConsigHdr]([NOCONSIG]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  UNIQUE  INDEX [SntConsigHdrHstI0] ON [dbo].[SntConsigHdrHst]([NOCONSIG], [NOAPLICA], [DOCTYPE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [SntConsigHdrHstI1] ON [dbo].[SntConsigHdrHst]([NOAPLICA], [DOCDATE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [SntConsigHdrHstI2] ON [dbo].[SntConsigHdrHst]([SLPRSNID], [DOCDATE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [SntConsigHdrHstI3] ON [dbo].[SntConsigHdrHst]([LOCNCODE], [DOCDATE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  INDEX [SntConsigHdrHstI4] ON [dbo].[SntConsigHdrHst]([DOCDATE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  UNIQUE  INDEX [SntConsigLocI0] ON [dbo].[SntConsigLoc]([LOCNCODE]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  UNIQUE  INDEX [SntDescCategoriaI0] ON [dbo].[SntDescCategoria]([CUSTNMBR], [ITMCLSCD]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO


/****** Object:  Table [dbo].[T_SANT_CAJA_DEV]    Script Date: 12/27/2007 11:13:28 AM ******/
CREATE TABLE [dbo].[T_SANT_CAJA_DEV] (
	[ID_DEV] [numeric](18, 0) NOT NULL ,
	[ID_CAJA] [int] NOT NULL ,
	[FE_REG] [datetime] NULL ,
	[ID_ST_CAJA] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_CAJA_ORD]    Script Date: 12/27/2007 11:13:29 AM ******/
CREATE TABLE [dbo].[T_SANT_CAJA_ORD] (
	[ID_ORD] [numeric](18, 0) NOT NULL ,
	[ID_CAJA] [int] NOT NULL ,
	[FE_INI] [datetime] NULL ,
	[FE_FIN] [datetime] NULL ,
	[ID_ST_CAJA] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_COD_PROD]    Script Date: 12/27/2007 11:13:31 AM ******/
CREATE TABLE [dbo].[T_SANT_COD_PROD] (
	[ID_PROD] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[COD_PROD] [char] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_DET_CAJA_DEV]    Script Date: 12/27/2007 11:13:32 AM ******/
CREATE TABLE [dbo].[T_SANT_DET_CAJA_DEV] (
	[ID_DEV] [numeric](18, 0) NULL ,
	[ID_CAJA] [int] NULL ,
	[ID_PROD] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CANT_PROD] [int] NULL ,
	[SER_PROD] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ST_PROD] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_DET_DEV_PRE]    Script Date: 12/27/2007 11:13:33 AM ******/
CREATE TABLE [dbo].[T_SANT_DET_DEV_PRE] (
	[ID_DEV] [numeric](18, 0) NULL ,
	[ID_PROD] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CANT_PROD] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_DET_ORD]    Script Date: 12/27/2007 11:13:33 AM ******/
CREATE TABLE [dbo].[T_SANT_DET_ORD] (
	[ID_ORD] [numeric](18, 0) NULL ,
	[ID_PROD] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DESC_PROD] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CANT_ORD] [int] NULL ,
	[SER_PROD] [varchar] (13) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_DEV]    Script Date: 12/27/2007 11:13:35 AM ******/
CREATE TABLE [dbo].[T_SANT_DEV] (
	[ID_DEV] [numeric](18, 0) IDENTITY (1, 1) NOT NULL ,
	[NB_CLTE_DEV] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FE_REG] [datetime] NULL ,
	[CANT_CAJA] [int] NULL ,
	[ID_CHEQ] [numeric](18, 0) NULL ,
	[COD_DEV] AS ('DEV-' + replicate('0',(7 - len(convert(varchar(7),[ID_DEV])))) + convert(varchar(7),[ID_DEV])) ,
	[ID_ST] [int] NULL ,
	[TEL_CLTE] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OBS] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[IN_ATC] [int] NULL ,
	[DIR_DEV] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ID_GUIA] [numeric](18, 0) NULL ,
	[FE_REC_ALM] [datetime] NULL ,
	[ST_ENT] [int] NULL ,
	[IN_CONSIG] [int] NULL ,
	[RIF_CLTE_DEV] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_GEST_ORD]    Script Date: 12/27/2007 11:13:35 AM ******/
CREATE TABLE [dbo].[T_SANT_GEST_ORD] (
	[ID_ORD] [numeric](18, 0) NOT NULL ,
	[ID_USR_ATC] [numeric](18, 0) NOT NULL ,
	[FE_GEST] [datetime] NOT NULL ,
	[OBS_GEST] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_GUIA_CARGA]    Script Date: 12/27/2007 11:13:36 AM ******/
CREATE TABLE [dbo].[T_SANT_GUIA_CARGA] (
	[ID_GUIA_CARGA] [numeric](18, 0) IDENTITY (1, 1) NOT NULL ,
	[FE_GUIA_CARGA] [datetime] NOT NULL ,
	[ID_USR] [numeric](18, 0) NOT NULL ,
	[ID_TRANSP] [numeric](18, 0) NOT NULL ,
	[COD_GUIA] AS ('GC-' + replicate('0',(7 - len(convert(varchar(7),[ID_GUIA_CARGA])))) + convert(varchar(7),[ID_GUIA_CARGA])) 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_MEMO_GUIA]    Script Date: 12/27/2007 11:13:36 AM ******/
CREATE TABLE [dbo].[T_SANT_MEMO_GUIA] (
	[ID_MEMO] [int] IDENTITY (1, 1) NOT NULL ,
	[ID_GUIA] [numeric](18, 0) NULL ,
	[CLTE_MEMO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CAJAS] [int] NULL ,
	[EJEMP] [numeric](18, 0) NULL ,
	[DEST_MEMO] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DIR_MEMO] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[OBS_MEMO] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COD_MEMO] AS ('MEMO-' + replicate('0',(7 - len(convert(varchar(7),[ID_MEMO])))) + convert(varchar(7),[ID_MEMO])) ,
	[FE_MEMO] [datetime] NULL ,
	[ST_ENT] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_ORD]    Script Date: 12/27/2007 11:13:37 AM ******/
CREATE TABLE [dbo].[T_SANT_ORD] (
	[ID_ORD] [numeric](18, 0) IDENTITY (1, 1) NOT NULL ,
	[ID_CHEQ] [int] NOT NULL ,
	[ID_TPO_ORD] [int] NOT NULL ,
	[ID_ST_ORD] [int] NOT NULL ,
	[ID_ORD_GP] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CIU_ENT] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NB_CLTE] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DIR_ENT] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[FE_ING_ORD] [datetime] NOT NULL ,
	[ID_TRANS] [int] NULL ,
	[TEL_CLTE1] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TEL_CLTE2] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FE_EMB_ORD] [datetime] NULL ,
	[FE_DESP_ORD] [datetime] NULL ,
	[OBS] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[IN_IMP] [int] NULL ,
	[CI_Transp] [numeric](18, 0) NULL ,
	[ID_LOTE] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ID_ST_GEST] [int] NULL ,
	[ID_USR_ATC] [numeric](18, 0) NULL ,
	[FE_USR_ATC] [datetime] NULL ,
	[ID_ST_ENT] [int] NULL ,
	[ID_GUIA] [numeric](18, 0) NULL ,
	[FE_ST_ENT] [datetime] NULL ,
	[OBS_ENT] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FE_ENT_FAC] [datetime] NULL ,
	[UPSZONE] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COMENT1] [char] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[COMENT2] [char] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FE_APR_LOT] [datetime] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_ORD_TRANSPORTE]    Script Date: 12/27/2007 11:13:42 AM ******/
CREATE TABLE [dbo].[T_SANT_ORD_TRANSPORTE] (
	[ID_ORD_GP] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NB_CLTE] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CIU_ENT] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TOT_ID_CAJA] [numeric](19, 5) NOT NULL ,
	[TOT_CANT_PROD] [numeric](19, 5) NOT NULL ,
	[ID_ORD] [numeric](20, 0) NULL ,
	[ID_ST_ORD] [numeric](5, 0) NOT NULL ,
	[TRANSPORTISTA] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CI_TRANSPORTISTA] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PLACA_TRANSPORTISTA] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[DESC_VEHIULO] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LIBRE1] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LIBRE2] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[LIBRE3] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_ST_DEV]    Script Date: 12/27/2007 11:13:43 AM ******/
CREATE TABLE [dbo].[T_SANT_ST_DEV] (
	[ID_ST] [int] NOT NULL ,
	[NB_ST] [varchar] (100) COLLATE Modern_Spanish_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_ST_ENT]    Script Date: 12/27/2007 11:13:43 AM ******/
CREATE TABLE [dbo].[T_SANT_ST_ENT] (
	[ID_ST_ENT] [int] NULL ,
	[NB_ST_ENT] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_ST_ORD]    Script Date: 12/27/2007 11:13:43 AM ******/
CREATE TABLE [dbo].[T_SANT_ST_ORD] (
	[ID_ST] [int] NOT NULL ,
	[NB_ST] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_USR]    Script Date: 12/27/2007 11:13:44 AM ******/
CREATE TABLE [dbo].[T_SANT_USR] (
	[ID_USR] [numeric](18, 0) IDENTITY (1, 1) NOT NULL ,
	[LOG_USR] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PWD_USR] [varchar] (15) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[NB_USR] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ID_PERF] [int] NOT NULL ,
	[FE_HORA_ULT_ING] [datetime] NULL ,
	[IN_ST] [int] NULL ,
	[IN_EG] [int] NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_Sant_Transp]    Script Date: 12/27/2007 11:13:45 AM ******/
CREATE TABLE [dbo].[T_Sant_Transp] (
	[CI_Transp] [numeric](18, 0) NULL ,
	[NB_Transp] [char] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[AP_Transp] [char] (31) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Tel_Transp] [char] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[Cel_Transp] [char] (19) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[TV_Transp] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[PL_Transp] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CC_Transp] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[compania] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Status_Transp] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[IN_ST] [char] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[T_SANT_DET_CAJA_ORD]    Script Date: 12/27/2007 11:13:46 AM ******/
CREATE TABLE [dbo].[T_SANT_DET_CAJA_ORD] (
	[ID_ORD] [numeric](18, 0) NOT NULL ,
	[ID_CAJA] [int] NOT NULL ,
	[ID_PROD] [varchar] (21) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[CANT_PROD] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[T_SANT_CAJA_ORD] WITH NOCHECK ADD 
	CONSTRAINT [PK_SANT_CAJA_ORD] PRIMARY KEY  CLUSTERED 
	(
		[ID_ORD],
		[ID_CAJA]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[T_SANT_COD_PROD] WITH NOCHECK ADD 
	CONSTRAINT [PK_SANT_COD_PROD] PRIMARY KEY  CLUSTERED 
	(
		[ID_PROD],
		[COD_PROD]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[T_SANT_USR] WITH NOCHECK ADD 
	 PRIMARY KEY  CLUSTERED 
	(
		[ID_USR]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[T_SANT_DET_CAJA_ORD] WITH NOCHECK ADD 
	CONSTRAINT [PK_SANT_DET_CAJA_ORD] PRIMARY KEY  CLUSTERED 
	(
		[ID_ORD],
		[ID_CAJA],
		[ID_PROD]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

 CREATE  CLUSTERED  INDEX [I_SANT_CAJA_DEV] ON [dbo].[T_SANT_CAJA_DEV]([ID_DEV], [ID_CAJA], [ID_ST_CAJA]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  CLUSTERED  INDEX [I_SANT_DET_ORD_1] ON [dbo].[T_SANT_DET_ORD]([ID_ORD], [ID_PROD], [SER_PROD]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  CLUSTERED  INDEX [I_SANT_DEV] ON [dbo].[T_SANT_DEV]([ID_DEV], [NB_CLTE_DEV], [FE_REG], [CANT_CAJA], [ID_ST], [ID_GUIA]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  CLUSTERED  INDEX [I_SANT_ORD_1] ON [dbo].[T_SANT_ORD]([ID_ORD], [ID_CHEQ], [ID_TPO_ORD], [ID_ST_ORD], [ID_ORD_GP], [FE_ING_ORD]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

 CREATE  CLUSTERED  INDEX [I_SANT_TRANSP] ON [dbo].[T_Sant_Transp]([CI_Transp], [Status_Transp], [IN_ST]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

ALTER TABLE [dbo].[T_SANT_CAJA_DEV] WITH NOCHECK ADD 
	CONSTRAINT [DF__T_SANT_CA__FE_RE__4316F928] DEFAULT (getdate()) FOR [FE_REG]
GO

ALTER TABLE [dbo].[T_SANT_CAJA_ORD] WITH NOCHECK ADD 
	CONSTRAINT [UN_SANT_CAJA_ORD] UNIQUE  NONCLUSTERED 
	(
		[ID_ORD],
		[ID_CAJA]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[T_SANT_DET_ORD] WITH NOCHECK ADD 
	CONSTRAINT [UN_SANT_DET_ORD] UNIQUE  NONCLUSTERED 
	(
		[ID_ORD],
		[ID_PROD]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[T_SANT_MEMO_GUIA] WITH NOCHECK ADD 
	CONSTRAINT [DF__T_SANT_ME__FE_ME__496D8C18] DEFAULT (getdate()) FOR [FE_MEMO]
GO

ALTER TABLE [dbo].[T_SANT_ORD] WITH NOCHECK ADD 
	CONSTRAINT [DF__T_SANT_OR__IN_IM__3C69FB99] DEFAULT (0) FOR [IN_IMP],
	CONSTRAINT [PK_SANT_ORD] PRIMARY KEY  NONCLUSTERED 
	(
		[ID_ORD]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [PK_SANT_ORD_ID_GP] UNIQUE  NONCLUSTERED 
	(
		[ID_ORD_GP]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	CONSTRAINT [UN_SANT_ORD] UNIQUE  NONCLUSTERED 
	(
		[ID_ORD_GP]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[T_SANT_USR] WITH NOCHECK ADD 
	 UNIQUE  NONCLUSTERED 
	(
		[LOG_USR]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] ,
	 UNIQUE  NONCLUSTERED 
	(
		[LOG_USR]
	) WITH  FILLFACTOR = 90  ON [PRIMARY] 
GO

 CREATE  INDEX [I_SANT_CAJA_ORD] ON [dbo].[T_SANT_CAJA_ORD]([ID_ORD], [ID_CAJA], [ID_ST_CAJA]) WITH  FILLFACTOR = 90 ON [PRIMARY]
GO

ALTER TABLE [dbo].[T_SANT_DET_CAJA_ORD] ADD 
	CONSTRAINT [FK_SANT_DET_CAJA_ORD] FOREIGN KEY 
	(
		[ID_ORD],
		[ID_CAJA]
	) REFERENCES [dbo].[T_SANT_CAJA_ORD] (
		[ID_ORD],
		[ID_CAJA]
	)
GO


