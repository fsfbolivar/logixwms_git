SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[Snt_actualizaDespachado]    Script Date: 01/11/2009 19:33:25 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_actualizaDespachado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_actualizaDespachado]
GO

CREATE  PROCEDURE Snt_actualizaDespachado
  @NOCONSIG char( 21)
AS

update sntconsigdet set
 QUANTITY_SHP = QUANTITY_ORD
 where noconsig = @NOCONSIG

update sntconsigdet set
 XTNDPRCE = QUANTITY_SHP * UNITPRCE
 where noconsig = @NOCONSIG and TIPO in ('ITEM','KIT')

update sntconsighdr set 
 subtotal = SNTVW_CALCTOTCNSG.subtotal,
 trdisamt = SNTVW_CALCTOTCNSG.trdisamt,
 docamnt  = SNTVW_CALCTOTCNSG.docamnt
  from SNTVW_CALCTOTCNSG
   where sntconsighdr.NOCONSIG = @NOCONSIG
    and sntconsighdr.noconsig  =  SNTVW_CALCTOTCNSG.noconsig


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[Snt_GenerarProximoNumero]    Script Date: 01/11/2009 19:35:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_GenerarProximoNumero]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_GenerarProximoNumero]
GO

CREATE PROCEDURE Snt_GenerarProximoNumero
	@SOPNUMBE char(21), @NewSOPNUMBE char(21) output
AS 

declare @Prefijo char(17),
	@Mask char(17),
	@Correlativo int,
	@CADENA char(21),
	@Indice int,
	@Largo int,
	@Caracter char( 3),
	@LargoMascara int



SET @Largo = LEN( @SOPNUMBE)
SET @Indice = @Largo
SET @CADENA = rtrim(@SOPNUMBE)

while @Indice > 0
begin
	SET @Caracter = '%' + RIGHT( rtrim(@CADENA), 1) + '%'
	-- El Caracter no es Numerico
	IF PATINDEX( @Caracter, '0123456789') = 0
	BEGIN
		SET @PREFIJO = @CADENA
				
		SET @Mask = REPLICATE( '0', @Largo - LEN(@CADENA))
		SET @Correlativo = convert( int, substring( rtrim(@SOPNUMBE), len(@PREFIJO) + 1, @Largo))		
		BREAK
	END

	SET @CADENA = substring( @CADENA, 1, LEN(@CADENA) - 1)

	SET @Indice = @Indice - 1

end


if @PREFIJO is null
	select @PREFIJO = '', @Correlativo  = convert( int, @SOPNUMBE)

if @Mask is null
	set @Mask = REPLICATE( '0', @Largo)



SET @Correlativo = @Correlativo + 1


if len( @Correlativo) > len(@Mask)
	SET @LargoMascara = 0
else
	SET @LargoMascara = len(@Mask) - len( @Correlativo) 


SET @NewSOPNUMBE = rtrim(@PREFIJO) + substring( @Mask, 1, @LargoMascara) + rtrim(convert( char(21), @Correlativo))

return(0)


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[SNT_actualiza_asignado]    Script Date: 01/11/2009 20:28:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SNT_actualiza_asignado]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SNT_actualiza_asignado]
GO

CREATE PROCEDURE SNT_actualiza_asignado
AS

/***************************************************************************************************/
/*        STORE PROCEDURE QUE ACTUALIZA LAS CANTIDADES ASIGNADAS POR PEDIDOS, TANTO DE             */
/*         VENTAS EN GREAT PLAINS (PEDIDOS DE VENTAS Y DE PROMOCI�N), COMO LOS PEDIDOS             */
/*                       EN EL M�DULO DE CONSIGNACIONES DE SYNERGIX                                */
/* Nota: ACTUALIZA LAS CANTIDADES ASIGNADAS TODOS LOS ALMACENES, INCLUYENDO EL ALMACEN TOTAL (' ') */
/***************************************************************************************************/

/*******************************************************************/
/* ---- VENTAS ----                                                */   
/* CREA LA TABLA TEMPORAL DE PEDIDOS ABIERTOS EN M�DULOS DE VENTAS */
/*******************************************************************/
create table #pedidos_asignado
(
LOCNCODE varchar(10), 
ITEMNMBR varchar(10), 
PED_ASIGNADO FLOAT
)

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[Snt_AsignarUnidadesConsig]    Script Date: 01/11/2009 20:33:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_AsignarUnidadesConsig]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_AsignarUnidadesConsig]
GO

CREATE      PROCEDURE Snt_AsignarUnidadesConsig
  @NoConsig char(21), @DesError char( 255) out
AS
declare @Quantity_Ord integer,
	@Quantity_Shp integer,
	@Quantity_Asig integer, 
	@Existencia integer,
	@LocnCode char( 11),
	@ItemnMbr char(31),
	@Update_Error int,
	@n_kit int	--indica si es kit,... @nkit>0 -> kit, sino es item

  
set @Update_Error = 0
set @DesError = ''



BEGIN TRANSACTION  


-- Obteniendo Almacen de encabezado Consignacion
SELECT @LocnCode = LocnCode FROM SntConsigHdr where NoConsig = @NoConsig

DECLARE	Cursor_Asignar CURSOR 
FOR SELECT itemnmbr, quantity_ord, Quantity_Shp FROM SntConsigDet where NoConsig = @NoConsig


OPEN Cursor_Asignar
FETCH NEXT FROM Cursor_Asignar INTO @ItemnMbr, @Quantity_Ord, @Quantity_Shp

WHILE (@@fetch_status <> -1)
BEGIN
	IF @Update_Error <> 0
		Break

	-- Solo procesor lo que no tenga unidades asignadas
	IF (@@fetch_status <> -2) and @Quantity_Shp = 0
	BEGIN

		SELECT @Existencia  = 0
		-- Verificar Existencia
		select @Existencia = (case when QTYONHND - ATYALLOC <= 0 then 0 else QTYONHND - ATYALLOC end) from IV00102 
		WHERE itemnmbr = @ItemnMbr and locncode = @LocnCode AND RCRDTYPE = 2

		if @Quantity_Ord > @Existencia 
			SET @Quantity_Asig = @Existencia
		else
			SET @Quantity_Asig = @Quantity_Ord

		if @Quantity_Asig > 0 
		BEGIN
			if @Update_Error = 0 
			BEGIN
				-- Asignar en almacen Origen
				UPDATE IV00102 
				SET ATYALLOC = ATYALLOC + @Quantity_Asig
				WHERE ITEMNMBR = @ItemnMbr
				     AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
			
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @DesError = 'Error al Asignar en almacen Origen (' + rtrim(@LocnCode) + '), Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
				END
			END

			if @Update_Error = 0 
			BEGIN
				-- Asignar en almacen Global
				UPDATE IV00102 
				SET ATYALLOC = ATYALLOC + @Quantity_Asig
				WHERE ITEMNMBR = @ItemnMbr
				     AND LOCNCODE = '' AND RCRDTYPE = 1
			
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @DesError = 'Error al Asignar en almacen Global Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
				END
			END

			if @Update_Error = 0 
			BEGIN
				-- Actualizar Linea de Item
				UPDATE SntConsigDet SET QUANTITY_SHP = @Quantity_Asig, XtndPrce = UNITPRCE * @Quantity_Asig WHERE Noconsig = @Noconsig and itemnmbr = @itemnmbr 
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @DesError = 'Error asignado unidades en consignacion, Item (' + rtrim(@itemnMbr) + '), Tabla (SntConsigDet)'		
				END
			END

		END ELSE BEGIN
			select @n_kit=count(*) from iv00104 where itemnmbr=@ItemnMbr--comprobacion de tipo (kit/item)
			--ahora, se comprueba la existencia de los items que conforman el kit
			SELECT @Existencia  = 0
			select @Existencia = min (case when QTYONHND - ATYALLOC <= 0 then 0 else QTYONHND - ATYALLOC end) from IV00102 
			WHERE itemnmbr IN (select cmptitnm from iv00104 where itemnmbr= @ItemnMbr) and locncode = @LocnCode AND RCRDTYPE = 2
	
			if @Quantity_Ord > @Existencia 
				SET @Quantity_Asig = @Existencia
			else
				SET @Quantity_Asig = @Quantity_Ord
	
			if @Quantity_Asig > 0 begin
			--fin comprobacion existencia
				IF @n_kit>0 begin --si es kit
					if @Update_Error = 0 
					BEGIN
						--print 'si entra en Asignar en almacen Origen'
						-- Asignar en almacen Origen para los kits
						UPDATE IV00102 
						SET ATYALLOC = ATYALLOC + @Quantity_Asig
						WHERE ITEMNMBR = @ItemnMbr
						     AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
					
						IF @@Error <> 0 
						BEGIN
							set @Update_Error = -1
							SET @DesError = 'Error al Asignar en almacen Origen (' + rtrim(@LocnCode) + '), Kit (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
						END
						-- Asignar en almacen Origen para los elementos del kit
						UPDATE IV00102 
						SET ATYALLOC = ATYALLOC + @Quantity_Asig
						WHERE ITEMNMBR IN (select cmptitnm from iv00104 where itemnmbr=@ItemnMbr)
						     AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
					
						IF @@Error <> 0 
						BEGIN
							set @Update_Error = -1
							SET @DesError = 'Error al Asignar en almacen Origen (' + rtrim(@LocnCode) + '), Item del kit (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
						END
		
					END
		
					if @Update_Error = 0 
					BEGIN
						--print 'si entra en Asignar en almacen Global'
						-- Asignar en almacen  para el kit
						UPDATE IV00102 
						SET ATYALLOC = ATYALLOC + @Quantity_Asig
						WHERE ITEMNMBR = @ItemnMbr
						     AND LOCNCODE = '' AND RCRDTYPE = 1
					
						IF @@Error <> 0 
						BEGIN
							set @Update_Error = -1
							SET @DesError = 'Error al Asignar en almacen Global Kit (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
						END
						-- Asignar en almacen  para los items del kit
						UPDATE IV00102 
						SET ATYALLOC = ATYALLOC + @Quantity_Asig
						WHERE ITEMNMBR IN (select cmptitnm from iv00104 where itemnmbr=@ItemnMbr)
						     AND LOCNCODE = '' AND RCRDTYPE = 1
					
						IF @@Error <> 0 
						BEGIN
							set @Update_Error = -1
							SET @DesError = 'Error al Asignar en almacen Global Item del kit (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
						END
					END
		
					if @Update_Error = 0 
					BEGIN
						--print 'si entra en Actualizar Linea'+cast(@Noconsig as varchar(100))+'--->'+cast(@itemnmbr as varchar(100))+'cantidad '+cast(@Quantity_Asig as varchar(100))
						-- Actualizar Linea de 
						UPDATE SntConsigDet SET QUANTITY_SHP = @Quantity_Asig, XtndPrce = UNITPRCE * @Quantity_Asig WHERE Noconsig = @Noconsig and itemnmbr = @itemnmbr 
						IF @@Error <> 0 
						BEGIN
							set @Update_Error = -1
							SET @DesError = 'Error asignado unidades en consignacion, Item (' + rtrim(@itemnMbr) + '), Tabla (SntConsigDet)'		
						END
		
						UPDATE SntConsigDet SET QUANTITY_SHP = @Quantity_Asig, XtndPrce = UNITPRCE * @Quantity_Asig WHERE Noconsig = @Noconsig and itemnmbr IN (select cmptitnm from iv00104 where itemnmbr=@ItemnMbr)
						IF @@Error <> 0 
						BEGIN
							set @Update_Error = -1
							SET @DesError = 'Error asignado unidades en consignacion, Item (' + rtrim(@itemnMbr) + '), Tabla (SntConsigDet)'		
						END
					END
				end --fin si es kit
			end--fin de si cantidad>0
		END

	END
	FETCH NEXT FROM Cursor_Asignar INTO @ItemnMbr, @Quantity_Ord, @Quantity_Shp
END

CLOSE Cursor_Asignar
DEALLOCATE Cursor_Asignar 


if @Update_Error = 0   
	COMMIT TRANSACTION
else
	ROLLBACK TRANSACTION

return( @Update_Error)   









GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[Snt_BorrarConsignacion]    Script Date: 01/11/2009 20:34:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_BorrarConsignacion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_BorrarConsignacion]
GO
 
--****************************************************
--*         Snt_BorrarConsignacion
--****************************************************
--*log de modificacion para el Snt_SalvarLineaConsig *
--****************************************************
--*este store procedure se ha modificado para
--*que al momento de hacer actualizaciones a 
--*las tablas iv00102 se ralize en vez de 
--*la variable @QUANTITY_SHP con la variable
--*@QUANTITY_ORD para actualizar el campo 
--*ATYALLOC al momento de increm
--*entarlo o decrementarlo,
--*la idea de
--*borrar o actulizar la tabla IV00102 al momento de 
--*borrar una consignacion.... y actualizar la tabla de
--*manera consona con las modificaciones hechas al 
--*procedure Snt_SalvarLineaConsig ve log de modif
--*************************************************
--*marca de modificacion: usuario + fecha de modifi
--**jlmejiasm-i modificacion 24-03-2006 
--**jlmejiasm-f modificacion 24-03-2006
--*****************
--********************************
--*al borrar una consignacion
--*se borrara  del sistema de logistica el registro
--*procedure Snt_SalvarLineaConsig ve log de modif
--*************************************************
--*marca de modificacion: usuario + fecha de modifi
--**jlmejiasm-i modificacion 01-08-2006 
--**jlmejiasm-f modificacion 01-08-2006
--*****************
--********************************

CREATE PROCEDURE Snt_BorrarConsignacion
  @NoConsig char(21), @DesError char( 255) out
AS

Declare @Update_Error int,
	@ItemnMbr char( 31),
	@LOCNCODE char( 11),
	@QUANTITY_SHP numeric( 19, 5),
--*jlmejiasm-i modificacion 01-08-2006	
	@id_ord   char(21),
--*jlmejiasm-f modificacion 01-08-2006
--*jlmejiasm-f modificacion 24-03-2006
	@QUANTITY_ORD numeric( 19, 5),
--*jlmejiasm-f modificacion 24-03-2006
	@ATYALLOC numeric( 19, 5)

SET @Update_Error = 0

BEGIN TRANSACTION BorrarConsignacion


SELECT TOP 1 @LOCNCODE = LOCNCODE from SntConsigHdr WHERE NoConsig = @NoConsig

-- Desasignar Unidades
DECLARE DetConsig CURSOR FOR 
--*jlmejiasm-I modificacion 24-03-2006
--*select ITEMNMBR, QUANTITY_SHP from SntConsigDet WHERE NOCONSIG =  @NoConsig order by ITEMNMBR
select ITEMNMBR, QUANTITY_ORD from SntConsigDet WHERE NOCONSIG =  @NoConsig order by ITEMNMBR
--*jlmejiasm-F modificacion 24-03-2006

OPEN DetConsig
--*jlmejiasm-I modificacion 24-03-2006
--*FETCH NEXT FROM DetConsig into @ITEMNMBR, @QUANTITY_SHP
FETCH NEXT FROM DetConsig into @ITEMNMBR, @QUANTITY_ORD
--*jlmejiasm-F modificacion 24-03-2006
WHILE @@FETCH_STATUS = 0
BEGIN	
	IF @Update_Error <> 0 
		BREAK

	-- Desasignar Almacen Central
	if exists( select TOP 1 itemnmbr from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = @LocnCode AND RCRDTYPE = 2)
	BEGIN
--*jlmejiasm-I modificacion 24-03-2006
--*		select @ATYALLOC = ATYALLOC from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
--*		UPDATE IV00102 SET ATYALLOC = ATYALLOC - @QUANTITY_SHP WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
		select @ATYALLOC = ATYALLOC from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
		UPDATE IV00102 SET ATYALLOC = ATYALLOC - @QUANTITY_ORD WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = @LocnCode AND RCRDTYPE = 2

--*jlmejiasm-F modificacion 24-03-2006
		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SET @DesError = 'Error al desasignar unidades en almacen Origen (' + rtrim(@LocnCode) + '), Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
		END
	END
	-- Desasignar Almacen Consolidado
	if exists( select TOP 1 itemnmbr from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = '' AND RCRDTYPE = 1)
	BEGIN
--*jlmejiasm-I modificacion 24-03-2006
--*		select @ATYALLOC = ATYALLOC from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = '' AND RCRDTYPE = 1
--*		UPDATE IV00102 SET ATYALLOC = ATYALLOC - @QUANTITY_SHP WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = '' AND RCRDTYPE = 1
		select @ATYALLOC = ATYALLOC from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = '' AND RCRDTYPE = 1
		UPDATE IV00102 SET ATYALLOC = ATYALLOC - @QUANTITY_ORD WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = '' AND RCRDTYPE = 1
--*jlmejiasm-I modificacion 24-03-2006

		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SET @DesError = 'Error al desasignar unidades en almacen Consolidado , Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
		END
	END
--*jlmejiasm-I modificacion 24-03-2006
--*	FETCH NEXT FROM DetConsig into @ITEMNMBR, @QUANTITY_SHP
	FETCH NEXT FROM DetConsig into @ITEMNMBR, @QUANTITY_ORD
--*jlmejiasm-I modificacion 24-03-2006
END

CLOSE DetConsig
DEALLOCATE DetConsig


if @Update_Error = 0 
BEGIN
	DELETE SntConsigHdr where NoConsig = @NoConsig
	IF @@Error <> 0 
	BEGIN
		set @Update_Error = -1
		SET @DesError = 'Error Borrando Encabezado Consignacion:' + @NoConsig + ', Tabla (SntConsigHdr)'		
	END
	DELETE SntConsigDet where NoConsig = @NoConsig
	IF @@Error <> 0 
	BEGIN
		set @Update_Error = -1
		SET @DesError = 'Error Borrando Detalle Consignacion:' + @NoConsig + ', Tabla (SntConsigDet)'		
	END

--*jlmejiasm-I modificacion 01-08-2006
	select @id_ord = id_ord from t_sant_ord where  ID_ORD_GP = @NoConsig

	update t_sant_ord 
	set ID_ST_ORD = 5
	where id_ord= @id_ord   
                                                    
        IF @@Error <> 0                                                                                      
        BEGIN                                                                                                
        	set @Update_Error = -1                                                                       
        	SET @DesError = 'Error actualizando header logistica:' + @NoConsig + ', Tabla (t_sant_ord)'
        END                                                                                                                                                                                                   
     
--*jlmejiasm-f modificacion 01-08-2006

END

  
IF @Update_Error = 0
BEGIN
	COMMIT TRANSACTION BorrarConsignacion
	-- Transaccion Correcta
	RETURN( 0)
END
ELSE
BEGIN
	ROLLBACK TRANSACTION BorrarConsignacion
	-- Error de Transaccion
	RETURN( 1)
END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[Snt_Consig_Act_AsigXorden]    Script Date: 01/11/2009 20:55:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_Consig_Act_AsigXorden]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_Consig_Act_AsigXorden]
GO

--****************************************************
-- **************************************************
-- ESTE PROCEDURE ESTA DEPRECATED NO SE USA EN NINGUN
-- SP NI EL PROGRAMA DE VB
-- **************************************************
--****************************************************
--*         Snt_Consig_Act_AsigXorden
--******************************************************
--*log de modificacion :     Snt_Consig_Act_AsigXorden *
--******************************************************
--*este procedure se ha creado para modificar LOS
--*REGISTROS DE LA TABLA IV00102 EN EL CAMPO ATYALLOC 
--* Usando la TABLA SntConsigDet SUM(qty ord) POR ITEMS
--* TODOD ESTO COMO PARTE DE LA MODIFICACION 
--*DEL procedure Snt_SalvarLineaConsig ve log de modif

--* se debe correr cuando se ejecuta el proceso 
--* conciliar IV de GreatPlains
--*************************************************

--*marca de modificacion: usuario + fecha de modifi
--**jlmejiasm-i mod 24-03-2006 
--**fbolivar	mod 09-05-2006

--*************************************************
CREATE PROCEDURE  Snt_Consig_Act_AsigXorden
 @DesError char( 255) out
AS

Declare
	@Update_Error int,
	@ItemnMbr char( 31),
	@LOCNCODE char( 11),
	@QUANTITY_SHP numeric( 19, 5),
	@QUANTITY_ORD numeric( 19, 5),
	@ATYALLOC numeric( 19, 5),
	@ATYALLOC_UPDATE numeric( 19, 5),
	@ATYALLOC_TOT numeric( 19, 5),
	@ATYALLOC_TOT2 numeric( 19, 5)
	
SET @Update_Error = 0

BEGIN TRANSACTION BorrarConsignacion
--*************************************************************
--*  		DECLARACION DE CURSOR DetConsig
--* PARA LA OBTENCION DE SUM(QUANTITY_ORD)SUMARIZADOS POR ITEMS  
--*************************************************************

DECLARE DetConsig CURSOR FOR 
select ITEMNMBR, SUM(QUANTITY_ORD) from SntConsigDet GROUP BY ITEMNMBR
--*************************************************************
--*  		OPEN DE CURSOR DetConsig
--*       SE ABRE EL CURSOR DetConsig
--*************************************************************

OPEN DetConsig

--*************************************************************
--*  		FETCH DE CURSOR DetConsig
--* SE HACE  EL FETCH DEL CURSOR DetConsig
--*************************************************************

FETCH NEXT FROM DetConsig into @ITEMNMBR,@QUANTITY_ORD

--*************************************************************
--*  	   PROCESAR HASTA FIN DEL CURSOR
--*************************************************************

WHILE @@FETCH_STATUS = 0
BEGIN	
	IF @Update_Error <> 0 
		BREAK
--*************************************************************
--*     	Desasignar Almacen Central
--* EN ESTE PARRAFO SE REVERSAN LAS ASIGNACIONES HECHAS POR EL 
--* MODULO DE CONSIGNACION.. HACIENDO 
--* ATYALLOC = @ATYALLOC - @QUANTITY_SHP 
--************************************************************* 
	BEGIN
		select @ATYALLOC = ATYALLOC from IV00102 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE = 'CENTRAL'
		 AND RCRDTYPE = 2
		
		set @ATYALLOC_TOT = @ATYALLOC
		
		IF @ATYALLOC_TOT < 0 
		BEGIN
		    set @ATYALLOC_TOT = 0
			UPDATE IV00102 
			 SET ATYALLOC  = @ATYALLOC_TOT
			WHERE ITEMNMBR = @ItemnMbr 
			AND LOCNCODE  = 'CENTRAL'
			AND RCRDTYPE  = 2

			print 'el item en CENTRAL ' 	     + rtrim(@ItemnMbr)
			print 'el @ATYALLOC en CENTRAL '     + rtrim(@ATYALLOC)
			print 'el @QUANTITY_SHP en CENTRAL ' + rtrim(@QUANTITY_SHP)


		END

-- Almacen GLOBAL

		select @ATYALLOC = ATYALLOC from IV00102 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE = ''
		 AND RCRDTYPE = 1
		
		set @ATYALLOC_TOT = @ATYALLOC
		
		IF @ATYALLOC_TOT < 0 
		BEGIN
		    set @ATYALLOC_TOT = 0
			UPDATE IV00102 
			 SET ATYALLOC  = @ATYALLOC_TOT
			WHERE ITEMNMBR = @ItemnMbr 
			AND LOCNCODE  = '' 
			AND RCRDTYPE  = 1

			
			print 'el item en GLOBAL ' 	     + rtrim(@ItemnMbr)
			print 'el @ATYALLOC en GLOBAL '     + rtrim(@ATYALLOC)
			print 'el @QUANTITY_SHP en GLOBAL ' + rtrim(@QUANTITY_SHP)
			
		END
		
		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SET @DesError = 'Error al desasignar unidades en almacen Origen (' + rtrim(@LocnCode) + '), Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
			print @DesError
		END

	END
--*************************************************************
--*     	Asignar Almacen Central
--* ACA SE CARGA DE NUEVO EL VALOR DEL ATYALLOC PERO EN BASE AL
--* VALOR DE EL @QUANTITY_ORD DE LA TABLA SntConsigDet DE 
--* CONSIGNACIONES.
--************************************************************* 
	BEGIN
		select @ATYALLOC_UPDATE = ATYALLOC from IV00102 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE = 'CENTRAL'
		 AND RCRDTYPE = 2

		UPDATE IV00102 
		SET ATYALLOC = @ATYALLOC_UPDATE + @QUANTITY_ORD 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE  = 'CENTRAL'
		 AND RCRDTYPE = 2
		 
-- Almacen GLOBAL

		select @ATYALLOC_UPDATE = ATYALLOC from IV00102 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE = '' 
		 AND RCRDTYPE = 1

		UPDATE IV00102 
		SET ATYALLOC = @ATYALLOC_UPDATE + @QUANTITY_ORD 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE  = ''
		 AND RCRDTYPE = 1

--

		print 'el item en CENTRAL: ' 	 + rtrim(@ItemnMbr)
		print 'el @ATYALLOC UPDATE en CENTRAL: ' + rtrim(@ATYALLOC_UPDATE)
		print 'el @QUANTITY_ORD UPDATE en CENTRAL : ' + rtrim(@QUANTITY_ORD)

		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SET @DesError = 'Error al desasignar unidades en almacen Consolidado , Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
			print @DesError
		END

		
	END

	FETCH NEXT FROM DetConsig into @ITEMNMBR,@QUANTITY_ORD

END
--*********************************************************************
--*			CERRAR CURSOR
--*se cierra el cursor y se dealoca el cursor
--*********************************************************************
CLOSE DetConsig
DEALLOCATE DetConsig

--*************************************************************
--*  	   	  HACER COMMIT
--* SI NO HAY ERROR DE HACE EL COMMIT DE LAS ACTUALIZACIONES
--*************************************************************   
IF @Update_Error = 0
BEGIN
	COMMIT TRANSACTION BorrarConsignacion
	-- Transaccion Correcta
	RETURN( 0)
END
ELSE
BEGIN
	ROLLBACK TRANSACTION BorrarConsignacion
	-- Error de Transaccion
	RETURN( 1)
END
--*************************************************************
--*  	   	          FIN
--*************************************************************  

--GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[SNT_CONSIGNAR]    Script Date: 06/03/2009 12:08:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SNT_CONSIGNAR]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SNT_CONSIGNAR]
GO

CREATE PROCEDURE [dbo].[SNT_CONSIGNAR] (
	@NoConsig char( 21),
	@ErrorDesc char( 255) OUTPUT
) AS
-- variables de trabajo
DECLARE @ATYALLOC decimal( 19, 5)
-- FFB 2008-11-15
DECLARE @sp_con_proc int
DECLARE @BACHNUMB char( 15)
DECLARE @DATERECD DATETIME
DECLARE @DebACCTTYPE smallint
DECLARE @DebDECPLACS smallint
DECLARE @DECPLCUR smallint
DECLARE @DECPLQTY smallint
DECLARE @DESCUENTO decimal( 5, 2)
DECLARE @DOCDATE  DATETIME
DECLARE @DSCRIPTNDebito char(31)
DECLARE @FechaContable DATETIME
DECLARE @GLPOSTDT DATETIME
DECLARE @IMPUESTO decimal( 19, 5)
DECLARE @IndxDebito int
DECLARE @ITEMDESC char( 51)
DECLARE @ITEMISKIT int
DECLARE @ITEMNMBR char( 31)
DECLARE @IVDOCTYP smallint
DECLARE @IVIVINDX int
DECLARE @IVIVOFIX int
DECLARE @LNSEQNBR numeric( 19, 5)
DECLARE @LOCNCODE char( 11)
DECLARE @LocSource char( 11)
DECLARE @LocTarget char( 11)
DECLARE @NOTEINDX numeric( 19, 5)
DECLARE @PRICELVL char( 15)
DECLARE @QTYBSUOM numeric( 19, 5)
DECLARE @QUANTITY_ORD  decimal( 19, 5)
DECLARE @QUANTITY_SHP  decimal( 19, 5)
DECLARE @SEQUENCE int
DECLARE @SOURCEINDICATOR smallint
DECLARE @SRCRFRNCNMBR char(31)
DECLARE @strTXTRDNUM  char( 21)
DECLARE @SumAmt NUMERIC(19,5)
DECLARE @TIPO CHAR(60)
DECLARE @TRFQTYTY smallint
DECLARE @TRTQTYTY smallint
DECLARE @TXTRDNUM char( 21)
DECLARE @UNITCOST decimal( 19, 5)
DECLARE @UNITPRCE decimal( 19, 5)
DECLARE @UOFM char( 9)
DECLARE @USAGETYPE smallint
DECLARE @XTNDPRCE decimal( 19, 5)

-- para el manejo de errores dentro del script
DECLARE @UPDATE_ERROR INT
SET @UPDATE_ERROR = 0
SET @ErrorDesc = ''

-- print '-- comenzar el proceso; algunas inicializaciones triviales'
SET @BACHNUMB = 'CONSIG'
set @DECPLCUR = 1
set @DECPLQTY = 1
SET @IVDOCTYP = 3
set @IVIVINDX = 0
set @IVIVOFIX = 0
set @LNSEQNBR = 0
SET @NOTEINDX = 0
set @QTYBSUOM = 1
SET @SOURCEINDICATOR = 0
SET @SRCRFRNCNMBR = @NoConsig
set @TRFQTYTY = 1  -- Siempre Tipo Disponible = 1
set @TRTQTYTY = 1  -- Siempre Tipo Disponible = 1
set @UNITCOST = 0
set @UOFM = ''
SET @USAGETYPE = 0


-- print '-- Recuperar algunos parametros iniciales, desde la tabla de configuracion'
IF EXISTS(SELECT TOP 1 locncode FROM SntConsigCfg)
BEGIN
	-- Almacen Destino y Cuenta inventario, cuenta de Debito. Almacen Consignacion
	-- FFB 2008-11-15 RECOGER ESTATUS POSTEO CONSIG
	SELECT TOP 1 @LocTarget = locncode, @IndxDebito = actindx, @sp_con_proc = sp_con_proc 
		FROM SntConsigCfg
	SELECT @DebACCTTYPE = ACCTTYPE, @DebDECPLACS = DECPLACS, @DSCRIPTNDebito = SUBSTRING( ActDescr, 1, 31)
		FROM gl00100
		WHERE ACTINDX = @IndxDebito
END ELSE BEGIN
	SET @Update_Error = -1
	SET @ErrorDesc = 'Error configuracion, Almacen destino (Tabla:SntConsigCfg)'
	RETURN (1) -- aqui sin el goto ya que no he comenzado la transaccion
END

IF Cursor_Status('LOCAL', 'DetConsig') = -3 -- NO existe el cursor
BEGIN

-- FFB 2008-11-15
IF @sp_con_proc <> 0 OR @sp_con_proc IS NULL
BEGIN
	SET @Update_Error = -1
	SET @ErrorDesc = 'BLOQUEO DE PROCESO, ESPERE QUE TERMINE OTRO USUARIO DE PROCESAR (Tabla:SntConsigCfg)'
	RETURN (25) -- aqui sin el goto ya que no he comenzado la transaccion
END ELSE 
BEGIN
  UPDATE SntConsigCfg SET sp_con_proc = 1
END

-- print '-- calcular la fecha contable'
SET @FechaContable = convert( datetime, convert( char(10), GETDATE(), 112), 112)
SET @GLPOSTDT = @FechaContable
SET @DATERECD = @FechaContable
SET @DOCDATE  = @FechaContable
SELECT @FechaContable = PERIODDT, @GLPOSTDT = PERIODDT, @DATERECD = PERIODDT, @DOCDATE = PERIODDT
	FROM SY40100
	WHERE SERIES   = '3'
		AND   CLOSED   = '0'
		AND   PERIODID <> '0'
		AND   ODESCTN  = 'Sales Transaction Entry'
		AND NOT( @FechaContable BETWEEN PERIODDT AND PERDENDT )

-- print @FechaContable
-- print '-- comenzar la parte del proceso que afecta a los datos'
BEGIN TRANSACTION CONSIGNAR_TRANSACTION

-- print '-- buscar el siguiente ID correlativo'
--
-- FBB 2008-12-16 se cambia el correlativo GP por el numero de consignacion
SET @TXTRDNUM = @NoConsig
/*
IF EXISTS( SELECT TOP 1 TXTRDNUM FROM IV40100 WHERE SETUPKEY = 1)
BEGIN

	-- print '-- Obtener Correlativo'
	SELECT @TXTRDNUM = TXTRDNUM
		FROM IV40100
		WHERE SETUPKEY = 1

	EXEC Snt_GenerarProximoNumero @TXTRDNUM, @strTXTRDNUM out

	-- print '-- Incrementando siguiente correlativo'
	UPDATE IV40100
		SET TXTRDNUM = @strTXTRDNUM
		WHERE SETUPKEY = 1
	-- print '-- datalength (linea 129)'
	
	IF @@Error <> 0
	BEGIN
		SET @Update_Error = -2
		SET @ErrorDesc = 'Error actualizando correlativo para transferencia (Tabla:IV40100)'
		GOTO FINAL
	END
END ELSE BEGIN
	SET @Update_Error = -3
	SET @ErrorDesc = 'Error buscando correlativo de transferencia (Tabla:IV40100)'
	GOTO FINAL
END
*/

-- print '-- Ahora vamos a hacer un bucle sobre los items que contiene la consignacion'
DECLARE DetConsig CURSOR LOCAL FOR 
	SELECT
		SEQUENCE,     ITEMNMBR, ITEMDESC, UOFM,
		UNITPRCE,     UNITCOST, XTNDPRCE, QUANTITY_ORD,
		QUANTITY_SHP, LOCNCODE, PRICELVL, DESCUENTO,
		IMPUESTO,     TIPO
	FROM SntConsigDet
	WHERE NOCONSIG =  @NoConsig
	AND QUANTITY_SHP > 0
	ORDER BY SEQUENCE

OPEN DetConsig
FETCH NEXT FROM DetConsig INTO @SEQUENCE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO, @TIPO
WHILE @@FETCH_STATUS = 0
BEGIN

	-- print 'en el bucle -- Almacen Origen'
	SET @LocSource = @LOCNCODE
	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	-- print '-- @  1 Verificar que exista el Item  e investiga si es un kit    @'
	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	SET @ITEMISKIT = NULL
	SELECT @ITEMISKIT = CASE ITEMTYPE WHEN 3 THEN 1 ELSE 0 END
		FROM IV00101
		WHERE ITEMNMBR = @ITEMNMBR
	IF @ITEMISKIT IS NULL
	BEGIN
		SET @Update_Error = -4
		SET @ErrorDesc = 'Producto no Existe en Tabla IM00101: (' + RTRIM(@ITEMNMBR) + ')'
		GOTO FINAL
	END

	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	-- print '-- @  CREAR HISTORICO DETALLE CONSIGNACION    @'
	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	INSERT SntConsigDetHst (NOCONSIG, NOAPLICA, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO)
	VALUES( @NoConsig, @NoConsig, 'G', @SEQUENCE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @QUANTITY_SHP, @LocTarget, @PRICELVL, @DESCUENTO, @IMPUESTO, @TIPO)
	IF @@Error <> 0 
	BEGIN
		SET @Update_Error = -1
		SET @ErrorDesc = 'Error Actualizando SntConsigDetHst, Consignacion no. ' + @NoConsig + ', Item: ' + @ITEMNMBR
		GOTO FINAL
	END

	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	-- print '-- @ Ignorar a los kits ... por completo'
	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	IF (@ITEMISKIT = 1)
		GOTO FINAL_BUCLE

	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	-- print '-- @  2 Verificar que exista el Item en los almacenes  @'
	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	IF NOT EXISTS( SELECT TOP 1 ITEMNMBR FROM IV00102 WHERE ITEMNMBR = @ITEMNMBR AND LOCNCODE = @LocSource AND RCRDTYPE = 2)
	BEGIN
		SET @Update_Error = -5
		SET @ErrorDesc = 'Producto (' + RTRIM(@itemnmbr) + ') no Existe en Almacen Origen : (' + RTRIM(@LocSource) + ')'
		GOTO FINAL
	END
	IF NOT EXISTS( SELECT TOP 1 itemnmbr FROM IV00102 WHERE ITEMNMBR = @ITEMNMBR AND LOCNCODE = @LocTarget)
	BEGIN
		SET @Update_Error = -100
		SET @ErrorDesc = 'Producto (' + rtrim(@itemnmbr) + ') no Existe en Almacen Destino : (' + rtrim(@LocTarget) + ')'
		GOTO FINAL
	END

	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	-- print '-- @  Actualizando inventario almacen Origen (los kits se ignoran por completo aqui)  @'
	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	SELECT @ATYALLOC = ATYALLOC
		FROM IV00102
		WHERE ITEMNMBR = @ItemNmbr
			AND locncode = @LocSource
			AND RCRDTYPE = 2
	IF (@ATYALLOC >= @QUANTITY_SHP)
	BEGIN
		UPDATE IV00102
			SET ATYALLOC = ATYALLOC - @QUANTITY_ORD
			WHERE itemnmbr = @ItemnMbr
				AND locncode = @LocSource
				AND RCRDTYPE = 2
		IF @@Error <> 0 
		BEGIN
			SET @Update_Error = -7
			SET @ErrorDesc = 'Error Actualizando Unidades en Almacen, Producto (' + RTRIM(@itemnmbr) + '), Almacen Origen : (' + RTRIM(@LocSource) + ')'
			GOTO FINAL
		END
	END ELSE BEGIN
		IF (NOT EXISTS (SELECT ITEMNMBR FROM IV00104 WHERE CMPTITNM=@ItemnMbr)) BEGIN
			SET @Update_Error = -8
			SET @ErrorDesc = '***Error Cantidad Asignada no concuerda con Asignada, Producto (' + RTRIM(@itemnmbr) + '), Almacen Origen : (' + RTRIM(@LocSource) + ')'
			GOTO FINAL
		END
	END

	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

	-- print '-- @  Desasignar almacen Global @'
	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	SELECT @ATYALLOC = ATYALLOC
		FROM IV00102
		WHERE itemnmbr = @ItemnMbr
			AND locncode = ''
			AND RCRDTYPE = 1
	IF (@ATYALLOC >= @QUANTITY_SHP) 
	BEGIN
		UPDATE IV00102
			SET ATYALLOC = ATYALLOC - @QUANTITY_ORD
			WHERE itemnmbr = @ItemNmbr
			AND locncode = ''
			AND RCRDTYPE = 1
		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -6
			SET @ErrorDesc = 'Error desasignando Unidades en Almacen Global, Producto (' + rtrim(@itemnmbr) + ')'
			GOTO FINAL
		END
	END ELSE BEGIN
		IF (NOT EXISTS (SELECT CMPTITNM FROM IV00104 WHERE CMPTITNM=@ItemnMbr)) BEGIN
			SET @Update_Error = -9
			SET @ErrorDesc = 'Error Desasignando unidades en Almacen Global,  Producto (' + rtrim(@itemnmbr) + '), No hay suficiente unidades asignadas'
			GOTO FINAL
		END
	END

	SELECT @QTYBSUOM = QTYBSUOM
		FROM IV00106
		WHERE ITEMNMBR = @ITEMNMBR
			AND UOFM = @UOFM
	SELECT @IVIVINDX = IVIVINDX, @DECPLCUR = DECPLCUR, @DECPLQTY = DECPLQTY
		FROM IV00101
		WHERE ITEMNMBR = @ITEMNMBR
	SELECT @LNSEQNBR = @LNSEQNBR + 16384
	SELECT @IVIVOFIX = ACTINDX
		FROM SntConsigCfg -- FFB 2008-01-02 CUENTA CONSIGNA

	INSERT IV10001 (
	  IVDOCNBR  , IVDOCTYP  , ITEMNMBR  , LNSEQNBR  , UOFM      , QTYBSUOM  , TRXQTY    , 
	  UNITCOST  , EXTDCOST   			, 
	  TRXLOCTN  , TRNSTLOC  , TRFQTYTY  , TRTQTYTY  , IVIVINDX  ,
	  IVIVOFIX  , IVWLNMSG  , DECPLCUR  , DECPLQTY  , USAGETYPE )
	VALUES (
	  @TXTRDNUM , @IVDOCTYP , @ITEMNMBR , @LNSEQNBR , @UOFM     , @QTYBSUOM , @QUANTITY_SHP, 
	  @UNITCOST , @QUANTITY_SHP * @UNITCOST         ,
	  @LocSource, @LocTarget, @TRFQTYTY , @TRTQTYTY , @IVIVINDX ,
	  @IVIVOFIX , 0x00000000, @DECPLCUR , @DECPLQTY , @USAGETYPE )
	IF @@ERROR <> 0
	BEGIN
		SET @Update_Error = -11
		SELECT @ErrorDesc = 'Error Actualizando IV10001, Consignacion no. ' + @NoConsig + ', SECUENCIA: ' + @LNSEQNBR
		GOTO FINAL
	END

	FINAL_BUCLE:
	FETCH NEXT FROM DetConsig INTO @SEQUENCE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO, @TIPO
END

CLOSE DetConsig
DEALLOCATE DetConsig

-- print '-- Mover los datos desde las tablas de trabajo hasta las historicas'
-- SntConsigHdr --> SntConsigHdrHst
INSERT INTO SntConsigHdrHst
   (NOCONSIG, NOAPLICA, DOCTYPE, CUSTNMBR, CNTCPRSN, CONTACT, ADRSCODE, ADDRESS1, ADDRESS2, CITY, STATE, ZIPCODE,
    COUNTRY, PHONE1, PHONE2, FAX, CSTPONBR, SLPRSNID, DOCDATE, LOCNCODE, PRCLEVEL, TaxSchId, SUBTOTAL, TRDISAMT,
	DESCPRC, FRTAMNT, MISCAMNT, TAXAMNT, DOCAMNT, USUERACT    , FECHAACT , MONEDA, COMENT1, COMENT2, LOCNConsig,
	NoControl, ITMCLSCD, FIRMAYSELLO) 
SELECT 
	NOCONSIG, NOCONSIG, 'G'    , CUSTNMBR, CNTCPRSN, CONTACT, ADRSCODE, ADDRESS1, ADDRESS2, CITY, STATE, ZIPCODE,
	COUNTRY, PHONE1, PHONE2, FAX, CSTPONBR, SLPRSNID, DOCDATE, LOCNCODE, PRCLEVEL, TaxSchId, SUBTOTAL, TRDISAMT,
	DESCPRC, FRTAMNT, MISCAMNT, TAXAMNT, DOCAMNT, SYSTEM_USER, getdate(), MONEDA, COMENT1, COMENT2, LOCNConsig,
	NoControl, ITMCLSCD, FIRMAYSELLO
FROM SntConsigHdr
WHERE  NOCONSIG = @NoConsig
	-- chequear por errores
IF @@Error <> 0 
BEGIN
	SET @Update_Error = -1
	SELECT @ErrorDesc = 'Error Insertando registro en SntConsigHdrHst, Consignacion no.' + @NoConsig
	GOTO FINAL
END

-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- print '-- @  PROCESAR ENCABEZADO CONSIGNACION    @'
-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
SELECT @SumAmt = SUM(TRXQTY)
	FROM IV10001
	WHERE IVDOCTYP = 3
		AND IVDOCNBR = @TXTRDNUM
--
INSERT IV10000 (
  BACHNUMB   , BCHSOURC   , IVDOCNBR   , RCDOCNUM    , IVDOCTYP   , DOCDATE    ,
  MODIFDT    , MDFUSRID   , POSTEDDT   , PTDUSRID    , GLPOSTDT   , PSTGSTUS   ,
  TRXQTYTL   , IVWHRMSG   , NOTEINDX   , SRCRFRNCNMBR, SOURCEINDICATOR )
VALUES (
  @BACHNUMB  , 'IV_Trans' , @TXTRDNUM  , @TXTRDNUM   , @IVDOCTYP  , @DOCDATE   ,
  @DOCDATE   , 'SA'	  , '19000101' , ''          , @DOCDATE	  , 0          ,
  @SumAmt    , 0x00000000 , @NOTEINDX  ,@SRCRFRNCNMBR, @SOURCEINDICATOR )

IF NOT EXISTS( SELECT 1 FROM SY00500 where BACHNUMB = @BACHNUMB AND GLPOSTDT = '19000101' AND SERIES = 5) -- FFB 2008-01-02 NEW COND
BEGIN
	-- print '-- Agregar Lote ID'
	INSERT SY00500 ( 
	  BCHSOURC  , BACHNUMB  , SERIES    , NUMOFTRX  , BACHFREQ  , BCHCOMNT  ,
	  BRKDNALL  , BCHTOTAL  , MODIFDT               , CREATDDT              ,
	  ORIGIN    , POSTTOGL )
	VALUES(
	  'IV_Trans', @BACHNUMB , 5	    , 1         , 1	    , 'Consignacion',
	  0         , @SumAmt   , @FechaContable        , @FechaContable        ,
 	  '2'       , 1        )
	IF @@Error <> 0 
	BEGIN
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Agregando Lote - SY00500, Consignacion no. ' + @NoConsig
		GOTO FINAL
	END
END ELSE BEGIN 
	-- print 'line 358'
	UPDATE SY00500
		SET BCHTOTAL = BCHTOTAL + @SumAmt
		WHERE BACHNUMB = @BACHNUMB
			AND GLPOSTDT = '19000101' AND SERIES = 5 -- FFB 2008-01-02 NEW COND
		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SELECT @ErrorDesc = 'Error Actualizando Lote - SY00500, Consignacion no. ' + @NoConsig 
			GOTO FINAL
		END
END


-- print '-- Al final del proceso, eliminar el registro asociado de SntConsigHdr y SntConsigDet'
-- Primero vamos con SntConsigHdr
DELETE SntConsigHdr WHERE  NOCONSIG = @NoConsig
IF @@Error <> 0 
BEGIN
	set @Update_Error = -1
	SELECT @ErrorDesc = 'Error Borrando registro en SntConsigHdr, Consignacion no.' + @NoConsig
	GOTO FINAL
END
-- print '-- Y ahora vamos con SntConsigDet'
DELETE SntConsigDet WHERE  NOCONSIG = @NoConsig
IF @@Error <> 0 
BEGIN
	SET @Update_Error = -1
	SELECT @ErrorDesc = 'Error Borrando registro en SntConsigDet, Consignacion no.' + @NoConsig
	GOTO FINAL
END

FINAL:
-- print '-- Finalmente, consignar la transaccion de base de datos si todo bien, o devolver en caso contrario'
IF @UPDATE_ERROR = 0
	COMMIT TRANSACTION CONSIGNAR_TRANSACTION
ELSE
	ROLLBACK TRANSACTION CONSIGNAR_TRANSACTION
--
-- FFB 2008-11-15 DEVOLVER VALOR A LIBERAR POSTEO
UPDATE SntConsigCfg SET sp_con_proc = 0
--
END -- CURSOR_STATUS
 ELSE BEGIN
  	SET @Update_Error = 50
	SET @ErrorDesc = 'Error Cursor DetConsig en uso.'
 END

RETURN (@UPDATE_ERROR)

GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[Snt_DevolverConsignacion]    Script Date: 01/11/2009 19:59:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_DevolverConsignacion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_DevolverConsignacion]
GO

CREATE                          PROCEDURE [dbo].[Snt_DevolverConsignacion] 
	@DEVONUMB char( 21),
--JLMEJIASM-I 13-07-2007
--	@NoControl char( 10),
	@NoControl char( 30),
--JLMEJIASM-F 13-07-2007
	@ErrorDesc char( 255) OUTPUT
--***************************************************************
--* L . O . G               DE MODIFICACIONES                   *
--***************************************************************
--*   MARCA        /        DESCRIPCION DE LA MODIFICACION      *
--***************************************************************
--*JLMEJIASM-I 17-05-2006   SE HA CAMBIADO LA FECHA  
--*JLMEJIASM-F 17-05-2006   @FECHAHOY DEL VALOR DEL DIA ACTUAL 
--*      		    POR EL VALOR DEL PERIODO FISCAL
--*			    PROXIMO QUE ESTE AVIERTO.
--*			    DODE LA SERIE SEA SERIES   = '3'
--* 			    EQUIVALENTE A VENTAS Y
--* 			    ODESCTN  = 'Sales Transaction Entry'
--*			    ADEMAS QUE EL PERIODO ESTE ABIERTO.=0	
--*		
--*JLMEJIASM-I 24-06-2006   SE HA COMENTADO EL DOCDATE PARA QUE TENGA 
--*JLMEJIASM-F 24-06-2006   EFECTO EL CAMBIO DEL 17-05-2006 
--*
--*JLMEJIASM-I 05-01-2007   SE HA MODIFICADO EL VALOR DE LA VARIABLE  @UNITCOST
--*			    POR CURRCOST FROM IV00101 PARA EL ITEM
--*JLMEJIASM-F 05-01-2007   ESTO PARA QUE EL COSTO EN LA DEVOLUCION SEA EL MISMO  
--*			    QUE TIENE EN LA TABLA IV00101.	
--*JLMEJIASM-I 06-03-2007   SE HA MODIFICADO EL SP PARA ELIMINAR REGISTROS DE 
--*JLMEJIASM-F 06-07-2007   DEVOLUCIONES CON QUANTITY-SHP = 0
--*
--*JLMEJIASM-I 11-04-2007   SE HA INCLUIDO UNA VALIDACION PARA VERIFICAR 		
--*JLMEJIASM-F 11-04-2007   QUE EL MONTO DEL MOVIMIENTO EN GL ES IGUAL EN INVENTARIO IV 
--*
--*JLMEJIASM-I 04-05-2007   ESTA ES UNA ADAPTACION DEL PROCESO DE CONSIGNAR AL LAS TABLAS
--*JLMEJIASM-I 04-05-2007   DE G.P 9.	
--*JLMEJIASM-I 13-07-2007   SE AMPLIO EL TAMA�O DEL NOCONTROL. A 30.
--*JLMEJIASM-I 13-07-2007
--***************************************************************
--***************************************************************

AS 

Declare @Update_Error  	int,
	@NoAplica 	char( 21),
	@ItemnMbr 	char( 31),
-- FFB 2007-08-22 CAMBIO LONGITUD CAMPO 
	@ItemDesc 	char(101),
	@PRICELVL 	char( 11), 
	@SEQUENCE 	int, 
	@UOFM 		char(9), 
--*JLMEJIASM-I 05-01-2007
	@UNITPRCE 	NUMERIC(19,5), 
	@UNITCOST 	NUMERIC(19,5),
	@DESCUENTO 	NUMERIC(5,2), 
	@IMPUESTO 	NUMERIC(19,5),
	@SUBTOTAL 	NUMERIC(19,5),  
	@DOCAMNT 	NUMERIC(19,5),  
	@TAXAMNT	NUMERIC(19,5), 
--*JLMEJIASM-F 05-01-2007 
	@CUSTNMBR 	char( 15), 
	@CNTCPRSN 	char( 31), 
	@CONTACT 	char( 31), 
	@ADRSCODE 	char( 15), 
	@ADDRESS1 	char( 31), 
	@ADDRESS2 	char( 31), 
	@CITY 		char( 31), 
	@STATE 		char( 29), 
	@ZIPCODE 	char(11), 
	@COUNTRY 	char(21), 
	@PHONE1 	char(21), 
	@PHONE2 	char(21), 
	@FAX 		char(21), 
	@CSTPONBR 	char(21), 
	@SLPRSNID 	char(15), 
	@DOCDATE 	datetime, 
	@LOCNCODE 	char( 11), 
	@PRCLEVEL 	char( 11), 
	@TaxSchId 	char( 15), 
--*JLMEJIASM-I 05-01-2007
	@TRDISAMT 	NUMERIC(19,5), 
	@DESCPRC 	NUMERIC(19,5), 
	@Saldo 		NUMERIC(19,5), 
	@Saldo_DEV 	NUMERIC(19,5), 
	@Saldo_DEVK 	NUMERIC(19,5), 
	@Devuelto 	NUMERIC(19,5),
	@XTNDPRCE 	NUMERIC(19,5), 
	@QTYONHND  	NUMERIC(19,5), 
	@QUANTITY_ORD  	NUMERIC(19,5), 
	@QUANTITY_SHP  	NUMERIC(19,5), 
	@QTYSUOM 	NUMERIC(19,5),
--*JLMEJIASM-F 05-01-2007
	@LocSource 	char( 11),
	@LocTarget 	char( 11),
	@TXTRDNUM 	char( 21),
	@SOURCDOC 	char( 11), 
	@NTRXSNUM 	int,
	@TRXSORCE 	char(13),
	@IVDOCTYP 	smallint,
	@NUMEROSTR 	char( 8),
	@DOCNUMBR 	char(21),
	@BCHSOURC 	char( 15),
	@BACHNUMB 	char( 15),
--*JLMEJIASM-I 05-01-2007	
--	@NOTEINDX 	NUMERIC,
	@NOTEINDX 	NUMERIC(19,5),
--*JLMEJIASM-F 05-01-2007
	@GLPOSTDT 	datetime,
	@SRCRFRNCNMBR 	char(31),
	@SOURCEINDICATOR smallint,
	@HSTMODUL 	char( 3),
--*JLMEJIASM-I 05-01-2007		
	@LNSEQNBR 	NUMERIC(19,5),
	@TRXQTY 	NUMERIC(19,5),
	@EXTDCOST 	NUMERIC(19,5),
--*JLMEJIASM-F 05-01-2007	
	@TRXLOCTN 	char( 11),
	@TRNSTLOC 	char( 11),
	@TRFQTYTY 	smallint,
	@TRTQTYTY 	smallint,
	@IVIVINDX 	int,
	@IVIVOFIX 	int,
	@DECPLCUR 	smallint,
	@DECPLQTY 	smallint,
	@QTYBSUOM 	numeric(19,5),
	@TXTRDNUMNew 	char( 21),
	@NumTXTRDNUM 	int,
	@MONEDA 	char(15), 
	@COMENT1 	char(60), 
	@COMENT2 	char(60), 
	@LOCNConsig 	char( 11),
--*JLMEJIASM-I 05-01-2007	
	@REMANENTE  	NUMERIC(19,5),
	@QTYRECVD  	NUMERIC(19,5),
	@QTYSOLD  	NUMERIC(19,5),
	@Asignar  	NUMERIC(19,5) ,
--*JLMEJIASM-F 05-01-2007	
	@LocDestino 	char( 11),
	@DATERECD 	datetime, 
	@RCTSEQNM 	smallint, 
	@RCPTSOLD 	tinyint, 
--*JLMEJIASM-I 05-01-2007
	@QTYCOMTD   	NUMERIC(19,5), 
	@QTYRESERVED   	NUMERIC(19,5),
--*JLMEJIASM-F 05-01-2007 
	@FLRPLNDT 	datetime, 
	@PCHSRCTY 	smallint, 
	@RCPTNMBR 	char( 21), 
	@VENDORID 	char( 15), 
	@PORDNMBR 	char( 21), 
	@QTYTYPE 	smallint, 
	@Landed_Cost 	tinyint,
	@DEX_ROW_ID 	int,
	@intTXTRDNUM 	INT,
	@strTXTRDNUM  	char( 21),
	@Mask 		char( 17),
	@IndxDebito 	int,	
	@IndxCredito 	int,	
	@CrearContable 	smallint,
	@CredACCTTYPE 	smallint, 
	@CredDECPLACS 	smallint,
	@DebACCTTYPE 	smallint, 
	@DebDECPLACS 	smallint,
	@CURRNIDX 	smallint,
	@DSCRIPTN 	char(31),
	@DSCRIPTNDebito char(31),
--*JLMEJIASM-I 05-01-2007 
--	@SumAmt    	NUMERIC,
	@SumAmt    	NUMERIC(19,5),
--*JLMEJIASM-F 05-01-2007 
	@FechaContable 	datetime,
	@SQNCLINE 	int, 
	@ActIndx 	int,
	@AcctType 	smallint,
	@DECPLACS 	smallint,
	@CreditAmt 	NUMERIC(19,5),
	@REGISTROS AS 	INT, 		--FFB 2006-02-02
	@TIPO 		VARCHAR(60), 	--JCHERNANDEZ MODIFICACI�N 17/10/2006
	@EL_TIPO  	VARCHAR(60), 	-- FFB 2007-11-20
	@AUX 		numeric, 	--JCHERNANDEZ MODIFICACI�N 22/12/2006
	@ITEM 		VARCHAR(20),	--JCHERNANDEZ MODIFICACI�N 22/12/2006
	@COSTO 		NUMERIC,	--JCHERNANDEZ MODIFICACI�N 22/12/2006
	@DESCRIPCION 	VARCHAR(1000),	--JCHERNANDEZ MODIFICACI�N 22/12/2006
--jlmejiasm-i 11-04-2007
	@total_IV 	numeric( 19, 5),	
	@total_GL 	numeric( 19, 5),	
--jlmejiasm-i 11-04-2007
--JLMEJIASM-I 04-05-2007 uno
	@QTYONHND_MODIF decimal( 19, 5),
	@QTYRECVD_gp9 	decimal( 19, 5),
--JLMEJIASM-I 04-05-2007
--
-- FFB 2008-01-02 VARIABLE DE IV10001
	@USAGETYPE 	smallint



-- Para correlativo de Transferencia
set @Mask = '00000000000000000'


SET @DATERECD = CONVERT( DATETIME, convert( char(8), getdate(), 112), 112)
SET @RCTSEQNM = 0
SET @RCPTSOLD = '0'    -- Linea Abierta
SET @QTYRECVD = 0 
SET @QTYSOLD = 0
SET @QTYCOMTD = 0
SET @QTYRESERVED = 0
SET @FLRPLNDT = '1/1/1900'
SET @PCHSRCTY = 3  -- Entrada 
SET @VENDORID = 'INV XFR'        
SET @PORDNMBR = 'INV XFR' 
SET @UNITCOST = 0 
SET @QTYTYPE = 1
SET @Landed_Cost = 0
-- FFB 2008-01-02 VARIABLE IV10001
SET @USAGETYPE = 0


-- Colocar variables de Manejo de error a Cero
SET @Update_Error = 0
SET @ErrorDesc = ''

SET @CrearContable = 1

-- Valores por Default
-- Tipo de Transaccion de Inventario 3 = Transferencia
-- Encabezado Trx Inventario
SET @IVDOCTYP = 3
SELECT @DOCDATE = convert( datetime, convert( char(10), GETDATE(), 112), 112)
SET @BCHSOURC = 'XIV_Trans'
SET @BACHNUMB = 'CONSIG'
SET @NOTEINDX = 0
SET @GLPOSTDT = convert( datetime, convert( char(10), GETDATE(), 112), 112)
--
-- INICIO FFB 2006-10-20
-- Asignar el numero de devolucion en el campo
-- SRCRFRNCNMBR de la tabla IV30200
SET @SRCRFRNCNMBR = @DEVONUMB
-- FIN 	  FFB 2006-10-20
--
SET @SOURCEINDICATOR = 0
--Detalle Trx Inventario
SET @HSTMODUL = 'IV'
SELECT @CUSTNMBR = ''
-- SELECT @CUSTNMBR = CUSTNMBR, @LocSource = LocnConsig  FROM SntConsigHdrHst WHERE  NOCONSIG = @DEVONUMB  -- Buscar codigo de cliente
-- Buscar Almacen Origen Consignacion
-- Se debe cambiar por el que tenga el encabezado de Consignaciones
if exists( select TOP 1 locncode from sntconsigcfg)
BEGIN
	-- Almacen Destino y Cuenta inventario, cuenta de Debito. Almacen Consignacion
 	select TOP 1 @LocSource = locncode, @IndxDebito = actindx from sntconsigcfg
	select @DebACCTTYPE = ACCTTYPE, @DebDECPLACS = DECPLACS, @DSCRIPTNDebito = substring( ActDescr, 1, 31) from gl00100 where ACTINDX = @IndxDebito
	SET @FechaContable = convert( datetime, convert( char(10), GETDATE(), 112), 112)
END
else
begin
	set @Update_Error = -1
	SET @ErrorDesc = 'Error configuracion, Almacen Origen (Tabla:SntConsigCfg)'
end

set @LNSEQNBR = 0
set @UOFM = ''
set @TRXQTY = 0
set @UNITCOST = 0
set @EXTDCOST = 0
set @TRXLOCTN = ''
set @TRNSTLOC = ''
set @TRFQTYTY = 1  -- Siempre Tipo Disponible = 1
set @TRTQTYTY = 1  -- Siempre Tipo Disponible = 1
set @IVIVINDX = 0
set @IVIVOFIX = 0
set @DECPLCUR = 1
set @DECPLQTY = 1
set @QTYBSUOM = 1
--*JLMEJIASM-I 25-05-2006
--********************************************
--*           10000-inicio 
--*este parrafo se encarga de obtener la fecha
--* del proximo periodo fiscal abierto
--* FECHA FIN DE PERIODO fiscal abierto @FECHAHOYICIERR0
--* FECHA INI DE PERIODO fiscla abierto @FECHAHOYICIERR0
--* FECHA DEL PROXIMO periodo abierto CIERRE @FECHAHOYPROXCIER 
--* SI LA @FECHAHOY esta entre  ENTRE LOS VALORES DEL MES CERRADO
--* SE LE ASIGNARA LA FECHA DEL PROXIMO periodo fiscal abierto
--* DE LO CONTRARIO SE QUEDARA CON EL MISMO VALOR
--* mes cerrado CLOSED   = '1'
--* mes abierto CLOSED   = '0'
--********************************************
declare @FECHAHOYICIERR0  datetime
declare @PERIODID1        char
declare @FECHAHOYPROXCIER datetime
declare @PERIODID4        char
declare @FECHAHOYFCIERR0  datetime


	select 	@FECHAHOYICIERR0 = (PERIODDT),
		@PERIODID1       = (PERIODID),
		@FECHAHOYFCIERR0 = (PERDENDT) 
	from SY40100
	where SERIES   = '3'
	and   CLOSED   = '0'
	and   PERIODID <> '0'
	and   ODESCTN  = 'Sales Transaction Entry'


	select @FECHAHOYPROXCIER = @FECHAHOYICIERR0

	IF (@FechaContable >  @FECHAHOYICIERR0) AND  (@FechaContable < @FECHAHOYFCIERR0)
 	BEGIN
      		SET @FechaContable = convert( datetime, convert( char(10), GETDATE(), 112), 112)
 	END
	ELSE 
  	BEGIN
		IF  @FECHAHOYPROXCIER IS NOT NULL
		BEGIN
			SELECT @FechaContable =	@FECHAHOYPROXCIER
     		SELECT @GLPOSTDT      =	@FECHAHOYPROXCIER    
			SELECT @DATERECD      =	@FECHAHOYPROXCIER      
			SELECT @DOCDATE       =	@FECHAHOYPROXCIER      
 		END
	END

--*JLMEJIASM-I 25-05-2006


BEGIN TRANSACTION DevolverConsignacion

-- @@@@@@@@@@@@@@@@@@@@@@@@@@
-- @ COMIENZA GREAT PLAINS  @
-- @@@@@@@@@@@@@@@@@@@@@@@@@@

-- -- Buscar Correlativo de Transferencia
-- if @Update_Error = 0
-- BEGIN
-- 	if exists( select top 1 TXTRDNUM from IV40100 where SETUPKEY = 1)
-- 	begin
-- 		-- Obtener correlativo
-- 		select @TXTRDNUM = TXTRDNUM from IV40100 where SETUPKEY = 1
-- 		
-- 		exec Snt_GenerarProximoNumero @TXTRDNUM, @strTXTRDNUM out
-- --		select @intTXTRDNUM = convert( int, @TXTRDNUM) + 1
-- --		select len( @intTXTRDNUM)
-- --		select @strTXTRDNUM = substring( @Mask, 1, 17 - len( @intTXTRDNUM)) + convert( char( 17), @intTXTRDNUM)
-- 
-- 		-- Incrementando siguiente correlativo
-- 		update IV40100 set TXTRDNUM = @strTXTRDNUM where SETUPKEY = 1
-- 		-- datalength
-- 		if @@Error <> 0
-- 		begin
-- 			set @Update_Error = -1
-- 			SET @ErrorDesc = 'Error actualizando correlativo para transferencia (Tabla:IV40100)'
-- 		end
-- 	end
-- 	else
-- 	BEGIN
-- 		SET @Update_Error = -1
-- 		SET @ErrorDesc = 'Error buscando correlativo de transferencia (Tabla:IV40100)'
-- 	END
-- 	
-- END
SET @TXTRDNUM = @DEVONUMB

--
-- FFB 2008-01-02 CAMBIO A TABLAS DE TRABAJO GP
--   NO SE UTILIZA ESTE CORRELATIVO
--
-- Buscar Correlativo interno de Transferencia INV
--if @Update_Error = 0
--BEGIN
--	if exists( select top 1 NTRXSNUM from SY01000 WHERE SERIES = 5 AND TRXSRCPX = 'IVTFR')
--	begin
--		select @SOURCDOC = SOURCDOC, @NTRXSNUM = NTRXSNUM from SY01000 WHERE SERIES = 5 AND TRXSRCPX = 'IVTFR'
		-- Incrementando siguiente correlativo
--		update SY01000  set NTRXSNUM = NTRXSNUM + 1 WHERE SERIES = 5 AND TRXSRCPX = 'IVTFR'
--		if @@Error <> 0
--		begin
--			set @Update_Error = -1
--			SET @ErrorDesc = 'Error actualizando correlativo interno transacciones IV (Tabla:SY01000)'
--		end
--		else
--			SELECT @TRXSORCE = 'IVTFR' + REPLICATE( '0', 8 - len(@NTRXSNUM)) + rtrim(convert( char(8), @NTRXSNUM))
--	end
--	else
--	BEGIN
--		SET @Update_Error = -1
--		SET @ErrorDesc = 'Error buscando correlativo interno transacciones IV (Tabla:SY01000)'
--	END
--END
--
-- @@@@@@@@@@@@@@@@@@@@@@@@@@
-- @  FINAL GREAT PLAINS    @
-- @@@@@@@@@@@@@@@@@@@@@@@@@@
--
--
-- 2008-01-02 FFB
-- se elimina ya que el proceso solo
-- creara las transacciones en las tablas
-- de trabajo de GP
--
-- Tabla Temporal Asiento Contable
--CREATE TABLE #DetalleContable ( ActIndx int, AcctType smallint, DECPLACS smallint, CrdtAmt numeric( 19, 5))
--
-- JLM 2007-05-02 ASIENTO CONT
-- FFB 2007-04-16  
-- AGREGAR DELETE A LA TABLA #DetalleContable
-- 2008-01-02 FFB 
--	DELETE #DetalleContable
-- FFB 2007-04-16
-- JLM 2007-05-02 ASIENTO CONT

-- Crear Tabla temporal con select INTO
SELECT * INTO #SntConsigDetHst from SntConsigDetHst where 1=2
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@  Modificaci�n de la consulta que carga el cursos    @
--@  realizada por Jean Hern�ndez, 13-02-2007           @
--@  reparaci�n de bug de devoluciones                  @
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
DECLARE DevolucionDet CURSOR FOR 
select a.NoAplica, a.ITEMNMBR, a.ITEMDESC, sum(a.Saldo), sum(a.devuelto) from #encabezado  as a
where NoAplica in (select distinct b.NoAplica from #Encabezado as b where b.devuelto>0)
and a.ITEMNMBR in (select distinct c.ITEMNMBR from #Encabezado as c where c.devuelto>0)
group by a.NoAplica,a.ITEMNMBR, a.ITEMDESC
--
--@@@@@@@@@@@@@@@@@@@@@@@@
--Fin de modificaci�n    @
--@@@@@@@@@@@@@@@@@@@@@@@@
SET @SEQUENCE = 0
OPEN DevolucionDet
FETCH NEXT FROM DevolucionDet into @NoAplica, @ITEMNMBR, @ITEMDESC, @Saldo, @devuelto
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @SEQUENCE = @SEQUENCE + 1
--
-- FFB - 2007-05-02
	--SELECT 'Valores:',@NoAplica, @ITEMNMBR, @ITEMDESC, @Saldo, @devuelto
--
	-- Buscar datos de consignacion Detalle Origen
	select @UOFM = UOFM, @UNITPRCE = UNITPRCE, @UNITCOST = UNITCOST, @LOCNCODE = LOCNCODE, @PRICELVL = PRICELVL, @DESCUENTO = DESCUENTO, @IMPUESTO = IMPUESTO, @TIPO=TIPO from SntConsigDetHst where itemnmbr = @itemnmbr and NOCONSIG = @NoAplica and DOCTYPE = 'G'
	
--*JLMEJIASM-I 05-01-2007
	SELECT 	@UNITCOST = CURRCOST FROM IV00101 WHERE ITEMNMBR = @itemnmbr
--*JLMEJIASM-F 05-01-2007

	IF EXISTS(SELECT ITEMNMBR FROM IV00104 WHERE ITEMNMBR=@ITEMNMBR) BEGIN
		SELECT @UNITCOST=SUM(CURRCOST) FROM IV00101 WHERE ITEMNMBR IN (SELECT CMPTITNM FROM IV00104 WHERE ITEMNMBR=@ITEMNMBR)
	END

	INSERT #SntConsigDetHst
              (NOCONSIG, NoAplica, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO,TIPO)
	VALUES( @DEVONUMB, @NoAplica, 'D', @SEQUENCE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @UNITPRCE * @Devuelto, 0, @Devuelto, 0, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO,@TIPO)
--
-- FFB 2007-05-02
-- ACTUALIZA Y VALIDA EL REMANENTE 
--  EN CADA LINEA
--
	SELECT 	@Saldo_DEV = saldo FROM snt_devolver_vw WHERE NoAplica = @NoAplica
 	  AND ITEMNMBR = @ITEMNMBR
--
--
	IF @Saldo_DEV - @Devuelto >= 0 BEGIN
	UPDATE SntConsigDetHst SET REMANENTE = @Saldo_DEV - @Devuelto 
		FROM SntConsigDetHst WHERE
		    SntConsigDetHst.NoConsig = @NoAplica
		and SntConsigDetHst.itemnMbr = @ITEMNMBR
		and SntConsigDetHst.DocType = 'G'
	END
	ELSE BEGIN
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Act. REMANENTE SntConsigDetHst, Devolucion no. ' + @NoAplica + ', Item: ' + @ITEMNMBR
		BREAK
 	END
--
-- FIN: ACTUALIZA Y VALIDA REMANENTE
--
	IF EXISTS(SELECT ITEMNMBR FROM IV00104 WHERE ITEMNMBR=@ITEMNMBR) BEGIN
		-- FFB 2007-11-20 CAMBIO DE CURSOR PARA
		-- DEVOLVER TODOS LOS COMPONENTES DEL KIT
		-- INCLUYENDO AQUELLOS CAMBIADOS PERO QUE
		-- QUEDARON CON EXISTENCIA EN SNTCONSIGDETHST
		SET @EL_TIPO = 'HIJO DE '+@ITEMNMBR
		DECLARE ITEMS CURSOR FOR
		SELECT DISTINCT A.ITEMNMBR, B.ITEMDESC, B.CURRCOST FROM
		  SNTCONSIGDETHST A, IV00101 B, snt_saldoHkits_vw C
		  WHERE A.NOCONSIG = @NoAplica AND A.NOCONSIG = C.NOAPLICA
		  AND A.TIPO = @EL_TIPO
		  AND A.ITEMNMBR = C.ITEMNMBR
		  AND A.ITEMNMBR = B.ITEMNMBR AND A.DOCTYPE = 'G'
--
-- FFB 2007-11-20
--		SELECT DISTINCT A.CMPTITNM,B.ITEMDESC,B.CURRCOST
--		  FROM IV00104 AS A INNER JOIN IV00101 AS B ON A.CMPTITNM=B.ITEMNMBR WHERE A.ITEMNMBR=@ITEMNMBR 
		OPEN ITEMS 
		FETCH NEXT FROM ITEMS INTO @ITEM,@DESCRIPCION,@COSTO
		WHILE @@FETCH_STATUS=0 BEGIN

			SET @SEQUENCE=@SEQUENCE+1

			INSERT #SntConsigDetHst 
		              (NOCONSIG, NoAplica, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO,TIPO)
			VALUES( @DEVONUMB, @NoAplica, 'D', @SEQUENCE, @ITEM, @DESCRIPCION, @UOFM, 0, @COSTO, 0, 0, @Devuelto, 0, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO,'HIJO DE '+@ITEMNMBR)
--
-- FFB 2007-05-02
-- Valida los elem. de kits 
			IF EXISTS (SELECT * FROM snt_saldoHkits_vw
			  WHERE NoAplica = @NoAplica AND ITEMNMBR = @ITEM
			 AND SALDO > 0) BEGIN

 			  SELECT @Saldo_DEVK = Saldo FROM snt_saldoHkits_vw
			    WHERE NoAplica = @NoAplica AND ITEMNMBR = @ITEM AND SALDO > 0

			  IF @Saldo_DEVK - @Devuelto >= 0 BEGIN
			  UPDATE SntConsigDetHst SET REMANENTE = @Saldo_DEVK - @Devuelto 
				FROM SntConsigDetHst WHERE
				    SntConsigDetHst.NoConsig = @NoAplica
				and SntConsigDetHst.itemnMbr = @ITEM
				and SntConsigDetHst.DocType = 'G'
			  END
			  ELSE BEGIN
				set @Update_Error = -1
				SELECT @ErrorDesc = 'Error Act. REMANENTE SntConsigDetHst, Devolucion no. ' + @NoAplica + ', Item: ' + @ITEM
				BREAK
		 	  END
			END ELSE BEGIN
	  		   set @Update_Error = -1
			   SELECT @ErrorDesc = 'Error El item Hijo Kit, Devolucion. No esta en:' + @NoAplica + ', Item: ' + @ITEM
			   BREAK			
			END
-- FIN VALIDA ELEM KITS ------------------			
-- ---------------------------------------
			FETCH NEXT FROM ITEMS INTO @ITEM,@DESCRIPCION,@COSTO
		END
		CLOSE ITEMS
		DEALLOCATE ITEMS
	END

	if @@Error <> 0
	begin
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Actualizando SntConsigDetHst, Devolucion no. ' + @NoAplica + ', Item: ' + @ITEMNMBR
		BREAK
	END

	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	-- @  Agregar Asiento Contable temporal   @
	-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	-- FFB 2007-06-20 SE AGREGA LA INSTRUCCION :
	--  AND (NOT EXISTS (SELECT ITEMNMBR FROM IV00104 WHERE ITEMNMBR=@ItemnMbr))
	--  PARA VALIDAR SI ES KIT (PAPA)
	if ( @Update_Error = 0 ) AND (NOT EXISTS (SELECT ITEMNMBR FROM IV00104 WHERE ITEMNMBR=@ItemnMbr))
	BEGIN
		-- Cuenta inventario, cuenta de Credito
		select @IndxCredito = IVIVINDX from IV00101 where ITEMNMBR = @ITEMNMBR
		-- Tipo de Cuenta Credito
		select @CredACCTTYPE = ACCTTYPE, @CredDECPLACS = DECPLACS from gl00100 where ACTINDX = @IndxCredito
-- 2008-01-02 FFB
-- SE REMUEVEN LAS INSERCIONES DEL ASIENTO CONTABLE
--		INSERT #DetalleContable ( ActIndx, AcctType, DECPLACS, CrdtAmt)
--		VALUES( @IndxCredito, @CredACCTTYPE, @CredDECPLACS, @UNITCOST * @devuelto)

		IF @@Error <> 0 
		BEGIN
			SET @Update_Error = -1
			SELECT @ErrorDesc = 'Error CONSULTANDO CUENTAS Contables, Devolucion no.' + @DEVONUMB 
		END
	END


	FETCH NEXT FROM DevolucionDet into @NoAplica, @ITEMNMBR, @ITEMDESC, @Saldo, @devuelto
END

CLOSE DevolucionDet
DEALLOCATE DevolucionDet

--
-- FFB 2007-05-02
-- Finaliza cursor DevolucionDet
--PRINT 'Finalizo cursor DevolucionDet'
--SELECT * FROM #SntConsigDetHst
--
--
-- Definir Fecha de Documento
--
--
-- Buscar Total Devolucion
SELECT 	@SUBTOTAL = sum( UNITPRCE * QUANTITY_SHP), @TAXAMNT = sum( UNITPRCE * QUANTITY_SHP * IMPUESTO/100), 
	@DOCAMNT  = sum( ((UNITPRCE * QUANTITY_SHP) - (UNITPRCE * QUANTITY_SHP * DESCUENTO/100)) + 
			(UNITPRCE * QUANTITY_SHP * IMPUESTO/100)),
	@TRDISAMT = sum(UNITPRCE * QUANTITY_SHP * DESCUENTO/100)
from #SntConsigDetHst where tipo not like 'HIJO DE %'

if @TRDISAMT = 0
	SET @DESCPRC = 0
ELSE
	SET @DESCPRC = @TRDISAMT * 100/@SUBTOTAL

DECLARE @FIRMAYSELLO INT
DECLARE DevolucionHdr CURSOR FOR 
select distinct NoAplica from #encabezado where devuelto > 0

OPEN DevolucionHdr
FETCH NEXT FROM DevolucionHdr into @NoAplica
WHILE @@FETCH_STATUS = 0
BEGIN
-- 
-- Buscar datos de Consignacion Encabezado Origen
	SELECT @CUSTNMBR = CUSTNMBR, @CNTCPRSN = CNTCPRSN, @CONTACT = CONTACT, @ADRSCODE = ADRSCODE, 
	@ADDRESS1 = ADDRESS1, @ADDRESS2 = ADDRESS2, @CITY = CITY, @STATE = STATE, @ZIPCODE = ZIPCODE, 
	@COUNTRY = COUNTRY, @PHONE1 = PHONE1, @PHONE2 = PHONE2, @FAX = FAX, @CSTPONBR = CSTPONBR, 
	@SLPRSNID = SLPRSNID,
--JLMEJIASM-I 24-08-2006
--	@DOCDATE = DOCDATE,
--JLMEJIASM-F 24-08-2006
	@LOCNCODE = LOCNCODE, @PRCLEVEL = PRCLEVEL, 
	@TaxSchId = TaxSchId, @MONEDA = MONEDA, @COMENT1 = COMENT1, @COMENT2 = COMENT2, @LOCNConsig = LOCNConsig,
	@FirmaySello = FIRMAYSELLO
	 from SntConsigHdrHst where NOCONSIG = @NoAplica and DOCTYPE = 'G'


-- Crear Encabezado de Devoluciones
	INSERT SntConsigHdrHst (NOCONSIG, FIRMAYSELLO, NOAPLICA, DOCTYPE, CUSTNMBR, CNTCPRSN, CONTACT, ADRSCODE, ADDRESS1, ADDRESS2, CITY, STATE, ZIPCODE, COUNTRY, PHONE1, PHONE2, FAX, CSTPONBR, SLPRSNID, DOCDATE, LOCNCODE, PRCLEVEL, TaxSchId, SUBTOTAL, TRDISAMT, DESCPRC, FRTAMNT, MISCAMNT, TAXAMNT, DOCAMNT, USUERACT, FECHAACT, MONEDA, COMENT1, COMENT2, LOCNConsig, NoControl)
	VALUES( @DEVONUMB, @FIRMAYSELLO, @NoAplica, 'D', @CUSTNMBR, @CNTCPRSN, @CONTACT, @ADRSCODE, @ADDRESS1, @ADDRESS2, @CITY, @STATE, @ZIPCODE, @COUNTRY, @PHONE1, @PHONE2, @FAX, @CSTPONBR, @SLPRSNID, @DATERECD, @LOCNCODE, @PRCLEVEL, @TaxSchId, @SUBTOTAL, @TRDISAMT, @DESCPRC, 0, 0, @TAXAMNT, @DOCAMNT, SYSTEM_USER, getdate(), @MONEDA, @COMENT1, @COMENT2, @LOCNConsig, @NoControl)
--      Se cambio para que tome la fecha del sistema
--	VALUES( @DEVONUMB, @NoAplica, 'D', @CUSTNMBR, @CNTCPRSN, @CONTACT, @ADRSCODE, @ADDRESS1, @ADDRESS2, @CITY, @STATE, @ZIPCODE, @COUNTRY, @PHONE1, @PHONE2, @FAX, @CSTPONBR, @SLPRSNID, @DOCDATE, @LOCNCODE, @PRCLEVEL, @TaxSchId, @SUBTOTAL, @TRDISAMT, @DESCPRC, 0, 0, @TAXAMNT, @DOCAMNT, SYSTEM_USER, getdate(), @MONEDA, @COMENT1, @COMENT2, @LOCNConsig, @NoControl)

	if @@Error <> 0
	begin
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Actualizando SntConsigHdrHst, Devolucion no. ' + @DEVONUMB + ', Consignacion : ' + @NoAplica 
		BREAK
	END

	FETCH NEXT FROM DevolucionHdr into @NoAplica
END

CLOSE DevolucionHdr
DEALLOCATE DevolucionHdr

--
-- FFB 2007-05-02 Final cursor
--SELECT * FROM SntConsigHdrHst WHERE NOCONSIG = @DEVONUMB
-- 
-- Actualizar Detalle desde temporal
IF @Update_Error = 0 
BEGIN
	INSERT INTO SntConsigDetHst (NOCONSIG, NoAplica, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO,TIPO) 
	SELECT NOCONSIG, NoAplica, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO from #SntConsigDetHst

	if @@Error <> 0
	begin
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Agregando lineas en el SntConsigDetHst(TMP), Devolucion No.' + @DEVONUMB 
	end
END

--
-- FFB 2007-05-02 inserta registros en tabla Consig
--
-- 
-- Almacen Destino
SET @LocTarget = @LOCNCODE
-- 
-- *********************************************
-- Actualizar Detalle en Inventario Great Plains
IF @Update_Error = 0 
BEGIN

	DECLARE DetConsig CURSOR FOR 
	select SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, PRICELVL, DESCUENTO, IMPUESTO 
	from SntConsigDetHst
	WHERE NOCONSIG =  @DEVONUMB
	AND ITEMNMBR NOT IN (SELECT DISTINCT ITEMNMBR FROM IV00104) 	--MODIFICADO POR JEAN HERNANDEZ, 23-02-2007
	order by sequence						--PARA EXCLUIR A LOS KITS DE LOS MOVIMIENTOS DE
									--INVENTARIO, YA QUE NO DEBEN MOVILIZAR NADA
	
	OPEN DetConsig
	FETCH NEXT FROM DetConsig into @SEQUENCE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @PRICELVL, @DESCUENTO, @IMPUESTO
	WHILE @@FETCH_STATUS = 0
	BEGIN	
		IF @Update_Error <> 0 
			BREAK

-- FFB 2008-01-02
-- SE ELIMINAN ACTUALIZACIONES A ESTA TABLA YA
-- QUE SE ACTUALIZAN SOLO TABLAS DE TRABAJO DE GP
---------------------------------
-- FFB 2006-02-02 
---------------------------------
-- Caso devoluciones colgadas BEG
---------------------------------
--		SELECT @REGISTROS = COUNT(VENDORID) FROM IV10200
--		 WHERE VENDORID ='INV XFR' AND QTYRECVD - QTYSOLD < 0 AND TRXLOCTN = 'CONSIGNA' AND RCPTSOLD = 0
--
--		IF @REGISTROS > 0
--		 BEGIN
--		   UPDATE IV10200
--		    SET QTYSOLD = 0
--		    WHERE VENDORID ='INV XFR' AND QTYRECVD - QTYSOLD < 0 AND TRXLOCTN = 'CONSIGNA' AND RCPTSOLD = 0 
--		 END
---------------------------------
-- Caso devoluciones colgadas END
---------------------------------
--
		-- FFB 2007-05-02
		-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		-- @  1 Verificar que exista el Item  @
		-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		if not exists( SELECT TOP 1 itemnmbr FROM IV00101 where itemnmbr = @itemnmbr)
		BEGIN
			SET @Update_Error = -1
			SET @ErrorDesc = 'Producto no Existe en Tabla IV00101: (' + rtrim(@itemnmbr) + ')'
			-- FFB 2007-05-02 PRT
			--PRINT 'Producto No existe:' + @itemnmbr
		END

-- FFB 2008-01-02
-- SE ELIMINAN ACTUALIZACIONES A ESTA TABLA YA
-- QUE SE ACTUALIZAN SOLO TABLAS DE TRABAJO DE GP
-- NI SE TOCA QTYONHND
--
		-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		-- @  2 Verificar que exista el Item en los almacenes  @
		-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--		if @Update_Error = 0
--		BEGIN
--			if not exists( SELECT TOP 1 itemnmbr FROM IV00102 where itemnmbr = @itemnmbr and LOCNCODE = @LocSource AND RCRDTYPE = 2)
--			BEGIN
--				SET @Update_Error = -1
--				SET @ErrorDesc = 'Producto (' + rtrim(@itemnmbr) + ') no Existe en Almacen Origen : (' + rtrim(@LocSource) + ')'
--				-- FFB 2007-05-02 PRT
--				--PRINT 'Producto (' + rtrim(@itemnmbr) + ') no Existe en Almacen Origen : (' + rtrim(@LocSource) + ')'
--			END
--			ELSE
--			BEGIN
				-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				-- @  Actualizando inventario almacen Origen @
				-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--
--				select @QTYONHND = QTYONHND from IV00102 WHERE itemnmbr = @ItemnMbr and locncode = @LocSource AND RCRDTYPE = 2			
--				IF @QTYONHND >= @QUANTITY_SHP
--				BEGIN
--					UPDATE IV00102 SET QTYONHND = QTYONHND - @QUANTITY_SHP WHERE itemnmbr = @ItemnMbr and locncode = @LocSource AND RCRDTYPE = 2
--					if @@Error <> 0
--					begin
--						set @Update_Error = -1
--						SET @ErrorDesc = 'Error Actualizando Unidades en Almacen, Producto (' + rtrim(@itemnmbr) + '), Almacen Origen : (' + rtrim(@LocSource) + ')'
--				FFB 2007-05-02 PRT
--						--PRINT 'Error Act. Unid. Alm, Prod:(' + rtrim(@itemnmbr) + '), Alm. Orig: (' + rtrim(@LocSource) + ')'
--					end
--				END
--				ELSE
--				BEGIN
--					SET @Update_Error = -1
--					SET @ErrorDesc = 'Error No hay suficiente existencia en almacen, Producto (' + rtrim(@itemnmbr) + '), Almacen Origen : (' + rtrim(@LocSource) + ')'
--				FFB 2007-05-02 PRT
--					--PRINT 'Error No hay sufic. exist alm, Prod:(' + rtrim(@itemnmbr) + '), Alm.Orig.: (' + rtrim(@LocSource) + ')'
--				END
--
				-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
				-- @  Actualizando inventario almacen Destino @
				-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--				IF @Update_Error = 0
--				BEGIN 
--					UPDATE IV00102 SET QTYONHND = QTYONHND + @QUANTITY_SHP WHERE itemnmbr = @ItemnMbr and locncode = @LocTarget AND RCRDTYPE = 2
--					if @@Error <> 0
--					begin
--						set @Update_Error = -1
--						SET @ErrorDesc = 'Error Actualizando Unidades en Almacen, Producto (' + rtrim(@itemnmbr) + '), Almacen Destino : (' + rtrim(@LocTarget) + ')'
--				FFB 2007-05-02 PRT ERROR
--						--PRINT 'Error Act. Unid.Alm., Prod.(' + rtrim(@itemnmbr) + '), Alm.Destino : (' + rtrim(@LocTarget) + ')'
--					end
--				END
--			END
--		END
--		
--		if @Update_Error = 0
--		BEGIN
--			if not exists( SELECT TOP 1 itemnmbr FROM IV00102 where itemnmbr = @itemnmbr and LOCNCODE = @LocTarget)
--			BEGIN
--				SET @Update_Error = -1
--				SET @ErrorDesc = 'Producto (' + rtrim(@itemnmbr) + ') no Existe en Almacen Destino : (' + rtrim(@LocTarget) + ')'
--				-- FFB 2007-05-02
--				--PRINT 'Producto (' + rtrim(@itemnmbr) + ') no Existe en Alm Dest.: (' + rtrim(@LocTarget) + ')'
--			END
--		END
--
		-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		-- @ Crear Transacciones en Inventario  @
		-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		if @Update_Error = 0
		BEGIN
			select @QTYBSUOM = QTYBSUOM from IV00106 where itemnmbr = @ITEMNMBR and UOFM = @UOFM
-- FFB 2008-01-02 CAMBIO A TABLAS DE TRABAJO GP
--			SELECT @IVIVINDX = IVIVINDX, @IVIVOFIX = IVIVOFIX, @DECPLCUR = DECPLCUR, @DECPLQTY = DECPLQTY from iv00101 where itemnmbr = @ITEMNMBR
			SELECT @IVIVOFIX = IVIVINDX, @DECPLCUR = DECPLCUR, @DECPLQTY = DECPLQTY from iv00101 where itemnmbr = @ITEMNMBR
			SELECT @LNSEQNBR = @LNSEQNBR + 16384
			SELECT @IVIVINDX = actindx FROM SntConsigCfg -- FFB 2008-01-02 CUENTA CONSIGNA
--
			-- Detalle Transaccion de Inventario
-- FFB 2008-01-02
--  CAMBIO AL INSERT DE IV30300 A IV10001
--
--			INSERT IV30300 
--			      (TRXSORCE  , DOCTYPE   , DOCNUMBR  , DOCDATE  , HSTMODUL , CUSTNMBR , ITEMNMBR , LNSEQNBR,
--			       UOFM      , TRXQTY    , UNITCOST  , EXTDCOST , TRXLOCTN ,
--			       TRNSTLOC  , TRFQTYTY  , TRTQTYTY  , IVIVINDX , IVIVOFIX , DECPLCUR , DECPLQTY , QTYBSUOM)
--			VALUES(@TRXSORCE , @IVDOCTYP , @TXTRDNUM , @DOCDATE , @HSTMODUL, @CUSTNMBR, @ITEMNMBR, @LNSEQNBR, 
--			       @UOFM     , @QUANTITY_SHP, @UNITCOST, @QUANTITY_SHP * @UNITCOST, @LocSource, 
--			       @LocTarget, @TRFQTYTY , @TRTQTYTY , @IVIVINDX, @IVIVOFIX, @DECPLCUR, @DECPLQTY, @QTYBSUOM)
--
--
			INSERT IV10001 (
			  IVDOCNBR  , IVDOCTYP  , ITEMNMBR  , LNSEQNBR  , UOFM      , QTYBSUOM  , TRXQTY    , 
			  UNITCOST  , EXTDCOST   			, 
			  TRXLOCTN  , TRNSTLOC  , TRFQTYTY  , TRTQTYTY  , IVIVINDX  ,
			  IVIVOFIX  , IVWLNMSG  , DECPLCUR  , DECPLQTY  , USAGETYPE )
			VALUES
			 (@TXTRDNUM , @IVDOCTYP , @ITEMNMBR , @LNSEQNBR , @UOFM     , @QTYBSUOM , @QUANTITY_SHP, 
			  @UNITCOST , @QUANTITY_SHP * @UNITCOST         ,
			  @LocSource, @LocTarget, @TRFQTYTY , @TRTQTYTY , @IVIVINDX ,
			  @IVIVOFIX , 0x00000000, @DECPLCUR , @DECPLQTY , @USAGETYPE )
-- FFB 2008-01-02
--  SOLAMENTE ESTAS VARIABLES QUEDARON SIN ASIGNAR 
--  AL HACER EL CAMBIO AL INSERT DE IV30300 A IV10001
--  @DOCDATE , @HSTMODUL, @CUSTNMBR, 
--
			
			IF @@ERROR <> 0 
			BEGIN
				set @Update_Error = -1
				SELECT @ErrorDesc = 'Error Actualizando IV10001, Devolucion no. ' + @DEVONUMB + ', Item: ' + @ITEMNMBR
				-- FFB 2007-05-02
				--PRINT 'Error Act IV30300, Dev no. ' + @DEVONUMB + ', Item: ' + @ITEMNMBR
			END
		END
-- ----------------------------------------------
-- FFB 2008-01-02 SOLO ACT TABLA TRAB GP BEGIN-01
-- ----------------------------------------------
--
		-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		-- @  actualizar Entrada y Salida en Inventario  @
		-- @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--		if @Update_Error = 0
--		BEGIN
--			SET @TRXLOCTN = @LocSource
--			SET @LocDestino = @LocTarget
--			SET @RCPTNMBR = @TXTRDNUM
--			SET @Asignar = @QUANTITY_SHP
--
-- FFB 2008-01-02 SE GRABA SOLO EN TABLAS DE TRABAJO GP
			-- CREAR TABLA Temporal 
--			CREATE TABLE #Entradas ( ITEMNMBR CHAR(31), TRXLOCTN char( 11), QTYRECVD decimal( 19, 5), RCTSEQNM int IDENTITY)
			
			
--			while @Asignar > 0
--			BEGIN
				-- FFB 2007-06-20 (INICIO)
				--   VALIDA SI EL SIGUIENTE QUERY TIENE REGISTROS
				--   SI NO TIENE ASIGNA 0 CERO A LAS VARIABLES
				--   YA QUE ESTABA GENERANDO QUE EL SP QUEDARA EN UN 
				--   BUCLE INFINITO
--				IF EXISTS( select TOP 1 QTYRECVD, QTYSOLD, UNITCOST, DEX_ROW_ID from IV10200 
--				where ITEMNMBR = @ITEMNMBR and TRXLOCTN = @TRXLOCTN and RCPTSOLD = 0
--				order by DATERECD)
--				BEGIN
--				  select TOP 1 @QTYRECVD = QTYRECVD, @QTYSOLD = QTYSOLD, @REMANENTE = QTYRECVD - QTYSOLD, @UNITCOST = UNITCOST, @DEX_ROW_ID = DEX_ROW_ID 
--				  from IV10200 
--				  where ITEMNMBR = @ITEMNMBR and TRXLOCTN = @TRXLOCTN and RCPTSOLD = 0
--				  order by DATERECD
--				END
--				ELSE -- ASIGNA 0 CERO A LAS VARIABLES
--				BEGIN
--		 		  SET @QTYRECVD = 0
--				  SET @QTYSOLD  = 0
--				  SET @REMANENTE = 0
--				  SET @UNITCOST = 0
--				  SET @DEX_ROW_ID = 0
--				END
				-- FFB 2007-06-20 (FIN)
				--   VALIDA SI EL SIGUIENTE QUERY TIENE REGISTROS
				--
				--
				-- FFB 2007-06-20
				-- SE AGREGA A EL SIGUIENTE IF
				--  LA CONDICION: AND (@DEX_ROW_ID > 0) AND (@DEX_ROW_ID IS NOT NULL)				--
				--
--				IF ( @Asignar >= @REMANENTE ) AND (@DEX_ROW_ID > 0) AND (@DEX_ROW_ID IS NOT NULL)
--				BEGIN
--					-- Actualizar Almacen Origen
--					UPDATE IV10200 SET QTYSOLD = QTYSOLD + @REMANENTE, RCPTSOLD = 1
--					where DEX_ROW_ID = @DEX_ROW_ID 
--					IF @@Error <> 0 
--					BEGIN
--						set @Update_Error = -1
--						SELECT @ErrorDesc = 'Error actualizando salidas en Great Plains (IV10200), Devolucion no.' + @DEVONUMB + ', Item : ' + @ITEMNMBR
--						-- FFB 2007-05-02 PRINT ERROR
--					END
--					-- Actualizar Almacen Destino
--					INSERT #Entradas ( ITEMNMBR, TRXLOCTN, QTYRECVD) VALUES( @ITEMNMBR, @LocDestino, @REMANENTE)
--					SET @Asignar = @Asignar - @REMANENTE
--
					/*********************************************
					* Modificaci�n por Jean Hern�ndez 05/03/2007 *
					*********************************************/
--					SELECT @UNITCOST = CURRCOST FROM IV00101 where itemnmbr = @ITEMNMBR
--					-- Buscar Siguiente Transaccion
--					SELECT @RCTSEQNM = isnull(max(RCTSEQNM), 0) + 1 FROM IV10200 WHERE ITEMNMBR = @ITEMNMBR and  TRXLOCTN = @LocDestino and  QTYTYPE = @QTYTYPE and DATERECD = @DATERECD
--					SELECT @QTYONHND_MODIF = QTYONHND FROM IV00102 WHERE itemnmbr = @ITEMNMBR AND LOCNCODE = '' 
--	
--					INSERT IV10200 (ITEMNMBR, TRXLOCTN, DATERECD, RCTSEQNM, RCPTSOLD, QTYRECVD, QTYSOLD, QTYCOMTD, QTYRESERVED, FLRPLNDT, PCHSRCTY, RCPTNMBR, VENDORID, PORDNMBR, UNITCOST, QTYTYPE, Landed_Cost,NEGQTYSOPINV,VCTNMTHD,ADJUNITCOST,QTYONHND)
--					SELECT @ITEMNMBR, @LocDestino, @DATERECD, @RCTSEQNM, 0, sum(QTYRECVD), @QTYSOLD, @QTYCOMTD, @QTYRESERVED, @FLRPLNDT, @PCHSRCTY, @RCPTNMBR, @VENDORID, @PORDNMBR, 
--					@UNITCOST, @QTYTYPE, @Landed_Cost,0,3,@UNITCOST,@QTYONHND_MODIF  from #Entradas
--
--					IF @@Error <> 0 
--					BEGIN
--						set @Update_Error = -1
--						SELECT @ErrorDesc = 'Error actualizando entradas en Great Plains (IV10200), Devolucion no.' + @DEVONUMB 
--						-- FFB 2007-05-02 PRINT ERROR
--					END
--
					/*********************************************
					* Fin modificaci�n                           *
					*********************************************/
--				END 
--				ELSE
--				BEGIN
--					-- Actualizar Almacen Origen
--					UPDATE IV10200 SET QTYSOLD = QTYSOLD + @Asignar
--					where DEX_ROW_ID = @DEX_ROW_ID 
--					IF @@Error <> 0 
--					BEGIN
--						set @Update_Error = -1
--						SELECT @ErrorDesc = 'Error actualizando salidas en Great Plains (IV10200), Devolucion no.' + @DEVONUMB + ', Item : ' + @ITEMNMBR
--						-- FFB 2007-05-02
--						--PRINT 'Error act SALIDAS en GP (IV10200), Dev no.' + @DEVONUMB + ', Item : ' + @ITEMNMBR
--					END
--
-- FFB 2008-01-02 SE GRABA SOLO EN TABLAS DE TRABAJO GP			
--					-- Actualizar Almacen Destino
--					INSERT #Entradas ( ITEMNMBR, TRXLOCTN, QTYRECVD) VALUES( @ITEMNMBR, @LocDestino, @Asignar)
--					SET @Asignar = 0
--			
					/*********************************************
					* Modificaci�n por Jean Hern�ndez 05/03/2007 *
					*********************************************/
--					SELECT @UNITCOST = CURRCOST FROM IV00101 where itemnmbr = @ITEMNMBR
--					-- Buscar Siguiente Transaccion
--					SELECT @RCTSEQNM = isnull(max(RCTSEQNM), 0) + 1 FROM IV10200 WHERE ITEMNMBR = @ITEMNMBR and  TRXLOCTN = @LocDestino and  QTYTYPE = @QTYTYPE and DATERECD = @DATERECD
--					SELECT @QTYONHND_MODIF = QTYONHND FROM IV00102 WHERE itemnmbr = @ITEMNMBR AND LOCNCODE = '' 
--
--					INSERT IV10200 (ITEMNMBR, TRXLOCTN, DATERECD, RCTSEQNM, RCPTSOLD, QTYRECVD, QTYSOLD, QTYCOMTD, QTYRESERVED, FLRPLNDT, PCHSRCTY, RCPTNMBR, VENDORID, PORDNMBR, UNITCOST, QTYTYPE, Landed_Cost,NEGQTYSOPINV,VCTNMTHD,ADJUNITCOST,QTYONHND)
--					SELECT @ITEMNMBR, @LocDestino, @DATERECD, @RCTSEQNM, 0, sum(QTYRECVD), @QTYSOLD, @QTYCOMTD, @QTYRESERVED, @FLRPLNDT, @PCHSRCTY, @RCPTNMBR, @VENDORID, @PORDNMBR, 
--					@UNITCOST, @QTYTYPE, @Landed_Cost,0,3,@UNITCOST,@QTYONHND_MODIF  from #Entradas
--
--					IF @@Error <> 0 
--					BEGIN
--						set @Update_Error = -1
--						SELECT @ErrorDesc = 'Error actualizando entradas en Great Plains (IV10200), Devolucion no.' + @DEVONUMB 
--						-- FFB 2007-05-02 PRT ERROR
--						--PRINT 'Error actualizando entradas en Great Plains (IV10200), Dev no.' + @DEVONUMB 
--					END
					/*********************************************
					* Fin modificaci�n                           *
					*********************************************/			
--				END			
--			END
--	
--			DROP TABLE #Entradas
--		END
-- --------------------------------------------
-- FFB 2008-01-02 SOLO ACT TABLA TRAB GP END-01
-- --------------------------------------------
--
		FETCH NEXT FROM DetConsig into @SEQUENCE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @PRICELVL, @DESCUENTO, @IMPUESTO
	END
	
	CLOSE DetConsig
	DEALLOCATE DetConsig
END

if @Update_Error = 0
BEGIN
	-- Encabezado Transacciones de Inventario
--	INSERT IV30200 (TRXSORCE, IVDOCTYP, DOCNUMBR, DOCDATE, BCHSOURC, BACHNUMB, NOTEINDX, GLPOSTDT, SRCRFRNCNMBR, SOURCEINDICATOR)
--	VALUES(@TRXSORCE, @IVDOCTYP, @TXTRDNUM, @DOCDATE, @BCHSOURC, @BACHNUMB, @NOTEINDX, @GLPOSTDT, @SRCRFRNCNMBR, @SOURCEINDICATOR)
--
-- FFB 2008-01-02 SE ALMACENA EN TABLAS DE TRABAJO GP
	SELECT @SumAmt = sum(TRXQTY) FROM IV10001 a WHERE IVDOCTYP = 3 AND IVDOCNBR = @TXTRDNUM
--
	INSERT IV10000 (
	  BACHNUMB   , BCHSOURC   , IVDOCNBR   , RCDOCNUM    , IVDOCTYP   , DOCDATE    ,
          MODIFDT    , MDFUSRID   , POSTEDDT   , PTDUSRID    , GLPOSTDT   , PSTGSTUS   ,
	  TRXQTYTL   , IVWHRMSG   , NOTEINDX   , SRCRFRNCNMBR, SOURCEINDICATOR )
	VALUES (
	  @BACHNUMB  , 'IV_Trans' , @TXTRDNUM  , @TXTRDNUM   , @IVDOCTYP  , @DOCDATE   ,
	  @DOCDATE   , 'SA'	  , '19000101' , ''          , @DOCDATE	  , 0          ,
	  @SumAmt    , 0x00000000 , @NOTEINDX  ,@SRCRFRNCNMBR, @SOURCEINDICATOR )
--
	if @@Error <> 0

	begin
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Actualizando IV30200, Devolucion no. ' + @DEVONUMB 
		-- FFB 2007-05-02
		--PRINT 'Error Actualizando IV30200, Devolucion no. ' + @DEVONUMB 
	END
END

-- ************************************************
-- Actualizar Remanente de La Consignacion Original
-- ************************************************
--IF @Update_Error = 0 
--BEGIN
-- FFB 2007-05-02 
--  SE COMENTA EL CALCULO PARA HACERLO LINEA A LINEA
--  AL INSERTAR EN #SNTCONSIGDETHST
--
--	SELECT @AUX = (SntConsigDetHst.REMANENTE - #SntConsigDetHst.QUANTITY_SHP)
--	FROM SntConsigDetHst inner join #SntConsigDetHst ON SntConsigDetHst.NoConsig = #SntConsigDetHst.NoAplica 
--		and SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr
--		and SntConsigDetHst.DocType = 'G'
--	WHERE (SntConsigDetHst.REMANENTE - #SntConsigDetHst.QUANTITY_SHP) < 0
--
	-- FFB 2007-05-01 IDENTIFICAR ALGUNA LINEA QUE haga EL REMANENTE  < 0
--
--	IF @AUX<0 BEGIN
--		set @Update_Error = -1
--		SELECT @ErrorDesc = 'Error Actualizando campo Remanente en SntConsigDetHst(TMP), Devolucion No.' + @DEVONUMB + ', Probablemente otra persona esta haciendo devoluciones al mismo cliente'
--		-- FFB 2007-05-02 PRT ERROR
--		PRINT 'Error Act Remanente en SntConsigDetHst(TMP), Dev No.' + @DEVONUMB + ', Probablemente otra persona esta haciendo dev al mismo cliente'
--	END ELSE BEGIN
--		UPDATE SntConsigDetHst SET REMANENTE = SntConsigDetHst.REMANENTE - #SntConsigDetHst.QUANTITY_SHP 
--		FROM SntConsigDetHst inner join #SntConsigDetHst ON SntConsigDetHst.NoConsig = #SntConsigDetHst.NoAplica 
--			and SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr
--			and SntConsigDetHst.DocType = 'G'
--		if @@Error <> 0
--		begin
--			set @Update_Error = -1
--			SELECT @ErrorDesc = 'Error Actualizando campo Remanente en SntConsigDetHst(TMP), Devolucion No.' + @DEVONUMB 
--			-- FFB 2007-05-02 PRT ERROR
--			PRINT 'Error Act campo Remanente en SntConsigDetHst(TMP), Dev No.' + @DEVONUMB 
--		END
--	END
--END

if @Update_Error = 0 and @CrearContable = 1
BEGIN
	-- Crear Asiento Contable
	DECLARE @ValorRet int, 
		@NJRNLENT int, 
		@Ok tinyint, 
		@ErrorNum int 
	
	set @NJRNLENT = 0
	
--	EXEC @ValorRet = glGetNextJournalEntry 1, 1, @NJRNLENT OUT, @Ok OUT, @ErrorNum OUT 
--
--	if @ValorRet <> 0 or @Ok  <> 1 
--	BEGIN
--		SET @Update_Error = @ErrorNum
--		SELECT @ErrorDesc = 'Error Actualizando Numero de Diario, Devolucion no. ' + @DEVONUMB 
--		-- FFB 2007-05-02 PRT ERROR
--	END
--	ELSE
--	BEGIN
--		select @CURRNIDX = FUNCRIDX from MC40000 where FUNLCURR = @Moneda
-- FFB 2008-01-02 CAMBIO A TABLAS DE TRABAJO GP
--		SELECT @SumAmt = sum(EXTDCOST) FROM IV30300 a WHERE DOCTYPE = 3 AND DOCNUMBR = @TXTRDNUM
-- FFB 2007-06-20
--
-- se reemplaza el calculo original por diferencias que estaba dando
--  GL vs IV
--
		if not exists( select 1 from SY00500 where BACHNUMB = @BACHNUMB 
			 AND GLPOSTDT = '19000101' AND SERIES = 5) -- FFB 2008-01-02 NEW COND
		BEGIN
			-- Agregar Lote ID
			INSERT SY00500 ( 
			  BCHSOURC  , BACHNUMB  , SERIES    , NUMOFTRX  , BACHFREQ  , BCHCOMNT  ,
			  BRKDNALL  , BCHTOTAL  , MODIFDT               , CREATDDT              ,
			  ORIGIN    , POSTTOGL )
			VALUES(
			  'IV_Trans', @BACHNUMB , 5	    , 1         , 1	    , 'Devolucion de consignacion',
			  0         , @SumAmt   , @FechaContable        , @FechaContable        ,
 			  '2'       , 1        )
--			VALUES( 'GL_Normal', @TRXSORCE, 2, 1, 1, 'Consignaciones Devolucion', 1, @SumAmt, @FechaContable, @FechaContable,'1' )
--
			IF @@Error <> 0 
			BEGIN
				set @Update_Error = -1
				SELECT @ErrorDesc = 'Error Agregando Lote - SY00500, Devolucion no. ' + @DEVONUMB 
				-- FFB 2007-05-02 PRT ERROR
				--PRINT 'Error Agregando Lote - SY00500, Devolucion no. ' + @DEVONUMB 
			END
		END
		ELSE
		BEGIN
			UPDATE SY00500 SET BCHTOTAL = BCHTOTAL + @SumAmt where BACHNUMB = @BACHNUMB
			  AND GLPOSTDT = '19000101' AND SERIES = 5 -- FFB 2008-01-02 NEW COND
			IF @@Error <> 0 
			BEGIN
				set @Update_Error = -1
				SELECT @ErrorDesc = 'Error Actualizando Lote - SY00500, Devolucion no. ' + @DEVONUMB 
				-- FFB 2007-05-02 PRT ERROR
				--PRINT 'Error Actualizando Lote - SY00500, Devolucion no. ' + @DEVONUMB 
			END
		END

-- FFB 2008-01-02 CAMBIO A TABLAS DE TRABAJO GP
--
		-- Encabezado de Asiento Contable
--		if @Update_Error = 0
--		BEGIN
--			INSERT GL10000 ( BACHNUMB, BCHSOURC, JRNENTRY, RCTRXSEQ, SOURCDOC, REFRENCE, TRXDATE, RCRNGTRX, BALFRCLC, PSTGSTUS, USWHPSTD, TRXTYPE, SQNCLINE, SERIES, ORPSTDDT, ORTRXSRC, OrigDTASeries, DTAControlNum, DTATRXType, CURNCYID, CURRNIDX, XCHGRATE, EXCHDATE, PRNTSTUS)
--			VALUES( @TRXSORCE, 'GL_Normal', @NJRNLENT, 0, 'IVTFR', 'Consignaciones Devolucion', @FechaContable, 0, 0, 1, SYSTEM_USER, 0, 16384, 5, @FechaContable, @TRXSORCE, 0, @TXTRDNUM, 3, @Moneda, @CURRNIDX, 0, '1900.1.1', 1)
--			IF @@Error <> 0 
--			BEGIN
--				set @Update_Error = -1
--				SELECT @ErrorDesc = 'Error Actualizando Encabezado Asiento - GL10000, Devolucion no. ' + @DEVONUMB 
--				-- FFB 2007-05-02 PRT ERROR
--				--PRINT 'Error Actualizando Encabezado Asiento - GL10000, Devolucion no. ' + @DEVONUMB 
--			END
--		END

--		if @Update_Error = 0
--		BEGIN
--
--			DECLARE DetContable CURSOR FOR 
--			SELECT IVIVINDX, ACCTTYPE, DECPLACS, sum(EXTDCOST) 'CrdtAmt' FROM IV30300 a, GL00100 b 
--			 WHERE DOCTYPE = 3 AND DOCNUMBR = @TXTRDNUM
--			 AND a.IVIVINDX = b.ACTINDX
--			 GROUP BY IVIVINDX, ACCTTYPE, DECPLACS
--
-- JLM 2007-05-02 ASIENTO CONT		
--			SET @SQNCLINE = 0		
--			OPEN DetContable
--			FETCH NEXT FROM DetContable into @ActIndx, @AcctType, @DECPLACS, @CreditAmt
--			WHILE @@FETCH_STATUS = 0
--			BEGIN	
--				IF @Update_Error <> 0 
--					BREAK
--				SET @SQNCLINE = @SQNCLINE + 16384
--
--				select @DSCRIPTN = substring(ACTDESCR, 1, 31) from gl00100 where ActIndx = @ActIndx
--
--				INSERT GL10001 (BACHNUMB, JRNENTRY, SQNCLINE, ACTINDX, XCHGRATE, DSCRIPTN, CURRNIDX, ACCTTYPE, DECPLACS, ORCTRNUM, ORDOCNUM, ORTRXTYP, DTA_GL_Status, INTERID, CRDTAMNT, DEBITAMT, ORCRDAMT, ORDBTAMT, LNESTAT)
--				VALUES( @TRXSORCE, @NJRNLENT, @SQNCLINE, @ActIndx, 0, @DSCRIPTN, @CURRNIDX, @ACCTTYPE, @DECPLACS, @TXTRDNUM, @TXTRDNUM, 3, 40, DB_NAME(), 0, @CreditAmt, 0, @CreditAmt, 5)
--				IF @@Error <> 0 
--				BEGIN
--					set @Update_Error = -1
--					SELECT @ErrorDesc = 'Error Actualizando Detalle Asiento - GL10000, Devolucion no. ' + @DEVONUMB 
--					-- FFB 2007-05-02 PRT ERROR
--					--PRINT 'Error Actualizando Detalle Asiento - GL10000, Devolucion no. ' + @DEVONUMB 
--				END
--	
--				FETCH NEXT FROM DetContable into @ActIndx, @AcctType, @DECPLACS, @CreditAmt
--			END
--			CLOSE DetContable
--			DEALLOCATE DetContable
--				
--			IF @Update_Error = 0 
--			BEGIN
--				SET @SQNCLINE = @SQNCLINE + 16384
--	
--				INSERT GL10001 (BACHNUMB, JRNENTRY, SQNCLINE, ACTINDX, XCHGRATE, DSCRIPTN, CURRNIDX, ACCTTYPE, DECPLACS, ORCTRNUM, ORDOCNUM, ORTRXTYP, DTA_GL_Status, INTERID, CRDTAMNT, DEBITAMT, ORCRDAMT, ORDBTAMT, LNESTAT)
--				VALUES( @TRXSORCE, @NJRNLENT, @SQNCLINE, @IndxDebito, 0, @DSCRIPTNDebito, @CURRNIDX, @DebACCTTYPE, @DebDECPLACS, @TXTRDNUM, @TXTRDNUM, 3, 40, DB_NAME(), @SumAmt, 0, @SumAmt, 0, 5)
--				IF @@Error <> 0 
--				BEGIN
--					set @Update_Error = -1
--					SELECT @ErrorDesc = 'Error Actualizando Detalle Asiento - GL10000, Devolucion no. ' + @DEVONUMB 
--					-- FFB 2007-05-02 PRT ERROR
--					--PRINT 'Error Actualizando Detalle Asiento - GL10000, Devolucion no. ' + @DEVONUMB 
--				END
--			END
--		END
--
--		SELECT @total_GL = SUM(CRDTAMNT) FROM GL10001 WHERE ORCTRNUM = @TXTRDNUM
--		SELECT @total_IV = SUM(EXTDCOST) FROM IV30300 WHERE DOCNUMBR = @TXTRDNUM
--		AND ITEMNMBR NOT IN ( SELECT ITEMNMBR FROM IV00101 WHERE ITEMTYPE = 3 )				
--		if @total_GL <> @total_IV
--		BEGIN
--			set @Update_Error = -1
--			SELECT @ErrorDesc = 'Error el valor de  Detalle del Asiento - GL10001, no coincide con el de inventario Consignacion no. ' + @DEVONUMB 
--			-- FFB 2007-05-02
--			--PRINT 'Error el valor Det Asiento - GL10001, no coincide con IV DEVOL no. ' + @DEVONUMB 
--		END
--	END
END
--
-- FFB 2008-01-02 CAMBIO A TABLAS DE TRABAJO GP
-- DROP TABLE #DetalleContable
drop table #SntConsigDetHst

--JLMEJIASM-I 06-03-2007
DELETE SNTCONSIGDETHST 
WHERE   QUANTITY_SHP = 0
and DOCTYPE = 'D'
--JLMEJIASM-f 06-03-2007

IF @Update_Error = 0
BEGIN
	COMMIT TRANSACTION DevolverConsignacion
	--PRINT 'HIZO COMMIT DE TRX'
	-- Transaccion Correcta
	RETURN( 0)
END
ELSE
BEGIN
	ROLLBACK TRANSACTION DevolverConsignacion
	--PRINT 'ERROR EN ** COMMIT DE TRX **'
	-- Error de Transaccion
	RETURN( 1)
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[Snt_LiquidarConsignacion]    Script Date: 01/13/2009 17:57:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/****** Object:  StoredProcedure [dbo].[Snt_LiquidarConsignacion]    Script Date: 01/11/2009 20:24:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_LiquidarConsignacion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_LiquidarConsignacion]
GO
CREATE                    PROCEDURE [dbo].[Snt_LiquidarConsignacion]
	@NOFACTURA char( 21),
	@ErrorDesc char( 255) OUTPUT,
	@ActualPrecio smallint = 0,
	@DstoLineaNegocio decimal( 5, 2) = 0
-- Modificacion para descuento por linea de Negocios
--***************************************************************
--* L . O . G               DE MODIFICACIONES                   *
--***************************************************************
--*   MARCA        /        DESCRIPCION DE LA MODIFICACION      *
--***************************************************************
--*JLMEJIASM-I 17-05-2006   SE HA CAMBIADO LA FECHA  
--*JLMEJIASM-F 17-05-2006   @FECHAHOY DEL VALOR DEL DIA ACTUAL 
--*      		    POR EL VALOR DEL PERIODO FISCAL
--*			    PROXIMO QUE ESTE AVIERTO.
--*			    DODE LA SERIE SEA SERIES   = '3'
--* 			    EQUIVALENTE A VENTAS Y
--* 			    ODESCTN  = 'Sales Transaction Entry'
--*			    ADEMAS QUE EL PERIODO ESTE ABIERTO.=0	
--*JHERNANDEZT 14-05-2006   MODIFICACIONES PARA TRABAJAR CON KITS
--*JLMEJIASM-I 30-01-2007   ESTA MODIFICACION ES PARA VALIDAR 
--*			    QUE EL VALOR DE LOS REMANENTES NO SEA MENOR 
--*JLMEJIASM-I 30-01-2007   A CERO O NEGATIVO.
--*JLMEJIASM-I 28-02-2007   SE HA INCLUIDO EL CAMPO TIPO EN EL 
--*JLMEJIASM-F 28-02-2007   INSERT A LA TABLA SNTCONSIGDETHST
--FFB 2007-03-27            se han hecho modificaciones para guardar reg
--FFB 2007-03-27            en la tabla sop10105 para que las facturas
--*			    de consignacion se reflejen en el libro de compras	
--*
--*JLMEJIASM-I 07-05-2007   ESTA MODIFICACION ES PARA ADAPTAR A ESTE PROCEDIMIENTO
--*JLMEJIASM-F 07-05-2007   AL G.P 9.
--***************************************************************

AS 

DECLARE @FIRMAYSELLO INT
Declare @Update_Error int,
	@NoAplica char( 21),
	@ItemnMbr char( 31),
	@ItemDesc char( 101), -- FFB 2007-10-16 ampliar campo 51 a 101
	-- FBB 2007-10-16 NUEVAS VARIABLES PARA PROCESO 
	--  DE CALCULO DE REMANENTE PARA HIJOS DE KITS
	@KIT_ITEM  char( 31),
	@KIT_APLICA char( 21),
	@KIT_QTYSHP decimal( 19, 5),
	-- FBB 2007-10-16 END
	@PRICELVL char( 11),
	@UOFM char(9), 
	@UNITPRCE decimal( 19, 5), 
	@UNITCOST decimal( 19, 5), 
	@DESCUENTO decimal( 5, 2), 
	@IMPUESTO decimal( 19, 5),
	-- FFB 2007-08-24 CAMBIO @subtotal decimal( 19, 2) -> ( 19, 5)
	@SUBTOTAL decimal( 19, 5),
	-- FFB 2007-08-24 CAMBIO @DOCAMNT decimal( 19, 2) -> ( 19, 5)
	@DOCAMNT decimal( 19, 5),
	-- FFB 2007-08-24 CAMBIO @TAXAMNT decimal( 19, 2) -> ( 19, 5)  
	@TAXAMNT decimal( 19, 5), 
	@CUSTNMBR char( 15), 
	@CNTCPRSN char( 65), 
	@CONTACT char( 31), 
	@ADRSCODE char( 15), 
	@ADDRESS1 char( 31), 
	@ADDRESS2 char( 31), 
	@CITY char( 31), 
	@STATE char( 29), 
	@ZIPCODE char(11), 
	@COUNTRY char(21), 
	@PHONE1 char(21), 
	@PHONE2 char(21), 
	@FAX char(21), 
	@CSTPONBR char(21), 
	@SLPRSNID char(15), 
	@DOCDATE datetime, 
	@LOCNCODE char( 11), 
	@PRCLEVEL char( 11), 
	@TaxSchId char( 15), 
	-- FFB 2007-08-24 CAMBIO @TRDISAMT decimal( 19, 2) -> ( 19, 5)
	@TRDISAMT decimal( 19, 5),
	-- FFB 2007-08-24 CAMBIO @DESCPRC decimal( 19, 2) -> ( 19, 5) 
	@DESCPRC decimal( 19, 5), 
	-- FFB 2007-08-24 CAMBIO @Saldo decimal( 19, 2) -> ( 19, 5)
	@Saldo decimal( 19, 5), 
	-- FFB 2007-08-24 CAMBIO @Facturar decimal( 19, 2) -> ( 19, 5)
	@Facturar decimal( 19, 5),
	@FECHAHOY datetime,
	@DUEDTDS datetime,
	@PYMTRMID char(21),
	@MSTRNUMB int,
	@PRBTADCD char(15), 
	@PRSTADCD char( 15),
	@SHIPMTHD char( 15),
	@Moneda char( 15),
	@CURRNIDX smallint,
	@LocnConsig char( 11),
	@Existencia numeric( 19, 5),
	@REGISTROS INT, --2006-02-21 FFB
	@TIPO2 VARCHAR(15), -- JLMEJIASM-I 28-02-2007
	-- FFB 2007-03-27
	-- agregar info del impuesto
	@TAXDTLID char(15),
        @IVA_ACTINDX INT,
        @IVACNTREG  INT,
	-- FFB 2007-08-24 CAMBIO @TOT_STAXAMNT decimal( 19, 2) -> ( 19, 5)
	@TOT_STAXAMNT numeric( 19, 5),
	-- FFB 2007-08-24 CAMBIO @TOT_TAXDTSLS decimal( 19, 2) -> ( 19, 5)
    	@TOT_TAXDTSLS numeric( 19, 5)
	-- FFB 2007-03-27
    	-- FIN


-- Colocar variables de Manejo de error a Cero
SET @Update_Error = 0
SET @ErrorDesc = ''
SET @FECHAHOY = convert( datetime, convert( char(8), getdate(), 112), 112)
--*JLMEJIASM-I 25-05-2006
--********************************************
--*           10000-inicio 
--*este parrafo se encarga de obtener la fecha
--* del proximo periodo fiscal abierto
--* FECHA FIN DE PERIODO fiscal abierto @FECHAHOYICIERR0
--* FECHA INI DE PERIODO fiscla abierto @FECHAHOYICIERR0
--* FECHA DEL PROXIMO periodo abierto CIERRE @FECHAHOYPROXCIER 
--* SI LA @FECHAHOY esta entre  ENTRE LOS VALORES DEL MES CERRADO
--* SE LE ASIGNARA LA FECHA DEL PROXIMO periodo fiscal abierto
--* DE LO CONTRARIO SE QUEDARA CON EL MISMO VALOR
--* mes cerrado CLOSED   = '1'
--* mes abierto CLOSED   = '0'
--********************************************
declare @FECHAHOYICIERR0  datetime
declare @PERIODID1        char
declare @FECHAHOYPROXCIER datetime
declare @PERIODID4        char
declare @FECHAHOYFCIERR0  datetime
--@@@ TIPO DE ITEM
DECLARE @TIPO VARCHAR(10)
--@@@ CONTADOR PARA ELEMENTOS DE ITEM EN SOP10200
DECLARE @SEQ_ELEMENT NUMERIC
SET ANSI_WARNINGS OFF

	select 	@FECHAHOYICIERR0 = (PERIODDT),
		@PERIODID1       = (PERIODID),
		@FECHAHOYFCIERR0 = (PERDENDT) 
	from SY40100
	where SERIES   = '3'
	and   CLOSED   = '0'
	and   PERIODID <> '0'
	and   ODESCTN  = 'Sales Transaction Entry'


	select @FECHAHOYPROXCIER = @FECHAHOYICIERR0

	IF (@FECHAHOY >  @FECHAHOYICIERR0) AND  (@FECHAHOY < @FECHAHOYFCIERR0)
 	BEGIN
      		SET @FECHAHOY = convert( datetime, convert( char(8), getdate(), 112), 112)
 	END
	ELSE 
  	BEGIN
		IF  @FECHAHOYPROXCIER IS NOT NULL

		BEGIN
			SELECT @FECHAHOY = @FECHAHOYPROXCIER
 		END
	END

--*JLMEJIASM-I 25-05-2006

SELECT TOP 1 @Moneda = isnull( Moneda, 0) from SntConsigCfg

BEGIN TRANSACTION LiquidarConsignacion

-- Crear Tabla temporal con select INTO
SELECT Identity(int,1,1) AS ROWID,*,null as bol_elekit INTO #SntConsigDetHst from SntConsigDetHst where 1=2

DECLARE LiquidacionDet CURSOR FOR 
select NoAplica, ITEMNMBR, ITEMDESC, Saldo, Liquidar from #encabezado where Liquidar > 0

-- 2008-06-03
--print 'ver tabla: #encabezado'
--select NoAplica, ITEMNMBR, ITEMDESC, Saldo, Liquidar from #encabezado where Liquidar > 0

OPEN LiquidacionDet
FETCH NEXT FROM LiquidacionDet into @NoAplica, @ITEMNMBR, @ITEMDESC, @Saldo, @Facturar
WHILE @@FETCH_STATUS = 0
BEGIN
	--@@@INICIO MODIFICACI�N JEAN 15-09-2006
	--DETERMINAR EL TIPO DE ITEM QUE SE LIQUIDA
	IF EXISTS(SELECT TOP 1 ITEMNMBR FROM IV00104 WHERE ITEMNMBR=@ItemnMbr)
		SET @TIPO='KIT'
	ELSE
		SET @TIPO='ITEM'
	--FINAL MODIFICACION

	-- Buscar datos de consignacion Detalle Origen
	select @UOFM = UOFM, @UNITPRCE = UNITPRCE, @UNITCOST = UNITCOST, @LOCNCODE = LOCNCODE, @PRICELVL = PRICELVL, @DESCUENTO = @DstoLineaNegocio, @IMPUESTO = IMPUESTO
          from SntConsigDetHst where itemnmbr = @itemnmbr and NOCONSIG = @NoAplica and DOCTYPE = 'G'
	--
	-- FFB 2008-05-08
	SET @UNITCOST = 0
	SELECT @UNITCOST = ISNULL( CURRCOST, 0 ) FROM IV00101 where itemnmbr = @itemnmbr
	-- FFB 2008-05-08 END
	--
	-- Precio Nuevo
	if @ActualPrecio = 1
	BEGIN
		SELECT @UNITPRCE = LISTPRCE FROM IV00105 WHERE ITEMNMBR = @ITEMNMBR and CURNCYID = @Moneda
		if @UNITPRCE is null
			set @UNITPRCE = 0
	END
--*JLMEJIASM-I 28-02-2007 
--	INSERT #SntConsigDetHst (NOCONSIG, NoAplica, DOCTYPE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO)
--	VALUES( @NOFACTURA, @NoAplica, 'F', @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @UNITPRCE * @Facturar, 0, @Facturar, 0, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO)
--
--
	INSERT #SntConsigDetHst (NOCONSIG, NoAplica, DOCTYPE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO,TIPO)
	VALUES( @NOFACTURA, @NoAplica, 'F', @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @UNITPRCE * @Facturar, 0, @Facturar, 0, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO,@TIPO )
--*JLMEJIASM-I 28-02-2007

	--@@@SI ES KIT TAMBIEN TOMA EN CUENTA LOS ITEMS DEL KIT
	--
	-- 2007-10-16 FBB modificaci�n variable @TIPO por 'HIJO DE '+@ITEMNMBR 
	--
	-- 2008-06-03 FFB se reemplaza C.UOFM por A.UOMSCHDL
	IF @TIPO='KIT' BEGIN
		INSERT #SntConsigDetHst (NOCONSIG, NoAplica, DOCTYPE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, bol_elekit,tipo)
		SELECT DISTINCT @NOFACTURA, @NoAplica, 'F', CMPTITNM, A.ITEMDESC, A.UOMSCHDL, 0, A.CURRCOST, 0, 0, @Facturar, 0, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO, 1 , 'HIJO DE '+@ITEMNMBR 
		FROM IV00104 
			inner join IV00101 AS A ON A.ITEMNMBR=IV00104.CMPTITNM 
			INNER JOIN IV00102 AS B ON B.ITEMNMBR=CMPTITNM 
			INNER JOIN SntConsigDetHst AS C ON C.ITEMNMBR=IV00104.CMPTITNM
		WHERE IV00104.ITEMNMBR = @ITEMNMBR AND C.NOAPLICA=@NoAplica
--
-- ffb 2008-06-03
--print 'ver registros insertados #SntConsigDetHst'
--select * from #SntConsigDetHst

	END
	--FIN MOVIMIENTO KIT

	IF @@Error <> 0 
	BEGIN
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Actualizando SntConsigDetHst, Consignacion no. ' + @NoAplica + ', Item: ' + @ITEMNMBR
		BREAK
	END

	FETCH NEXT FROM LiquidacionDet into @NoAplica, @ITEMNMBR, @ITEMDESC, @Saldo, @Facturar
END

CLOSE LiquidacionDet
DEALLOCATE LiquidacionDet

UPDATE #SntConsigDetHst SET SEQUENCE=ROWID

-- Definir Fecha de Documento

-- Buscar Total Liquidacion
--@@@INICIO MODIFICACION JHERNANDEZ 15-09-2006
--PARA EL CALCULO DE MONTOS NO SE TOMAN EN CUENTA LOS ELEMENTOS DEL KIT
IF @TIPO='KIT' BEGIN
	SELECT @SUBTOTAL = sum( UNITPRCE * QUANTITY_SHP), @TAXAMNT = sum( UNITPRCE * QUANTITY_SHP * IMPUESTO/100), @TRDISAMT = sum(UNITPRCE * QUANTITY_SHP * DESCUENTO/100),
	@DOCAMNT = sum( ((UNITPRCE * QUANTITY_SHP) - (UNITPRCE * QUANTITY_SHP * DESCUENTO/100)) + (UNITPRCE * QUANTITY_SHP * IMPUESTO/100))
	from #SntConsigDetHst WHERE bol_elekit<>1
END ELSE BEGIN
	SELECT @SUBTOTAL = sum( UNITPRCE * QUANTITY_SHP), @TAXAMNT = sum( UNITPRCE * QUANTITY_SHP * IMPUESTO/100), @TRDISAMT = sum(UNITPRCE * QUANTITY_SHP * DESCUENTO/100),
	@DOCAMNT = sum( ((UNITPRCE * QUANTITY_SHP) - (UNITPRCE * QUANTITY_SHP * DESCUENTO/100)) + (UNITPRCE * QUANTITY_SHP * IMPUESTO/100))
	from #SntConsigDetHst
END
--FIN MODIFICACION

DECLARE LiquidacionHdr CURSOR FOR 
select distinct NoAplica from #encabezado where Liquidar > 0 --son mismos elementos que estan en el grid

OPEN LiquidacionHdr
FETCH NEXT FROM LiquidacionHdr into @NoAplica
WHILE @@FETCH_STATUS = 0
BEGIN

	-- Buscar datos de Consignacion Encabezado Origen
	SELECT @CUSTNMBR = CUSTNMBR, @CNTCPRSN = ISNULL(CNTCPRSN,0), @CONTACT = CONTACT, @ADRSCODE = ADRSCODE, 
	@ADDRESS1 = ADDRESS1, @ADDRESS2 = ADDRESS2, @CITY = CITY, @STATE = STATE, @ZIPCODE = ZIPCODE, 
	@COUNTRY = isnull( COUNTRY, ''), @PHONE1 = PHONE1, @PHONE2 = PHONE2, @FAX = isnull( FAX, ''), @CSTPONBR = CSTPONBR, 
	@SLPRSNID = SLPRSNID, @DOCDATE = DOCDATE, @LOCNCODE = LOCNCODE, @PRCLEVEL = PRCLEVEL, 
	@TaxSchId = TaxSchId, @DESCPRC = DESCPRC, @LocnConsig = LocnConsig, @Moneda = Moneda,
	@FIRMAYSELLO = FIRMAYSELLO
	 from SntConsigHdrHst where NOCONSIG = @NoAplica and DOCTYPE = 'G'

    -- 2006-02-21 FFB BEGIN PARA EVITAR QUE SE INSERTEN REGISTROS DUPLICADOS
    SELECT @REGISTROS = COUNT(*) FROM SntConsigHdrHst
      WHERE NOCONSIG = @NOFACTURA AND NOAPLICA = @NoAplica AND DOCTYPE = 'F'
      
    IF @REGISTROS = 0
	BEGIN
	  INSERT SntConsigHdrHst (NOCONSIG, FIRMAYSELLO, NOAPLICA, DOCTYPE, CUSTNMBR, CNTCPRSN, CONTACT, ADRSCODE, ADDRESS1, ADDRESS2, CITY, STATE, ZIPCODE, COUNTRY, PHONE1, PHONE2, FAX, CSTPONBR, SLPRSNID, DOCDATE, LOCNCODE, PRCLEVEL, TaxSchId, SUBTOTAL, TRDISAMT, DESCPRC, FRTAMNT, MISCAMNT, TAXAMNT, DOCAMNT, USUERACT, FECHAACT, LocnConsig, Moneda)
	  VALUES( @NOFACTURA, @FIRMAYSELLO, @NoAplica, 'F', @CUSTNMBR, @CNTCPRSN, @CONTACT, @ADRSCODE, @ADDRESS1, @ADDRESS2, @CITY, @STATE, @ZIPCODE, @COUNTRY, @PHONE1, @PHONE2, @FAX, @CSTPONBR, @SLPRSNID, @FECHAHOY, @LOCNCODE, @PRCLEVEL, @TaxSchId, @SUBTOTAL, @TRDISAMT, @DESCPRC, 0, 0, @TAXAMNT, @DOCAMNT, SYSTEM_USER, getdate(), @LocnConsig, @Moneda)
	END
	-- 2006-02-21 FFB END

	IF @@Error <> 0 
	BEGIN
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Actualizando SntConsigHdrHst, Liquidacion no. ' + @NOFACTURA + ', Consignacion : ' + @NoAplica 
		BREAK

	END

	FETCH NEXT FROM LiquidacionHdr into @NoAplica
END

CLOSE LiquidacionHdr
DEALLOCATE LiquidacionHdr

-- Actualizar Detalle desde temporal
IF @Update_Error = 0 
BEGIN
--*JLMEJIASM-I 30-01-2007  
	DECLARE @REMANENEGTIVO NUMERIC (19),
	@ITEMNMBRNEGTVO char( 31), 
	@NOCONSIGNEGTVO char( 31),
	@NoAplicaNEGTVO char( 31)

	SET @REMANENEGTIVO = 0
	SET @NOCONSIGNEGTVO = ''
	SET @ITEMNMBRNEGTVO = ''
	SELECT  @ITEMNMBRNEGTVO=ITEMNMBR,@NOCONSIGNEGTVO=NOCONSIG,@REMANENEGTIVO = REMANENTE FROM #SntConsigDetHst WHERE REMANENTE < 0 


	IF @REMANENEGTIVO < 0 
	BEGIN 
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Actualizando campo Remanente en SntConsigDetHst(REMANE NEGATIVO1), Liquidacion No.' + @NOFACTURA + 'NOCONSIG' + @NOCONSIGNEGTVO + 'ITEM' + @ITEMNMBRNEGTVO
	END
--*JLMEJIASM-F 30-01-2007	

--*JLMEJIASM-I 28-02-2007 
--	INSERT SntConsigDetHst (NOCONSIG, NoAplica, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO) 
--	SELECT NOCONSIG, NoAplica, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO from #SntConsigDetHst order by sequence
	


	INSERT SntConsigDetHst (NOCONSIG, NoAplica, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO,TIPO) 
	SELECT NOCONSIG, NoAplica, DOCTYPE, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO,TIPO from #SntConsigDetHst order by sequence
--*JLMEJIASM-F 28-02-2007

	IF @@Error <> 0 
	BEGIN
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Agregando lineas en el SntConsigDetHst(TMP), Liquidacion No.' + @NOFACTURA 
	END
END

-- ************************************************
--
-- Actualizar Remanente de La Consignacion Original
--
-- ************************************************
IF @Update_Error = 0 
BEGIN
--*JLMEJIASM-I 30-01-2007  
	SET @REMANENEGTIVO = 0
	SET @NOCONSIGNEGTVO = ''
	SET @ITEMNMBRNEGTVO = ''
--
-- FFB 2007-10-18 CAMBIO INSTRUCCION PARA UTILIZAR VIEW
--
--	SELECT 	@NOCONSIGNEGTVO = #SntConsigDetHst.NoAplica,
--	 	@ITEMNMBRNEGTVO = #SntConsigDetHst.itemnMbr,
--	  	@REMANENEGTIVO  = SntConsigDetHst.REMANENTE - #SntConsigDetHst.QUANTITY_SHP 
--	FROM SntConsigDetHst inner join #SntConsigDetHst ON SntConsigDetHst.NoConsig = #SntConsigDetHst.NoAplica 
--		and SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr
--		and SntConsigDetHst.DocType = 'G'
--
	SELECT 	@NOCONSIGNEGTVO = #SntConsigDetHst.NoAplica,
	 	@ITEMNMBRNEGTVO = #SntConsigDetHst.itemnMbr,
	  	@REMANENEGTIVO  = snt_liquidar_vw.saldo
	 FROM snt_liquidar_vw, #SntConsigDetHst, SntConsigDetHst
	 where sntconsigdethst.noconsig = snt_liquidar_vw.NOAPLICA
	   AND sntconsigdethst.noconsig = #SntConsigDetHst.NoAplica -- TABLA TEMP.
	   AND sntconsigdethst.itemnmbr = snt_liquidar_vw.itemnmbr
	   AND SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr -- TABLA TEMP.
	   AND sntconsigdethst.DOCTYPE = 'G'
--
--
	IF @REMANENEGTIVO < 0
	BEGIN
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Actualizando campo Remanente en SntConsigDetHst(REMANE NEGATIVO2), Liquidacion No.' + @NOFACTURA +  'NOCONSIG'  + @NOCONSIGNEGTVO
	END
-- 
-- FBB 2007-10-19 SE ELIMINA LA INSTRUCCION PARA ACT. REMANENTE
-- FBB 2007-10-18 ACTUALIZAR REMANENTE CON NUEVO QUERY
--  DESDE VIEW SNT_LIQUIDAR_VW
--	UPDATE sntconsigdethst
--	 set remanente = snt_liquidar_vw.saldo
--	 FROM snt_liquidar_vw, #SntConsigDetHst
--	 where sntconsigdethst.noconsig = snt_liquidar_vw.NOAPLICA
--	   AND sntconsigdethst.noconsig = #SntConsigDetHst.NoAplica -- TABLA TEMP.
--	   AND sntconsigdethst.itemnmbr = snt_liquidar_vw.itemnmbr
--	   AND SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr -- TABLA TEMP.
--	   AND sntconsigdethst.DOCTYPE = 'G'
--	   AND snt_liquidar_vw.custnmbr = 'J-30750252-5'
--	   and sntconsigdethst.remanente <> snt_liquidar_vw.SALDO
--
--
--** JLMEJIASM-F 30-01-2007	
-- 
-- FBB 2007-10-18 SE REEMPLAZAN ESTOS UPDATES POR EL
--  QUERY INMEDIATO ANTERIOR ( DE ARRIBA )
--
--	UPDATE SntConsigDetHst SET REMANENTE = SntConsigDetHst.REMANENTE - #SntConsigDetHst.QUANTITY_SHP 
--	FROM SntConsigDetHst inner join #SntConsigDetHst ON SntConsigDetHst.NoConsig = #SntConsigDetHst.NoAplica 
--		and SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr
--		and SntConsigDetHst.DocType = 'G'
--		and SntConsigDetHst.TIPO IN ('ITEM', 'KIT') -- 2007-10-16 FBB inclusion de campo TIPO
--		and SntConsigDetHst.REMANENTE >= #SntConsigDetHst.QUANTITY_SHP -- 2007-10-16 FBB solo con esta condicion no dara 0
--
-- 2007-10-17 FBB se anexa esta segunda condicion para incluir los casos 
--  donde el remanente sea menor a lo que se liquida, aunque no deberia pasar, pasa =P
--	UPDATE SntConsigDetHst SET REMANENTE = 0 -- 
--	FROM SntConsigDetHst inner join #SntConsigDetHst ON SntConsigDetHst.NoConsig = #SntConsigDetHst.NoAplica 
--		and SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr
--		and SntConsigDetHst.DocType = 'G'
--		and SntConsigDetHst.TIPO IN ('ITEM', 'KIT') -- 2007-10-16 FBB inclusion de campo TIPO
--		and SntConsigDetHst.REMANENTE < #SntConsigDetHst.QUANTITY_SHP -- 2007-10-16 FBB solo con esta condicion no dara 0
--
--
-- 2007-10-16 FBB hacer fetch de kits para recorrer la liquidacion
--  y actualizar sus hijos en funcion del padre
--
	DECLARE Saldo_kitAplic CURSOR FOR
	SELECT ITEMNMBR, NoAplica, QUANTITY_SHP FROM #SntConsigDetHst WHERE
	 ITEMNMBR IN (select DISTINCT ITEMNMBR from iv00104)
--
	OPEN Saldo_kitAplic 
	FETCH NEXT FROM Saldo_kitAplic INTO @KIT_ITEM, @KIT_APLICA, @KIT_QTYSHP
--
	WHILE @@FETCH_STATUS=0 BEGIN -- WHILE 1
--
-- AQUI SOLO ACTUALIZAMOS LOS HIJOS DEL KIT
--
-- ****************************************************
-- FBB 2007-10-19 SE ELIMINA LA INSTRUCCION PARA ACT. REMANENTE
-- FBB 2007-10-18 ACTUALIZAR REMANENTE CON NUEVO QUERY
--  DESDE VIEW snt_saldoHkits_vw
--	UPDATE sntconsigdethst
--	 set remanente = snt_saldoHkits_vw.saldo
--	 FROM snt_saldoHkits_vw, #SntConsigDetHst
--	 where sntconsigdethst.noconsig = snt_saldoHkits_vw.NOAPLICA
--	   AND sntconsigdethst.noconsig = #SntConsigDetHst.NoAplica -- TABLA TEMP.
--	   AND sntconsigdethst.itemnmbr = snt_saldoHkits_vw.itemnmbr
--	   AND SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr -- TABLA TEMP.
--	   AND sntconsigdethst.DOCTYPE = 'G'
-- 	   AND SntConsigDetHst.TIPO = 'HIJO DE '+@KIT_ITEM
--
--
-- 
-- 	  UPDATE SntConsigDetHst SET REMANENTE = SntConsigDetHst.REMANENTE - #SntConsigDetHst.QUANTITY_SHP 
--	  FROM SntConsigDetHst inner join #SntConsigDetHst ON SntConsigDetHst.NoConsig = #SntConsigDetHst.NoAplica 
--		and SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr
--		and SntConsigDetHst.DocType = 'G'
--		and SntConsigDetHst.TIPO = 'HIJO DE '+@KIT_ITEM -- 2007-10-16 FBB inclusion de campo TIPO
--		and SntConsigDetHst.REMANENTE >= #SntConsigDetHst.QUANTITY_SHP -- 2007-10-16 FBB solo con esta condicion no dara 0
--
-- 2007-10-17 FBB se anexa esta segunda condicion para incluir los casos 
--  donde el remanente sea menor a lo que se liquida, aunque no deberia pasar, pasa =P
-- 	  UPDATE SntConsigDetHst SET REMANENTE = 0 -- 
--	  FROM SntConsigDetHst inner join #SntConsigDetHst ON SntConsigDetHst.NoConsig = #SntConsigDetHst.NoAplica 
--		and SntConsigDetHst.itemnMbr = #SntConsigDetHst.itemnMbr
--		and SntConsigDetHst.DocType = 'G'
--		and SntConsigDetHst.TIPO = 'HIJO DE '+@KIT_ITEM -- 2007-10-16 FBB inclusion de campo TIPO
--		and SntConsigDetHst.REMANENTE < #SntConsigDetHst.QUANTITY_SHP -- 2007-10-16 FBB solo con esta condicion no dara 0
--
--
	  FETCH NEXT FROM Saldo_kitAplic INTO @KIT_ITEM, @KIT_APLICA, @KIT_QTYSHP
	END -- WHILE 1

	CLOSE Saldo_kitAplic 
	DEALLOCATE Saldo_kitAplic 
--
-- 2007-10-16 FBB - END fetch de kits
--
--
	IF @@Error <> 0 
	BEGIN
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Actualizando campo Remanente en SntConsigDetHst(TMP), Liquidacion No.' + @NOFACTURA 
	END
END
--
--
-- Actualizar Great Plains
IF @Update_Error = 0 
BEGIN

	DECLARE @IVIVINDX int, @IVCOGSIX int, @SLSINDX int, @MRKDNAMT numeric( 19, 5), @XTNDPRCE numeric( 19, 5), @EXTDCOST numeric( 19, 5), 
	@IVSLDSIX int, @IVRETIDX int, @IVINUSIX int, @IVINSVIX int, @IVDMGIDX int, @ITMTSHID char( 15), @TAXOPTNS smallint,
	@QTYONHND numeric(19, 5), @DECPLQTY smallint, @DECPLCUR smallint, @CtaXCobrar int, @CtaVentas int, @CtaDescuento int,
	@DescuentoLinea numeric(19, 5), @SALSTERR char( 15), @ADDRESS3 char( 31), @PHONE3 char( 21), @SQNCLINE numeric( 19, 5),
	@DOCTYPE char( 1), @QUANTITY_ORD numeric( 19, 5), @QUANTITY_SHP  numeric( 19, 5), @REMANENTE numeric( 19, 5), 
	@CUSTNAME char( 65), @AcctType smallint, @DECPLACS smallint, @CrdtAmt numeric( 19, 5), @DebAmt numeric( 19, 5),
	@TXTRDNUM char( 21), @SumAmt numeric( 19, 5), @ActIndx int, @FechaContable datetime, @DiasAntiguedad smallint,
	@DOCID char( 15), @TotalLinea numeric( 19, 5)

	SET @PHONE3   = ''
	SET @ADDRESS3 = ''
	SET @CUSTNAME = ''


	-- Id de Factura
	select TOP 1 @DOCID = DOCID, @IVIVINDX = actindx from SntConsigCFG

	-- Tabla Temporal Asiento Contable
	CREATE TABLE #DetalleContable ( ActIndx int, AcctType smallint, DECPLACS smallint, CrdtAmt numeric( 19, 5), DebAmt numeric( 19, 5))
	IF @@Error <> 0 
	BEGIN
		set @Update_Error = -1
		SELECT @ErrorDesc = 'Error Creando Tabla temporal #DetalleContable, Factura no. ' + @NOFACTURA 
	END

	--	
	select @CtaXCobrar = rmaracc from rm00101 where custnmbr = @custnmbr

	-- Informacion Encabezado de la Factura.
	--  
	-- Informacion Cliente
	select @CNTCPRSN = CNTCPRSN, @CUSTNAME = CUSTNAME, @PYMTRMID = PYMTRMID, @PRBTADCD = PRBTADCD, @PRSTADCD = PRSTADCD, @SHIPMTHD = SHIPMTHD, @SALSTERR = SALSTERR 
	from  RM00101 where custnmbr = @CUSTNMBR

	-- Informacion Direccion Cliente
	select @ADDRESS1 = ADDRESS1, @ADDRESS2 = ADDRESS2, @ADDRESS3 = isnull( ADDRESS3, ''), @COUNTRY = COUNTRY, @CITY = CITY, @STATE = STATE, @ZIPCODE = ZIP, @PHONE1 = PHONE1, @PHONE2 = PHONE2, @PHONE3 = PHONE3, @FAX = FAX from RM00102 where custnmbr = '' and adrscode = ''

	if @PRCLEVEL is null
		select TOP 1 @PRCLEVEL = PRCLEVEL FROM RM40101 

	select @DUEDTDS = DUEDTDS from SY03300 where PYMTRMID in (select PYMTRMID from  RM00101 where custnmbr = @CUSTNMBR)

--	SELECT @MSTRNUMB = MAX( MSTRNUMB ) + 1  FROM SOP40500
--	INSERT SOP40500 (MSTRNUMB) VALUES( @MSTRNUMB)

	SELECT @Moneda = Moneda, @SLPRSNID = SLPRSNID, @SumAmt = DOCAMNT 
	FROM SntConsigHdrHst where NOCONSIG = @NOFACTURA and doctype = 'F'

	select @CURRNIDX = FUNCRIDX from MC40000 where FUNLCURR = @Moneda

	-- Actualizando SOP master number
	IF @Update_Error = 0 
	BEGIN
		select @MSTRNUMB = MAX( MSTRNUMB) + 1 from SOP40500
		INSERT SOP40500 ( MSTRNUMB) VALUES( @MSTRNUMB) 
		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SELECT @ErrorDesc = 'Error Actualizando Lote - SOP40500, Consignacion no. ' + @NOFACTURA 
		END

	END

	--JHERNANDEZ 21-09-2006, PARA EVITAR EL PROBLEMA DE LAS CLAVES DUPLICADAS
	--MODIFICACION PARA EVITAR EL BUG DE LA ASIGNACI�N INCORRECTA DE SECUENCIAS
	DECLARE
		@ITEMNMBR_H VARCHAR(20),
		@ITEMNMBR_P VARCHAR(20),
		@SECUENCIA NUMERIC
	
	SET @ITEMNMBR_P=''
	
	SELECT *,'NINGUNO' AS KIT INTO #CONSIGDET_HST FROM sntconsigdethst WHERE NOCONSIG = @NOFACTURA and doctype = 'F' ORDER BY SEQUENCE

--
-- fbb 2008-06-03
--print 'ver registros: #CONSIGDET_HST'
--select * from #CONSIGDET_HST
	
	DECLARE KITS CURSOR FOR
	SELECT ITEMNMBR,SEQUENCE FROM #CONSIGDET_HST
	
	OPEN KITS
	FETCH NEXT FROM KITS INTO @ITEMNMBR_H,@SECUENCIA
	WHILE @@FETCH_STATUS=0 BEGIN
		IF EXISTS (SELECT ITEMNMBR FROM IV00104 WHERE ITEMNMBR=@ITEMNMBR_H)
			SET @ITEMNMBR_P=@ITEMNMBR_H
	
		IF EXISTS (SELECT ITEMNMBR FROM IV00104 WHERE CMPTITNM=@ITEMNMBR_H) AND @ITEMNMBR_P<>'' BEGIN
			UPDATE #CONSIGDET_HST SET KIT=@ITEMNMBR_P WHERE SEQUENCE=@SECUENCIA
		END
		FETCH NEXT FROM KITS INTO @ITEMNMBR_H, @SECUENCIA
	END
	CLOSE KITS
	DEALLOCATE KITS
	
	UPDATE #CONSIGDET_HST SET KIT=CASE WHEN KIT='NINGUNO' THEN ITEMNMBR ELSE KIT END
	
	--FIN MODIFICACION PARA REPARAR EL BUG
	-- MODIFICADO POR JEAN HERN�NDEZ 06/07/2007 se agrego el campo tipo
	--
	-- 2007-10-16 FBB cambio NO se hizo
	-- select Identity(int,1,1) AS sequence, POR SELECT sequence
	select Identity(int,1,1) AS sequence, DOCTYPE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, 
	  sum(XTNDPRCE) as XTNDPRCE, sum(QUANTITY_ORD) as QUANTITY_ORD, sum(QUANTITY_SHP) as QUANTITY_SHP, 
	  sum(REMANENTE) as REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO into #def from
	#CONSIGDET_HST
	GROUP BY DOCTYPE,ITEMNMBR,ITEMDESC,UOFM,UNITPRCE,UNITCOST, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, KIT, TIPO
	ORDER BY kit, (select case when exists (select top 1 * from iv00104 where itemnmbr=#CONSIGDET_HST.itemnmbr) then 0 else 1 end)
	-- 2007-10-16 FBB cambio NO se hizo
	-- GROUP BY DOCTYPE, POR GROUP BY sequence,
	-- ORDER BY kit, POR ORDER BY sequence,

	DECLARE DetFactura CURSOR FOR 
	select DOCTYPE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST,XTNDPRCE,QUANTITY_ORD,
	  QUANTITY_SHP,REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO from #def order by sequence

-- FFB 2008-06-03
--PRINT 'VER LOS REGISTROS DEL CURSOR'
--select DOCTYPE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST,XTNDPRCE,QUANTITY_ORD,
--	  QUANTITY_SHP,REMANENTE, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO from #def order by sequence

	--FIN MODIFICACION
	--CAMBIO PARA Q LAS SECUENCIAS SALGAN COMO DEBEN
	DECLARE @ITEMNMBR_ANT CHAR(100)
	--VARIABLE DE CORRECCI�N DE BUG CAUSADO POR EL TRATAMIENTO DE LA SUMARIZACI�N SOBRE LAS CANTIDADES DE LOS KITS
	DECLARE @QUANTITY_FATHER NUMERIC(19,5)
	SET @ITEMNMBR_ANT=''
	-- MODIFICADO POR JEAN HERN�NDEZ 06/07/2007
	SET @SQNCLINE = 0		
	OPEN DetFactura
	FETCH NEXT FROM DetFactura into @DOCTYPE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @REMANENTE, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO, @TIPO
	WHILE @@FETCH_STATUS = 0
	BEGIN	
		IF @Update_Error <> 0 
			BREAK
		--@@@INICIO MODIFICACI�N JEAN 15-09-2006
		--DETERMINAR EL TIPO DE ITEM QUE SE LIQUIDA
		IF @TIPO<>'ITEM'
			IF EXISTS(SELECT TOP 1 ITEMNMBR FROM IV00104 WHERE ITEMNMBR=@ItemnMbr)
				SET @TIPO='KIT'
			ELSE
				IF EXISTS (SELECT TOP 1 CMPTITNM FROM IV00104 WHERE CMPTITNM=@ITEMNMBR)
					SET @TIPO='ELEMENTO'
		-- MODIFICADO POR JEAN HERN�NDEZ 06/07/2007
		--	ELSE
		--		SET @TIPO='ITEM'
		--FINAL MODIFICACION

		-- Verificar que exista el Item en el almacen
		if exists( select TOP 1 itemnmbr from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = @LocnConsig AND RCRDTYPE = 2)
		BEGIN
			-- Existencia en alamacen
			--INICIO MODIFICACI�N JEAN HERN�NDEZ, 14/09/2006
			--CON ESTA MODIFICACI�N TOMA EN CUENTA LAS CANTIDADES DE LOS ITEMS DE LOS KITS EN EL CASO DE QUE
			--SE QUIERA LIQUIDAR UN KIT
			IF @TIPO='KIT' BEGIN
				-- FFB 2008-06-03
				--print 'tipo KIT'
				SELECT @Existencia=MIN (case when QTYONHND - ATYALLOC <= 0 then 0 else QTYONHND - ATYALLOC end) FROM IV00102 WHERE ITEMNMBR IN (SELECT CMPTITNM FROM IV00104 WHERE ITEMNMBR=@ItemnMbr) AND LOCNCODE = @LocnConsig AND RCRDTYPE = 2
			END ELSE BEGIN
				-- FFB 2008-06-03
				--print 'tipo KIT ELSE'
				--select 'itemnmbr =', @ItemnMbr, 'locncode =', @LocnConsig
				--select (case when QTYONHND - ATYALLOC <= 0 then 0 else QTYONHND - ATYALLOC end), 
  				--  QTYONHND, ATYALLOC, ITEMNMBR, LOCNCODE from IV00102
				--  WHERE itemnmbr = @ItemnMbr and locncode = @LocnConsig AND RCRDTYPE = 2
				select @Existencia = (case when QTYONHND - ATYALLOC <= 0 then 0 else QTYONHND - ATYALLOC end) from IV00102 WHERE itemnmbr = @ItemnMbr and locncode = @LocnConsig AND RCRDTYPE = 2
			END
			--FIN MODIFICACION
			IF @QUANTITY_SHP > @Existencia  
			BEGIN
				set @Update_Error = -1
				SELECT @ErrorDesc = @TIPO + '--' + cast(@LocnConsig as varchar) + '' + cast(@QUANTITY_SHP as varchar(100)) + '<-->' + cast(@Existencia as varchar(100)) + 'Error No hay suficiente existencia en Almacen Origen, Item: ' + ltrim(rtrim(@Itemnmbr)) + ' , Almacen: ' + @LocnConsig  
				BREAK
			END 
			ELSE
			BEGIN
				-- Asignar en almacen Origen
				UPDATE IV00102 
				SET ATYALLOC = ATYALLOC + @QUANTITY_SHP
				WHERE ITEMNMBR = @ItemnMbr
				     AND LOCNCODE = @LocnConsig AND RCRDTYPE = 2
				--@@@COMENTADO DEBIDO A NO SER NECESARIO ACTUALIZAR A LOS ELEMENTOS DEL KIT YA QUE
				--ELLOS ESTAN INCLUIDOS DENTRO DEL CURSOR
				--IF @TIPO='KIT' BEGIN
					-- ASIGNAR EN ALMACEN ORIGEN PARA LOS ELEMENTOS DEL KIT
				--	UPDATE IV00102
				--	SET ATYALLOV = ATTYALLOC+@QUANTITY_SHP
				--	WHERE ITEMNMBR IN (SELECT CMTITNM FROM IV00104 WHERE ITEMNMBR=@ItemnMbr)
				--		AND LOCNCODE = @LocnConsig AND RCRDTYPE = 2
				--END
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @ErrorDesc = 'Error al Asignar en almacen Origen (' + @LocnCode + '), Item (' + ltrim(rtrim(@itemnMbr)) + '), Tabla (IV00102)'		
				END
			END
		END
		ELSE
		BEGIN
			SET @Update_Error = -1
			SET @ErrorDesc = 'Item (' + @itemnMbr + '), No existe en el almacen (' + @LocnConsig + '),  Tabla (IV00102)'		
		END


		if @Update_Error = 0 
		begin
			if exists( select TOP 1 itemnmbr from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = '' AND RCRDTYPE = 1)
			BEGIN
				-- Asignar en almacen Consolidado
				UPDATE IV00102 
				SET ATYALLOC = ATYALLOC + @QUANTITY_SHP
				WHERE ITEMNMBR = @ItemnMbr
				     AND LOCNCODE = '' AND RCRDTYPE = 1
				--@@@COMENTADO DEBIDO A NO SER NECESARIO ACTUALIZAR A LOS ELEMENTOS DEL KIT YA QUE
				--ELLOS ESTAN INCLUIDOS DENTRO DEL CURSOR
				--IF @TIPO='KIT' BEGIN
					-- ASIGNAR EN ALMACEN ORIGEN PARA LOS ELEMENTOS DEL KIT
				--	UPDATE IV00102
				--	SET ATYALLOV = ATTYALLOC+@QUANTITY_SHP
				--	WHERE ITEMNMBR IN (SELECT CMTITNM FROM IV00104 WHERE ITEMNMBR=@ItemnMbr)
				--		AND LOCNCODE = @LocnConsig AND RCRDTYPE = 1
				--END

				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @ErrorDesc = 'Error al Asignar en almacen consolidado, Item (' + @itemnmbr + '), Tabla (IV00102)'		
				END
			end
			ELSE
			BEGIN
				SET @Update_Error = -1
				SET @ErrorDesc = 'Item (' + @itemnMbr + '), No existe en el almacen Consolidado,  Tabla (IV00102)'		
			END
		END
		--JHERNANDEZ 19-09-2006

		--FIN MODIFICACION

--		SET @CURRNIDX = 0
		-- IVCOGSIX

--		SELECT @CtaVentas = IVSLSIDX, @CtaDescuento = IVSLDSIX, @IVCOGSIX = IVCOGSIX, @SLSINDX = IVCOGSIX, @IVSLDSIX = IVSLDSIX, @IVRETIDX = IVRETIDX , @IVINUSIX = IVINUSIX, @IVINSVIX = IVINSVIX, @IVDMGIDX = IVDMGIDX, @ITMTSHID = ITMTSHID, @TAXOPTNS = TAXOPTNS,

		SELECT @CtaVentas = IVSLSIDX, @CtaDescuento = IVSLDSIX, @IVCOGSIX = IVCOGSIX, @SLSINDX = IVSLSIDX, @IVSLDSIX = IVSLDSIX, @IVRETIDX = IVRETIDX , @IVINUSIX = IVINUSIX, @IVINSVIX = IVINSVIX, @IVDMGIDX = IVDMGIDX, @ITMTSHID = ITMTSHID, @TAXOPTNS = TAXOPTNS,
        @DECPLQTY = DECPLQTY, @DECPLCUR = DECPLCUR
		FROM IV00101 where ITEMNMBR = @ITEMNMBR
--
--
		SET @MRKDNAMT = ISNULL(@DESCUENTO * @UNITPRCE/100,0)

		SET @XTNDPRCE = (@UNITPRCE - @MRKDNAMT) * @QUANTITY_SHP

		SET @DescuentoLinea = @MRKDNAMT * @QUANTITY_SHP

		SET @EXTDCOST = @UNITCOST * @QUANTITY_SHP
		SELECT @QTYONHND = QTYONHND FROM IV00102 where ITEMNMBR = @ITEMNMBR AND RCRDTYPE = 1

		--IF @TAXAMNT IS NULL SET @TAXAMNT=cast(.0000 as decimal( 19, 2))
		--
		--
		--JHERNANDEZ 22/09/2006 MODIFICACI�N PARA EVITAR ERRORES DEL TIPO SE ESTA TRATANDO DE INSERTAR UN VALOR
		--NULL, OCURRE CON LOS ELEMENTOS DE LOS KITS, QUE TRAEN PRECIOS EN NULO O EN CERO
			SET @NOFACTURA=		ISNULL(@NOFACTURA,0)
			SET @UNITCOST=		ISNULL(@UNITCOST,0)
			SET @UNITPRCE=		ISNULL(@UNITPRCE,0)
			SET @XTNDPRCE=		ISNULL(@XTNDPRCE,0)
			SET @EXTDCOST=		ISNULL(@EXTDCOST,0)
			SET @MRKDNAMT=		ISNULL(@MRKDNAMT,0)
			SET @DESCUENTO=		ISNULL(@DESCUENTO,0)
			SET @TAXAMNT=		ISNULL(@TAXAMNT,0)
			SET @QUANTITY_SHP=	ISNULL(@QUANTITY_SHP,0)
			SET @QTYONHND=		ISNULL(@QTYONHND,0)
			SET @TAXOPTNS=		ISNULL(@TAXOPTNS,0) 
			SET @TAXSCHID=		ISNULL(@TAXSCHID,0)
			SET @SUBTOTAL=		ISNULL(@SUBTOTAL,0)
			SET @TRDISAMT=		ISNULL(@TRDISAMT,0)
			SET @PHONE1=		ISNULL(@PHONE1,'00000000000000')
			SET @PHONE2=		ISNULL(@PHONE2,'00000000000000')
			SET @PHONE3=		ISNULL(@PHONE3,'00000000000000')
			SET @FAX=		ISNULL(@FAX,'00000000000000')
			SET @COUNTRY=		ISNULL(@COUNTRY,'VENEZUELA')
			SET @CNTCPRSN=		ISNULL(@CNTCPRSN,0)
			if ltrim(rtrim(@PHONE1))='' SET @PHONE1='00000000000000'
			if ltrim(rtrim(@PHONE2))='' SET @PHONE2='00000000000000'
			if ltrim(rtrim(@PHONE3))='' SET @PHONE3='00000000000000'
			if ltrim(rtrim(@FAX))='' SET @FAX='00000000000000'
			if ltrim(rtrim(@COUNTRY))='' SET @COUNTRY='VENEZUELA'
		--FIN MODIFICACION


		IF @TIPO='ITEM' BEGIN
			SELECT @SQNCLINE=@SQNCLINE + 16384
			SET @SEQ_ELEMENT = 0
			INSERT SOP10200 ( SOPTYPE, SOPNUMBE, LNITMSEQ, CMPNTSEQ, ITEMNMBR, ITEMDESC, NONINVEN, DROPSHIP, UOFM, LOCNCODE, UNITCOST, ORUNTCST, 
			UNITPRCE, ORUNTPRC, XTNDPRCE, OXTNDPRC, REMPRICE, OREPRICE, EXTDCOST, OREXTCST, MRKDNAMT, ORMRKDAM, MRKDNPCT, MRKDNTYP, 
			INVINDX, CSLSINDX, SLSINDX, MKDNINDX, RTNSINDX, INUSINDX, INSRINDX, DMGDINDX, ITMTSHID, IVITMTXB, BKTSLSAM, ORBKTSLS, 
			TAXAMNT, ORTAXAMT, TXBTXAMT, OTAXTAMT, BSIVCTTL, TRDISAMT, ORTDISAM, DISCSALE, ORDAVSLS, QUANTITY, ATYALLOC, QTYINSVC, 
			QTYINUSE, QTYDMGED, QTYRTRND, QTYONHND, QTYCANCE, QTYCANOT, QTYONPO, QTYORDER, QTYPRBAC, QTYPRBOO, QTYPRINV, QTYPRORD, 
			QTYPRVRECVD, QTYRECVD, QTYREMAI, QTYREMBO, QTYTBAOR, QTYTOINV, QTYTORDR, QTYFULFI, QTYSLCTD, QTYBSUOM, EXTQTYAL, EXTQTYSEL, 
			ReqShipDate, FUFILDAT, ACTLSHIP, SHIPMTHD, SALSTERR, SLPRSNID, PRCLEVEL, COMMNTID, BRKFLD1, BRKFLD2, BRKFLD3, CURRNIDX, 
			TRXSORCE, ORGSEQNM, ITEMCODE, PURCHSTAT, DECPLQTY, DECPLCUR, ODECPLCU, QTYTOSHP, XFRSHDOC, EXCEPTIONALDEMAND, TAXSCHID, TXSCHSRC, 
			PRSTADCD, ShipToName, CNTCPRSN, ADDRESS1, ADDRESS2, ADDRESS3, CITY, STATE, ZIPCODE, COUNTRY, PHONE1, PHONE2, 
			PHONE3, FAXNUMBR)
			VALUES( 3, @NOFACTURA, @SQNCLINE, 0, @ITEMNMBR, @ITEMDESC, 0, 0, @UOFM, @LocnConsig, @UNITCOST, @UNITCOST, 
			@UNITPRCE, @UNITPRCE, @XTNDPRCE, @XTNDPRCE, @XTNDPRCE, @XTNDPRCE, @EXTDCOST, @EXTDCOST, @MRKDNAMT, @MRKDNAMT, CASE WHEN @DESCUENTO IS NULL THEN 0 WHEN @DESCUENTO<=0 THEN 0 ELSE @DESCUENTO*100 END, 0, 
			@IVIVINDX, @IVCOGSIX, @SLSINDX, @IVSLDSIX, @IVRETIDX, @IVINUSIX, @IVINSVIX, @IVDMGIDX, @ITMTSHID, @TAXOPTNS, 0, 0, 
			@TAXAMNT, @TAXAMNT, 0, 0, 0, 0, 0, 0, 0, @QUANTITY_SHP, @QUANTITY_SHP, 0, 
			0, 0, 0, 0, 0, 0, 0, @QUANTITY_SHP, 0, 0, 0, 0,
			0, 0, @QUANTITY_SHP, 0, 0, @QUANTITY_SHP, 0, @QUANTITY_SHP, @QUANTITY_SHP, 1, @QTYONHND, @QUANTITY_SHP,
			@FECHAHOY, @FECHAHOY, @FECHAHOY,  @SHIPMTHD, @SALSTERR, @SLPRSNID, @PRCLEVEL, '', 0, 0, 0, @CURRNIDX, 
			'', 0, '', 1, @DECPLQTY, @DECPLCUR, @DECPLCUR, 0, 0, 0, @TAXSCHID, 2,
	 		@PRSTADCD, @CUSTNAME, @CNTCPRSN, @ADDRESS1, @ADDRESS2, @ADDRESS3, @CITY, @STATE, @ZIPCODE, @COUNTRY, @PHONE1, @PHONE2, 
			@PHONE3, @FAX)
			SET @SEQ_ELEMENT=0
			SET @QUANTITY_FATHER=0
			--
			-- FFB 2007-03-27
			-- insertar impuestos ojojo 
			SET @TAXDTLID = ''
			select @TAXDTLID = TAXDTLID from TX00102 where taxschid = @TAXSCHID -- buscar el id impuesto
			if @TAXDTLID is not NULL and @TAXDTLID <> ''
			BEGIN
			  SET @IVA_ACTINDX = 0
 			  SELECT @IVA_ACTINDX = ACTINDX from TX00201 where txdtltyp = 1 and TAXDTLID = @TAXDTLID	
			  if @IVA_ACTINDX is not NULL and @IVA_ACTINDX >= 0
			  BEGIN 	
			    SELECT @IVA_ACTINDX = ACTINDX from TX00201 where txdtltyp = 1 and TAXDTLID = @TAXDTLID
			    INSERT SOP10105 ( 
			    SOPTYPE   , SOPNUMBE      , LNITMSEQ      , TAXDTLID     , ACTINDX     , BKOUTTAX      , TXABLETX   , STAXAMNT  ,
			    ORSLSTAX  , FRTTXAMT      , ORFRTTAX      , MSCTXAMT     , ORMSCTAX    , TAXDTSLS      , ORTOTSLS   , TDTTXSLS  ,
			    ORTXSLS   , TXDTOTTX      , OTTAXPON      , DELETE1      , CURRNIDX    , TRXSORCE       )
			    VALUES (
			    3         , @NOFACTURA    , @SQNCLINE     , @TAXDTLID    , @IVA_ACTINDX, 0             , 0          , @TAXAMNT  ,
			    @TAXAMNT  , 0             , 0             , 0            , 0           , @XTNDPRCE     , @XTNDPRCE  , @XTNDPRCE ,
			    @XTNDPRCE , 0             , 0             , 0            , @CURRNIDX   , ' '            )
			  END	
			 END
			-- FFB 2007-03-27
			-- FIN insertar impuestos
			---
		END ELSE IF @TIPO='KIT' BEGIN
			SET @SQNCLINE = @SQNCLINE + 16384
			SET @SEQ_ELEMENT = 0
			SELECT @UNITCOST=SUM(CURRCOST) FROM IV00101 WHERE ITEMNMBR IN (SELECT CMPTITNM FROM IV00104 WHERE ITEMNMBR=@ITEMNMBR)
			SET @EXTDCOST=@UNITCOST*@QUANTITY_SHP
			INSERT SOP10200 ( SOPTYPE, SOPNUMBE, LNITMSEQ, CMPNTSEQ, ITEMNMBR, ITEMDESC, NONINVEN, DROPSHIP, UOFM, LOCNCODE, UNITCOST, ORUNTCST, 
			UNITPRCE, ORUNTPRC, XTNDPRCE, OXTNDPRC, REMPRICE, OREPRICE, EXTDCOST, OREXTCST, MRKDNAMT, ORMRKDAM, MRKDNPCT, MRKDNTYP, 
			INVINDX, CSLSINDX, SLSINDX, MKDNINDX, RTNSINDX, INUSINDX, INSRINDX, DMGDINDX, ITMTSHID, IVITMTXB, BKTSLSAM, ORBKTSLS, 
			TAXAMNT, ORTAXAMT, TXBTXAMT, OTAXTAMT, BSIVCTTL, TRDISAMT, ORTDISAM, DISCSALE, ORDAVSLS, QUANTITY, ATYALLOC, QTYINSVC, 
			QTYINUSE, QTYDMGED, QTYRTRND, QTYONHND, QTYCANCE, QTYCANOT, QTYONPO, QTYORDER, QTYPRBAC, QTYPRBOO, QTYPRINV, QTYPRORD, 
			QTYPRVRECVD, QTYRECVD, QTYREMAI, QTYREMBO, QTYTBAOR, QTYTOINV, QTYTORDR, QTYFULFI, QTYSLCTD, QTYBSUOM, EXTQTYAL, EXTQTYSEL, 
			ReqShipDate, FUFILDAT, ACTLSHIP, SHIPMTHD, SALSTERR, SLPRSNID, PRCLEVEL, COMMNTID, BRKFLD1, BRKFLD2, BRKFLD3, CURRNIDX, 
			TRXSORCE, ORGSEQNM, ITEMCODE, PURCHSTAT, DECPLQTY, DECPLCUR, ODECPLCU, QTYTOSHP, XFRSHDOC, EXCEPTIONALDEMAND, TAXSCHID, TXSCHSRC, 
			PRSTADCD, ShipToName, CNTCPRSN, ADDRESS1, ADDRESS2, ADDRESS3, CITY, STATE, ZIPCODE, COUNTRY, PHONE1, PHONE2, 
			PHONE3, FAXNUMBR)
			VALUES( 3, @NOFACTURA, @SQNCLINE, 0, @ITEMNMBR, @ITEMDESC, 0, 0, @UOFM, @LocnConsig, @UNITCOST, @UNITCOST, 
			@UNITPRCE, @UNITPRCE, @XTNDPRCE, @XTNDPRCE, @XTNDPRCE, @XTNDPRCE, @EXTDCOST, @EXTDCOST, @MRKDNAMT, @MRKDNAMT, CASE WHEN @DESCUENTO IS NULL THEN 0 WHEN @DESCUENTO<=0 THEN 0 ELSE @DESCUENTO*100 END, 0, 
			@IVIVINDX, @IVCOGSIX, @SLSINDX, @IVSLDSIX, @IVRETIDX, @IVINUSIX, @IVINSVIX, @IVDMGIDX, @ITMTSHID, @TAXOPTNS, 0, 0, 
			@TAXAMNT, @TAXAMNT, 0, 0, 0, 0, 0, 0, 0, @QUANTITY_SHP, @QUANTITY_SHP, 0, 
			0, 0, 0, 0, 0, 0, 0, @QUANTITY_SHP, 0, 0, 0, 0,
			0, 0, @QUANTITY_SHP, 0, 0, @QUANTITY_SHP, 0, @QUANTITY_SHP, 0, 1, 0, 0,
			@FECHAHOY, @FECHAHOY, @FECHAHOY,  @SHIPMTHD, @SALSTERR, @SLPRSNID, @PRCLEVEL, '', 0, 0, 0, @CURRNIDX, 
			'', 0, '', 1, @DECPLQTY, @DECPLCUR, @DECPLCUR, 0, 0, 0, @TAXSCHID, 2,
	 		@PRSTADCD, @CUSTNAME, @CNTCPRSN, @ADDRESS1, @ADDRESS2, @ADDRESS3, @CITY, @STATE, @ZIPCODE, @COUNTRY, @PHONE1, @PHONE2, 
			@PHONE3, @FAX)
			SET @SEQ_ELEMENT=0
			SET @QUANTITY_FATHER=@QUANTITY_SHP
--
-- FFB 2007-03-27
-- insertar impuestos
			SET @TAXDTLID = ''
			select @TAXDTLID = TAXDTLID from TX00102 where taxschid = @TAXSCHID -- buscar el id impuesto
			if @TAXDTLID is not NULL and @TAXDTLID <> ''
			BEGIN
			  SET @IVA_ACTINDX = 0	
			  SELECT @IVA_ACTINDX = ACTINDX from TX00201 where txdtltyp = 1 and TAXDTLID = @TAXDTLID		   	
			  if @IVA_ACTINDX is not NULL and @IVA_ACTINDX >= 0 
			  BEGIN 	
			  	INSERT SOP10105 ( 
			  	SOPTYPE   , SOPNUMBE      , LNITMSEQ      , TAXDTLID     , ACTINDX     , BKOUTTAX      , TXABLETX   , STAXAMNT  ,
			  	ORSLSTAX  , FRTTXAMT      , ORFRTTAX      , MSCTXAMT     , ORMSCTAX    , TAXDTSLS      , ORTOTSLS   , TDTTXSLS  ,
			  	ORTXSLS   , TXDTOTTX      , OTTAXPON      , DELETE1      , CURRNIDX    , TRXSORCE       )
			  	VALUES (
			  	3         , @NOFACTURA    , @SQNCLINE     , @TAXDTLID    , @IVA_ACTINDX, 0             , 0          , @TAXAMNT  ,
			  	@TAXAMNT  , 0             , 0             , 0            , 0           , @XTNDPRCE     , @XTNDPRCE  , @XTNDPRCE ,
			  	@XTNDPRCE , 0             , 0             , 0            , @CURRNIDX   , ' '            )

			  END
			END
-- FFB 2007-03-27
-- FIN insertar impuestos
--
		END ELSE IF @TIPO='ELEMENTO' BEGIN
			SET @SEQ_ELEMENT=@SEQ_ELEMENT+16384
			INSERT SOP10200 ( SOPTYPE, SOPNUMBE, LNITMSEQ, CMPNTSEQ, ITEMNMBR, ITEMDESC, NONINVEN, DROPSHIP, UOFM, LOCNCODE, UNITCOST, ORUNTCST, 
			UNITPRCE, ORUNTPRC, XTNDPRCE, OXTNDPRC, REMPRICE, OREPRICE, EXTDCOST, OREXTCST, MRKDNAMT, ORMRKDAM, MRKDNPCT, MRKDNTYP, 
			INVINDX, CSLSINDX, SLSINDX, MKDNINDX, RTNSINDX, INUSINDX, INSRINDX, DMGDINDX, ITMTSHID, IVITMTXB, BKTSLSAM, ORBKTSLS, 
			TAXAMNT, ORTAXAMT, TXBTXAMT, OTAXTAMT, BSIVCTTL, TRDISAMT, ORTDISAM, DISCSALE, ORDAVSLS, QUANTITY, ATYALLOC, QTYINSVC, 
			QTYINUSE, QTYDMGED, QTYRTRND, QTYONHND, QTYCANCE, QTYCANOT, QTYONPO, QTYORDER, QTYPRBAC, QTYPRBOO, QTYPRINV, QTYPRORD, 
			QTYPRVRECVD, QTYRECVD, QTYREMAI, QTYREMBO, QTYTBAOR, QTYTOINV, QTYTORDR, QTYFULFI, QTYSLCTD, QTYBSUOM, EXTQTYAL, EXTQTYSEL, 
			ReqShipDate, FUFILDAT, ACTLSHIP, SHIPMTHD, SALSTERR, SLPRSNID, PRCLEVEL, COMMNTID, BRKFLD1, BRKFLD2, BRKFLD3, CURRNIDX, 
			TRXSORCE, ORGSEQNM, ITEMCODE, PURCHSTAT, DECPLQTY, DECPLCUR, ODECPLCU, QTYTOSHP, XFRSHDOC, EXCEPTIONALDEMAND, TAXSCHID, TXSCHSRC, 
			PRSTADCD, ShipToName, CNTCPRSN, ADDRESS1, ADDRESS2, ADDRESS3, CITY, STATE, ZIPCODE, COUNTRY, PHONE1, PHONE2, 
			PHONE3, FAXNUMBR)
			VALUES( 3, @NOFACTURA, @SQNCLINE, @SEQ_ELEMENT, @ITEMNMBR, @ITEMDESC, 0, 0, @UOFM, @LocnConsig, @UNITCOST, @UNITCOST, 
			0, 0, 0, 0, 0, 0, @EXTDCOST, @EXTDCOST, 0, 0, 0, 0, 
			@IVIVINDX, @IVCOGSIX, @SLSINDX, @IVSLDSIX, @IVRETIDX, @IVINUSIX, @IVINSVIX, @IVDMGIDX, @ITMTSHID, 0, 0, 0, 
			@TAXAMNT, @TAXAMNT, 0, 0, 0, 0, 0, 0, 0, @QUANTITY_SHP, @QUANTITY_SHP, 0, 
			0, 0, 0, 0, 0, 0, 0, @QUANTITY_SHP, 0, 0, 0, 0,
			0, 0, @QUANTITY_SHP, 0, 0, @QUANTITY_SHP, 0, @QUANTITY_SHP, @QUANTITY_SHP, 1, @QTYONHND, 0,
			@FECHAHOY, @FECHAHOY, @FECHAHOY,  '', '', '', '', '', 0, 0, 0, 0, 
			'', 0, '', 1, @DECPLQTY, @DECPLCUR, @DECPLCUR, 0, 0, 0, '', 0,
	 		'', '', '', '', '', '', '', '', '', '', '', '', 
			'', '')
			--
			-- 2007-10-17 FBB 
			-- @QUANTITY_SHP reemplaza a @QUANTITY_FATHER
			-- VALUES( 3, @NOFACTURA, @SQNCLINE, @SEQ_ELEMENT, @ITEMNMBR, @ITEMDESC, 0, 0, @UOFM, @LocnConsig, @UNITCOST, @UNITCOST, 
			-- 0, 0, 0, 0, 0, 0, @EXTDCOST, @EXTDCOST, 0, 0, 0, 0, 
			-- @IVIVINDX, @IVCOGSIX, @SLSINDX, @IVSLDSIX, @IVRETIDX, @IVINUSIX, @IVINSVIX, @IVDMGIDX, @ITMTSHID, 0, 0, 0, 
			-- @TAXAMNT, @TAXAMNT, 0, 0, 0, 0, 0, 0, 0, @QUANTITY_FATHER, @QUANTITY_FATHER, 0, 
			-- 0, 0, 0, 0, 0, 0, 0, @QUANTITY_FATHER, 0, 0, 0, 0,
			-- 0, 0, @QUANTITY_FATHER, 0, 0, @QUANTITY_FATHER, 0, @QUANTITY_FATHER, @QUANTITY_FATHER, 1, @QTYONHND, 0,
			-- @FECHAHOY, @FECHAHOY, @FECHAHOY,  '', '', '', '', '', 0, 0, 0, 0, 
			-- '', 0, '', 1, @DECPLQTY, @DECPLCUR, @DECPLCUR, 0, 0, 0, '', 0,
	 		-- '', '', '', '', '', '', '', '', '', '', '', '', 
			-- '', '')
			--

		END

		--SET @ErrorDesc= LTRIM(RTRIM(@TIPO))+'3'+ltrim(rtrim(@NOFACTURA))+'['+ltrim(rtrim(CONVERT(VARCHAR(100),@SQNCLINE)))+']'+'['+ltrim(rtrim(CONVERT(VARCHAR(100),@SEQ_ELEMENT)))+']'
		--RAISERROR ( @ErrorDesc ,17,1)
		--set @ErrorDesc=''
		--RAISERROR(@TIPO,17,1)

		--set @@ERROR=0

		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SELECT @ErrorDesc = 'Error Creando Detalle Factura GP - SOP10200, Factura no. ' + @NOFACTURA 
			BREAK
		END
		

		IF @Update_Error = 0  and @TIPO<>'ELEMENTO'
		BEGIN

			-- Contable Ventas
			if (@XTNDPRCE + @DescuentoLinea) > 0
			BEGIN
				select @ACCTTYPE = ACCTTYPE, @DECPLACS = DECPLACS from gl00100 where ACTINDX = @CtaVentas
				INSERT #DetalleContable ( ActIndx, AcctType, DECPLACS, CrdtAmt, DebAmt)  
				VALUES( @CtaVentas, 1, @DECPLACS, @XTNDPRCE + @DescuentoLinea, 0)
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SELECT @ErrorDesc = 'Error Creando Movimiento Contable Ventas, Factura no. ' + @NOFACTURA 
				END
			END
			
			if @DescuentoLinea > 0
			BEGIN
				-- Contable Descuento
				select @ACCTTYPE = ACCTTYPE, @DECPLACS = DECPLACS from gl00100 where ACTINDX = @CtaDescuento
				INSERT #DetalleContable ( ActIndx, AcctType, DECPLACS, CrdtAmt, DebAmt)  
				VALUES( @CtaDescuento, 10, @DECPLACS, 0, @DescuentoLinea)
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SELECT @ErrorDesc = 'Error Creando Movimiento Contable Descuento, Factura no. ' + @NOFACTURA 
				END
			END

			-- Contable CxC
			if (@XTNDPRCE) > 0
			BEGIN
				select @ACCTTYPE = ACCTTYPE, @DECPLACS = DECPLACS from gl00100 where ACTINDX = @CtaXCobrar
				INSERT #DetalleContable ( ActIndx, AcctType, DECPLACS, CrdtAmt, DebAmt)  
				VALUES( @CtaXCobrar, 2, @DECPLACS, 0, @XTNDPRCE)
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SELECT @ErrorDesc = 'Error Creando Movimiento Contable CXC, Factura no. ' + @NOFACTURA 
				END
			END



		end

		FETCH NEXT FROM DetFactura into @DOCTYPE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @REMANENTE, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO, @TIPO
	END
-- 
-- FFB 2007-03-27
--INSERTAR LINEA RESUMEN IMP.
	SELECT @IVACNTREG = 0
	SELECT @IVACNTREG = COUNT(*) FROM sop10105 where sopnumbe = @NOFACTURA
	IF @IVACNTREG > 0 
	BEGIN
	  select @TOT_STAXAMNT = sum(STAXAMNT), @TOT_TAXDTSLS = sum(TAXDTSLS) from sop10105 where sopnumbe = @NOFACTURA
	  INSERT SOP10105 ( 
	    SOPTYPE       , SOPNUMBE      , LNITMSEQ      , TAXDTLID     , ACTINDX     , BKOUTTAX      , TXABLETX     , STAXAMNT  ,
		ORSLSTAX      , FRTTXAMT      , ORFRTTAX      , MSCTXAMT     , ORMSCTAX    , TAXDTSLS      , ORTOTSLS     , TDTTXSLS  ,
		ORTXSLS       , TXDTOTTX      , OTTAXPON      , DELETE1      , CURRNIDX    , TRXSORCE       )
		VALUES (
		3             , @NOFACTURA    , 0             , @TAXDTLID    , @IVA_ACTINDX, 0             , 0            , @TOT_STAXAMNT,
		@TOT_STAXAMNT , 0             , 0             , 0            , 0           ,@TOT_TAXDTSLS  ,@TOT_TAXDTSLS , @TOT_TAXDTSLS,
		@TOT_TAXDTSLS , 0             , 0             , 0            , @CURRNIDX   , ' '            )

	END
-- FFB 2007-03-27
-- FIN INSERTAR LINEA RESUMEN IMP.
--
	CLOSE DetFactura
	DEALLOCATE DetFactura
	DROP TABLE #CONSIGDET_HST
	DROP TABLE #def
	-- Dias de Antiguedad

	-- ojo2
	select @DiasAntiguedad = isnull( DUEDTDS, 0) from SY03300 where PYMTRMID in (select PYMTRMID from  RM00101 where custnmbr = @custnmbr)
	if @DiasAntiguedad is null
		SET @DiasAntiguedad = 0


	SELECT @DUEDTDS = DATEADD(day, @DiasAntiguedad, @FECHAHOY)

	-- Tabla de Localizacion
	IF @Update_Error = 0 
	BEGIN
		INSERT nsaTW_SOP10100
-- FFB 2008-01-18 -- Lista anterior de campos
-- ( SOPTYPE, SOPNUMBE, BCHSOURC, BACHNUMB, APPLYTAX      , TAXAMNT, ORTAXAMT, APPLYWTH,               WTHAMNT, OWTHAMNT  )
	( SOPTYPE, SOPNUMBE, BCHSOURC, BACHNUMB, ApplyTax, TAXAMNT, ORTAXAMT,
	 ApplyWth, WthAmnt , OWthamnt )
		VALUES( 3, @NOFACTURA, 'Sales Entry', 'LOT_CONSIG', 1, 0, 0, 0, 0, 0)
		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SELECT @ErrorDesc = 'Error Creando Encabezado de Impuesto Localizacion - nsaTW_SOP10100, Factura no. ' + @NOFACTURA 
		END
	END


	select @SUBTOTAL=SUM(XTNDPRCE)-(SUM(XTNDPRCE)*(DESCUENTO/100)) from sntconsigdethst where
	  noconsig=@NOFACTURA AND TIPO IN ( 'ITEM', 'KIT' ) GROUP BY DESCUENTO
	select @TRDISAMT=SUM(XTNDPRCE)*(DESCUENTO/100) from sntconsigdethst where

	  noconsig=@NOFACTURA AND TIPO IN ( 'ITEM', 'KIT' ) GROUP BY DESCUENTO

  IF @Update_Error = 0 
	BEGIN


		INSERT SOP10100 ( SOPTYPE, SOPNUMBE, ORIGTYPE, DOCID, DOCDATE, INVODATE, ReqShipDate, FUFILDAT, ACTLSHIP, DISCDATE, DUEDATE, TRXFREQU, 
		PYMTRMID, PRCLEVEL, LOCNCODE, BCHSOURC, BACHNUMB, CUSTNMBR, CUSTNAME, CSTPONBR, MSTRNUMB, PICTICNU, MRKDNAMT, ORMRKDAM, 
		PRBTADCD, PRSTADCD, CNTCPRSN, ShipToName, ADDRESS1, ADDRESS2, ADDRESS3, CITY, STATE, ZIPCODE, COUNTRY, PHNUMBR1, 
		PHNUMBR2, PHONE3, FAXNUMBR, COMMAMNT, OCOMMAMT, CMMSLAMT, ORCOSAMT, SHIPMTHD, TRDISAMT, ORTDISAM, TRDISPCT, SUBTOTAL, 
		ORSUBTOT,REMSUBTO, OREMSUBT, EXTDCOST, OREXTCST, TXENGCLD, TAXSCHID, TXSCHSRC, BSIVCTTL, FRTSCHID, FRTTXAMT, ORFRTTAX, 
		FRGTTXBL, MSCSCHID, MSCTXAMT, ORMSCTAX, MISCTXBL, TAXAMNT, ORTAXAMT, DOCAMNT, ORDOCAMT, ACCTAMNT, ORACTAMT, SALSTERR, 
		SLPRSNID, UPSZONE, ALLOCABY, NOTEINDX, CURNCYID, CURRNIDX)
		SELECT TOP 1 '3', @NOFACTURA, '0', @DOCID, @FECHAHOY, @FECHAHOY, @FECHAHOY, @FECHAHOY, @FECHAHOY, @FECHAHOY, @DUEDTDS, 0, 
		@PYMTRMID, @PRCLEVEL, LOCNConsig, 'Sales Entry', 'LOT_CONSIG', CUSTNMBR, CNTCPRSN, '', @MSTRNUMB, '', @TRDISAMT, @TRDISAMT, 
		@PRBTADCD, @PRSTADCD, CONTACT, ADRSCODE, ADDRESS1, ADDRESS2, '', CITY, STATE, ZIPCODE, @COUNTRY, @PHONE1, 
		@PHONE2, @PHONE3, @FAX, 0, 0, 0, 0, @SHIPMTHD, 0, 0, 0, @SUBTOTAL, 
		@SUBTOTAL, @SUBTOTAL, @SUBTOTAL, 0, 0, 1, TaxSchId, 2, 0, '',  0, 0, 
		1, '', 0, 0, 1, 0, 0, @SUBTOTAL, @SUBTOTAL, @SUBTOTAL, @SUBTOTAL, '',
		SLPRSNID, '', 0, 0, MONEDA, @CURRNIDX 
		FROM SntConsigHdrHst  where NOCONSIG = @NOFACTURA and doctype = 'F'


		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SELECT @ErrorDesc = 'Error Creando Encabezado Factura GP - SOP10100, Factura no. ' + @NOFACTURA 
		END
	END

	-- Actualizando Lote
	IF @Update_Error = 0 
	BEGIN
		SET @SumAmt=ISNULL(@SumAmt,0)
		if not exists( select 1 from SY00500 where BACHNUMB = 'LOT_CONSIG')
		BEGIN
			-- Agregar Lote ID
			INSERT SY00500 ( BCHSOURC, BACHNUMB, SERIES, NUMOFTRX, BACHFREQ, BCHCOMNT, BRKDNALL, BCHTOTAL, MODIFDT, CREATDDT, ORIGIN)
			VALUES( 'Sales Entry', 'LOT_CONSIG', 3, 1, 1, 'Consignaciones', 0, @SumAmt, @FECHAHOY, @FECHAHOY, 1 )
			IF @@Error <> 0 
			BEGIN
				set @Update_Error = -1
				SELECT @ErrorDesc = 'Error Agregando Lote - SY00500, Consignacion no. ' + @NOFACTURA 
			END
		END
		ELSE
		BEGIN
			UPDATE SY00500 SET BCHTOTAL = BCHTOTAL + @SumAmt, NUMOFTRX = NUMOFTRX + 1 where BACHNUMB = 'LOT_CONSIG'
			IF @@Error <> 0 
			BEGIN
				set @Update_Error = -1
				SELECT @ErrorDesc = 'Error Actualizando Lote - SY00500, Consignacion no. ' + @NOFACTURA 
			END
		END

	END


	-- Asiento de Distribucion Contable
	IF @Update_Error = 0 
	BEGIN

		DECLARE CursorAsiento CURSOR FOR 
		SELECT ActIndx, AcctType, DECPLACS, sum( CrdtAmt), SUM( DebAmt) FROM #DetalleContable group by ActIndx, AcctType, DECPLACS
		

		OPEN CursorAsiento
		
		SET @SQNCLINE = 0
		FETCH NEXT FROM CursorAsiento INTO @ActIndx, @AcctType, @DECPLACS, @CrdtAmt, @DebAmt
		WHILE (@@fetch_status <> -1)
		BEGIN
			IF (@@fetch_status <> -2)
			BEGIN
				SET @SQNCLINE = @SQNCLINE + 16384
				INSERT SOP10102 ( SOPTYPE, SOPNUMBE, SEQNUMBR, DISTTYPE, DistRef, ACTINDX, DEBITAMT, ORDBTAMT, CRDTAMNT, ORCRDAMT, CURRNIDX, TRXSORCE, POSTED)
				VALUES( 3, @NOFACTURA, @SQNCLINE, @AcctType, 'Factura Consignacion', @ActIndx, @DebAmt, @DebAmt, @CrdtAmt, @CrdtAmt, @CURRNIDX, ' ', 0)
				if @@Error <> 0
				begin
					set @Update_Error = -1
					SET @ErrorDesc = 'Error actualizando Distribucion Contable (Tabla:SOP10102)'
					BREAK
				end

			END
			FETCH NEXT FROM CursorAsiento INTO @ActIndx, @AcctType, @DECPLACS, @CrdtAmt, @DebAmt

		END
		
		CLOSE CursorAsiento
		DEALLOCATE CursorAsiento
	END

-- Impuesto Select * from sop10105


END


drop table #SntConsigDetHst
	
IF @Update_Error = 0
BEGIN
	COMMIT TRANSACTION LiquidarConsignacion
	-- Transaccion Correcta
	RETURN( 0)
END
ELSE
BEGIN
	ROLLBACK TRANSACTION LiquidarConsignacion
	-- Error de Transaccion
	RETURN( 1)
END



SET ANSI_WARNINGS ON



SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[Snt_ModifConsigeventual]    Script Date: 01/11/2009 21:23:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_ModifConsigeventual]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_ModifConsigeventual]
GO
--****************************************************
--*         Snt_ModifConsigeventual
--******************************************************
--*log de modificacion para el Snt_ModifConsigeventual *
--******************************************************
--*este procedure se ha creado para modificar LOS
--*REGISTROS DE LA TABLA IV00102 EN EL CAMPO ATYALLOC 
--*LA IDEA ES REBAJAR A ESTE CAMPO EL QUANTITY_SHP DE LA  
--*TABLA SntConsigDet SUMARIZADO POR ITEMS Y RESTARLOS EJEMPLO
--* (ATYALLOC - SntConsigDet.QUANTITY_SHP) 
--*DESPUES SE AUMENTARA EL MISMO CAMPO PERO  
--*ADICINANEDO EL CAMPO QUANTITY_ORD DE LA TABLA SntConsigDet
--*AL CAMPO ATYALLOC EJEMPLO (ATYALLOC - SntConsigDet.QUANTITY_ORD)
--* TODOD ESTO COMO PARTE DE LA MODIFICACION 
--*DEL procedure Snt_SalvarLineaConsig ve log de modif
--*************************************************
--*marca de modificacion: usuario + fecha de modifi
--**jlmejiasm-i modificacion 24-03-2006 
--**jlmejiasm-f modificacion 24-03-2006
--*************************************************
CREATE PROCEDURE Snt_ModifConsigeventual
  @DesError char( 255) out
AS

Declare @Update_Error int,
	@ItemnMbr char( 31),
	@LOCNCODE char( 11),
	@QUANTITY_SHP numeric( 19, 5),
	@QUANTITY_ORD numeric( 19, 5),
	@ATYALLOC numeric( 19, 5),
	@ATYALLOC_UPDATE numeric( 19, 5),
	@ATYALLOC_TOT numeric( 19, 5),
	@ATYALLOC_TOT2 numeric( 19, 5)
SET @Update_Error = 0

BEGIN TRANSACTION BorrarConsignacion

--*************************************************************
--*  		DECLARACION DE CURSOR DetConsig
--* PARA LA OBTENCION DE SUM(QUANTITY_ORD)SUMARIZADOS POR ITEMS  
--*************************************************************

DECLARE DetConsig CURSOR FOR 
select ITEMNMBR, SUM(QUANTITY_SHP),SUM(QUANTITY_ORD) from SntConsigDet GROUP BY ITEMNMBR

--*************************************************************
--*  		OPEN DE CURSOR DetConsig
--* SE ABRE EL CURSOR DetConsig
--*************************************************************

OPEN DetConsig

--*************************************************************
--*  		FETCH DE CURSOR DetConsig
--* SE HACE  EL FETCH DEL CURSOR DetConsig
--*************************************************************

FETCH NEXT FROM DetConsig into @ITEMNMBR,@QUANTITY_SHP,@QUANTITY_ORD


--*************************************************************
--*  	   PROCESAR ASTA FIN DEL CURSOR
--*************************************************************

WHILE @@FETCH_STATUS = 0
BEGIN
	IF @Update_Error <> 0 
		BREAK
--*************************************************************
--*     	Desasignar Almacen Central
--* EN ESTE PARRAFO SE REVERSAN LAS ASIGNACIONES HECHAS POR EL 
--* MODULO DE CONSIGANACION.. HACIENDO 
--* ATYALLOC = @ATYALLOC - @QUANTITY_SHP 
--************************************************************* 
	BEGIN
		select @ATYALLOC = ATYALLOC from IV00102 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE in ('central') 
		 AND RCRDTYPE = 2

		set @ATYALLOC_TOT = @ATYALLOC - @QUANTITY_SHP
		
		IF @ATYALLOC_TOT < 0 
		BEGIN
		    set @ATYALLOC_TOT = 0
		    PRINT 'SETIO A CERO'
		END

		UPDATE IV00102 
		 SET ATYALLOC  = @ATYALLOC_TOT
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE  in ('central') 
		 AND RCRDTYPE  = 2
	
		print 'el item es ' 	     + rtrim(@ItemnMbr)
		print 'el @ATYALLOC es '     + rtrim(@ATYALLOC)
		print 'el @QUANTITY_SHP es ' + rtrim(@QUANTITY_SHP)

		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
			SET @DesError = 'Error al actualizar ATYALLOC en iv00102 (' + rtrim(@ATYALLOC) + '), Item (' + rtrim(@itemnMbr)+')EL @QUANTITY_SHP'+ RTRIM(@QUANTITY_SHP)		
			print 'Error al desasignar unidades en almacen Origen CENTRAL('+ rtrim(@itemnMbr) + '), Tabla (IV00102)' 		
		END 
	
--*************************************************************
--*     	Asignar Almacen Central
--* ACA SE CARGA DE NUEVO EL VALOR DEL ATYALLOC PERO EN BASE AL
--* VALOR DE EL @QUANTITY_ORD DE LA TABLA SntConsigDet DE 
--* CONSIGNACIONES.
--************************************************************* 
		select @ATYALLOC_UPDATE = ATYALLOC from IV00102 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE IN ('CENTRAL') 
		 AND RCRDTYPE = 2

		UPDATE IV00102 
		SET ATYALLOC = @ATYALLOC_UPDATE + @QUANTITY_ORD 
		WHERE ITEMNMBR = @ItemnMbr 
		 AND LOCNCODE  IN ('CENTRAL') 
		 AND RCRDTYPE = 2

		print 'el item es2 ' 	 + rtrim(@ItemnMbr)
		print 'el @ATYALLOC UPDATE es2 ' + rtrim(@ATYALLOC_UPDATE)
		print 'el @QUANTITY_ORD UPDATE es2 ' + rtrim(@QUANTITY_ORD)
		
		IF @@Error <> 0 
		BEGIN
			set @Update_Error = -1
		 	SET @DesError = 'Error al actualizar ATYALLOC en iv00102 (' + rtrim(@ATYALLOC_UPDATE) + '), Item (' + rtrim(@itemnMbr)+')'
			print 'Error al desasignar unidades en almacen Origen (' + rtrim(@LocnCode) + '), Item (' + rtrim(@itemnMbr) 	
		END 
	END
	FETCH NEXT FROM DetConsig into @ITEMNMBR,@QUANTITY_SHP,@QUANTITY_ORD
END
--*************************************************************
--*  	   		CERRAR CURSOR
--*se cierra el cursor y se dealoca el cursor
--*************************************************************
CLOSE DetConsig
DEALLOCATE DetConsig

--*************************************************************
--*  	   	  HACER COMMIT
--* SI NO HAY ERROR DE HACE EL COMMIT DE LAS ACTUALIZACIONES
--*************************************************************  
IF @Update_Error = 0
BEGIN
	COMMIT TRANSACTION BorrarConsignacion
	-- Transaccion Correcta
END
ELSE
BEGIN
	ROLLBACK TRANSACTION BorrarConsignacion
	-- Error de Transaccion
END
--*************************************************************
--*  	   	          F . I . N
--*************************************************************  


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[Snt_SalvarLineaConsig]    Script Date: 01/11/2009 21:40:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_SalvarLineaConsig]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_SalvarLineaConsig]
GO
--****************************************************
--*         Snt_SalvarLineaConsig
--****************************************************
--*log de modificacion para el Snt_SalvarLineaConsig *
--****************************************************
--*este store procedure se ha modificado para
--*que al momento de hacer actualizaciones a 
--*las tablas iv00102 se ralize en vez de 
--*la variable @QUANTITY_SHP con la variable
--*@QUANTITY_ORD para actualizar el campo 
--*ATYALLOC al momento de incrementarlo,la idea de
--*esta modificacion es comprometer la mercancia   
--*al momento de tener la intencion de hacer una
--*consignacion.
--*************************************************
--*marca de modificacion: usuario + fecha de modifi
--**jlmejiasm-i modificacion 23-03-2006 
--**jlmejiasm-f modificacion 23-03-2006
--**jlmejiasm-i modificacion 10-07-2007 mosdificacion para evitar insert de reg duplicados
--**jlmejiasm-f modificacion 10-07-2007
--**JLMEJIASM-I 16-08-2007 SE HA MODIFICADO LA MODIFICACION DE FELIX BOLIVAR FFB 2007-07-14 PARA QUE SE LE PUEDA PONER CUALQUIER PRECIO A 
--**FBOLIVAR 10-10-2007 MODIFICACION LONGITUD VARIABLE @ITEMDESC DE 51 A 101
--*************************************************
CREATE                     PROCEDURE Snt_SalvarLineaConsig
  @NoConsig char(21), @ItemnMbr char( 31), @SEQUENCE integer, @ITEMDESC char(101), @UOFM char( 9), @UNITPRCE decimal (19,5), 
  @UNITCOST decimal (19,5), @XTNDPRCE decimal (19,5), @QUANTITY_ORD integer, @QUANTITY_SHP integer, 
  @LOCNCODE char(11), @PRICELVL char( 15), @DESCUENTO decimal (5,2), @IMPUESTO decimal (19,5),
  @TIPO CHAR(60), --JCHERNANDEZ MODIFICACI�N 17/10/2006
  @DesError char( 255) out
AS
declare @UnidadesAsignar integer,

--*jlmejiasm-i modificacion 23-03-2006
	@UnidadesAsignarOrd integer,
	@ATYALLOCOrd        decimal( 19, 5),
--*jlmejiasm-f modificacion 23-03-2006
	@Existencia integer,
	@Update_Error int,
	@ATYALLOC decimal( 19, 5),
	@RegistroNuevo char(1),
--modificaci�n Jean 04-09-2006
--JLMEJIASM-I 16-08-2007
	@UNITPRCE_AUX decimal (19,5),
	@SI_PRIMERA_VEZ INT,	
--JLMEJIASM-F 16-08-2007
	@AUX2 NUMERIC,
	@aux NUMERIC,
	@PRECIO decimal (19,5) -- FFB 2007-07-14 VAR DEFINIDA POR PROBLEMAS CON PRECIO 0 PARA KITS
--JLMEJIASM-I 22-08-2007  
set @SI_PRIMERA_VEZ = 0
--JLMEJIASM-F 22-08-2007  
set @Update_Error = 0
set @DesError = ''
	
SET @RegistroNuevo = '0'

-- Modificar registro
if exists( select 1 from SntConsigDet where NoConsig = @NoConsig and ItemnMbr = @ItemnMbr AND TIPO=@TIPO)--VARIABLE TIPO DETERMINA SI ES KIT, HIJO DE KIT, ITEM O ELEMENTO DE KIT COMO NORMAL
begin

	select @UnidadesAsignar = @QUANTITY_SHP - QUANTITY_SHP from SntConsigDet where NoConsig = @NoConsig and ItemnMbr = @ItemnMbr
	SET @RegistroNuevo = '0'

--*jlmejiasm-i modificacion 23-03-2006
	select @UnidadesAsignarOrd = @QUANTITY_ORD - QUANTITY_ORD from SntConsigDet where NoConsig = @NoConsig and ItemnMbr = @ItemnMbr
	SET @RegistroNuevo = '0'
--*jlmejiasm-f modificacion 23-03-2006
end
else
begin
	SET @RegistroNuevo = '1'

 	select @UnidadesAsignar = @QUANTITY_SHP

--*jlmejiasm-I modificacion 23-03-2006
	select @UnidadesAsignarOrd = @QUANTITY_ORD
--*jlmejiasm-f modificacion 23-03-2006
end

--*jlmejiasm-I modificacion 23-03-2006
if @UnidadesAsignar > 0  
--*jlmejiasm-I modificacion 23-03-2006
	if @QUANTITY_SHP > @QUANTITY_ORD
	BEGIN
		SET @Update_Error = -1
		SET @DesError = 'Cantidad a Enviar no puede ser mayor a cantidad Ordenada'		
		return( @Update_Error)   
	END

	select @aux=count(*) from iv00104 where itemnmbr=@ITEMNMBR

begin transaction  

--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
--@  Actualizar Great Plains Asignar Unidades   @
--@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
-- Validar asignacion

--MODIFICACION PARA QUE SE TOME EN CUENTA LAS CANTIDADES TANTO DE KITS COMO DE INDIVIDUALES
IF @aux=0 BEGIN
	select @Existencia = (case when QTYONHND - ATYALLOC <= 0 then 0 else QTYONHND - ATYALLOC end) from IV00102 
	WHERE itemnmbr = @ItemnMbr and locncode = @LocnCode AND RCRDTYPE = 2
END
ELSE 
BEGIN
	SELECT @Existencia = MIN(case when QTYONHND - ATYALLOC <= 0 then 0 else QTYONHND - ATYALLOC end) FROM IV00102 
	WHERE ITEMNMBR IN (SELECT CMPTITNM FROM IV00104 WHERE ITEMNMBR='M12DBV') AND LOCNCODE='YAGUARA'
END


--*jlmejiasm-I modificacion 23-03-2006
--*SET @ATYALLOC    = @UnidadesAsignar
   SET @ATYALLOC    = @UnidadesAsignarOrd
--*jlmejiasm-f modificacion 23-03-2006

-- No hay suficiente Existencia
    IF @ATYALLOC > 0 
	

	IF @Existencia - @ATYALLOC < 0
	BEGIN
		SET @Update_Error = -1
		SET @DesError = 'Imposible Asignar, No hay suficiente Existencia en almacen (' + rtrim(@LocnCode) + '), Item (' + rtrim(@itemnMbr) + '), Disponible: ' + convert( char(6), @Existencia) + ', Tabla (IV00102)'		

	END


--JLMEJIASM-I 16-08-2007
---- FFB 2007-07-14
---- UPDATE DEL PRECIO, COSTO y EXTENDED_PRICE

----SELECT @UNITPRCE = B.LISTPRCE, @UNITCOST = A.CURRCOST FROM
----  IV00101 AS A INNER JOIN IV00105 AS B ON A.ITEMNMBR=B.ITEMNMBR AND A.ITEMNMBR = @ITEMNMBR

---- SELECT @XTNDPRCE = @QUANTITY_ORD * @UNITPRCE
---- FFB 2007-07-14 FIN


IF  @UNITPRCE = 0
BEGIN
SELECT @UNITPRCE = B.LISTPRCE, @UNITCOST = A.CURRCOST FROM
  IV00101 AS A INNER JOIN IV00105 AS B ON A.ITEMNMBR=B.ITEMNMBR AND A.ITEMNMBR = @ITEMNMBR
END 
--JLMEJIASM-F 16-08-2007



	
IF @RegistroNuevo = '1' 
BEGIN
	select @SEQUENCE = isnull( max(SEQUENCE), 0) + 1 from SntConsigDet where NoConsig = @NoConsig

	IF @aux>0 --SI ES UN KIT SE INSERTAN EN EL DETALLE LOS PRODUCTOS CONTENIDOS
	BEGIN
--jlmejiasm-i 10-07-2007
--		SELECT @SEQUENCE=MAX (SEQUENCE+1) FROM SntConsigDet 
--	FFB CAMBIO INSTRUCCION PARA EVITAR SEQUENCE NULL
		SELECT @SEQUENCE= isnull( max(SEQUENCE), 0) + 10 FROM SntConsigDet where NOCONSIG = @NOCONSIG
		SET @SEQUENCE = @SEQUENCE + 11

--jlmejiasm-f 10-07-2007

		INSERT INTO SntConsigDet (NOCONSIG, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO) 
		VALUES( @NOCONSIG, @SEQUENCE, @ITEMNMBR, @ITEMDESC,@UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO, @TIPO)


		DECLARE DETALLE CURSOR FOR 
			SELECT A.CMPTITNM, B.ITEMDESC
			FROM IV00104 AS A INNER JOIN IV00101 AS B ON A.CMPTITNM=B.ITEMNMBR
			WHERE A.ITEMNMBR=@ITEMNMBR

		OPEN DETALLE

		DECLARE @SUBITEM VARCHAR(50)

		FETCH NEXT FROM DETALLE INTO 
		@SUBITEM, @ITEMDESC
		SET @SEQUENCE=@SEQUENCE + 1
					
		WHILE @@FETCH_STATUS=0
		BEGIN

--jlmejiasm-i 10-07-2007
--			INSERT INTO SntConsigDet (NOCONSIG, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO) 
--			SELECT @NOCONSIG, @SEQUENCE, @SUBITEM, @ITEMDESC, @UOFM, B.LISTPRCE, A.CURRCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO, 'HIJO DE '+@ITEMNMBR FROM IV00101 AS A INNER JOIN IV00105 AS B ON A.ITEMNMBR=B.ITEMNMBR AND A.ITEMNMBR=@SUBITEM			
--			VALUES (@NOCONSIG, @SEQUENCE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO)
--JLMEJIASM-I 16-08-2007
			SET @UNITPRCE_AUX = 0
			SET @UNITPRCE_AUX = @UNITPRCE			
--JLMEJIASM-F 16-08-2007									
			select @UNITPRCE = B.LISTPRCE, @UNITCOST = A.CURRCOST FROM IV00101 AS A INNER JOIN IV00105 AS B ON A.ITEMNMBR=B.ITEMNMBR AND A.ITEMNMBR=@SUBITEM
--JLMEJIASM-I 16-08-2007			
			IF @UNITPRCE_AUX <> 0 
			BEGIN 
				SET @UNITPRCE = @UNITPRCE_AUX
			END

			IF EXISTS(SELECT ITEMNMBR FROM IV00104 WHERE CMPTITNM = @SUBITEM)
			BEGIN  
				select @UNITPRCE = B.LISTPRCE FROM IV00101 AS A INNER JOIN IV00105 AS B ON A.ITEMNMBR=B.ITEMNMBR AND A.ITEMNMBR=@SUBITEM			
			END 
--JLMEJIASM-F 16-08-2007

			INSERT INTO SntConsigDet (NOCONSIG, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO) 
			VALUES (@NOCONSIG, @SEQUENCE, @SUBITEM, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO,'HIJO DE '+ @ITEMNMBR)

--jlmejiasm-f 10-07-2007

			FETCH NEXT FROM DETALLE INTO 
			@SUBITEM, @ITEMDESC
			SET @SEQUENCE=@SEQUENCE+1
		END

		CLOSE DETALLE
		DEALLOCATE DETALLE

		
	END
	ELSE BEGIN
		INSERT SntConsigDet (NOCONSIG, SEQUENCE, ITEMNMBR, ITEMDESC, UOFM, UNITPRCE, UNITCOST, XTNDPRCE, QUANTITY_ORD, QUANTITY_SHP, LOCNCODE, PRICELVL, DESCUENTO, IMPUESTO, TIPO) 
		VALUES( @NOCONSIG, @SEQUENCE, @ITEMNMBR, @ITEMDESC, @UOFM, @UNITPRCE, @UNITCOST, @XTNDPRCE, @QUANTITY_ORD, @QUANTITY_SHP, @LOCNCODE, @PRICELVL, @DESCUENTO, @IMPUESTO, @TIPO)

		-- FFB 2007-07-10
		SET @SEQUENCE=@SEQUENCE+1
	END
--JLMEJIASM-I 22-08-2007
	set @SI_PRIMERA_VEZ = '1'
--JLMEJIASM-F 22-08-2007
END


-- Cantidad asignada no debe quedar en negativo
-- @ATYALLOC >= 0 AND 
--*jlmejiasm-I modificacion 23-03-2006
--*IF @Existencia - @UnidadesAsignar >= 0 AND @Update_Error = 0
IF @Existencia - @UnidadesAsignarOrd >= 0 AND @Update_Error = 0
--*jlmejiasm-f modificacion 23-03-2006
begin   
--*jlmejiasm-I modificacion 23-03-2006
--*	if @UnidadesAsignar <> 0 
	if @UnidadesAsignarOrd <> 0 
--*jlmejiasm-f modificacion 23-03-2006	
	BEGIN
		if @aux>0 begin --Si es un kit
			
			SELECT @AUX2=COUNT(*) FROM IV00104 WHERE CMPTITNM NOT IN (SELECT ITEMNMBR FROM IV00102 WHERE ITEMNMBR IN (SELECT CMPTITNM FROM IV00104 where itemnmbr=@ItemnMbr) AND LOCNCODE=@LocnCode) AND itemnmbr=@ItemnMbr

			if @AUX2=0
			BEGIN
			-- Asignar en almacen Origen
				UPDATE IV00102
				SET ATYALLOC = ATYALLOC + @UnidadesAsignarOrd
				WHERE ITEMNMBR = @ItemnMbr
				     AND LOCNCODE = @LocnCode AND RCRDTYPE = 2


				UPDATE IV00102
				SET ATYALLOC = ATYALLOC + @UnidadesAsignarOrd
				WHERE ITEMNMBR  IN (SELECT CMPTITNM FROM IV00104 WHERE ITEMNMBR=@ItemnMbr)
				     AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
		
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @DesError = 'Error al Asignar en almacen Origen (' + rtrim(@LocnCode) + '), Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
				END
			END
			ELSE
			BEGIN
				SET @Update_Error = -1
				SET @DesError = 'Item (' + rtrim(@itemnMbr) + '), No existe en el almacen (' + rtrim(@LocnCode) + '),  Tabla (IV00102)'		
			END
			if @Update_Error = 0 
			begin
				SELECT @AUX2=COUNT(*) FROM IV00104 WHERE CMPTITNM NOT IN (SELECT ITEMNMBR FROM IV00102 WHERE ITEMNMBR IN (SELECT CMPTITNM FROM IV00104 where itemnmbr=@ItemnMbr) AND LOCNCODE=@LocnCode) AND itemnmbr=@ItemnMbr

				if @AUX2=0
				BEGIN
				-- Asignar en almacen Consolidado
					UPDATE IV00102
					SET ATYALLOC = ATYALLOC + @UnidadesAsignarOrd
					WHERE ITEMNMBR = @ItemnMbr
					     AND LOCNCODE = '' AND RCRDTYPE = 1
		
					UPDATE IV00102
					SET ATYALLOC = ATYALLOC + @UnidadesAsignarOrd
					WHERE ITEMNMBR IN (SELECT CMPTITNM FROM IV00104 WHERE ITEMNMBR= @ItemnMbr)
					     AND LOCNCODE = '' AND RCRDTYPE = 1


					IF @@Error <> 0 
					BEGIN
						set @Update_Error = -1
						SET @DesError = 'Error al Asignar en almacen consolidado, Item (' + rtrim(@itemnmbr) + '), Tabla (IV00102)'		
					END
				end
				ELSE
				BEGIN
					SET @Update_Error = -1
					SET @DesError = 'Item (' + rtrim(@itemnMbr) + '), No existe en el almacen Consolidado,  Tabla (IV00102)'		
				END
			END
		end else begin
			if exists( select TOP 1 itemnmbr from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = @LocnCode AND RCRDTYPE = 2)
			BEGIN
			-- Asignar en almacen Origen
				UPDATE IV00102
--*jlmejiasm-I modificacion 23-03-2006
--*			SET ATYALLOC = ATYALLOC + @UnidadesAsignar
				SET ATYALLOC = ATYALLOC + @UnidadesAsignarOrd
--*jlmejiasm-F modificacion 23-03-2006
				WHERE ITEMNMBR = @ItemnMbr
				     AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
		
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @DesError = 'Error al Asignar en almacen Origen (' + rtrim(@LocnCode) + '), Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
				END
			END
			ELSE
			BEGIN
				SET @Update_Error = -1
				SET @DesError = 'Item (' + rtrim(@itemnMbr) + '), No existe en el almacen (' + rtrim(@LocnCode) + '),  Tabla (IV00102)'		
			END
			if @Update_Error = 0 
			begin
				if exists( select TOP 1 itemnmbr from IV00102 WHERE ITEMNMBR = @ItemnMbr AND LOCNCODE = '' AND RCRDTYPE = 1)
				BEGIN
				-- Asignar en almacen Consolidado
					UPDATE IV00102
					SET ATYALLOC = ATYALLOC + @UnidadesAsignarOrd
					WHERE ITEMNMBR = @ItemnMbr
					     AND LOCNCODE = '' AND RCRDTYPE = 1
		
					IF @@Error <> 0 
					BEGIN
						set @Update_Error = -1
						SET @DesError = 'Error al Asignar en almacen consolidado, Item (' + rtrim(@itemnmbr) + '), Tabla (IV00102)'		
					END
				end
				ELSE
				BEGIN
					SET @Update_Error = -1
					SET @DesError = 'Item (' + rtrim(@itemnMbr) + '), No existe en el almacen Consolidado,  Tabla (IV00102)'		
				END
			END
		END
	END
	if @Update_Error = 0 
	BEGIN
--JLMEJIASM-I 22-08-2007
	      IF  @SI_PRIMERA_VEZ = 0
--JLMEJIASM-F 22-08-2007
	      BEGIN	
			if @aux>0 begin

				UPDATE SntConsigDet
				 SET UNITPRCE = @UNITPRCE, XTNDPRCE = @XTNDPRCE, QUANTITY_ORD = @QUANTITY_ORD, QUANTITY_SHP = @QUANTITY_SHP, DESCUENTO = @DESCUENTO, IMPUESTO = @IMPUESTO
				WHERE  NoConsig = @NoConsig and ItemnMbr = @ItemnMbr

				UPDATE SntConsigDet SET QUANTITY_ORD = @QUANTITY_ORD, QUANTITY_SHP = @QUANTITY_SHP, DESCUENTO = @DESCUENTO, IMPUESTO = @IMPUESTO
				WHERE  NoConsig = @NoConsig and ItemnMbr IN (SELECT CMPTITNM FROM IV00104 WHERE ITEMNMBR=@ItemnMbr) and tipo='HIJO DE '+@ItemnMbr
			
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @DesError = 'Error actualizando linea (SntConsigDet), Item (' + rtrim(@itemnmbr) + ')'		
				END
		
		end else begin
			UPDATE SntConsigDet SET UNITPRCE = @UNITPRCE, XTNDPRCE = @XTNDPRCE, QUANTITY_ORD = @QUANTITY_ORD, QUANTITY_SHP = @QUANTITY_SHP, DESCUENTO = @DESCUENTO, IMPUESTO = @IMPUESTO
			WHERE  NoConsig = @NoConsig and ItemnMbr = @ItemnMbr
			
			IF @@Error <> 0 
			BEGIN
				set @Update_Error = -1
				SET @DesError = 'Error actualizando linea (SntConsigDet), Item (' + rtrim(@itemnmbr) + ')'		
			END
		end
--JLMEJIASM-I 22-08-2007
	    END	
--JLMEJIASM-F 22-08-2007
	END
	


end  

-- FFB 2007-07-14
-- UPDATE DEL PRECIO, COSTO y EXTENDED_PRICE
--JLMEJIASM-I 16-07-2007 SE DEBE AGREGAR EL CAMPO CODIGO DE LA MONEDA(PENDIENTE)
--SELECT @PRECIO = B.LISTPRCE FROM IV00105 AS B WHERE B.ITEMNMBR = @ITEMNMBR
SELECT @PRECIO = B.LISTPRCE FROM IV00105 AS B WHERE B.ITEMNMBR = @ITEMNMBR 
AND CURNCYID = 'BOLIVARES'
--JLMEJIASM-F 16-07-2007 SE DEBE AGREGAR EL CAMPO CODIGO DE LA MONEDA(PENDIENTE)
 
--
UPDATE SntConsigDet
  SET UNITPRCE = @PRECIO
WHERE  NoConsig = @NoConsig and ItemnMbr = @ItemnMbr and UNITPRCE = 0

-- FFB 2007-07-14 FIN

if @Update_Error = 0   
begin
	commit transaction  
end
else begin
	rollback transaction  

return( @Update_Error)   
end





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[Snt_concilia_Cnsg]    Script Date: 01/13/2009 18:03:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_concilia_Cnsg]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_concilia_Cnsg]
GO

CREATE PROCEDURE Snt_concilia_Cnsg
 @DesError char( 255) out
AS
declare @Quantity_Shp integer,
	@Quantity_Asig integer, 
	@Existencia integer,
	@LocnCode char( 11),
	@ItemnMbr char(31),
	@Update_Error int

set @Update_Error = 0
set @DesError = ''

BEGIN TRANSACTION concilia_Cnsg

-- Cursor para calcular cantidad a despachar del almacen origen
DECLARE	Cursor_Asignar CURSOR 
FOR SELECT ITEMNMBR, LOCNCODE, SUM(QUANTITY_SHP) "qty_shp" from sntconsigdet
 where LOCNCODE <> ''
 GROUP BY ITEMNMBR, LOCNCODE
 HAVING SUM(QUANTITY_SHP) > 0


OPEN Cursor_Asignar
FETCH NEXT FROM Cursor_Asignar INTO @ItemnMbr, @LocnCode, @Quantity_Shp

WHILE (@@fetch_status <> -1)
BEGIN
	IF @Update_Error <> 0
		Break

	-- Solo procesor lo que tenga unidades asignadas
	IF (@@fetch_status <> -2) and @Quantity_Shp > 0
	BEGIN

		SELECT @Existencia  = 0, @Quantity_Asig = 0

		-- Verificar Existencia

		SELECT @Quantity_Asig = @Existencia + @Quantity_Shp

		if @Quantity_Asig > 0 
		BEGIN
			if @Update_Error = 0 
			BEGIN
				-- Asignar en almacen Origen
				UPDATE IV00102 
				SET ATYALLOC = ATYALLOC + @Quantity_Asig
				WHERE ITEMNMBR = @ItemnMbr
				     AND LOCNCODE = @LocnCode AND RCRDTYPE = 2
			
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @DesError = 'Error al Asignar en almacen Origen (' + rtrim(@LocnCode) + '), Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
				END
			END

			if @Update_Error = 0 
			BEGIN
				-- Asignar en almacen Global
				UPDATE IV00102 
				SET ATYALLOC = ATYALLOC + @Quantity_Asig
				WHERE ITEMNMBR = @ItemnMbr
				     AND LOCNCODE = '' AND RCRDTYPE = 1
			
				IF @@Error <> 0 
				BEGIN
					set @Update_Error = -1
					SET @DesError = 'Error al Asignar en almacen Global Item (' + rtrim(@itemnMbr) + '), Tabla (IV00102)'		
				END
			END

		END

	END
	FETCH NEXT FROM Cursor_Asignar INTO @ItemnMbr, @LocnCode, @Quantity_Shp
END

CLOSE Cursor_Asignar
DEALLOCATE Cursor_Asignar 


if @Update_Error = 0   
	COMMIT TRANSACTION concilia_Cnsg
else
	ROLLBACK TRANSACTION concilia_Cnsg

return( @Update_Error) 

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[Snt_devoluciones_batch]    Script Date: 01/11/2009 21:41:45 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Snt_devoluciones_batch]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Snt_devoluciones_batch]
GO
CREATE      PROCEDURE Snt_devoluciones_batch
	@NoControl char(31)
--*******************************
--*		Paso I     
--* se puede corre mas de una vez 
--* pero se actualizara el # de devolucion
--PROCESO BATCH PARA CARGAR 
--* DEVOLUCIONES DESDE LOGISTICA
--* VER. 1.5
--*******************************
AS
DECLARE 
	@Update_Error int,
	@DEVONUMB   char( 30),
	@PROX_DEVN  	  INT,
	@return_status	  INT, -- FFB 2007-06-07
	@PROX_DEVC  char( 30),
	@NoAplica   CHAR( 30), 
	@ITEM	    CHAR( 40),
	@ITEM_DEV   CHAR( 40), 
	@CUSTNMBR   CHAR( 50),
	@TIPO	    CHAR(100),
	@ErrorDesc  char( 255),
	@ID_DEV	    numeric(18, 0),
	@Saldo	    numeric(19, 5),
	@SALDO_REST NUMERIC(19, 5),
	@SALDO_CNSG NUMERIC(19, 5),
	@DEVUELTO   NUMERIC(19, 5),
	@DEVUELTO_F NUMERIC(19, 5),
	@MAX_DEVOL  NUMERIC(19, 5)
--
-- FFB 2007-06-08 inicializa variable
SET NOCOUNT ON
SET @Update_Error = 0
--
-- valida si ya fue creada esta devolucion logistica en
--  snt_statusdev
IF NOT EXISTS(SELECT * FROM snt_statusdev WHERE cod_dev = @NoControl)
BEGIN
--
--BEGIN TRANSACTION Snt_devoluciones_batch
--*************************************
--*      PROX NUM DEVOLUCION
--*************************************
select top 1 @DEVONUMB = DEVONUMB from SntConsigCfg
--********************************************
--SELECT 'NUMERO DEVOLUCION A USAR:', @DEVONUMB
--********************************************
--**************************************
-- Actualiza correlativo en SNTCONSIGCFG
--**************************************
SET @PROX_DEVN = convert(int, SUBSTRING(@DEVONUMB,4,8) + 1 )
SET @PROX_DEVC = 'DEV' + replicate('0',(8 - len(convert(char,@PROX_DEVN)))) + convert(char, @PROX_DEVN )
-- SELECT 'Correlativo SNTCONSIGCFG Actualizado a:', @PROX_DEVC
--**************************************+
-- YA ACTUALIZA PROXIMO No DEVOLUCION ***
--***************************************
UPDATE SntConsigCfg SET DEVONUMB = @PROX_DEVC
--
if @@Error <> 0
begin
  set @Update_Error = -1
  SET @ErrorDesc = '-2,Error actualizando correlativo para proximo numero de devolucion'
	raiserror(@ErrorDesc,17,1)
	return 1
--  print 'error uno ojo'	
end
--
-- ----------------
-- ITEMS A DEVOLVER
-- ----------------
--IF EXISTS(SELECT NAME FROM tempdb..SYSOBJECTS WHERE NAME like '#encabezado%')
-- BEGIN
--   DROP TABLE #encabezado
--   DROP TABLE #encabezado_tmp
-- END
--
CREATE TABLE #encabezado
 ( NoAplica char(21)        , ITEMNMBR char( 31)        , ITEMDESC char( 51)    , Saldo numeric( 19, 5), Devuelto numeric( 19, 5),
   difer_dev numeric( 19, 5), Devlt_Def numeric( 19, 5) , Danado numeric( 19, 5), Consecutivo integer  , Tipo varchar(100))
--
-- print 'creo la tabla de emcabezado ojo'
--******************************
-- Obtener codigo de cliente
--******************************
select @CUSTNMBR = CUSTNMBR from rm00101 where custname in 
( select NB_CLTE_DEV from t_sant_dev where COD_DEV = @NoControl )
--
-- select @CUSTNMBR
--
-- Obtener ID_DEV
select @ID_DEV = ID_DEV from t_sant_dev where COD_DEV = @NoControl AND ID_ST = 3
-- ID_ST debe ser 3 status procesada
-- SELECT @ID_DEV 
-- 
--********************************
--* Insertar items desde Logistica+
--********************************
INSERT INTO #encabezado
     ( ITEMNMBR , Saldo, Devuelto      , difer_dev, Devlt_Def, Danado, Consecutivo , Tipo )
SELECT ID_PROD  , 0    , SUM(CANT_PROD), 0        , 0        , 0     , 0           , ''
  from t_sant_det_caja_dev where ID_DEV = @ID_DEV
  GROUP BY ID_PROD
--
--
if @@Error <> 0
begin
  set @Update_Error = -1
  SET @ErrorDesc = '-2,Error insertando #encabezado' + rtrim(@ID_DEV)
	raiserror(@ErrorDesc,17,1)
	return 2
end
--
--****************************************
-- Asignar saldo disponible en el cliente
--  para devolver
--****************************************
DECLARE Saldo_clt CURSOR FOR
SELECT ITEMNMBR, sum(SALDO) FROM snt_devolver_vw WHERE CUSTNMBR = @CUSTNMBR
 AND ITEMNMBR IN (SELECT DISTINCT ITEMNMBR FROM #encabezado)
 GROUP BY ITEMNMBR
--
OPEN Saldo_clt 
--
FETCH NEXT FROM Saldo_clt  INTO @ITEM, @SALDO
WHILE @@FETCH_STATUS=0 BEGIN
--***********************************
-- VALIDAR CANTIDAD MINIMA DE COMP
--  DE KITS
--***********************************
IF EXISTS( SELECT * FROM IV00101 WHERE ITEMTYPE = 3 AND ITEMNMBR = @ITEM ) BEGIN
-- SELECT @DEVUELTO_F = Devuelto FROM #encabezado WHERE #encabezado.ITEMNMBR = @ITEM
--
  SELECT @MAX_DEVOL = MIN(SALDO) FROM snt_saldoHkits_vw
   WHERE CUSTNMBR = @CUSTNMBR
  AND ITEMNMBR IN ( SELECT DISTINCT CMPTITNM FROM IV00104 WHERE ITEMNMBR = @ITEM )
  HAVING MIN(SALDO) > 0
--
--
  IF @MAX_DEVOL < @SALDO BEGIN
	UPDATE #encabezado
	 SET Saldo = @MAX_DEVOL
	WHERE #encabezado.ITEMNMBR = @ITEM
  END
  ELSE BEGIN
	UPDATE #encabezado
	 SET Saldo = @SALDO
	WHERE #encabezado.ITEMNMBR = @ITEM
  END
END
ELSE BEGIN --  SI NO ES KIT
	UPDATE #encabezado
	 SET Saldo = @SALDO
	WHERE #encabezado.ITEMNMBR = @ITEM
END
	FETCH NEXT FROM Saldo_clt  INTO @ITEM, @SALDO
END
CLOSE Saldo_clt 
DEALLOCATE Saldo_clt 
--***********************************************
-- Actualiza items que no se pueden devolver
-- completos y registra la diferencia
--***********************************************
UPDATE #encabezado
 SET difer_dev = CASE WHEN Devuelto > Saldo THEN Devuelto - Saldo else 0 end,
     Devlt_Def = CASE WHEN Devuelto > Saldo THEN Saldo else Devuelto end
--
IF @@Error <> 0 
BEGIN
   set @Update_Error = -1
   SET @ErrorDesc = '-2,Error Actualizando #encabezado, campo -devuelto-'
	raiserror(@ErrorDesc,17,1)
	return 3
--   print 'error tres ojo'
END
--******************************
-- Actualiza unidades con
--  cantidad da�ado
--******************************
UPDATE #encabezado
 SET danado = t_sant_det_caja_dev.CANT_PROD
FROM t_sant_det_caja_dev where ID_DEV = @ID_DEV
  AND ST_PROD = 2
  AND t_sant_det_caja_dev. ID_PROD = #encabezado.ITEMNMBR 
--
IF @@Error <> 0 
BEGIN
   set @Update_Error = -1
   SET @ErrorDesc = '-2,Error Actualizando #encabezado, cantidad -da�ado-'
	raiserror(@ErrorDesc,17,1)
	return 4
--   print 'error cuatro ojo'
END
--
--****************************************
-- Actualiza items que son componentes
-- de Kits para que no se puedan devolver
--****************************************
UPDATE #encabezado
 SET difer_dev = Devuelto,
     Devlt_Def = 0,
     TIPO     = 'HIJO DE KIT...'
 WHERE ITEMNMBR IN (SELECT DISTINCT CMPTITNM FROM IV00104)
--
IF @@Error <> 0 
BEGIN
   set @Update_Error = -1
   SET @ErrorDesc = '-2,Error Actualizando #encabezado difer_dev,devlt,tipo'
	raiserror(@ErrorDesc,17,1)
	return 5
--   print 'error cinco ojo'	
END
--
--*******************************
-- Actualiza descripcion Item
--*******************************
UPDATE #encabezado
 SET ITEMDESC = iv00101.ITEMDESC,
     TIPO     = 'ITEM'
 FROM iv00101
 WHERE #encabezado.ITEMNMBR = iv00101.ITEMNMBR
 AND   #encabezado.ITEMNMBR NOT IN (SELECT DISTINCT CMPTITNM FROM IV00104)
--
IF @@Error <> 0 
BEGIN
   set @Update_Error = -1
   SET @ErrorDesc = '-2,Error Actualizando #encabezado: descripcion del item'
	raiserror(@ErrorDesc,17,1)
	return 6
--   print 'error seis ojo'
END
--
--********************************
-- Actualiza campo TIPO PARA KITS
--********************************
UPDATE #encabezado
 SET TIPO     = 'KIT'
 FROM iv00101
 WHERE #encabezado.ITEMNMBR = iv00101.ITEMNMBR
 AND ITEMTYPE = 3
--
IF @@Error <> 0 
BEGIN
   set @Update_Error = -1
   SET @ErrorDesc = '-2,Error Actualizando #encabezado: campo TIPO para kits'
	raiserror(@ErrorDesc,17,1)
	return 7
--      print 'error siete ojo'
END
--
--***************************************
-- Crea tabla de trabajo #encabezado_tmp
--***************************************
SELECT * INTO #encabezado_tmp from #encabezado where 1=2
--
--*************************************
-- transfiere todos los datos a tabla
-- de trabajo
--*************************************
INSERT INTO #encabezado_tmp
 SELECT * FROM #encabezado
--
IF @@Error <> 0 
BEGIN
   set @Update_Error = -1
   SET @ErrorDesc = '-2,Error insertando #encabezado_tmp DESDE #encabezado'
	raiserror(@ErrorDesc,17,1)
	return 8
--   print 'error oho ojo'
END
-- print 'borro la p... tabla de encabesados ojo '
--**************************
-- Borra tabla encabezado
--**************************
DELETE FROM #encabezado
--
-- 
-- SELECT * FROM #encabezado_tmp
-- compute sum(saldo), sum(devuelto), sum(difer_dev), sum(devlt_def)
--
--**********************************************************
-- CURSOR PARA ASIGNAR LOS NoAplica
-- a las devoluciones previo a la ejecucion del SP devolver
--**********************************************************
DECLARE Saldo_cltAplic CURSOR FOR
SELECT ITEMNMBR, sum(Devlt_Def), TIPO FROM #encabezado_tmp WHERE Devlt_Def > 0
 GROUP BY ITEMNMBR, TIPO
--
OPEN Saldo_cltAplic 
--
FETCH NEXT FROM Saldo_cltAplic INTO @ITEM, @SALDO, @TIPO
--**************************************
-- Asigna unidades a variable control
--**************************************
SET @SALDO_REST = @SALDO
--
WHILE @@FETCH_STATUS=0 BEGIN -- WHILE 1
--*********************************
-- CURSOR PARA BUSCAR LOS NoAplica
--*********************************
	DECLARE Saldo_dev CURSOR FOR
	SELECT NoAplica, ITEMNMBR, sum(SALDO) FROM snt_devolver_vw
	 WHERE CUSTNMBR = @CUSTNMBR AND ITEMNMBR = @ITEM
	 GROUP BY NoAplica, ITEMNMBR
	--
	OPEN Saldo_dev
	FETCH NEXT FROM Saldo_dev INTO @NoAplica, @ITEM_DEV, @SALDO_CNSG
	WHILE @@FETCH_STATUS=0 BEGIN -- WHILE 2
-- CALCULA LAS UNIDADES A ASIGNAR Y DISMINUYE LA
--  VARIABLE DE CONTROL
--		SELECT 'CURSOR1', @ITEM, @SALDO, @TIPO, @SALDO_REST
--		SELECT 'CURSOR2' ,@NoAplica, @ITEM_DEV, @SALDO_CNSG
--
		IF @SALDO_CNSG >= @SALDO_REST BEGIN
		   SET @DEVUELTO = @SALDO_REST
		   SET @SALDO_REST = 0 
		END ELSE BEGIN
		   SET @DEVUELTO = @SALDO_CNSG
		   SET @SALDO_REST = @SALDO_REST - @SALDO_CNSG
		END
--**************************************
-- INSERTA EL REGISTRO DE LA DEVOLUCION
--**************************************
		INSERT INTO #encabezado
  		  ( NoAplica    , ITEMNMBR  , Saldo       , Devuelto , difer_dev , Devlt_Def , Consecutivo, Tipo )
		VALUES
		  ( @NoAplica   , @ITEM     , @SALDO_CNSG , @DEVUELTO, 0         , 0         , 0          , @TIPO)
--
		IF @@Error <> 0 
		BEGIN
   			set @Update_Error = -1
   			SET @ErrorDesc = '-2,Insertando  #encabezado del registro de devolucion'+ rtrim(@ITEM)+ rtrim(@DEVUELTO)
	raiserror(@ErrorDesc,17,1)
	return 9
		END
--
--****************************************
-- VALIDA SI LA VARIABLE DE CONTROL LLEGO
--  A CERO ( NO HAY MAS QUE ASIGNAR )
--****************************************
		IF @SALDO_REST = 0 BEGIN
		  BREAK
		END
--********************
-- CONTINUA EL CICLO
--********************
	FETCH NEXT FROM Saldo_dev INTO @NoAplica, @ITEM_DEV, @SALDO_CNSG
	END -- WHILE 2
	CLOSE Saldo_dev
	DEALLOCATE Saldo_dev
--
	FETCH NEXT FROM Saldo_cltAplic INTO @ITEM, @SALDO, @TIPO
	SET @SALDO_REST = @SALDO
END -- WHILE 1
CLOSE Saldo_cltAplic 
DEALLOCATE Saldo_cltAplic 
-- PRINT 'casi termina la vaina ojo '
--
--*****************************
-- Actualizar la descripcion
--  del item
--*****************************
UPDATE #encabezado
 SET ITEMDESC = iv00101.ITEMDESC
 FROM iv00101
 WHERE #encabezado.ITEMNMBR = iv00101.ITEMNMBR
--
-- PRINT 'SALIO DEL UPDATE casi termina la vaina ojo '
IF @@Error <> 0 
BEGIN
	set @Update_Error = -1
	SET @ErrorDesc = '-2,error actualizando encabezado descripcion'
	raiserror(@ErrorDesc,17,1)
	return 10
END
-- PRINT 'LA VAINA DIO UN ERROR OJO '+  rtrim(@DEVONUMB)+ rtrim(@NoControl)
-- select * from #encabezado
--
-- SELECT '@Update_Error=', @Update_Error
--
-- entra si hay registros y no tenemos ningun error
--
-- select * from #ENCABEZADO
--
--SELECT * FROM #ENCABEZADO
IF @Update_Error = 0 AND EXISTS(SELECT * FROM #ENCABEZADO)
BEGIN	

--SELECT * FROM #ENCABEZADO
--**********************************************
--* 		INICIO PASO II	
--* INICIO ejecucion  del proceso de devolucion
--**********************************************
-- PRINT 'llamo el procedure.... ojo'
	DECLARE @ErrorDescOUT  char( 255)
	EXEC @return_status = Snt_DevolverConsignacion  @DEVONUMB, @NoControl, @ErrorDescOUT OUT
	
	if(len(@ErrorDescOUT)>0)
	begin
--		set @ErrorDescOUT='-2,FALLO EL PROCEDIMIENTO DE DEVOLUCIONES EL ERRO ES '+@ErrorDescOUT
--		raiserror(@ErrorDescOUT,17,1)
--		return 11
	
		SET @ErrorDesc = '-2,FALLO EL PROCEDIMIENTO DE DEVOLUCIONES' + @ErrorDescOUT
		raiserror(@ErrorDesc,17,1)
		return 11

	end
--
-- SELECT 'EL ERROR Snt_DevolverConsignacion: ', RTRIM (@ErrorDescOUT), ',@return_status = ', @return_status
--
--****************************************
--*		FIN DEL PASO II
--****************************************
--**********************************************
--*             INICIO DEL PASO III
-- Insertar el status de los items en tabla fija
-- **********************************************
--
-- OJO Insertar si fue existoso El SP de devoluciones PENDIENTE
--
INSERT INTO snt_statusDev
 ( NoAplica , COD_DEV  , ITEMNMBR , ITEMDESC  , Saldo , Devuelto ,
   difer_dev, Devlt_Def, Danado   , Consecutivo, Tipo )
SELECT
  @DEVONUMB, @NoControl, 
  ITEMNMBR , ITEMDESC   , Saldo , Devuelto ,
   difer_dev, Devlt_Def, Danado   , Consecutivo, Tipo
 from #encabezado_tmp
--
--SELECT * FROM snt_statusDev
IF @@Error <> 0 
BEGIN
	set @Update_Error = -1
	SET @ErrorDesc = '-2,error insertando registro en  snt_statusDev' + rtrim(@DEVONUMB)+ rtrim(@NoControl)
		raiserror(@ErrorDesc,17,1)
		return 12
END
--**********************************************
--*           F . I . N . DEL PASO III
-- **********************************************

 IF @return_status = 0 -- FFB 2007-06-07 VALIDA EN FUNCION DEL RETORNO DEL SP
-- BEGIN 
-- COMMIT TRANSACTION Snt_devoluciones_batch
-- print 'commit devoluciones batch'
-- Transaccion Correcta
-- RETURN( 0)
-- END
-- ELSE
  BEGIN
--	ROLLBACK TRANSACTION Snt_devoluciones_batch
--	-- print 'ROLLBACK devoluciones batch'
--	-- Error de Transaccion
--	RETURN( 13)
		SET @ErrorDesc = '0,LA DEVOLUCION CORRECTA EL NUMERO DE DEVOLUCION ES :' + rtrim(@DEVONUMB)+ ' EL NUMERO DE CONTROL ES :' +rtrim(@NoControl)
		raiserror(@ErrorDesc,17,1)
		return (0)
  END
-- ------
END -- IF @Update_Error = 0 AND EXISTS(SELECT * FROM #ENCABEZADO)
 ELSE
 BEGIN
--	ROLLBACK TRANSACTION Snt_devoluciones_batch
	-- print 'ROLLBACK devoluciones batch'
	-- Error de Transaccion
--	RETURN( 14)
		SET @ErrorDesc = '-2,ES POSIBLE QUE LA TABLA EMCABEZADOS ESTA VACIA'-- + rtrim(@DEVONUMB)+ rtrim(@NoControl)
		raiserror(@ErrorDesc,17,1)
		return 12

 END
--
--
END -- IF EXISTS snt_statusdev
ELSE
 BEGIN
		SET @ErrorDesc = '-2,Ya existe devolucion en  snt_statusDev: ' + rtrim( @NoControl )
		raiserror(@ErrorDesc,17,1)
		return 15
--   RETURN(1)
 END
--
--*************************************
--*       	F . I . N  
--*       	 paso I
-- *************************************



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_CONSIG] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_CONSIG]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_CONSIG]
GO

CREATE PROCEDURE SP_SANT_CONSIG(@NOCONSIG CHAR(21), @ID_ST_ORD INT = 0)
AS
	DECLARE @ID_ORD NUMERIC
	DECLARE @CIUDAD VARCHAR(31)
	DECLARE @DIR_ENT VARCHAR(500)
	DECLARE @TEL1 VARCHAR(20)
	DECLARE @TEL2 VARCHAR(20)
	DECLARE @ITEM VARCHAR(20)
	DECLARE @CANT NUMERIC
	DECLARE @DESC_ITEM VARCHAR(60)
	DECLARE @NOMCLIENTE VARCHAR(100)
	DECLARE @COMENT1 VARCHAR (60)
	DECLARE @COMENT2 VARCHAR (60)
	DECLARE @CUSTNMBR CHAR(15)
	DECLARE @ADRSCODE CHAR(15)
	DECLARE @UPSZONE CHAR(3)
	DECLARE @CHEQUEADOR INT

	-- Asigna a Mery Charris a todos los pedidos
	SET @CHEQUEADOR = 20

	-- Dado el n�mero de la consignaci�n (@ID_ORD_GP = @NOCONSIG) se determina el
	-- correlativo del pedido en Log�stica
	SET @ID_ORD = NULL
	SELECT @ID_ORD=ID_ORD
	FROM T_SANT_ORD
	WHERE ID_ORD_GP=@NOCONSIG


	-- What?!?!?! No existe el pedido en Log�stica. Entonces es necesario crear un encabezado para el pedido
	IF (@ID_ORD IS NULL)
	BEGIN
		-- Dado el n�mero de la consignaci�n se recupera la informaci�n del encabezado
		SELECT
			@DIR_ENT=ADDRESS1+' '+ADDRESS2,
			@CIUDAD=CITY,
			@CUSTNMBR=CUSTNMBR,
			@ADRSCODE=ADRSCODE,
			@NOMCLIENTE=CNTCPRSN,
			@COMENT1=COMENT1,
			@COMENT2=COMENT2
		FROM SNTCONSIGHDR
		WHERE NOCONSIG=@NOCONSIG

		--  Buscar el UPSZONE
		SELECT @UPSZONE = UPSZONE
		FROM RM00102
		WHERE CUSTNMBR = @CUSTNMBR AND ADRSCODE = @ADRSCODE

		-- Recuperar la informaci�n que a�n no tengo en mano
		SELECT
			@TEL1 = PHONE1,
			@TEL2 = PHONE2
		FROM RM00101
		WHERE CUSTNMBR = @CUSTNMBR

		-- Crear el registro en el encabezado de log�stica
		INSERT INTO T_SANT_ORD (
			ID_CHEQ,ID_TPO_ORD,ID_ST_ORD,ID_ORD_GP,CIU_ENT,
			NB_CLTE, DIR_ENT, FE_ING_ORD,TEL_CLTE1,TEL_CLTE2,
			CUSTNMBR,ADRSCODE,UPSZONE,COMENT1,COMENT2)
		VALUES(
			@CHEQUEADOR,1,@ID_ST_ORD,@NOCONSIG,@CIUDAD,
			@NOMCLIENTE,@DIR_ENT,GETDATE(),@TEL1,@TEL2,
			@CUSTNMBR,@ADRSCODE,@UPSZONE,@COMENT1,@COMENT2)

		-- Y claro, ya que tengo el encabezado es necesario recuperar el ID del pedido
		-- que la base de datos le asign� (es un campo con autoincremento)
		SELECT @ID_ORD=ID_ORD FROM T_SANT_ORD 
		WHERE ID_ORD_GP=@NOCONSIG
	END

	-- Como de todas todas, el pedido existe, entonces se cargan los detalles
	INSERT INTO T_SANT_DET_ORD 
		SELECT @ID_ORD,A.ITEMNMBR, A.ITEMDESC,A.QUANTITY_ORD,SUBSTRING(B.ITMSHNAM,1,13)  
		FROM SNTCONSIGDET A, IV00101 B WHERE A.NOCONSIG=@NOCONSIG
			AND B.ITEMNMBR=A.ITEMNMBR
			AND A.TIPO IN ('KIT','ITEM')

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_ACT_10104] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_ACT_10104]') AND type in (N'P', N'PC'))
DROP 	PROCEDURE [dbo].[SP_SANT_ACT_10104]
GO

CREATE	PROCEDURE [dbo].[SP_SANT_ACT_10104] 
(@ID_ORD_GP VARCHAR(23), @ID_ST_ORD INT = 0)
AS

	DECLARE @CUSTNMBR CHAR(15)
	DECLARE @ADRSCODE CHAR(15)
	DECLARE @NB_CLTE VARCHAR(60)
	DECLARE @DIR_ENT VARCHAR(500)
	DECLARE @SOPTYPE NUMERIC
	DECLARE @CANT NUMERIC
	DECLARE @DESC_ITEM VARCHAR(60)
	DECLARE @CIUDAD VARCHAR(60)
	DECLARE @TEL1 VARCHAR(20)
	DECLARE @TEL2 VARCHAR(20)
	DECLARE @ID_ORD NUMERIC
	DECLARE @ITEM VARCHAR(20)
	DECLARE @ITEM2 VARCHAR(20)
	DECLARE @ITMSHNAM VARCHAR(13)
	DECLARE @LOTE VARCHAR(10)
	DECLARE @UPSZONE CHAR(03)
	DECLARE @COMENT1 VARCHAR (60)
	DECLARE @COMENT2 VARCHAR (60)
	DECLARE @LOCNCODE CHAR(11)

	-- Configuraci�n seg�n la tabla SOP10200
	SELECT TOP 1 
		@CANT=QUANTITY, 
		@DESC_ITEM=ITEMDESC, 
		@DIR_ENT=ADDRESS1+ADDRESS2+ADDRESS3, 
		@CIUDAD=CITY, 
		@TEL1=PHONE1, 
		@TEL2=PHONE2, 
		@NB_CLTE=SHIPTONAME,
		@LOCNCODE = LOCNCODE
	FROM SOP10200
	WHERE SOPNUMBE=@ID_ORD_GP

	-- Configuraci�n seg�n la tabla SOP10100
	SELECT
		@SOPTYPE=SOPTYPE,
		@LOTE=BACHNUMB 
	FROM SOP10100
	WHERE SOPNUMBE=@ID_ORD_GP

	-- Indicar en GP10 que el documento se ha impreso. Este truco hace que el documento no pueda ser eliminado all� :-)
	UPDATE SOP10100
	SET TIMESPRT = 1
	WHERE SOPNUMBE = @ID_ORD_GP

	-- TODO:
	-- se quito la siguiente condici�n:  and (@LOCNCODE <> 'BOLEITA')
	-- no estoy seguro si esta bien! revisar si el almacen BOLEITA existira en GP 10
	IF(@SOPTYPE IN (2,6)) -- and (@LOTE <> 'LOTEFYS'))
	BEGIN
		IF EXISTS(SELECT * FROM SOP10200 A, IV00101 B WHERE A.SOPNUMBE=@ID_ORD_GP AND A.ITEMNMBR=B.ITEMNMBR AND CMPNTSEQ = 0 AND A.QUANTITY-A.QTYTBAOR > 0)
		BEGIN

			SET @ID_ORD = NULL
			SELECT @ID_ORD=ID_ORD FROM T_SANT_ORD 
			WHERE ID_ORD_GP=@ID_ORD_GP

			-- What?!?! Es un pedido nuevo dado que no se tiene su ID_ORD en la tabla T_SANT_ORD
			IF (@ID_ORD IS NULL)
			BEGIN

				-- Recuperar la informaci�n del cliente desde SOP10100
				SELECT
					@CUSTNMBR = CUSTNMBR,
					@ADRSCODE = PRSTADCD
				FROM SOP10100
				WHERE SOPNUMBE = @ID_ORD_GP

				-- Y ahora el UPSZONE 'Ruta de env�o' desde RM00102
				SELECT @UPSZONE = UPSZONE
				FROM RM00102
				WHERE @CUSTNMBR = CUSTNMBR
					AND @ADRSCODE = ADRSCODE

				-- Comentarios ...
				SELECT @COMENT1 = SUBSTRING(a.CMMTTEXT, 1, 60),@COMENT2 = SUBSTRING(a.CMMTTEXT, 61, 60)
				FROM  SY04200 a, sop10100 b
				WHERE  a.COMMNTID = b.COMMNTID
					AND  b.SOPNUMBE = @ID_ORD_GP

				-- Guardar el registro nuevo en Log�stica. El pedido existe ahora y ser� procesado seg�n lo indicado en
				-- el estatus inicial, configurado seg�n el par�metro del procedimiento llamado @ID_ST_ORD
				INSERT INTO T_SANT_ORD (
					ID_LOTE,ID_CHEQ,ID_TPO_ORD,ID_ST_ORD,ID_ORD_GP,
					CIU_ENT, NB_CLTE, DIR_ENT, FE_ING_ORD,FE_APR_LOT,
					TEL_CLTE1,TEL_CLTE2,UPSZONE,COMENT1,COMENT2,
					CUSTNMBR, ADRSCODE)
				VALUES(
					@LOTE,0,0,@ID_ST_ORD,@ID_ORD_GP,
					@CIUDAD, @NB_CLTE, @DIR_ENT, GETDATE(),GETDATE(),
					@TEL1,@TEL2,@UPSZONE,@COMENT1,@COMENT2,
					@CUSTNMBR, @ADRSCODE)
	
				-- Ahora que el pedido existe, la base de datos le asigno un correlativo (es un campo con autoincremento)
				-- por lo que se recupera dicho ID para seguir trabajando con �l
				SELECT @ID_ORD=ID_ORD FROM T_SANT_ORD
				WHERE ID_ORD_GP=@ID_ORD_GP
	
			END

			-- El encabezado seguro que existe por lo que procedemos a copiar los detalles
			INSERT INTO T_SANT_DET_ORD 
				SELECT @ID_ORD, A.ITEMNMBR, A.ITEMDESC, A.QUANTITY-A.QTYTBAOR, B.ITMSHNAM
				FROM SOP10200 A, IV00101 B
				WHERE A.SOPNUMBE=@ID_ORD_GP AND A.ITEMNMBR=B.ITEMNMBR
					AND CMPNTSEQ = 0
					AND A.QUANTITY-A.QTYTBAOR > 0
					AND @ID_ORD NOT IN (SELECT ID_ORD FROM T_SANT_det_ORD)

		END
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_ACT_ORD] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_ACT_ORD]') AND type in (N'P', N'PC'))
DROP 	PROCEDURE [dbo].[SP_SANT_ACT_ORD]
GO

CREATE  PROCEDURE SP_SANT_ACT_ORD (@ID_ORD NUMERIC, @ID_ST INT, @OBS VARCHAR(500)
)
AS
BEGIN
DECLARE @ID_STS INT

SELECT @ID_STS = ID_ST_ORD FROM T_SANT_ORD WHERE ID_ORD=@ID_ORD
IF(@ID_STS=5)
BEGIN
	RAISERROR('-1,Esta orden ha sido eliminada',17,1)
	RETURN
END
ELSE
BEGIN
	UPDATE T_SANT_ORD SET ID_ST_ORD=@ID_ST, FE_EMB_ORD=GETDATE(),OBS=@OBS
	WHERE ID_ORD=@ID_ORD 

	RAISERROR('0,',17,1)
	RETURN
END
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[SP_SANT_AGREGAR_ITEM_GUIA]    Script Date: 01/11/2009 21:47:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_AGREGAR_ITEM_GUIA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_AGREGAR_ITEM_GUIA]
GO
CREATE   PROCEDURE SP_SANT_AGREGAR_ITEM_GUIA
(@ID_GUIA NUMERIC, @ID_ITEM NUMERIC, @TPO INT)
AS
BEGIN

IF(@TPO=1) --ORDEN
BEGIN
	UPDATE 	T_SANT_ORD 
	SET 	ID_GUIA=@ID_GUIA , FE_DESP_ORD=GETDATE() , 
		ID_ST_ORD=3, ID_ST_ENT=0, 
		FE_ST_ENT=GETDATE()
	WHERE 	ID_ORD=@ID_ITEM
END
ELSE
BEGIN
	IF(@TPO=2) -- DEVOLUCION
	BEGIN
		UPDATE 	T_SANT_DEV 
		SET 	ID_GUIA=@ID_GUIA , 
			ID_ST=3
		WHERE 	ID_DEV=@ID_ITEM	
	END
	ELSE
	BEGIN
		IF(@TPO=3) -- MEMO
		BEGIN
			UPDATE 	T_SANT_MEMO 
			SET 	ID_GUIA=@ID_GUIA 
			WHERE 	ID_MEMO=@ID_ITEM
		END
	END
END
END 




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_APROB_LOTE]    Script Date: 01/11/2009 21:49:28 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_APROB_LOTE]') AND type in (N'P', N'PC'))
DROP 	PROCEDURE [dbo].[SP_SANT_APROB_LOTE]
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- 
--**********************************
--*	LOG DE MODIFICACIONES
--**********************************
--*Ivan Garcerant 3/10/2007 almacenar la hora y fecha de aprobacion del lote
--*JLMEJIASM-I 23-07-2007 SE MODIFICO EL SP 
--*JLMEJIASM-F 23-07-2007 PARA EVITAR QUE ENTREN PEDIDO DUPLICADOS 
--*			  AL APROBAR UN LOTE.
--**********************************
CREATE   PROCEDURE SP_SANT_APROB_LOTE 
(@ID_LOTE VARCHAR(15))
AS

	IF(EXISTS(SELECT * FROM T_SANT_ORD WHERE ID_LOTE=@ID_LOTE))
	BEGIN
		UPDATE T_SANT_ORD SET ID_ST_ORD=0, FE_APR_LOT=GETDATE() WHERE ID_LOTE=@ID_LOTE
		AND ID_ST_ORD =4
		RAISERROR('0,Lote autorizado',17,1)
		RETURN
	END	ELSE BEGIN
		RAISERROR('-1,Lote no existe',17,1)
		RETURN
	END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_CERRAR_CAJA]    Script Date: 01/11/2009 21:50:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_CERRAR_CAJA]') AND type in (N'P', N'PC'))
DROP 	PROCEDURE [dbo].[SP_SANT_CERRAR_CAJA]
GO

CREATE 	PROCEDURE SP_SANT_CERRAR_CAJA (@ID_ORD NUMERIC,
@ID_CAJA NUMERIC)
AS
BEGIN
	UPDATE T_SANT_CAJA_ORD SET FE_FIN=GETDATE(), ID_ST_CAJA=1
	WHERE ID_ORD=@ID_ORD AND ID_CAJA=@ID_CAJA
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

/****** Object:  StoredProcedure [dbo].[SP_SANT_DAT_GUIA_CARGA]    Script Date: 01/11/2009 21:56:18 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_DAT_GUIA_CARGA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_DAT_GUIA_CARGA]
GO
CREATE PROCEDURE SP_SANT_DAT_GUIA_CARGA
(@ID_GUIA NUMERIC)
AS
BEGIN
DECLARE @RET VARCHAR(100)

SELECT @RET='0,'+CONVERT(VARCHAR(10), A.FE_GUIA_CARGA, 103)+','+B.NB_USR
FROM T_SANT_GUIA_CARGA A, T_SANT_USR B
WHERE A.ID_USR=B.ID_USR
AND A.ID_GUIA_CARGA=@ID_GUIA

RAISERROR(@RET,17,1)
RETURN
END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_ELIMINAR_ITEM_GUIA]    Script Date: 01/11/2009 21:57:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_ELIMINAR_ITEM_GUIA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_ELIMINAR_ITEM_GUIA]
GO
CREATE  PROCEDURE SP_SANT_ELIMINAR_ITEM_GUIA
(@ID_GUIA NUMERIC, @ID_ITEM NUMERIC, @TPO INT)
AS
BEGIN

IF(@TPO=1) --ORDEN
BEGIN
	UPDATE 	T_SANT_ORD 
	SET 	ID_GUIA=NULL , FE_DESP_ORD=NULL , 
		ID_ST_ORD=2, ID_ST_ENT=NULL, 
		FE_ST_ENT=NULL
	WHERE 	ID_ORD=@ID_ITEM
END
ELSE
BEGIN
	IF(@TPO=2) -- DEVOLUCION
	BEGIN
		UPDATE 	T_SANT_DEV 
		SET 	ID_GUIA=NULL , 
			ID_ST=1
		WHERE 	ID_DEV=@ID_ITEM	
	END
	ELSE
	BEGIN
		IF(@TPO=3) -- MEMO
		BEGIN
			DELETE 	T_SANT_MEMO_GUIA
			WHERE 	ID_MEMO=@ID_ITEM
		END
	END
END
END 





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_GEST_ORD]    Script Date: 01/11/2009 21:58:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_GEST_ORD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_GEST_ORD]
GO
CREATE PROCEDURE SP_SANT_GEST_ORD 
(
	@ID_ORD NUMERIC, @ID_USR_ATC NUMERIC,@OBS VARCHAR(500), @TPO INT
)
AS
	INSERT INTO T_SANT_GEST_ORD (ID_ORD,ID_USR_ATC,FE_GEST,OBS_GEST)
	VALUES (@ID_ORD, @ID_USR_ATC, GETDATE(), @OBS)

	IF(@TPO=1)
		UPDATE T_SANT_ORD
		SET ID_ST_GEST=1
		WHERE ID_ORD=@ID_ORD

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_MEMO_GUIA]    Script Date: 01/11/2009 22:00:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_MEMO_GUIA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_MEMO_GUIA]
GO
CREATE  PROCEDURE SP_SANT_MEMO_GUIA
(@ID_GUIA NUMERIC,
@CLIENTE VARCHAR(100),
@BULTOS INT,
@EJEMP INT,
@DEST VARCHAR(100),
@DIR VARCHAR(500),
@COMENT VARCHAR(500))
as
BEGIN

	INSERT INTO T_SANT_MEMO_GUIA (FE_MEMO,ID_GUIA,CLTE_MEMO ,CAJAS ,EJEMP ,DEST_MEMO ,DIR_MEMO ,OBS_MEMO)
	VALUES (GETDATE(),@ID_GUIA,@CLIENTE ,@BULTOS ,@EJEMP ,@DEST ,@DIR ,@COMENT)
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_OBT_DAT_USR]    Script Date: 01/11/2009 22:03:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_OBT_DAT_USR]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_OBT_DAT_USR]
GO
CREATE PROCEDURE SP_SANT_OBT_DAT_USR(@USR VARCHAR(15))
AS
BEGIN

/********************************************************************************

PROCEDIMIENTO ALMACENADO.

FINALIDAD:   Proveer de la informacion del usuario solicitado
RETORNA:     Los datos del usuario nombre, id, cargo
FECHA:                  08/03/2006
DESARROLLADO:      Dario Carnelutti - SITVEN, C.A.

********************************************************************************/

DECLARE @NB_USR VARCHAR(60)
DECLARE @ID_USR NUMERIC(7,0)
DECLARE @EMAIL VARCHAR(60)
DECLARE @ID_PERF INT
DECLARE @RET VARCHAR(100)


SELECT @NB_USR=REPLACE(NB_USR,',',''), @ID_USR=ID_USR,
@ID_PERF=ISNULL(ID_PERF,0)
FROM T_SANT_USR (READPAST)
WHERE LOG_USR=@USR

 

SET @RET=@NB_USR+','+CONVERT(VARCHAR(6),@ID_USR)+','+CONVERT(VARCHAR(6),@ID_PERF)

RAISERROR(@RET,17,1)

END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO



/****** Object:  StoredProcedure [dbo].[SP_SANT_REG_DEV]    Script Date: 01/11/2009 22:04:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REG_DEV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REG_DEV]
GO
CREATE      PROCEDURE SP_SANT_REG_DEV (@ID_CHEQ NUMERIC, @RIF_CLTE VARCHAR(20), @NB_CLTE VARCHAR(60), @CANT INT, @PH VARCHAR(25), @TPO INT)
AS
BEGIN
DECLARE @DEV VARCHAR(50)
DECLARE @DIRECCION VARCHAR(500)

SELECT @DIRECCION=LTRIM(RTRIM(ADDRESS1))+' '+LTRIM(RTRIM(ADDRESS2))+' '+LTRIM(RTRIM(ADDRESS3))+' '+LTRIM(RTRIM(CITY))+' '+LTRIM(RTRIM(STATE)) 
FROM RM00101
WHERE LTRIM(RTRIM(CUSTNAME))=LTRIM(RTRIM(@NB_CLTE))

IF(@TPO=1)
	INSERT INTO T_SANT_DEV (DIR_DEV,ID_ST,ID_CHEQ,FE_REG, RIF_CLTE_DEV, NB_CLTE_DEV, CANT_CAJA, TEL_CLTE,IN_ATC ) 
	VALUES (@DIRECCION,0,@ID_CHEQ,GETDATE(),@RIF_CLTE,@NB_CLTE, @CANT, @PH,@TPO)
ELSE
	INSERT INTO T_SANT_DEV (DIR_DEV,ID_ST,ID_CHEQ,FE_REG, RIF_CLTE_DEV, NB_CLTE_DEV, CANT_CAJA, TEL_CLTE,IN_ATC ) 
	VALUES (@DIRECCION,1,@ID_CHEQ,GETDATE(),@RIF_CLTE,@NB_CLTE, @CANT, @PH,@TPO)

SELECT @DEV=COD_DEV 
FROM T_SANT_DEV 
WHERE ID_DEV=SCOPE_IDENTITY()

RAISERROR(@DEV,17,1)
END





GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REG_GUIA_CARGA]    Script Date: 01/11/2009 22:05:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REG_GUIA_CARGA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REG_GUIA_CARGA]
GO
CREATE      PROCEDURE SP_SANT_REG_GUIA_CARGA 
(
	@ID_TRANSP VARCHAR(10),
	@ID_USR NUMERIC
)
AS
BEGIN
SET NOCOUNT ON 
DECLARE @ID_GUIA NUMERIC
DECLARE @RET VARCHAR(20)

update t_sant_Transp set Status_Transp =2 where CI_Transp = @ID_TRANSP


	INSERT INTO T_SANT_GUIA_CARGA (FE_GUIA_CARGA, ID_USR, ID_TRANSP)
	VALUES (GETDATE(), @ID_USR, @ID_TRANSP)

SET @ID_GUIA=SCOPE_IDENTITY()

SET @RET='0,'+convert(varchar(10),@ID_GUIA)
RAISERROR (@RET,17,1)
RETURN
END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REG_SERIAL]    Script Date: 01/11/2009 22:06:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REG_SERIAL]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REG_SERIAL]
GO
CREATE      PROCEDURE SP_SANT_REG_SERIAL
(
@ID_ORD NUMERIC,
@SERIAL CHAR(14),
@CANT NUMERIC
)
AS
BEGIN
DECLARE @CAJA INT
DECLARE @ID_PROD VARCHAR(20)
DECLARE @ID_ST INT

SELECT @ID_ST = ID_ST_ORD FROM T_SANT_ORD WHERE ID_ORD=@ID_ORD
IF(@ID_ST=5)
BEGIN
	RAISERROR('-1,Esta orden ha sido eliminada',17,1)
	RETURN
END
ELSE
BEGIN
	SELECT TOP 1 @ID_PROD=A.ID_PROD FROM V_SANT_COD_PROD A, T_SANT_DET_ORD B
	WHERE A.COD_PROD=@SERIAL AND A.ID_PROD=B.ID_PROD AND B.ID_ORD=@ID_ORD

	IF(EXISTS(
	SELECT A.ID_PROD, A.CANT_ORD , SUM(B.CANT_PROD) FROM T_SANT_DET_ORD A LEFT JOIN T_SANT_DET_CAJA_ORD B
	ON A.ID_ORD=B.ID_ORD AND A.ID_PROD=B.ID_PROD
	WHERE A.ID_ORD=@ID_ORD
	AND A.ID_PROD=@ID_PROD 
	GROUP BY A.ID_PROD, A.CANT_ORD 
	HAVING A.CANT_ORD <SUM(B.CANT_PROD) +@CANT

))
	BEGIN
		RAISERROR('-1,Se esta sobrepasando la cantidad de ejemplares solicitados',17,1)
		RETURN		
	END

	UPDATE T_SANT_ORD SET ID_ST_ORD=1 WHERE ID_ORD=@ID_ORD 
	
	IF(NOT EXISTS(SELECT ID_CAJA FROM T_SANT_CAJA_ORD WHERE ID_ORD=@ID_ORD))
	BEGIN
		SET @CAJA=1
		INSERT INTO T_SANT_CAJA_ORD (ID_ORD,ID_CAJA, FE_INI,ID_ST_CAJA)
		VALUES (@ID_ORD, @CAJA, GETDATE(),0)
	
	END
	ELSE
	BEGIN
		IF(EXISTS(SELECT ID_CAJA FROM T_SANT_CAJA_ORD WHERE ID_ORD=@ID_ORD AND ID_ST_CAJA=0))
		BEGIN
			SELECT @CAJA=MAX(ID_CAJA) FROM T_SANT_CAJA_ORD WHERE ID_ORD=@ID_ORD AND ID_ST_CAJA=0
		END
		ELSE
		BEGIN
			SELECT @CAJA=MAX(ID_CAJA)+1 FROM T_SANT_CAJA_ORD WHERE ID_ORD=@ID_ORD
			INSERT INTO T_SANT_CAJA_ORD (ID_ORD,ID_CAJA, FE_INI,ID_ST_CAJA)
			VALUES (@ID_ORD, @CAJA, GETDATE(),0)
		END
		
	END
	IF(NOT EXISTS(SELECT * FROM T_SANT_DET_CAJA_ORD WHERE ID_ORD=@ID_ORD AND ID_PROD=@ID_PROD AND ID_CAJA=@CAJA))
	BEGIN
		INSERT INTO T_SANT_DET_CAJA_ORD
		(ID_ORD, ID_CAJA, ID_PROD, CANT_PROD)
		VALUES (@ID_ORD, @CAJA, @ID_PROD, @CANT)
	END
	ELSE
	BEGIN
		UPDATE T_SANT_DET_CAJA_ORD SET CANT_PROD=CANT_PROD+@CANT
		WHERE ID_ORD=@ID_ORD AND ID_CAJA=@CAJA AND ID_PROD=@ID_PROD
	END
	RAISERROR('0,',17,1)
	RETURN

END
END







GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REG_SERIAL_DEV]    Script Date: 01/11/2009 22:08:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REG_SERIAL_DEV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REG_SERIAL_DEV]
GO
CREATE      PROCEDURE SP_SANT_REG_SERIAL_DEV
(
@ID_DEV NUMERIC,
@SERIAL VARCHAR(15),
@CANT NUMERIC,
@ID_ST INT,
@ID_USR NUMERIC,
@ID VARCHAR(30)
)
AS
BEGIN
DECLARE @CAJA INT
DECLARE @ID_PROD VARCHAR(20)
DECLARE @NB_PROD VARCHAR(60)

UPDATE T_SANT_DEV SET ID_ST=2 , ID_CHEQ=@ID_USR WHERE ID_DEV=@ID_DEV 

--- HAY QUE APUNTAR A LA TABLA CON EL DETALLE
--- OJO MODIFICAR

--SELECT 
SELECT @NB_PROD=LTRIM(RTRIM(ITEMDESC)), @ID_PROD=ITEMNMBR 
FROM iv00101 WHERE ITEMNMBR=@ID


IF(NOT EXISTS(SELECT ID_CAJA FROM T_SANT_CAJA_DEV WHERE ID_DEV=@ID_DEV))
BEGIN

	SET @CAJA=1
	INSERT INTO T_SANT_CAJA_DEV (ID_DEV,ID_CAJA, FE_REG,ID_ST_CAJA)
	VALUES (@ID_DEV, @CAJA, GETDATE(),0)

END
ELSE
BEGIN

	IF(EXISTS(SELECT ID_CAJA FROM T_SANT_CAJA_DEV WHERE ID_DEV=@ID_DEV AND ID_ST_CAJA=0))
	BEGIN

		SELECT @CAJA=MAX(ID_CAJA) FROM T_SANT_CAJA_DEV WHERE ID_DEV=@ID_DEV AND ID_ST_CAJA=0
	END
	ELSE
	BEGIN

		SELECT @CAJA=MAX(ID_CAJA)+1 FROM T_SANT_CAJA_DEV WHERE ID_DEV=@ID_DEV
		INSERT INTO T_SANT_CAJA_DEV (ID_DEV,ID_CAJA, FE_REG,ID_ST_CAJA)
		VALUES (@ID_DEV, @CAJA, GETDATE(),0)
	END
	
END
IF(NOT EXISTS(SELECT * FROM T_SANT_DET_CAJA_DEV WHERE ID_DEV=@ID_DEV AND ID_PROD=@ID_PROD AND ID_CAJA=@CAJA AND ST_PROD=@ID_ST))
BEGIN
	INSERT INTO T_SANT_DET_CAJA_DEV
	(ID_DEV, ID_CAJA, ID_PROD, CANT_PROD, SER_PROD,ST_PROD)
	VALUES (@ID_DEV, @CAJA, @ID_PROD, @CANT, @SERIAL,@ID_ST)

END
ELSE
BEGIN
	UPDATE T_SANT_DET_CAJA_DEV SET CANT_PROD=CANT_PROD+@CANT
	WHERE ID_DEV=@ID_DEV AND ID_CAJA=@CAJA AND ID_PROD=@ID_PROD AND ST_PROD=@ID_ST
END

END

DECLARE @RETORNO VARCHAR(100)
IF(@@ERROR=0)
BEGIN
	SET @RETORNO='0,'+REPLACE(@NB_PROD,',','||')+','+@ID_PROD
	RAISERROR(@RETORNO,17,1)
	RETURN
END
ELSE
BEGIN
	SET @RETORNO='-1,Error al registrar el producto'
	RAISERROR(@RETORNO,17,1)
	RETURN
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REG_SERIAL_DEV_PRE]    Script Date: 01/11/2009 22:11:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REG_SERIAL_DEV_PRE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REG_SERIAL_DEV_PRE]
GO
CREATE      PROCEDURE SP_SANT_REG_SERIAL_DEV_PRE
(
	@ID_DEV NUMERIC,
	@SERIAL VARCHAR(20),
	@CANT INT,
	@ID_USR_ATC NUMERIC,
	@ID_PROD VARCHAR(20)
)
AS
BEGIN

DECLARE @NB_PROD VARCHAR(100)
DECLARE @MSG VARCHAR(200)

SELECT @NB_PROD=LTRIM(RTRIM(ITEMDESC))
FROM iv00101 WHERE ITEMNMBR=@ID_PROD

IF(NOT EXISTS(SELECT * FROM T_SANT_DET_DEV_PRE WHERE ID_DEV=@ID_DEV AND ID_PROD=@ID_PROD))
	INSERT INTO T_SANT_DET_DEV_PRE (ID_DEV, ID_PROD, CANT_PROD)
	VALUES (@ID_DEV, @ID_PROD, @CANT)
SET @NB_PROD = REPLACE(@NB_PROD,',','||')
SET @MSG='0,'+@NB_PROD+','+@ID_PROD

	RAISERROR(@MSG,17,1)
	RETURN
END




GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REP20MAS]    Script Date: 01/11/2009 22:13:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REP20MAS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REP20MAS]
GO
CREATE      PROCEDURE SP_SANT_REP20MAS
(@FECHA AS DATETIME)
AS
BEGIN

SELECT CUSTCLAS, ITMCLSCD, ITEMNMBR, ITEMDESC,
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTFAC, 
SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTDEV, 
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END + 
    CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%')THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END) AS CANTNETA,

SUM(CASE 
        WHEN (SOPTYPE = 3) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN (PRECIOV*1)
        ELSE CASE WHEN (SOPTYPE = 3) THEN PRECIOV*CANT ELSE 0 END
   END) AS VTAFAC,

SUM(CASE 
        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN CASE 
                  WHEN  (ITEMNMBR LIKE '%DESC%')
                  THEN 0
                  ELSE (PRECIOV*(-1))*(CANT*(-1))
             END
        ELSE CASE 
                  WHEN  (SOPTYPE = 4)
                  THEN (PRECIOV*(-1)) * ((CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END)*(-1))
                  ELSE 0
             END
     END) AS MTO_DEV,

SUM(CASE
	WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN CASE 
                  WHEN  (ITEMNMBR LIKE '%DESC%')
                  THEN  
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)

                  ELSE  
			(CASE 
			        WHEN (SOPTYPE = 3) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN (PRECIOV*1)
			        ELSE CASE WHEN (SOPTYPE = 3) THEN PRECIOV*CANT ELSE 0 END
			END)+
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN 0
			                  ELSE (PRECIOV*(-1))*(CANT*(-1))
			             END
			        ELSE CASE 
			                  WHEN  (SOPTYPE = 4)
			                  THEN (PRECIOV*(-1)) * ((CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END)*(-1))
			                  ELSE 0
			             END
			END)-
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)
             END
        ELSE 
		(CASE 
		        WHEN (SOPTYPE = 3) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN (PRECIOV*1)
		        ELSE CASE WHEN (SOPTYPE = 3) THEN PRECIOV*CANT ELSE 0 END
		END)+
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN 0
		                  ELSE (PRECIOV*(-1))*(CANT*(-1))
		             END
		        ELSE CASE 
		                  WHEN  (SOPTYPE = 4)
		                  THEN (PRECIOV*(-1)) * ((CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END)*(-1))
		                  ELSE 0
		             END
		END)-
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN (PRECIOV*(-1))*(CANT*(-1))
		                  ELSE (MRKDNAMT*CANT)
		             END
		        ELSE (MRKDNAMT*CANT)
		END)
     END) AS VTA_LIQ
INTO #20_ITEM_MAS_VENTA
FROM VTASTOTPRODAFA 
WHERE     
	  --(CUSTCLAS = 'CONTADO') AND	
          (ITMCLSCD <> 'SERVICIO') AND 
          (PRECIOV <> 0) AND 
          (SOPTYPE = 3 OR SOPTYPE = 4) AND 
          (SOPNUMBE NOT LIKE 'DP%') AND
          (VOIDSTTS = 0) AND  
          --(SOPNUMBE NOT LIKE 'NC0004857%') AND 

--          (SOPNUMBE NOT LIKE 'F0003266%') AND 
          --(SOPNUMBE NOT LIKE 'NC0005683%') AND 
          
       (DOCID <> 'DEV-PROMOCION') AND 
           
          (DOCDATE BETWEEN CONVERT(DATETIME, CAST(DATEPART(YEAR,@FECHA)-1 AS VARCHAR)+'-01-01 00:00:00', 102) AND 
                           CONVERT(DATETIME, CONVERT(VARCHAR(10),DATEADD(YEAR,-1,@FECHA),120)+' 00:00:00', 102))
GROUP BY  CUSTCLAS, ITMCLSCD, ITEMNMBR, ITEMDESC

SELECT
	ITMCLSCD, 
	ITEMNMBR, 
	ITEMDESC,
	SUM(CANTFAC) AS CANTFAC, 
	SUM(CANTDEV) AS CANTDEV, 
	SUM(CANTNETA) AS CANTNETA
FROM  
	#20_ITEM_MAS_VENTA 
GROUP BY
	ITMCLSCD,
	ITEMNMBR,
	ITEMDESC
ORDER BY 
	ITMCLSCD, 
	SUM(CANTNETA) DESC


DROP TABLE #20_ITEM_MAS_VENTA

END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REPCONSIGNAHST]    Script Date: 01/11/2009 22:14:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REPCONSIGNAHST]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REPCONSIGNAHST]
GO
CREATE 	    PROCEDURE SP_SANT_REPCONSIGNAHST(@FECHA DATETIME)
AS
BEGIN

create table #saldo 
(CUSTCLAS varchar(15), ITMCLSCD varchar(15), Cant2006 NUMERIC, Cant2007 NUMERIC, Saldo2006 NUMERIC, Saldo2007 NUMERIC)


insert into #saldo ( CUSTCLAS, ITMCLSCD, Cant2006, Cant2007, Saldo2006, Saldo2007 )
SELECT  RM00101.CUSTCLAS, IV00101.ITMCLSCD, 
SUM( case when SNTCONSIGHDRHST_VW.doctype = 'G' 
          then SntConsigDetHst.QUANTITY_SHP
          else -SntConsigDetHst.QUANTITY_SHP end) as CANT1, 0 as CANT2, 
SUM( case when SNTCONSIGHDRHST_VW.doctype = 'G' 
          then SntConsigDetHst.QUANTITY_SHP*SntConsigDetHst.UNITPRCE*(1-(SntConsigDetHst.DESCUENTO/100))
          else -SntConsigDetHst.QUANTITY_SHP*SntConsigDetHst.UNITPRCE*(1-(SntConsigDetHst.DESCUENTO/100)) end)  as SALDO1, 0 as SALDO2 
FROM         RM00101 INNER JOIN
                      SntConsigDetHst INNER JOIN
                      SNTCONSIGHDRHST_VW ON SntConsigDetHst.NOCONSIG = SNTCONSIGHDRHST_VW.NOCONSIG INNER JOIN
                      IV00101 ON SntConsigDetHst.ITEMNMBR = IV00101.ITEMNMBR ON 
                      RM00101.CUSTNMBR = SNTCONSIGHDRHST_VW.CUSTNMBR
WHERE   --(SntConsigDetHst.ITEMNMBR not like 'zz%') and 
(SNTCONSIGHDRHST_VW.DOCDATE  < DATEADD(YEAR, -1, @FECHA) ) 
and SntConsigDetHst.ITEMNMBR IN 
(SELECT ITEMNMBR FROM SNTCONSIGDETHST WHERE NOCONSIG = NOAPLICA AND DOCTYPE='G' AND TIPO IN ('KIT','ITEM') )
group by RM00101.CUSTCLAS, IV00101.ITMCLSCD
union all
SELECT  RM00101.CUSTCLAS, IV00101.ITMCLSCD,
 0 as CANT1 ,
SUM( case when SNTCONSIGHDRHST_VW.doctype = 'G' 
          then SntConsigDetHst.QUANTITY_SHP
          else -SntConsigDetHst.QUANTITY_SHP end) as CANT2,

 0 as SALDO1 ,
SUM( case when SNTCONSIGHDRHST_VW.doctype = 'G' 
          then SntConsigDetHst.QUANTITY_SHP*SntConsigDetHst.UNITPRCE*(1-(SntConsigDetHst.DESCUENTO/100))
          else -SntConsigDetHst.QUANTITY_SHP*SntConsigDetHst.UNITPRCE*(1-(SntConsigDetHst.DESCUENTO/100)) end)  as SALDO2
FROM         RM00101 INNER JOIN
                      SntConsigDetHst INNER JOIN
                      SNTCONSIGHDRHST_VW ON SntConsigDetHst.NOCONSIG = SNTCONSIGHDRHST_VW.NOCONSIG INNER JOIN
                      IV00101 ON SntConsigDetHst.ITEMNMBR = IV00101.ITEMNMBR ON 
                      RM00101.CUSTNMBR = SNTCONSIGHDRHST_VW.CUSTNMBR
WHERE   
	--(SntConsigDetHst.ITEMNMBR not like 'zz%') and 
(SNTCONSIGHDRHST_VW.DOCDATE  < @FECHA) 
	and SntConsigDetHst.ITEMNMBR IN (SELECT ITEMNMBR FROM SNTCONSIGDETHST WHERE NOCONSIG = NOAPLICA AND DOCTYPE='G' AND TIPO IN ('KIT','ITEM') )
group by RM00101.CUSTCLAS, IV00101.ITMCLSCD


Select  CUSTCLAS, ITMCLSCD, sum(Cant2006) as CANT1 , sum(Cant2007) as CANT2,
sum(Saldo2006) as MONTO1 , sum(Saldo2007) as MONTO2
from  #saldo
group by CUSTCLAS, ITMCLSCD


drop table #saldo

END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REPFACDEV]    Script Date: 01/11/2009 22:16:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REPFACDEV]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REPFACDEV]
GO
CREATE      PROCEDURE SP_SANT_REPFACDEV
(@FECHA AS DATETIME, @FECHA1 AS DATETIME)
AS
BEGIN

CREATE TABLE #TEMPREPORT 
(CUSTNMBR VARCHAR(15), ITMCLSCD VARCHAR(15), ITEMNMBR VARCHAR(31), ITEMDESC VARCHAR(101), 
                                              CANTFAC1 int, CANTDEV1 int, CANTNETA1 int, 
                                              VTAFAC1 money, MTO_DEV1 money, VTA_LIQ1 money, 
                                              CANTFAC2 int, CANTDEV2 int, CANTNETA2 int, 
                                              VTAFAC2 money, MTO_DEV2 money, VTA_LIQ2 money)


INSERT INTO #TEMPREPORT (CUSTNMBR , ITMCLSCD , ITEMNMBR, ITEMDESC, 
                                                      CANTFAC1 , CANTDEV1 , CANTNETA1 , 
                                                       VTAFAC1 , MTO_DEV1 , VTA_LIQ1 , 
                                                       CANTFAC2 , CANTDEV2 , CANTNETA2 , 
                                                       VTAFAC2 , MTO_DEV2 , VTA_LIQ2 )
SELECT  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC,
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTFAC1, 
SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTDEV1, 
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END + 
    CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END) AS CANTNETA1,

SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
         THEN PRECIOV*CANT 
         ELSE 0
         END) AS VTAFAC1,

SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
         THEN PRECIOV*CANT
	 ELSE 0
         END) AS MTO_DEV1,

SUM(CASE
	WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN CASE 
                  WHEN  (ITEMNMBR LIKE '%DESC%')
                  THEN  
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)
                  ELSE  
		   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
		         THEN PRECIOV*CANT 
		         ELSE 0
		         END)+	
		   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
		         THEN PRECIOV*CANT
			 ELSE 0
		         END)-
			(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			      THEN CASE WHEN  (ITEMNMBR LIKE '%DESC%')
			                THEN (PRECIOV*(-1))*(CANT*(-1))
			                ELSE (MRKDNAMT*CANT)
			                END
			      ELSE (MRKDNAMT*CANT)
			      END)
             END
        ELSE 
	   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
	         THEN PRECIOV*CANT 
	         ELSE 0
	         END)+	
	   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
	         THEN PRECIOV*CANT
		 ELSE 0
	         END)-
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN (PRECIOV*(-1))*(CANT*(-1))
		                  ELSE (MRKDNAMT*CANT)
		             END
		        ELSE (MRKDNAMT*CANT)
		END)
     END) AS VTA_LIQ1,
 0 AS CANTFAC2, 0 AS CANTDEV2, 0 AS CANTNETA2, 0 AS VTAFAC2, 0 AS MTO_DEV2, 0 AS VTA_LIQ2
FROM VTASTOTPRODAFA 
WHERE     
          (ITMCLSCD <> 'SERVICIO') AND 
          (PRECIOV <> 0) AND 
          (SOPTYPE = 3 OR SOPTYPE = 4) AND 
          (SOPNUMBE NOT LIKE 'DP%') AND
          (VOIDSTTS = 0) AND  
          (SOPNUMBE NOT LIKE 'NC0004857%') AND 
          (SOPNUMBE NOT LIKE 'F0003266%') AND 
          (SOPNUMBE NOT LIKE 'NC0005683%') AND 
          (DOCID <> 'DEV-PROMOCION') AND 
          (DOCDATE 
		BETWEEN 
			CONVERT(DATETIME, CAST(DATEPART(YEAR,@FECHA)-1 AS VARCHAR)+'-01-01 00:00:00', 102) 
		AND 
			CONVERT(DATETIME, CONVERT(VARCHAR(10),DATEADD(YEAR,-1,@FECHA),120)+' 23:59:59', 102)
           )
GROUP BY  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC
UNION ALL
SELECT  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC, 0 AS CANTFAC1, 0 AS CANTDEV1, 0 AS CANTNETA1, 0 AS VTAFAC1, 0 AS MTO_DEV1, 0 AS VTA_LIQ1,
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTFAC2, 
SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTDEV2, 
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END + 
    CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END) AS CANTNETA2,

SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
         THEN PRECIOV*CANT 
         ELSE 0
         END) AS VTAFAC2,

SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
         THEN PRECIOV*CANT
	 ELSE 0
         END) AS MTO_DEV2,


SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
         THEN CASE WHEN (ITEMNMBR LIKE '%DESC%')
                   THEN (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			      THEN CASE WHEN  (ITEMNMBR LIKE '%DESC%')
			                THEN (PRECIOV*(-1))*(CANT*(-1))
			                ELSE (MRKDNAMT*CANT)
			                END
			      ELSE (MRKDNAMT*CANT)
			      END)

                   ELSE 	   
		        (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
			         THEN PRECIOV*CANT 
			         ELSE 0
			         END)+	
			(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
			         THEN PRECIOV*CANT
				 ELSE 0
			         END)-
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)
             END
        ELSE 
	   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
	         THEN PRECIOV*CANT 
	         ELSE 0
	         END)+	
	   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
	         THEN PRECIOV*CANT
		 ELSE 0
	         END)-
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN (PRECIOV*(-1))*(CANT*(-1))
		                  ELSE (MRKDNAMT*CANT)
		             END
		        ELSE (MRKDNAMT*CANT)
		END)
     END) AS VTA_LIQ2
FROM VTASTOTPRODAFA 
WHERE     
          (ITMCLSCD <> 'SERVICIO') AND 
          (PRECIOV <> 0) AND 
          (SOPTYPE = 3 OR SOPTYPE = 4) AND 
          (SOPNUMBE NOT LIKE 'DP%') AND
          (VOIDSTTS = 0) AND  
          (SOPNUMBE NOT LIKE 'NC0004857%') AND 
          (SOPNUMBE NOT LIKE 'F0003266%') AND 
          (SOPNUMBE NOT LIKE 'NC0005683%') AND 
          (DOCID <> 'DEV-PROMOCION') AND 
          (DOCDATE 
		BETWEEN 
			CONVERT(DATETIME, CAST(DATEPART(YEAR,@FECHA) AS VARCHAR)+'-01-01 00:00:00', 102) 
		AND 
			CONVERT(DATETIME, CONVERT(VARCHAR(10),@FECHA,120)+' 23:59:59', 102)
	  )
GROUP BY  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC

SELECT
	RM00101.CUSTCLAS, 
	RM00101.CUSTNMBR, 
	RM00101.CUSTNAME, 
	RM00101.STATE,  
	SUM(#TEMPREPORT.CANTFAC2) AS CANTFAC2, 
	SUM(#TEMPREPORT.CANTDEV2) AS CANTDEV2, 
	SUM(#TEMPREPORT.CANTNETA2) AS CANTNETA2, 
	SUM(#TEMPREPORT.VTAFAC2) AS VTAFAC2, 
	SUM(#TEMPREPORT.MTO_DEV2) AS MTO_DEV2, 
	SUM(#TEMPREPORT.VTA_LIQ2) AS VTA_LIQ2,
	SUM(#TEMPREPORT.CANTFAC1) AS CANTFAC1, 
	SUM(#TEMPREPORT.CANTDEV1) AS CANTDEV1, 
	SUM(#TEMPREPORT.CANTNETA1) AS CANTNETA1, 
	SUM(#TEMPREPORT.VTAFAC1) AS VTAFAC1, 
	SUM(#TEMPREPORT.MTO_DEV1) AS MTO_DEV1, 
	SUM(#TEMPREPORT.VTA_LIQ1) AS VTA_LIQ1
FROM  
	#TEMPREPORT INNER JOIN 
	RM00101 
		ON #TEMPREPORT.CUSTNMBR = RM00101.CUSTNMBR
GROUP BY  
	RM00101.CUSTCLAS, 
	RM00101.CUSTNMBR, 
	RM00101.CUSTNAME, 
	RM00101.STATE 
ORDER BY 
	SUM(#TEMPREPORT.CANTNETA2) 
DESC

DROP TABLE #TEMPREPORT

END
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REPFACDEV2]    Script Date: 01/11/2009 22:21:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REPFACDEV2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REPFACDEV2]
GO
CREATE      PROCEDURE SP_SANT_REPFACDEV2
(@FECHA AS DATETIME)
AS
BEGIN

CREATE TABLE #TEMPREPORT 
(CUSTNMBR VARCHAR(15), ITMCLSCD VARCHAR(15), ITEMNMBR VARCHAR(31), ITEMDESC VARCHAR(101), CANTFAC1 NUMERIC, CANTDEV1 NUMERIC, CANTNETA1 NUMERIC, 
                                              VTAFAC1 NUMERIC, MTO_DEV1 NUMERIC, VTA_LIQ1 NUMERIC, 
                                              CANTFAC2 NUMERIC, CANTDEV2 NUMERIC, CANTNETA2 NUMERIC, 
                                              VTAFAC2 NUMERIC, MTO_DEV2 NUMERIC, VTA_LIQ2 NUMERIC)


INSERT INTO #TEMPREPORT (CUSTNMBR , ITMCLSCD , ITEMNMBR, ITEMDESC, CANTFAC1 , CANTDEV1 , CANTNETA1 , 
                                              VTAFAC1 , MTO_DEV1 , VTA_LIQ1 , 
                                              CANTFAC2 , CANTDEV2 , CANTNETA2 , 
                                              VTAFAC2 , MTO_DEV2 , VTA_LIQ2 )
SELECT  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC,
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTFAC1, 
SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTDEV1, 
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END + 
    CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END) AS CANTNETA1,

SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
         THEN PRECIOV*CANT 
         ELSE 0
         END) AS VTAFAC1,

SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
         THEN PRECIOV*CANT
	 ELSE 0
         END) AS MTO_DEV1,

SUM(CASE
	WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN CASE 
                  WHEN  (ITEMNMBR LIKE '%DESC%')
                  THEN  
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)
                  ELSE  
		   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
		         THEN PRECIOV*CANT 
		         ELSE 0
		         END)+	
		   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
		         THEN PRECIOV*CANT
			 ELSE 0
		         END)-
			(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			      THEN CASE WHEN  (ITEMNMBR LIKE '%DESC%')
			                THEN (PRECIOV*(-1))*(CANT*(-1))
			                ELSE (MRKDNAMT*CANT)
			                END
			      ELSE (MRKDNAMT*CANT)
			      END)
             END
        ELSE 
	   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
	         THEN PRECIOV*CANT 
	         ELSE 0
	         END)+	
	   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
	         THEN PRECIOV*CANT
		 ELSE 0
	         END)-
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN (PRECIOV*(-1))*(CANT*(-1))
		                  ELSE (MRKDNAMT*CANT)
		             END
		        ELSE (MRKDNAMT*CANT)
		END)
     END) AS VTA_LIQ1,
 0 AS CANTFAC2, 0 AS CANTDEV2, 0 AS CANTNETA2, 0 AS VTAFAC2, 0 AS MTO_DEV2, 0 AS VTA_LIQ2
FROM VTASTOTPRODAFA 
WHERE     
	  --(CUSTCLAS = 'LIBRERIAS') AND	
          (ITMCLSCD <> 'SERVICIO') AND 
          (PRECIOV <> 0) AND 
          (SOPTYPE = 3 OR SOPTYPE = 4) AND 
          (SOPNUMBE NOT LIKE 'DP%') AND
          (VOIDSTTS = 0) AND  
          --(SOPNUMBE NOT LIKE 'NC0004857%') AND 
          --(SOPNUMBE NOT LIKE 'F0003266%') AND 
          --(SOPNUMBE NOT LIKE 'NC0005683%') AND 
          (DOCID <> 'DEV-PROMOCION') AND 
          --((ITMCLSCD = 'TEXTO') OR (ITMCLSCD = 'EGIJ') OR (ITMCLSCD = 'IDIOMAS')) AND           
          (DOCDATE 
		BETWEEN 
			CONVERT(DATETIME, CAST(DATEPART(YEAR,@FECHA)-1 AS VARCHAR)+'-01-01 00:00:00', 102) 
		AND 
			CONVERT(DATETIME, CONVERT(VARCHAR(10),DATEADD(YEAR,-1,@FECHA),120)+' 23:59:59', 102)
	  )
GROUP BY  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC
UNION ALL
SELECT  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC, 0 AS CANTFAC1, 0 AS CANTDEV1, 0 AS CANTNETA1, 0 AS VTAFAC1, 0 AS MTO_DEV1, 0 AS VTA_LIQ1,
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTFAC2, 
SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTDEV2, 
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END + 
    CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END) AS CANTNETA2,

SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
         THEN PRECIOV*CANT 
         ELSE 0
         END) AS VTAFAC2,

SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
         THEN PRECIOV*CANT
	 ELSE 0
         END) AS MTO_DEV2,


SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
         THEN CASE WHEN (ITEMNMBR LIKE '%DESC%')
                   THEN (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			      THEN CASE WHEN  (ITEMNMBR LIKE '%DESC%')
			                THEN (PRECIOV*(-1))*(CANT*(-1))
			                ELSE (MRKDNAMT*CANT)
			                END
			      ELSE (MRKDNAMT*CANT)
			      END)

                   ELSE 	   
		        (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
			         THEN PRECIOV*CANT 
			         ELSE 0
			         END)+	
			(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
			         THEN PRECIOV*CANT
				 ELSE 0
			         END)-
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)
             END
        ELSE 
	   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
	         THEN PRECIOV*CANT 
	         ELSE 0
	         END)+	
	   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
	         THEN PRECIOV*CANT
		 ELSE 0
	         END)-
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN (PRECIOV*(-1))*(CANT*(-1))
		                  ELSE (MRKDNAMT*CANT)
		             END
		        ELSE (MRKDNAMT*CANT)
		END)
     END) AS VTA_LIQ2
FROM VTASTOTPRODAFA 
WHERE     
	  --(CUSTCLAS = 'LIBRERIAS') AND	
          (ITMCLSCD <> 'SERVICIO') AND 
          (PRECIOV <> 0) AND 
          (SOPTYPE = 3 OR SOPTYPE = 4) AND 
          (SOPNUMBE NOT LIKE 'DP%') AND
          (VOIDSTTS = 0) AND  
          --(SOPNUMBE NOT LIKE 'NC0004857%') AND 
          --(SOPNUMBE NOT LIKE 'F0003266%') AND 
          --(SOPNUMBE NOT LIKE 'NC0005683%') AND 
          (DOCID <> 'DEV-PROMOCION') AND 
--          ((ITMCLSCD = 'TEXTO') OR (ITMCLSCD = 'EGIJ') OR (ITMCLSCD = 'IDIOMAS')) AND 
	  (DOCDATE 
		BETWEEN 
			CONVERT(DATETIME, CAST(DATEPART(YEAR,@FECHA) AS VARCHAR)+'-01-01 00:00:00', 102) 
		AND 
			CONVERT(DATETIME, CONVERT(VARCHAR(10),@FECHA,120)+' 23:59:59', 102)
	  )
GROUP BY  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC

SELECT
	RM00101.CUSTCLAS, 
	#TEMPREPORT.ITMCLSCD,
	SUM(#TEMPREPORT.CANTFAC2) AS CANTFAC2, 
	SUM(#TEMPREPORT.CANTDEV2) AS CANTDEV2, 
	SUM(#TEMPREPORT.CANTNETA2) AS CANTNETA2, 
	SUM(#TEMPREPORT.VTAFAC2) AS VTAFAC2, 
	SUM(#TEMPREPORT.MTO_DEV2) AS MTO_DEV2, 

	SUM(#TEMPREPORT.VTA_LIQ2) AS VTA_LIQ2,
	SUM(#TEMPREPORT.CANTFAC1) AS CANTFAC1, 
	SUM(#TEMPREPORT.CANTDEV1) AS CANTDEV1, 
	SUM(#TEMPREPORT.CANTNETA1) AS CANTNETA1, 
	SUM(#TEMPREPORT.VTAFAC1) AS VTAFAC1, 
	SUM(#TEMPREPORT.MTO_DEV1) AS MTO_DEV1, 
	SUM(#TEMPREPORT.VTA_LIQ1) AS VTA_LIQ1
FROM  
	#TEMPREPORT INNER JOIN 
	RM00101 
		ON #TEMPREPORT.CUSTNMBR = RM00101.CUSTNMBR
GROUP BY
	RM00101.CUSTCLAS, 
	#TEMPREPORT.ITMCLSCD
ORDER BY
	RM00101.CUSTCLAS,
	#TEMPREPORT.ITMCLSCD


DROP TABLE #TEMPREPORT

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REPFACDEV3]    Script Date: 01/11/2009 22:23:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REPFACDEV3]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REPFACDEV3]
GO
CREATE      PROCEDURE SP_SANT_REPFACDEV3
(@FECHA AS DATETIME)
AS
BEGIN

CREATE TABLE #TEMPREPORT 
(CUSTNMBR VARCHAR(15), ITMCLSCD VARCHAR(15), ITEMNMBR VARCHAR(31), ITEMDESC VARCHAR(101), CANTFAC1 NUMERIC, CANTDEV1 NUMERIC, CANTNETA1 NUMERIC, 
                                              VTAFAC1 NUMERIC, MTO_DEV1 NUMERIC, VTA_LIQ1 NUMERIC, 
                                              CANTFAC2 NUMERIC, CANTDEV2 NUMERIC, CANTNETA2 NUMERIC, 
                                              VTAFAC2 NUMERIC, MTO_DEV2 NUMERIC, VTA_LIQ2 NUMERIC)


INSERT INTO #TEMPREPORT (CUSTNMBR , ITMCLSCD , ITEMNMBR, ITEMDESC, CANTFAC1 , CANTDEV1 , CANTNETA1 , 
                                              VTAFAC1 , MTO_DEV1 , VTA_LIQ1 , 
                                              CANTFAC2 , CANTDEV2 , CANTNETA2 , 
                                              VTAFAC2 , MTO_DEV2 , VTA_LIQ2 )
SELECT  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC,
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTFAC1, 
SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTDEV1, 
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END + 
    CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END) AS CANTNETA1,

SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
         THEN PRECIOV*CANT 
         ELSE 0
         END) AS VTAFAC1,

SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
         THEN PRECIOV*CANT
	 ELSE 0
         END) AS MTO_DEV1,

SUM(CASE
	WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN CASE 
                  WHEN  (ITEMNMBR LIKE '%DESC%')
                  THEN  
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)
                  ELSE  
		   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
		         THEN PRECIOV*CANT 
		         ELSE 0
		         END)+	
		   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
		         THEN PRECIOV*CANT
			 ELSE 0
		         END)-
			(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			      THEN CASE WHEN  (ITEMNMBR LIKE '%DESC%')
			                THEN (PRECIOV*(-1))*(CANT*(-1))
			                ELSE (MRKDNAMT*CANT)
			                END
			      ELSE (MRKDNAMT*CANT)
			      END)
             END
        ELSE 
	   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
	         THEN PRECIOV*CANT 
	         ELSE 0
	         END)+	
	   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
	         THEN PRECIOV*CANT
		 ELSE 0
	         END)-
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN (PRECIOV*(-1))*(CANT*(-1))
		                  ELSE (MRKDNAMT*CANT)
		             END
		        ELSE (MRKDNAMT*CANT)
		END)
     END) AS VTA_LIQ1,
 0 AS CANTFAC2, 0 AS CANTDEV2, 0 AS CANTNETA2, 0 AS VTAFAC2, 0 AS MTO_DEV2, 0 AS VTA_LIQ2
FROM VTASTOTPRODAFA 
WHERE     
	  --(CUSTCLAS = 'LIBRERIAS') AND	
          (ITMCLSCD <> 'SERVICIO') AND 
          (PRECIOV <> 0) AND 
          (SOPTYPE = 3 OR SOPTYPE = 4) AND 
          (SOPNUMBE NOT LIKE 'DP%') AND
          (VOIDSTTS = 0) AND  
          --(SOPNUMBE NOT LIKE 'NC0004857%') AND 
          --(SOPNUMBE NOT LIKE 'F0003266%') AND 
          --(SOPNUMBE NOT LIKE 'NC0005683%') AND 
          (DOCID <> 'DEV-PROMOCION') AND 
          --((ITMCLSCD = 'TEXTO') OR (ITMCLSCD = 'EGIJ') OR (ITMCLSCD = 'IDIOMAS')) AND           
          (DOCDATE 
		BETWEEN 
			CONVERT(DATETIME, CAST(DATEPART(YEAR,@FECHA)-1 AS VARCHAR)+'-01-01 00:00:00', 102) 
		AND 
			CONVERT(DATETIME, CONVERT(VARCHAR(10),DATEADD(YEAR,-1,@FECHA),120)+' 23:59:59', 102)
	  )
GROUP BY  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC
UNION ALL
SELECT  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC, 0 AS CANTFAC1, 0 AS CANTDEV1, 0 AS CANTNETA1, 0 AS VTAFAC1, 0 AS MTO_DEV1, 0 AS VTA_LIQ1,
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTFAC2, 
SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTDEV2, 
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END + 
    CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END) AS CANTNETA2,

SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
         THEN PRECIOV*CANT 
         ELSE 0
         END) AS VTAFAC2,

SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
         THEN PRECIOV*CANT
	 ELSE 0
         END) AS MTO_DEV2,


SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
         THEN CASE WHEN (ITEMNMBR LIKE '%DESC%')
                   THEN (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			      THEN CASE WHEN  (ITEMNMBR LIKE '%DESC%')
			                THEN (PRECIOV*(-1))*(CANT*(-1))
			                ELSE (MRKDNAMT*CANT)
			                END
			      ELSE (MRKDNAMT*CANT)
			      END)

                   ELSE 	   
		        (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
			         THEN PRECIOV*CANT 
			         ELSE 0
			         END)+	
			(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
			         THEN PRECIOV*CANT
				 ELSE 0
			         END)-
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)
             END
        ELSE 
	   (CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE '%DESC%') AND (ITEMNMBR NOT LIKE '%DEV%') 
	         THEN PRECIOV*CANT 
	         ELSE 0
	         END)+	
	   (CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') 
	         THEN PRECIOV*CANT
		 ELSE 0
	         END)-
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN (PRECIOV*(-1))*(CANT*(-1))
		                  ELSE (MRKDNAMT*CANT)
		             END
		        ELSE (MRKDNAMT*CANT)
		END)
     END) AS VTA_LIQ2
FROM VTASTOTPRODAFA 
WHERE     
	  --(CUSTCLAS = 'LIBRERIAS') AND	
          (ITMCLSCD <> 'SERVICIO') AND 
          (PRECIOV <> 0) AND 
          (SOPTYPE = 3 OR SOPTYPE = 4) AND 
          (SOPNUMBE NOT LIKE 'DP%') AND
          (VOIDSTTS = 0) AND  
          --(SOPNUMBE NOT LIKE 'NC0004857%') AND 
          --(SOPNUMBE NOT LIKE 'F0003266%') AND 
          --(SOPNUMBE NOT LIKE 'NC0005683%') AND 
          (DOCID <> 'DEV-PROMOCION') AND 
--          ((ITMCLSCD = 'TEXTO') OR (ITMCLSCD = 'EGIJ') OR (ITMCLSCD = 'IDIOMAS')) AND 
	  (DOCDATE 
		BETWEEN 
			CONVERT(DATETIME, CAST(DATEPART(YEAR,@FECHA) AS VARCHAR)+'-01-01 00:00:00', 102) 
		AND 
			CONVERT(DATETIME, CONVERT(VARCHAR(10),@FECHA,120)+' 23:59:59', 102)
	  )
GROUP BY  CUSTNMBR, ITMCLSCD, ITEMNMBR, ITEMDESC

SELECT 
	rm00101.STATE, 
	rm00101.CUSTCLAS, 
	#TEMPREPORT.ITMCLSCD,
	sum(#TEMPREPORT.CANTFAC2) AS CANTFAC2, 
	sum(#TEMPREPORT.CANTDEV2) AS CANTDEV2, 
	sum(#TEMPREPORT.CANTNETA2) AS CANTNETA2, 
	sum(#TEMPREPORT.VTAFAC2) AS VTAFAC2, 
	sum(#TEMPREPORT.MTO_DEV2) AS MTO_DEV2, 
	sum(#TEMPREPORT.VTA_LIQ2) AS VTA_LIQ2,
	sum(#TEMPREPORT.CANTFAC1) AS CANTFAC1, 
	sum(#TEMPREPORT.CANTDEV1) AS CANTDEV1, 
	sum(#TEMPREPORT.CANTNETA1) AS CANTNETA1, 
	sum(#TEMPREPORT.VTAFAC1) AS VTAFAC1, 
	sum(#TEMPREPORT.MTO_DEV1) AS MTO_DEV1, 
	sum(#TEMPREPORT.VTA_LIQ1) AS VTA_LIQ1
FROM  
	#TEMPREPORT INNER JOIN 
	rm00101 
		ON #TEMPREPORT.CUSTNMBR = rm00101.CUSTNMBR
GROUP BY  
	rm00101.STATE, 
	rm00101.CUSTCLAS, 
	#TEMPREPORT.ITMCLSCD
order by  
	rm00101.STATE, 
	rm00101.CUSTCLAS, 
	#TEMPREPORT.ITMCLSCD

DROP TABLE #TEMPREPORT

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_REPMASVENDIDOS2]    Script Date: 01/11/2009 22:25:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_REPMASVENDIDOS2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_REPMASVENDIDOS2]
GO
CREATE      PROCEDURE SP_SANT_REPMASVENDIDOS2
(@FECHA AS DATETIME)
AS
BEGIN

SELECT CUSTCLAS, ITMCLSCD, ITEMNMBR, ITEMDESC,
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTFAC, 
SUM(CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END) AS CANTDEV, 
SUM(CASE WHEN (SOPTYPE = 3) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END + 
    CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%')THEN CASE WHEN CANT IS NULL THEN 0 ELSE CANT END ELSE 0 END) AS CANTNETA,

SUM(CASE 
        WHEN (SOPTYPE = 3) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN (PRECIOV*1)
        ELSE CASE WHEN (SOPTYPE = 3) THEN PRECIOV*CANT ELSE 0 END
   END) AS VTAFAC,

SUM(CASE 
        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN CASE 
                  WHEN  (ITEMNMBR LIKE '%DESC%')
                  THEN 0
                  ELSE (PRECIOV*(-1))*(CANT*(-1))
             END
        ELSE CASE 
                  WHEN  (SOPTYPE = 4)
                  THEN (PRECIOV*(-1)) * ((CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END)*(-1))
                  ELSE 0
             END
     END) AS MTO_DEV,

SUM(CASE
	WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
        THEN CASE 
                  WHEN  (ITEMNMBR LIKE '%DESC%')
                  THEN  
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)

                  ELSE  
			(CASE 
			        WHEN (SOPTYPE = 3) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN (PRECIOV*1)
			        ELSE CASE WHEN (SOPTYPE = 3) THEN PRECIOV*CANT ELSE 0 END
			END)+
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN 0
			                  ELSE (PRECIOV*(-1))*(CANT*(-1))
			             END
			        ELSE CASE 
			                  WHEN  (SOPTYPE = 4)
			                  THEN (PRECIOV*(-1)) * ((CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END)*(-1))
			                  ELSE 0
			             END
			END)-
			(CASE 
			        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
			        THEN CASE 
			                  WHEN  (ITEMNMBR LIKE '%DESC%')
			                  THEN (PRECIOV*(-1))*(CANT*(-1))
			                  ELSE (MRKDNAMT*CANT)
			             END
			        ELSE (MRKDNAMT*CANT)
			END)
             END
        ELSE 
		(CASE 
		        WHEN (SOPTYPE = 3) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN (PRECIOV*1)
		        ELSE CASE WHEN (SOPTYPE = 3) THEN PRECIOV*CANT ELSE 0 END
		END)+
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN 0
		                  ELSE (PRECIOV*(-1))*(CANT*(-1))
		             END
		        ELSE CASE 
		                  WHEN  (SOPTYPE = 4)
		                  THEN (PRECIOV*(-1)) * ((CASE WHEN (SOPTYPE = 4) AND (ITEMNMBR NOT LIKE 'NOITEM%') THEN CANT ELSE 0 END)*(-1))
		                  ELSE 0
		             END
		END)-
		(CASE 
		        WHEN (SOPTYPE = 4) AND (ITEMNMBR LIKE 'NOITEM%') 
		        THEN CASE 
		                  WHEN  (ITEMNMBR LIKE '%DESC%')
		                  THEN (PRECIOV*(-1))*(CANT*(-1))
		                  ELSE (MRKDNAMT*CANT)
		             END
		        ELSE (MRKDNAMT*CANT)
		END)
     END) AS VTA_LIQ
INTO #20_ITEM_MAS_VENTA
FROM VTASTOTPRODAFA 
WHERE     
	  --(CUSTCLAS = 'CONTADO') AND	
          (ITMCLSCD <> 'SERVICIO') AND 
          (PRECIOV <> 0) AND 
          (SOPTYPE = 3 OR SOPTYPE = 4) AND 
          (SOPNUMBE NOT LIKE 'DP%') AND
          (VOIDSTTS = 0) AND  
          --(SOPNUMBE NOT LIKE 'NC0004857%') AND 
	  --(SOPNUMBE NOT LIKE 'F0003266%') AND 
          --(SOPNUMBE NOT LIKE 'NC0005683%') AND 
          
       (DOCID <> 'DEV-PROMOCION') AND 
           
          (DOCDATE BETWEEN CONVERT(DATETIME, CAST(DATEPART(YEAR,@FECHA)-1 AS VARCHAR)+'-01-01 00:00:00', 102) AND 
                           CONVERT(DATETIME, CONVERT(VARCHAR(10),DATEADD(YEAR,-1,@FECHA),120)+' 00:00:00', 102))
GROUP BY  CUSTCLAS, ITMCLSCD, ITEMNMBR, ITEMDESC

SELECT
	ITMCLSCD,
	CUSTCLAS, 
	ITEMNMBR, 
	ITEMDESC,
	SUM(CANTFAC) AS CANTFAC, 
	SUM(CANTDEV) AS CANTDEV, 
	SUM(CANTNETA) AS CANTNETA
FROM  
	#20_ITEM_MAS_VENTA 
GROUP BY
	ITMCLSCD,
	CUSTCLAS,
	ITEMNMBR,
	ITEMDESC
ORDER BY 
	ITMCLSCD, 
	SUM(CANTNETA) DESC


DROP TABLE #20_ITEM_MAS_VENTA

END


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_ST_ITEM_GUIA]    Script Date: 01/11/2009 22:26:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_ST_ITEM_GUIA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_ST_ITEM_GUIA]
GO
CREATE      PROCEDURE SP_SANT_ST_ITEM_GUIA
(@ID_ORD NUMERIC, @ID_ST INT, @OBS VARCHAR(500), @TIPO INT)
AS
BEGIN
	IF(@TIPO=1)
	BEGIN
		IF(@ID_ST=3)
		BEGIN
			UPDATE T_SANT_ORD 
			SET ID_GUIA=NULL , FE_DESP_ORD=NULL , 
				ID_ST_ORD=2, ID_ST_ENT=NULL, 
				FE_ST_ENT=NULL
			WHERE ID_ORD=@ID_ORD
		END
		ELSE
		BEGIN
			IF(@ID_ST=1)
			BEGIN
				UPDATE T_SANT_ORD SET ID_ST_ENT=@ID_ST, OBS_ENT=@OBS, FE_ENT_FAC=GETDATE()
				WHERE ID_ORD=@ID_ORD
			END
			ELSE
			BEGIN
				UPDATE T_SANT_ORD SET ID_ST_ENT=@ID_ST, OBS_ENT=@OBS
				WHERE ID_ORD=@ID_ORD
			END
		END
	END

	IF(@TIPO=0)
		UPDATE T_SANT_MEMO_GUIA SET ST_ENT=@ID_ST, OBS_MEMO=@OBS
		WHERE ID_MEMO=@ID_ORD

	IF(@TIPO=2)
		UPDATE T_SANT_DEV SET ST_ENT=@ID_ST, OBS=@OBS
		WHERE ID_DEV=@ID_ORD

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_UPDATE_ADDRESS]    Script Date: 01/11/2009 22:27:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_UPDATE_ADDRESS]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_UPDATE_ADDRESS]
GO
CREATE 	    PROCEDURE SP_SANT_UPDATE_ADDRESS @@DERROR CHAR(255) OUT AS
	DECLARE
		@ID_ORD_GP varchar(21),
		@DIR_ENT VARCHAR(500),
		@ADDRESS1 CHAR(31),
		@ADDRESS2 CHAR(31),
		@UPDATE_ERROR INTEGER

	SET @UPDATE_ERROR = 0

	-- INICIO DEL TRABAJO SUCIO
	BEGIN TRANSACTION

	-- DECLARAR EL CURSOR DE CARGAR LOS DATOS, INICIALIZARLO Y ABRIRLO
	DECLARE CURSOR_CARGAR CURSOR
		FOR SELECT ID_ORD_GP, ADDRESS1, ADDRESS2
		FROM v_sant_direcciones_vw
	OPEN CURSOR_CARGAR
	FETCH NEXT FROM CURSOR_CARGAR INTO @ID_ORD_GP, @ADDRESS1, @ADDRESS2

	-- BUCLE DE ACTUALIZACION
	WHILE (@@FETCH_STATUS <> -1)
	BEGIN
		IF (@UPDATE_ERROR <> 0)-- THEN
			BREAK

		-- ACTUALIZAR EL CAMPO DE DIRECCION
		SET @DIR_ENT = LTRIM(RTRIM(@ADDRESS1)) + ' ' + LTRIM(RTRIM(@ADDRESS2))
			UPDATE T_SANT_ORD
			SET DIR_ENT = @DIR_ENT
			WHERE ID_ORD_GP = @ID_ORD_GP
		IF @@ERROR <> 0 --THEN
			SET @UPDATE_ERROR = -1

		-- AVANZAR EN EL BUCLE
		FETCH NEXT FROM CURSOR_CARGAR INTO @ID_ORD_GP, @ADDRESS1, @ADDRESS2

	END

	-- FINALIZAR ALGUNAS COSAS
	CLOSE CURSOR_CARGAR
	DEALLOCATE CURSOR_CARGAR

	-- REGISTRAR LOS CAMBIOS SOLO EN CASO DE QUE TODO HAYA TERMINADO BIEN
	IF @UPDATE_ERROR = 0 --THEN
	BEGIN
		PRINT 'ACTUALIZACION REALIZADA SIN ERRORES DETECTADOS'
		COMMIT TRANSACTION
	END ELSE BEGIN
		PRINT 'ACTUALIZACION NO REALIZADA, SE ENCONTRARON ERRORES'
		PRINT 'ULTIMO ERROR: ' + RTRIM(@UPDATE_ERROR)
		ROLLBACK TRANSACTION
	END

--END PROCEDURE


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_UPDATE_ADDRESS2]    Script Date: 01/11/2009 22:29:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_UPDATE_ADDRESS2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_UPDATE_ADDRESS2]
GO
CREATE 	    PROCEDURE SP_SANT_UPDATE_ADDRESS2 @@DERROR CHAR(255) OUT AS
	DECLARE
		@ID_ORD_GP VARCHAR(21),
		@DIR_ENT VARCHAR(500),
		@ADDRESS1 CHAR(31),
		@ADDRESS2 CHAR(31),
		@UPDATE_ERROR INTEGER

	SET @UPDATE_ERROR = 0

	-- INICIO DEL TRABAJO SUCIO
	BEGIN TRANSACTION

	-- DECLARAR EL CURSOR DE CARGAR LOS DATOS, INICIALIZARLO Y ABRIRLO
	DECLARE CURSOR_CARGAR CURSOR
		FOR SELECT ID_ORD_GP, ADDRESS1, ADDRESS2
		FROM v_sant_direcciones_vw2
	OPEN CURSOR_CARGAR
	FETCH NEXT FROM CURSOR_CARGAR INTO @ID_ORD_GP, @ADDRESS1, @ADDRESS2

	-- BUCLE DE ACTUALIZACION
	WHILE (@@FETCH_STATUS <> -1)
	BEGIN
		IF (@UPDATE_ERROR <> 0)-- THEN
			BREAK

		-- ACTUALIZAR EL CAMPO DE DIRECCION
		SET @DIR_ENT = LTRIM(RTRIM(@ADDRESS1)) + ' ' + LTRIM(RTRIM(@ADDRESS2))
			UPDATE T_SANT_ORD
			SET DIR_ENT = @DIR_ENT
			WHERE ID_ORD_GP = @ID_ORD_GP
		IF @@ERROR <> 0 --THEN
			SET @UPDATE_ERROR = -1

		-- AVANZAR EN EL BUCLE
		FETCH NEXT FROM CURSOR_CARGAR INTO @ID_ORD_GP, @ADDRESS1, @ADDRESS2

	END

	-- FINALIZAR ALGUNAS COSAS
	CLOSE CURSOR_CARGAR
	DEALLOCATE CURSOR_CARGAR

	-- REGISTRAR LOS CAMBIOS SOLO EN CASO DE QUE TODO HAYA TERMINADO BIEN
	IF @UPDATE_ERROR = 0 --THEN
	BEGIN
		PRINT 'ACTUALIZACION REALIZADA SIN ERRORES DETECTADOS'
		COMMIT TRANSACTION
	END ELSE BEGIN
		PRINT 'ACTUALIZACION NO REALIZADA, SE ENCONTRARON ERRORES'
		PRINT 'ULTIMO ERROR: ' + RTRIM(@UPDATE_ERROR)
		ROLLBACK TRANSACTION
	END

--END PROCEDURE


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


/****** Object:  StoredProcedure [dbo].[SP_SANT_UPD_SERIAL_DEV_PRE]    Script Date: 01/11/2009 22:30:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_UPD_SERIAL_DEV_PRE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_UPD_SERIAL_DEV_PRE]
GO
CREATE  PROCEDURE SP_SANT_UPD_SERIAL_DEV_PRE
(@ID_DEV NUMERIC,
 @CANT_CAJA INT,
 @ID_PROD VARCHAR(20),
 @CANT_PROD INT,
 @STATUS INT)
AS
BEGIN

UPDATE T_SANT_DEV WITH (ROWLOCK) 
SET CANT_CAJA=@CANT_CAJA 
WHERE ID_DEV=@ID_DEV

UPDATE T_SANT_DET_DEV_PRE  WITH (ROWLOCK)  
SET CANT_PROD=@CANT_PROD
WHERE ID_DEV=@ID_DEV 
	AND ID_PROD=@ID_PROD

END



GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO



/****** Object:  StoredProcedure [dbo].[SP_SANT_VALIDAUSUARIO]    Script Date: 01/11/2009 22:31:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SP_SANT_VALIDAUSUARIO]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[SP_SANT_VALIDAUSUARIO]
GO
CREATE      PROCEDURE SP_SANT_VALIDAUSUARIO(@USR VARCHAR(15), @PWD VARCHAR(15))
AS
BEGIN
/********************************************************************************
PROCEDIMIENTO ALMACENADO. 
FINALIDAD: 	Provee del mecanismo de validacion para el ingreso de usuarios a
		la solucion.
RETORNA: 	0 si el usuario es valido
		-1 si el usuario no es valido
FECHA: 		07/03/2006
DESARROLLADO:	Dario Carnelutti - SITVEN, C.A.
********************************************************************************/
IF EXISTS(SELECT LOG_USR FROM T_SANT_USR (READPAST) WHERE LOG_USR=@USR AND PWD_USR=@PWD AND IN_ST=1)
BEGIN
	UPDATE T_SANT_USR SET FE_HORA_ULT_ING=GETDATE() WHERE LOG_USR=@USR AND PWD_USR=@PWD
	RAISERROR ('0',17,1)
	RETURN
END
ELSE
BEGIN
	RAISERROR ('-1',17,1)
	RETURN
END
END

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

