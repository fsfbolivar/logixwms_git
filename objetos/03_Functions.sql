--==================================================
--
--  Funci�n booleana que investiga si un documento
--  esta suspendido (TRUE) o no (FALSE) en GP 10
--
--  Nota: Debiera funcionar tambi�n en GP 9
--
--==================================================

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE NAME = 'FN_SANT_SUSPENDIDO_EN_GP' AND TYPE = 'FN')
	DROP FUNCTION FN_SANT_SUSPENDIDO_EN_GP
GO

CREATE FUNCTION FN_SANT_SUSPENDIDO_EN_GP (@SOPNUMBE CHAR(21), @SOPTYPE SMALLINT, @PRCHLDID CHAR(15))
RETURNS INT
AS BEGIN
	DECLARE @VALUE INT
	IF @SOPTYPE IS NULL
		SET @SOPTYPE = 2
	IF @PRCHLDID IS NULL
		SELECT @VALUE = COUNT(*)
		FROM SOP10104
		WHERE SOPNUMBE = @SOPNUMBE
			AND SOPTYPE = @SOPTYPE
			AND DELETE1 = 0
	ELSE
		SELECT @VALUE = COUNT(*)
		FROM SOP10104
		WHERE SOPNUMBE = @SOPNUMBE
			AND SOPTYPE = @SOPTYPE
			AND DELETE1 = 0
			AND PRCHLDID = @PRCHLDID
	IF @VALUE > 0
		SET @VALUE = 1
	ELSE
		SET @VALUE = 0
	RETURN @VALUE
END
GO

--==================================================
--
--  Calcula la fecha contable de un documento.
--  Es decir, devuelve la fecha del d�a si el modulo
--  esta abierto, pero dice la primera fecha del
--  siguiente periodo si resulta estar cerrado.
--
--==================================================

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE NAME = 'FN_SANT_FECHA_CONTABLE' AND TYPE = 'FN')
	DROP FUNCTION FN_SANT_FECHA_CONTABLE
GO

CREATE FUNCTION FN_SANT_FECHA_CONTABLE () RETURNS DATETIME
AS BEGIN
	DECLARE @HayAbiertos INT
	DECLARE @FechaContable DATETIME

	-- Investigar cuantos periodos hay abiertos (generalmente 1 o 0)
	SELECT @HayAbiertos = COUNT(*)
	FROM SY40100
	WHERE SERIES   = '3'              -- ventas
		AND   CLOSED   = '0'          -- que esta abierto
		AND   PERIODID <> '0'         -- que sea 'enero', 'febrero' y as�...
		AND   ODESCTN  = 'Sales Transaction Entry'

	-- �Que fecha es hoy?
	SET @FechaContable = CONVERT( DATETIME, CONVERT( CHAR(10), GETDATE(), 112), 112)

	-- Segun haya periodos abiertos o no
	IF @HayAbiertos > 0
	BEGIN
		-- como hay periodos abiertos, entonces tomar la fecha de hoy o la inicial
		-- del periodo
		SELECT @FechaContable = PERIODDT
		FROM SY40100
		WHERE SERIES   = '3'              -- ventas
			AND   CLOSED   = '0'          -- que esta abierto
			AND   PERIODID <> '0'         -- que sea 'enero', 'febrero' y as�...
			AND   ODESCTN  = 'Sales Transaction Entry'
										  -- obvio...
			AND NOT( @FechaContable BETWEEN PERIODDT AND PERDENDT )
										  -- que la fecha de hoy no este en el periodo
	END ELSE BEGIN
		-- como no hay periodos abiertos, entonces tomar la primera fecha del
		-- mes siguiente basandose en la fecha de hoy
		SET @FechaContable = DATEADD(MONTH, 1, DATEADD(DAY, -1 * (DAY(@FechaContable - 1)), @FechaContable))
	END

	-- al final retorno lo que tenga en este punto
	RETURN @FechaContable
END
GO



