--==================================================
--
--  Procedimiento para quitar la suspensión de un
--  documento en GP 10 (o GP 9)
--
--==================================================

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE NAME = 'SP_SANT_QUITAR_SUSPENSION' AND TYPE = 'P')
	DROP PROCEDURE SP_SANT_QUITAR_SUSPENSION
GO

CREATE  PROCEDURE [dbo].[SP_SANT_QUITAR_SUSPENSION] (@SOPNUMBE CHAR(21), @SOPTYPE SMALLINT, @PRCHLDID CHAR(15))
AS
	--IF @SOPTYPE IS NULL
	--	SET @SOPTYPE = 2
  --	IF @PRCHLDID IS NULL
	--	UPDATE SOP10104
	--	SET DELETE1 = 1
	--	WHERE SOPNUMBE = @SOPNUMBE AND SOPTYPE = @SOPTYPE
	--ELSE
		UPDATE SOP10104
		SET DELETE1 = 1
		WHERE SOPNUMBE = @SOPNUMBE --AND SOPTYPE = @SOPTYPE AND PRCHLDID like @PRCHLDID
GO

/****** Objeto:  StoredProcedure [dbo].[lote_promocion]    Fecha de la secuencia de comandos: 05/06/2009 22:18:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Objeto:  StoredProcedure [dbo].[lote_promocion]    Fecha de la secuencia de comandos: 05/06/2009 22:18:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lote_promocion]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[lote_promocion]
GO

CREATE  PROCEDURE [dbo].[lote_promocion] (@SOP CHAR(21)) --SUPNUMBE 
AS
if not exists( select 1 from SY00500 where BACHNUMB = 'PROMOCIO')
BEGIN DECLARE @num int EXEC dbo.zDP_SY00500SI '1900.01.01', 'Sales Entry', 'PROMOCIO', 3, 0, 0, 0, 0, 0, 1, '1900.01.01', 0, '', 0, 0, 0, 
'SA', '', 0.00000, 0x00000000, 0x00000000, '1900.01.01', '', '', 0, '1900.01.01', '1900.01.01', 0.00000, '', 0, 0, 0.00000, 0, 0, 
'1900.01.01', '', 1, 0, 0x00000000, '1900.01.01', 0, 0, 0, 0, '', 0, 0, 0, 0, '00:00:00', @num OUT SELECT @num END 

ELSE BEGIN
UPDATE SY00500 SET NUMOFTRX = NUMOFTRX + 1 where BACHNUMB = 'PROMOCIO'
END

UPDATE SOP10100 SET BACHNUMB = 'PROMOCIO' WHERE SOPNUMBE = @SOP 

GO

/****** Objeto:  StoredProcedure [dbo].[lote_facturable]    Fecha de la secuencia de comandos: 05/06/2009 22:18:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[lote_facturable]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[lote_facturable]
GO

CREATE  PROCEDURE [dbo].[lote_facturable] (@SOP CHAR(21)) --SUPNUMBE 
AS
if not exists( select 1 from SY00500 where BACHNUMB = 'FACTURABLE')
BEGIN DECLARE @num int EXEC dbo.zDP_SY00500SI '1900.01.01', 'Sales Entry', 'FACTURABLE', 3, 0, 0, 0, 0, 0, 1, '1900.01.01', 0, '', 0, 0, 0, 
'CCABRERA', '', 0.00000, 0x00000000, 0x00000000, '1900.01.01', '', '', 0, '1900.01.01', '1900.01.01', 0.00000, '', 0, 0, 0.00000, 0, 0, 
'1900.01.01', '', 1, 0, 0x00000000, '1900.01.01', 0, 0, 0, 0, '', 0, 0, 0, 0, '00:00:00', @num OUT SELECT @num END 

ELSE BEGIN
UPDATE SY00500 SET NUMOFTRX = NUMOFTRX + 1 where BACHNUMB = 'FACTURABLE'
END

UPDATE SOP10100 SET BACHNUMB = 'FACTURABLE' WHERE SOPNUMBE = @SOP 

GO