 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<STYLE>
@media print { DIV.PAGEBREAK {page-break-before: always}}
</STYLE>
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var ordenes=Request.Form("IMPORD")+"";
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var cantSol=0;
var cantEmb=0;
var cantDif=0;
var txOrd="";
var nbCliente="";
var dirClte="";
var tpoOrd="";
var telOrd="";
var ciuOrd="";
var txObs="";
var mes = new Array("01","02","03","04","05","06","07","08","09","10","11","12");
				a=new Date();
				m=a.getMonth();
				e=a.getYear();
				h=a.getHours();
				mi=a.getMinutes();
				s=a.getSeconds() ;
var fecha=a.getDate()+"-"+mes[m]+"-"+e+" "+h+":"+mi+":"+s ;
var ord=ordenes.split(',');
for(var i=0;i<ord.length;i++)
{
var idOrd=ord[i];
strSQL="SELECT ISNULL(IN_IMP,0) COPIA,CIU_ENT, DIR_ENT,TEL_CLTE1, TEL_CLTE2, ID_ST_ORD, ID_ORD, ID_ORD_GP, CASE ID_TPO_ORD WHEN 0 THEN 'Venta' ELSE 'Consignación' END ID_TPO_ORD, ";
strSQL+=" NB_CLTE, CONVERT(VARCHAR(12),FE_ING_ORD,103) FE_ING_ORD, 'Por embalar' ST, isnull(OBS,'') obs";
strSQL+=" FROM T_SANT_ORD WHERE ID_ORD="+idOrd;

Rs=Ejecutar(strSQL,2);

txOrd=Rs("ID_ORD_GP").Value;
nbCliente=Rs("NB_CLTE").Value;
dirClte=Rs("DIR_ENT").Value;
tpoOrd=Rs("ID_TPO_ORD").Value;
telOrd=Rs("TEL_CLTE1").Value+" "+Rs("TEL_CLTE2").Value;
ciuOrd=Rs("CIU_ENT").Value
txObs=Rs("OBS").Value
var copia=Rs("COPIA").Value
Rs.Close();

if(tpoOrd+""=="1")
{
	strSQL="UPDATE SNTCONSIGHDR SET STATUS='L' WHERE NOCONSIG='"+txOrd+"'";
	Ejecutar(strSQL,1);
}


strSQL="UPDATE T_SANT_ORD SET IN_IMP=1 WHERE ID_ORD="+idOrd;
Ejecutar(strSQL,1);

%>

 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
</head>
<body>

<table width="630" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="5" class="textonegrita" ><p><img src="images/logo.png" width="257" height="53"></p>          </td>
        <td width="30%" class="textonegrita" align='right'>
        <font size=5>
        <% 
        if(copia+""=="1")
			Response.Write(" COPIA ");
         %>
         </font>         </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td width="726"></td>
  </tr>
  <tr>
    <td height="188" valign="top" nowrap><table width="100%"  border="1" cellpadding="2" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="3" class="textoformulariocentrado"><div align="center" class="textoformulariocentrado">Orden de montaje </div></td>
        </tr>
        <tr class="textonegritaFB">
          <td width="15%" class="textonegritaFB">Nro pedido </td>
          <td width="54%"><%=txOrd%></td>
          <td width="31%">
            <div align="center"><%=Request.Cookies("usr")("nombre")%></div></td>
        </tr>
        <tr class="textonegritaFB">
          <td height="30" class="textonegritaFB">Nombre del cliente </td>
          <td ><%=nbCliente%></td>
		  <%
		  strSQL1="select ID_TPO_ORD, ID_ORD_GP from T_SANT_ORD where  ID_ORD="+idOrd;
		Rs5=Ejecutar(strSQL1,2);
		
		if (Rs5("ID_TPO_ORD")==0){
			strSQL2="Select DOCID from sop10100 where SOPNUMBE='"+Rs5("ID_ORD_GP")+"'";
			Rs7=Ejecutar(strSQL2,2);
			if(!Rs7.EOF){%>
		  <td width="31%">
            <div align="center"><%=Response.Write(Rs7("DOCID"))%></div></td>
			<%	}else {
				strSQL2="Select DOCID from sop30200 where SOPNUMBE='"+Rs5("ID_ORD_GP")+"'";
				res=Ejecutar(strSQL2,2);
				if(!res.EOF){%>
		  <td width="31%">
            <div align="center"><%=Response.Write(res("DOCID"))%></div></td>
			<%
			}
			res.Close();
			}
		Rs7.Close();
		}
		Rs5.Close();
	%>
        </tr>
	</table>
	<br>
	<p class="textonegrita"><%=fecha%></p>
	<%	
		strSQL1="select ID_TPO_ORD, ID_ORD_GP from T_SANT_ORD where  ID_ORD="+idOrd;
		Rs5=Ejecutar(strSQL1,2);
		if (Rs5("ID_TPO_ORD")==1){
			strSQL2="Select COMENT1,COMENT2 from sntconsighdr where NOCONSIG='"+Rs5("ID_ORD_GP")+"'";
			Rs7=Ejecutar(strSQL2,2);
			if(!Rs7.EOF){
	%>
	<p class="textonegritaFB"> Comentarios:</p> <br>
		<p class="textonegritaFB"> <%=Rs7("COMENT1")%></p><br>
		<p class="textonegritaFB"> <%=Rs7("COMENT2")%></p>
		<%	}
		Rs7.Close();
		}else{
		
			strSQL2="select COMMENT_1,COMMENT_2,COMMENT_3,COMMENT_4 from sop10106 where SOPTYPE = '2' and SOPNUMBE='"+Rs5("ID_ORD_GP")+"'";
			Rs7=Ejecutar(strSQL2,2);
			if(!Rs7.EOF){
			%>
		<p class="textonegritaFB"> Comentarios:</p> <br>
		<p class="textonegritaFB"> <%=Rs7("COMMENT_1")%></p><br>
		<p class="textonegritaFB"> <%=Rs7("COMMENT_2")%></p><br>
		<p class="textonegritaFB"> <%=Rs7("COMMENT_3")%></p><br>
		<p class="textonegritaFB"> <%=Rs7("COMMENT_4")%></p><br>
		<%	}
		Rs7.Close();
		}
		Rs5.Close();
	%>
	<table width="100%" border="1" cellpadding="2" cellspacing="1" class="tablamenu">
        <tr class="">
          <td colspan="5" class="textoformulariocentrado">Datos del pedido </td>
        </tr>
        <tr class="textoformulariocentrado">
          <td width="108" class=""><div align="center">C&oacute;digo del producto </div></td>
          <td width="518"><div align="center">Descripci&oacute;n</div></td>
          <td width="64"><div align="center">Cantidad solicitada</div></td>
          <td width="70"><div align="center">Confirmar</div></td>
        </tr>
        <%
        strSQL="SELECT A.SER_PROD,A.ID_PROD, A.DESC_PROD, A.CANT_ORD, sum(ISNULL(B.CANT_PROD,0)) EMB, A.CANT_ORD-SUM(ISNULL(B.CANT_PROD,0)) DIF ";
		strSQL+=" FROM T_SANT_DET_ORD A LEFT JOIN T_SANT_DET_CAJA_ORD B ";
		strSQL+=" ON A.ID_ORD=B.ID_ORD AND A.ID_PROD=B.ID_PROD ";
		strSQL+=" WHERE A.ID_ORD="+idOrd;
		strSQL+=" GROUP BY A.SER_PROD, A.ID_PROD, A.DESC_PROD, A.CANT_ORD";
		//Response.Write(strSQL);
		Response.Flush();
		Rs=Ejecutar(strSQL,2);
		while(!Rs.EOF)
		{
        %>        
        <tr class="textonegritaFB">
          <td class="textonegritaFB"><div align="left"><%=Rs("ID_PROD")%></div></td>
          <td><div align="left"><%=Rs("DESC_PROD")%> </div></td>
          <td><div align="center" id="CANT_ORD_<%=Rs("SER_PROD")%>" name="CANT_ORD_<%=Rs("SER_PROD")%>" ><%=Rs("CANT_ORD")%></div></td>
          <td><div align="center"> &nbsp;</div></td>
        </tr>
       <%
       cantSol+=parseFloat(Rs("CANT_ORD"));
       cantEmb+=parseFloat(Rs("EMB"));
       cantDif+=parseFloat(Rs("DIF"));
       Rs.MoveNext();
       }
       Rs.Close();
       %>
        <tr class="textoformulario">
          <td class="">&nbsp;</td>
          <td><div align="right">Total </div></td>
          <td><div align="center" id="CANT_SOL" name="CANT_SOL"><%=cantSol%></div></td>
          <td><div align="center"  id="CANT_EMB" name="CANT_EMB"></div>            <div align="center"  id="CANT_DIF" name="CANT_DIF"></div></td>
        </tr>
      </table>
    <p align="right"class="textonegrita">TOTAL DE UNIDADES A DESPACHAR: <%=cantSol%></p>
    <p align="right"class="textonegrita">TOTAL DE TITULOS A DESPACHAR: <% SQL="SELECT COUNT (DISTINCT ID_PROD) TT FROM T_SANT_DET_ORD WHERE ID_ORD="+idOrd;
	res=Ejecutar(SQL,2);
	if (!res.EOF){
		Response.Write(res("TT"));
	}
	%> </p></td>
  </tr>
</table>
</body>
</html>
<% if(i<ord.length-1)
{%>
<DIV CLASS="PAGEBREAK"></DIV>
<%}%>
<%
cantSol=0;
cantEmb=0;
cantDif=0;
}
Desconectar();
%>
<script>
window.print();
</script>