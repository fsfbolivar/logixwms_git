 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();
Server.ScriptTimeout=600

if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script>
window.resizeTo(800,500);

function Guardar()
{
	var items=new Array();
	var hay=false;
	var detener=false;
	items=(document.getElementById("items").value+"").split(";");
	if(document.getElementById("items").value+""!="")
	{
		for (var i=0; i<items.length && !detener ;i++)
		{
	
			var sel=document.getElementById("S_"+items[i].split("|")[0]);
			var obs=document.getElementById("OBS_"+items[i].split("|")[0]).value+"";
			//alert(sel.options.selectedIndex);
			
			if(parseFloat(sel.options[sel.options.selectedIndex].value)!=1 &&  parseFloat(sel.options[sel.options.selectedIndex].value)!=5 )
			{
				hay=true;
				if( obs=="" || obs=="undefined")
				{
					detener=true;
				}
			}	
			
		}
	}
	if(detener)
	{
		alert("Debe indicar las observaciones para las ordenes cuyo status no es 'Factura recibida en almac�n' / 'Devoluci�n entregada'");
	}
	else
	{
		if(hay)
			var liberar=window.confirm("Existen ordenes con status distinto a 'Factura recibida en almac�n' / 'Devoluci�n entregada'.\n �Desea liberar al transportista?");
		else
			var liberar=true;
			
		document.getElementById("solicitudes").action+="&liberar="+liberar;
		//alert(document.getElementById("solicitudes").action);
		document.getElementById("solicitudes").submit();
	}
	
}
</script>

<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<%
var ID_TRANSP=0;
var ID_GUIA=0;
var st_trans=0;

ID_TRANSP=Request.QueryString("idTransp")+"";

strSQL="SELECT STATUS_TRANSP FROM T_SANT_TRANSP WHERE CI_TRANSP= "+ID_TRANSP;
Rs=Ejecutar(strSQL,2,0,1);
st_trans=Rs(0).Value;
Rs.Close();

if(parseFloat(st_trans)==2) // no esta liberado se debe mostrar los de la ultima gu�a a parte
{
	strSQL="SELECT MAX(ID_GUIA_CARGA) FROM T_SANT_GUIA_CARGA WHERE ID_TRANSP="+ID_TRANSP;
	Rs=Ejecutar(strSQL,2,0,1);
	ID_GUIA=Rs(0).Value;
	Rs.Close();

}



var items=new Array();
%>
<p align="center">
 <form id='solicitudes' name='solicitudes' action='LiberarItemsF.asp?idTransp=<%=ID_TRANSP%>&tipo=<%=Request.QueryString("tipo")%>' method='POST'>

<div align="center">
<%

if(Request.QueryString("msg").Count>0)
	if(Request.QueryString("msg")+""=="1")
		Response.Write("<span class='texto_verde'>Transportista liberado</span>");
%>
  <table width="90%"  border="0" cellpadding="2" cellspacing="1" class="tablamenu">
    <tr class="tituloblanco">
      <td colspan="4">Ordenes &uacute;ltima gu&iacute;a </td>
    </tr>
    <tr class="titulo_subgrupo">
      <td width="13%">N&uacute;mero orden </td>
      <td width="38%">Cliente</td>
      <td width="20%">Status de entrega </td>
      <td width="29%">Observaci&oacute;n</td>
    </tr>
	<%
	
	strSQL="select 1 TIPOX,ID_ORD, ID_ST_ENT,ID_ORD_GP, NB_CLTE, (SELECT DISTINCT NB_ST_ENT FROM T_SANT_ST_ENT WHERE ID_ST_ENT=ISNULL(A.ID_ST_ENT,0)) ST_ENT, CONVERT(VARCHAR(10),FE_ST_ENT,103) FECHA, OBS_ENT ";
	strSQL+=" from t_sant_ord A WITH(NOLOCK) where id_guia = "+ID_GUIA;
	strSQL+=" UNION SELECT 0,ID_MEMO, 0,COD_MEMO, CLTE_MEMO,	(SELECT DISTINCT NB_ST_ENT FROM T_SANT_ST_ENT  WHERE ID_ST_ENT=ISNULL(B.ST_ENT,0)) ST_ENT,CONVERT(VARCHAR(10),FE_MEMO,103) FECHA,OBS_MEMO FROM T_SANT_MEMO_GUIA B WITH(NOLOCK) WHERE ID_GUIA ="+ID_GUIA;
	strSQL+=" UNION SELECT 2, ID_DEV, ST_ENT, COD_DEV,NB_CLTE_DEV, (SELECT DISTINCT NB_ST_ENT FROM T_SANT_ST_ENT WHERE ID_ST_ENT=ISNULL(ST_ENT,0)) ST_ENT ,CONVERT(VARCHAR(10),FE_REG,103) FECHA, OBS FROM T_SANT_DEV  WITH(NOLOCK) WHERE ID_GUIA = "+ID_GUIA;
	
	//Response.Write(strSQL);
	//Response.Flush();
	Rs=Ejecutar(strSQL,2,0,1);
	while(!Rs.EOF)
	{
	%>
		<tr class="textoformulario">
		  <td><%=Rs("ID_ORD_GP")%></td>
		  <td><%=Rs("NB_CLTE")%></td>
		  <td>
		  <select id='S_<%=Rs("ID_ORD")%>' name='S_<%=Rs("ID_ORD")%>' class='textonegrita'>
		  <%
		  strSQL="SELECT DISTINCT ID_ST_ENT, NB_ST_ENT FROM T_SANT_ST_ENT ORDER BY ID_ST_ENT";
		  Rs2=Ejecutar(strSQL,2,0,1);
		  while(!Rs2.EOF)
		  {
			if(parseFloat(Rs("ID_ST_ENT"))==parseFloat(Rs2("ID_ST_ENT")))
			{
				Response.Write("<option selected value='"+Rs2("ID_ST_ENT")+"'>"+Rs2("NB_ST_ENT")+"</option>");
			}
			else
			{
				Response.Write("<option value='"+Rs2("ID_ST_ENT")+"'>"+Rs2("NB_ST_ENT")+"</option>");
			}		  		
			Rs2.MoveNext();
		  }  
		  Rs2.Close();
		  %>
		  </select>
		  </td>
		  <td><TEXTAREA  class='textonegrita' id='OBS_<%=Rs("ID_ORD")%>' name='OBS_<%=Rs("ID_ORD")%>'><%=Rs("OBS_ENT")%></TEXTAREA></td>
	    </tr>
	  <%
	  items[items.length]=Rs("ID_ORD").Value+"|"+Rs("TIPOX");
	  Rs.MoveNext();
	  }
	  Rs.Close();
	  
	  %>
	  <input type="hidden" name="items" id="items" value="<%=items.join(";")%>">
    <tr class="textoformulario">
      <td colspan="4" class="titulo_subgrupo">&nbsp;</td>
    </tr>
  </table>
  <p>&nbsp;</p>
  <table width="90%"  border="0" cellpadding="2" cellspacing="1" class="tablamenu">
      <tr class="tituloblanco">
        <td colspan="5">Ordenes pendientes </td>
      </tr>
      <tr class="titulo_subgrupo">
        <td width="13%">N&uacute;mero orden </td>
        <td width="27%">Cliente</td>
        <td width="11%">Fecha status </td>
        <td width="20%">Status de entrega </td>
        <td width="29%">Observaci&oacute;n</td>
      </tr>
	<%
	var items=new Array();
	strSQL="select 1 TIPOX,CONVERT(VARCHAR(10),FE_ST_ENT,103) FECHA,ID_ORD, ID_ST_ENT,ID_ORD_GP, NB_CLTE, (SELECT DISTINCT NB_ST_ENT FROM T_SANT_ST_ENT WHERE ID_ST_ENT=ISNULL(A.ID_ST_ENT,0)) ST_ENT, CONVERT(VARCHAR(10),FE_ST_ENT,103) FECHA, OBS_ENT ";
	strSQL+=" from t_sant_ord A , t_sant_guia_carga b WITH (NOLOCK) where B.id_transp = "+ID_TRANSP+" AND A.ID_GUIA=B.ID_GUIA_CARGA AND A.ID_ST_ENT <> 1 AND B.ID_GUIA_CARGA<>"+ID_GUIA;
	strSQL+=" UNION SELECT 0,CONVERT(VARCHAR(10),X.FE_MEMO,103) FECHA,X.ID_MEMO, X.ST_ENT ,X.COD_MEMO , X.CLTE_MEMO,	(SELECT DISTINCT NB_ST_ENT FROM T_SANT_ST_ENT  WHERE ID_ST_ENT=ISNULL(X.ST_ENT,0)) ST_ENT ,CONVERT(VARCHAR(10),X.FE_MEMO,103) FECHA ,X.OBS_MEMO FROM T_SANT_MEMO_GUIA X, T_SANT_GUIA_CARGA Y  WITH (NOLOCK) WHERE X.ST_ENT <> 1 AND X.ID_GUIA <>"+ID_GUIA+"   AND X.ID_GUIA = Y.ID_GUIA_CARGA AND Y.ID_TRANSP = "+ID_TRANSP;
	strSQL+=" UNION SELECT  2, CONVERT(VARCHAR(10),FE_REG,103) FECHA,ID_DEV, ST_ENT, COD_DEV,NB_CLTE_DEV,(SELECT DISTINCT NB_ST_ENT FROM T_SANT_ST_ENT WHERE ID_ST_ENT=ISNULL(M.ST_ENT,0)) ST_ENT ,CONVERT(VARCHAR(10),FE_REG,103) ,OBS FROM T_SANT_DEV M,  T_SANT_GUIA_CARGA N WITH(NOLOCK) WHERE N.ID_GUIA_CARGA <> "+ID_GUIA+" AND M.ST_ENT NOT IN (1,5) AND N.ID_GUIA_CARGA=M.ID_GUIA AND n.ID_TRANSP="+ID_TRANSP;
	
	//Response.Write(strSQL);
	//Response.Flush();
	Rs=Ejecutar(strSQL,2,0,1);
	while(!Rs.EOF)
	{
	%>
      <tr class="textoformulario">
			<td><%=Rs("ID_ORD_GP")%></td>
			<td><%=Rs("NB_CLTE")%></td>
			<td><%=Rs("FECHA")%></td>
			<td>
		  <select id='S_<%=Rs("ID_ORD")%>' name='S_<%=Rs("ID_ORD")%>' class='textonegrita'>
		  <%
		  strSQL="SELECT DISTINCT ID_ST_ENT, NB_ST_ENT FROM T_SANT_ST_ENT ORDER BY ID_ST_ENT";
		  Rs2=Ejecutar(strSQL,2,0,1);
		  while(!Rs2.EOF)
		  {
			if(parseFloat(Rs("ID_ST_ENT"))==parseFloat(Rs2("ID_ST_ENT")))
			{
				Response.Write("<option selected value='"+Rs2("ID_ST_ENT")+"'>"+Rs2("NB_ST_ENT")+"</option>");
			}
			else
			{
				Response.Write("<option value='"+Rs2("ID_ST_ENT")+"'>"+Rs2("NB_ST_ENT")+"</option>");
			}		  		
			Rs2.MoveNext();
		  }  
		  Rs2.Close();
		  %>
		  </select>
		  </td>
		  <td><TEXTAREA  class='textonegrita' id='OBS_<%=Rs("ID_ORD")%>' name='OBS_<%=Rs("ID_ORD")%>'><%=Rs("OBS_ENT")%></TEXTAREA></td>
      </tr>

	  <%
	  items[items.length]=Rs("ID_ORD").Value+"|"+Rs("TIPOX");
	  Rs.MoveNext();
	  }
	  Rs.Close();
	  
	  %>
	  <input type="hidden" name="items2" id="items2" value="<%=items.join(";")%>">	        
      <tr class="textoformulario">
        <td colspan="5" class="titulo_subgrupo">&nbsp;</td>
      </tr>

  </table>

  <p>&nbsp;</p>
</div>
      </form>
<p align="center">
  <input name="Submit2" type="button" class="textonegrita" value="Guardar" onClick="JavaScript:Guardar();">
</p>

</body>
</html>

<%
Desconectar();
%>