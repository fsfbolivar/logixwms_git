<%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<%
/****************************************************************************
PAGINA FUNCIONAL.
FINALIDAD:		Verifica la informaci�n enviada por el usuario
RETORNO:		Exito: Ingresa a la soluci�n
				Falla: Retorna a la p�gina principal indicando el error
FECHA:			08/03/2006
DESARROLLADO:	Dario Carnelutti - SITVEN, C.A.
*****************************************************************************/


//Declaraci�n de variables
var strSQL="";
var usr="";
var pwd="";
var resultado=-1;
var Retorno="";

//Inicio conexion con la base de datos
if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

//Captura de la informacion de usuario
usr=Request.Form("usuario")+"";
pwd=Request.Form("pwd")+"";

//Validacion en Base de datos
strSQL="EXEC SP_SANT_VALIDAUSUARIO '"+usr+"','"+pwd+"' ";
resultado=Ejecutar(strSQL,1);

if(parseInt(resultado)==0)
{
	Session.Timeout=20;
	Session("activo")=1;
	strSQL="EXEC SP_SANT_OBT_DAT_USR '"+ usr +"'"
	Retorno=Ejecutar(strSQL,1);

	//Guardo las variables de usuario
	Response.Cookies("usr")("nombre")=Retorno.split(",")[0];
	Response.Cookies("usr")("id")=Retorno.split(",")[1];
	Response.Cookies("usr")("cr")=Retorno.split(",")[2];
	Response.Cookies("usr")("login")=usr;
	
	//Cierro la conexion con la base de datos
	Desconectar();	
	
	//Redirecciono a la pagina destino
	Response.Redirect("desk.asp");
}
else
{
	//Cierro la conexion con la base de datos
	Desconectar();
	
	//Redirecciono a la pagina inicial por error
	Response.Redirect("index.asp?error=2");

}
%>
