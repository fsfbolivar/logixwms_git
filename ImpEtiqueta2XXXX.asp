 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var cantSol=0;
var cantEmb=0;
var cantDif=0;
var txOrd="";
var nbCliente="";
var dirClte="";
var tpoOrd="";
var telOrd="";
var ciuOrd="";

        strSQL="SELECT CIU_ENT, DIR_ENT,TEL_CLTE1, TEL_CLTE2, ID_ST_ORD, ID_ORD, ID_ORD_GP, CASE ID_TPO_ORD WHEN 0 THEN 'Venta' ELSE 'Consignación' END ID_TPO_ORD, ";
		strSQL+=" NB_CLTE, CONVERT(VARCHAR(12),FE_ING_ORD,103) FE_ING_ORD, 'Por embalar' ST";
		strSQL+=" FROM T_SANT_ORD WHERE ID_ORD="+idOrd;

Rs=Ejecutar(strSQL,2);

txOrd=Rs("ID_ORD_GP").Value;
nbCliente=Rs("NB_CLTE").Value;
dirClte=Rs("DIR_ENT").Value;
tpoOrd=Rs("ID_TPO_ORD").Value;
telOrd=Rs("TEL_CLTE1").Value+" "+Rs("TEL_CLTE2").Value;
ciuOrd=Rs("CIU_ENT").Value
Rs.Close();

var idCaja=Request.QueryString("idcaja")+"";
%>

 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
</script>
<input type='hidden' name='ID_ORD' id='ID_ORD' value='<%=idOrd%>'>

<table width="561" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td height="32"><table width="513" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="572" colspan="6" class="textonegrita style1" ><div align="center"><font size=4> Caja <%=idCaja%> </font> </div></td>
      </tr>

    </table></td>
  </tr>

  <tr>
    <td width="547"></td>
  </tr>
  <tr>
    <td height="605" valign="top" nowrap>
      <table width="94%"  border="0" cellpadding="2" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="2" class="tituloblanco"><div align="center">Datos del pedido </div></td>
        </tr>
        <tr class="textoformulario">
          <td width="24%" class="textoformulario">Nro pedido </td>
          <td width="76%"><%=txOrd%>
          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td><%=nbCliente%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Direcci&oacute;n</td>
          <td><%=dirClte%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Ciudad</td>
          <td><%=ciuOrd%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Tipo</td>
          <td class="textoformulario"><%=tpoOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Telefonos</td>
          <td class="textoformulario"><%=telOrd%></td>
        </tr>
      
	</table>
      <table width="94%" border="0" cellpadding="2" cellspacing="1" class="tablamenu">
        <tr class="">
          <td colspan="3" class="tituloblanco">Productos en la caja numero <%=idCaja%></td>
        </tr>
        <tr class="titulo_subgrupo">
          <td width="89" class=""><div align="center">C&oacute;digo del producto </div></td>
          <td width="312"><div align="center">Descripci&oacute;n</div></td>
          <td width="96"><div align="center">Cantidad embalada </div></td>
        </tr>
        <%
        strSQL="SELECT A.SER_PROD,A.ID_PROD, A.DESC_PROD, A.CANT_ORD, ISNULL(B.CANT_PROD,0) EMB, A.CANT_ORD-ISNULL(B.CANT_PROD,0) DIF ";
		strSQL+=" FROM T_SANT_DET_ORD A LEFT JOIN T_SANT_DET_CAJA_ORD B ";
		strSQL+=" ON A.ID_ORD=B.ID_ORD AND A.ID_PROD=B.ID_PROD ";
		strSQL+=" WHERE A.ID_ORD="+idOrd+" AND B.ID_CAJA="+idCaja;
		Rs=Ejecutar(strSQL,2);
		while(!Rs.EOF)
		{
        %>        
        <tr class="textoformulario">
          <td class="textoformulario"><div align="center"><%=Rs("ID_PROD")%></div></td>
          <td><div align="center"><%=Rs("DESC_PROD")%> </div></td>
          <td><div align="center" id="CANT_ORD_<%=Rs("SER_PROD")%>" name="CANT_ORD_<%=Rs("SER_PROD")%>" ><%=Rs("EMB")%></div></td>
        </tr>
       <%
       cantSol+=parseFloat(Rs("CANT_ORD"));
       cantEmb+=parseFloat(Rs("EMB"));
       cantDif+=parseFloat(Rs("DIF"));
       Rs.MoveNext();
       }
       Rs.Close();
       %>
        <tr class="titulo_subgrupo">
          <td class="">&nbsp;</td>
          <td><div align="right">Total </div></td>
          <td><div align="center" id="CANT_SOL" name="CANT_SOL"><%=cantEmb%></div></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<script>window.print();</script>
<%
Desconectar();
%>