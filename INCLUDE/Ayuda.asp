 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%
//Se valida que aun la session sea válida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
  
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Ordenar(tipo,dir,lugar)
{
	document.getElementById("BUSQ_ORD").action+="?tipo="+tipo+"&dir="+dir+"&lug="+lugar;
	document.getElementById("BUSQ_ORD").submit();
}
function BusqDesk()
{
if(document.getElementById("nro_ord").value=="" && document.getElementById("nb_clte").value=="" && document.getElementById("fecomp").value=="" )
{
	alert("Debe indicar alguno de los campos para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_DESK").submit();
}
}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
function ImpOrd()
{
var ordenes=document.getElementsByName("IMPORD");
var hay=false;
for(var i=0;i<ordenes.length && !hay;i++)
{
	if(ordenes.item(i).checked)
		hay=true;
}
if(!hay)
	alert("Indique las ordenes que desea imprimir");
else
	document.getElementById("ordenes").submit();

}
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a), <%=Request.Cookies("usr")("nombre")%> </td>
  </tr>
  <tr>
    <td width="115"></td>
    <td width="631"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
	
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="4" class="tituloblanco"><div align="center">Instrucciones</div></td>
        </tr>
        <tr class="textoformulario">
          <td width="20%" class="textoformulario">&nbsp;</td>
          <td>&nbsp;</td>
         
          <td>&nbsp;</td>
        </tr>
        <form id="FRM_DESK" name="FRM_DESK" action='desk.asp?bsq=1' method="post">
        <tr class="">
          <td height="19" class="textoformulario">&nbsp;</td>
          <td class="textoformulario"><div align="center">Seguir el proceso del sistema haciendo click en los link de arriba hacia abajo:</div><ul>
            <li>Cargar todas las ubicaciones.</li>
            <li>Asignar a cada ubicacion los articulos asociados.</li>
            <li>Imprimir la orden de conteo donde se va a vaciar la informacion.</li>
            <li>Transcribir en el sistema la informacion contenida en las hojas de conteo e ir generando uno a uno y en orden los reportes corespondientes para asegurar que no queden conteos pendientes.</li>
            <li>Imprimir los reportes finales.      </li>
          </ul></td>
          <td width="15%" class="textoformulario">&nbsp;</td>
        </tr>
        </form>
</table>
      <p class="textonegrita">&nbsp;</p>
      <p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
</table>

</body>
</html>
<%

Desconectar();
%>