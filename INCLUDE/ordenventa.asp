 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var cantSol=0;
var cantEmb=0;
var cantDif=0;
var txOrd="";
var nbCliente="";
var dirClte="";
var tpoOrd="";
var telOrd="";
var ciuOrd="";
var txObs="";

strSQL="UPDATE T_SANT_ORD SET ID_ST_ORD=1 WHERE ID_ORD="+idOrd;
Ejecutar(strSQL,1);

        strSQL="SELECT CIU_ENT, DIR_ENT,TEL_CLTE1, TEL_CLTE2, ID_ST_ORD, ID_ORD, ID_ORD_GP, CASE ID_TPO_ORD WHEN 0 THEN 'Venta' ELSE 'Consignaci�n' END ID_TPO_ORD, ";
		strSQL+=" NB_CLTE, CONVERT(VARCHAR(12),FE_ING_ORD,103) FE_ING_ORD, 'Por embalar' ST, isnull(OBS,'') obs";
		strSQL+=" FROM T_SANT_ORD WHERE ID_ORD="+idOrd;

Rs=Ejecutar(strSQL,2);

txOrd=Rs("ID_ORD_GP").Value;
nbCliente=Rs("NB_CLTE").Value;
dirClte=Rs("DIR_ENT").Value;
tpoOrd=Rs("ID_TPO_ORD").Value;
telOrd=Rs("TEL_CLTE1").Value+" "+Rs("TEL_CLTE2").Value;
ciuOrd=Rs("CIU_ENT").Value
txObs=Rs("OBS").Value
Rs.Close();

%>

 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Guardar(tpo)
{
//alert(1);
	var obs=escape(document.getElementById("obs").value)+"";
	if(parseInt(tpo)==1)
	{
		if(parseFloat(document.getElementById("CANT_DIF").innerHTML)>0 && obs.length==0)
		{
			alert("Debe indicar el motivo por el cual la orden se despacha incompleta en el campo de observaciones");
			return(false);
		}
	}
	var idOrd=document.getElementById("ID_ORD").value;
	var url="Procesar.asp?tpo="+tpo+"&idOrd="+idOrd+"&obs="+obs;
	document.location.href=url;

}
var http=null;
function IniciarHTTP()
{
try{
	if (window.ActiveXObject)
    {
        // pero si es IE
        try 
        {
            http = new ActiveXObject ("Msxml2.XMLHTTP");
        }
        catch (e)
        {
            // en caso que sea una versi�n antigua
            try
            {
                http = new ActiveXObject ("Microsoft.XMLHTTP");
            }
            catch (e)
            {
            }
        }
    }
    else
    {
		http=null;
		alert("Aplicacion dise�ada solo para Internet Explorer");
    }
 }
 catch(e)
 {
	http=null;
 }
}
function RegSerial(serial)
{
try{
	var idOrd=document.getElementById("ID_ORD").value;
	var Cant=parseFloat(document.getElementById("cant_serial").value);
	
	var url="RegSerial.asp?idOrd="+idOrd+"&ser="+serial+"&cant="+Cant;
	StSerial("Registrando codigo");
	document.getElementById("SERIAL").disabled=true;
	
	if(!http)
		IniciarHTTP();
		
	if(!http)
	{
		StSerial("Error registrando el codigo");
		return(false);
	}
	
	http.open ('GET', url, false); // asignamos los m�todos open y send
    http.send (null);	
    var retorno=http.responseText;
    if(parseFloat(retorno)!="0")
	{
		alert("Error registrando serial");
		return(false);
	}
	
	if(true)
	{
		document.getElementById("CANT_EMB_"+serial).innerHTML=eval(parseFloat(document.getElementById("CANT_EMB_"+serial).innerHTML)+Cant);
		document.getElementById("CANT_DIF_"+serial).innerHTML=eval(parseFloat(document.getElementById("CANT_DIF_"+serial).innerHTML)-Cant);
		document.getElementById("CANT_DIF").innerHTML=eval(parseFloat(document.getElementById("CANT_DIF").innerHTML)-Cant);
		document.getElementById("CANT_EMB").innerHTML=eval(parseFloat(document.getElementById("CANT_EMB").innerHTML)+Cant);
		document.getElementById("cant_serial").value=1;
	}
}
catch(e)
{
	alert(e.description);
}
finally
{
	StSerial("");
	document.getElementById("SERIAL").disabled=false;
}
}
var arrSer=new Array();
function IniciarSerial(serial)
{
	arrSer[arrSer.length]=serial;
}

function StSerial(texto)
{
	document.getElementById("ST_SERIAL").innerHTML=texto;
}
function ImpEtiqueta()
{
if(window.confirm("�Est� seguro que desea imprimir la etiqueta?"))
{
	var idOrd=document.getElementById("ID_ORD").value;
	window.open("ImpEtiqueta.asp?idOrd="+idOrd,"impetiqueta","width=700,height=500,modal=yes,status=no, menubar=no, scroll=1, scrollbars=1");
}
}
function IngresoSerial(serial)
{
	var valido=false;
	if(event.keyCode==13 || event.keyCode==8  || event.keyCode==27  )
		return(false);
	if(serial.length==13)
	{
		StSerial("Validando codigo");		
		for(var i=0;i<arrSer.length && !valido;i++)
		{
			if(serial==arrSer[i])
			{
				valido=true;
			}
		}

	if(valido)
	{
		if((parseFloat(document.getElementById("CANT_DIF_"+serial).innerHTML)-parseFloat(document.getElementById("cant_serial").value))<0)
		{
			alert("Esta sobrepasando la cantidad de libros necesarios");
			StSerial("Cantidad excedida");		
		}
		else
			RegSerial(serial);
	}
	else
	{		
		alert("El codigo introducido no se encuentra dentro de los solicitados en este orden de venta.");
		StSerial("Codigo invalido");	
	}
	document.getElementById("cant_serial").value=1;
	document.getElementById("serial").value="";
	document.getElementById("serial").focus();
	
	
	}
	
}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
</script>
<input type='hidden' name='ID_ORD' id='ID_ORD' value='<%=idOrd%>'>

<table width="867" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          <p>&nbsp;</p></td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="108"></td>
    <td width="701"></td>
  </tr>
  <tr>
    <td height="605" valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
      <table width="100%"  border="0" cellpadding="2" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="2" class="tituloblanco"><div align="center">Datos del pedido </div></td>
        </tr>
        <tr class="textoformulario">
          <td width="15%" class="textoformulario">Nro pedido </td>
          <td><%=txOrd%>
          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td><%=nbCliente%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Direcci&oacute;n</td>
          <td><%=dirClte%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Ciudad</td>
          <td><%=ciuOrd%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Tipo</td>
          <td class="textoformulario"><%=tpoOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Telefonos</td>
          <td class="textoformulario"><%=telOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">&nbsp;</td>
          <td class="textoformulario">&nbsp;</td>
        </tr>
	</table>
<br>
      <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="3" class="tituloblanco">Producto a anexar </td>
        </tr>
        <tr class="textoformulario">
          <td width="43%"><div align="center">
            Cantidad:   
            <input name="cant_serial" id="cant_serial" onKeyUp="JavaScript:IngresoSerial(this.value);" type="text" class="textonegrita" size="20" maxlength="12">
          </div></td>
          <td width="26%">
            <input name="serial" id="serial" onKeyUp="JavaScript:IngresoSerial(this.value);" type="text" class="textonegrita" size="20" maxlength="13">
          </td>
          <td width="31%"><div id="ST_SERIAL"></div></td>
        </tr>
        <tr class="textoformulario">
          <td width="43%"><div align="center">
          </div></td>
          <td width="26%"><input name="button" type='button' class='textonegrita' onclick='JavaScript:ImpEtiqueta()' value='Imprimir etiqueta'></td>
          <td width="31%">&nbsp;</td>
        </tr>        
      </table>      
      <br>    <table width="758" border="0" cellpadding="2" cellspacing="1" class="tablamenu">
        <tr class="">
          <td colspan="6" class="tituloblanco">Datos del pedido </td>
        </tr>
        <tr class="titulo_subgrupo">
          <td width="172" class=""><div align="center">C&oacute;digo del producto </div></td>
          <td width="384"><div align="center">Descripci&oacute;n</div></td>
          <td width="57"><div align="center">Cantidad solicitada</div></td>
          <td width="59"><div align="center">Cantidad embalada </div></td>
          <td width="60"><div align="center">Cantidad pendiente </div></td>
        </tr>
        <%
        strSQL="SELECT A.SER_PROD,A.ID_PROD, A.DESC_PROD, A.CANT_ORD, sum(ISNULL(B.CANT_PROD,0)) EMB, A.CANT_ORD-SUM(ISNULL(B.CANT_PROD,0)) DIF ";
		strSQL+=" FROM T_SANT_DET_ORD A LEFT JOIN T_SANT_DET_CAJA_ORD B ";
		strSQL+=" ON A.ID_ORD=B.ID_ORD AND A.ID_PROD=B.ID_PROD ";
		strSQL+=" WHERE A.ID_ORD="+idOrd;
		strSQL+=" GROUP BY A.SER_PROD, A.ID_PROD, A.DESC_PROD, A.CANT_ORD";
		//Response.Write(strSQL);
		Response.Flush();
		Rs=Ejecutar(strSQL,2);
		while(!Rs.EOF)
		{
        %>        
        <tr class="textoformulario">
          <td class="textoformulario"><div align="center"><%=Rs("ID_PROD")%></div></td>
          <td><div align="center"><%=Rs("DESC_PROD")%> </div></td>
          <td><div align="center" id="CANT_ORD_<%=Rs("SER_PROD")%>" name="CANT_ORD_<%=Rs("SER_PROD")%>" ><%=Rs("CANT_ORD")%></div></td>
          <td><div align="center" id="CANT_EMB_<%=Rs("SER_PROD")%>" name="CANT_EMB_<%=Rs("SER_PROD")%>"><%=Rs("EMB")%></div></td>
          <td><div align="center" id="CANT_DIF_<%=Rs("SER_PROD")%>" name="CANT_DIF_<%=Rs("SER_PROD")%>"><%=Rs("DIF")%></div>
          <% Response.Write("<script>IniciarSerial('"+Rs("SER_PROD")+"');</script>"); %>
          </td>
          
        </tr>
       <%
       cantSol+=parseFloat(Rs("CANT_ORD"));
       cantEmb+=parseFloat(Rs("EMB"));
       cantDif+=parseFloat(Rs("DIF"));
       Rs.MoveNext();
       }
       Rs.Close();
       %>
        <tr class="titulo_subgrupo">
          <td class="">&nbsp;</td>
          <td><div align="right">Total </div></td>
          <td><div align="center" id="CANT_SOL" name="CANT_SOL"><%=cantSol%></div></td>
          <td><div align="center"  id="CANT_EMB" name="CANT_EMB"><%=cantEmb%></div></td>
          <td><div align="center"  id="CANT_DIF" name="CANT_DIF"><%=cantDif%></div></td>
        </tr>
      </table>
      <p>&nbsp;</p>
      <div align="right">
        <table width="758" border="0" align="left" cellpadding="0" cellspacing="0" class="tabla_borde_negro">
          <tr class="">
            <td height="21" colspan="7" class="tituloblanco">Observaciones</td>
          </tr>
          <tr class="titulo_subgrupo">
            <td colspan="7" class=""><div align="center"></div>              </td>
          </tr>
          <tr class="textoformulario">
            <td colspan="7" class="textoformulario"><div align="center">
              <textarea name="textarea" cols="90" rows="6" id='obs' name='obs'><%=txObs%></textarea>
            </div>              </td>
          </tr>
        </table>
        <p>&nbsp;</p>
        <p align="justify">&nbsp;</p>
      </div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <table width="324" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td width="149" height="41">       <span class="textoformulario">        </span><span class="textoformulario">
</span>
          <form name="form1" method="post" action="">
            <div align="center">
              <input type="button" name="Submit" value="Guardar" onClick="JavaScript:Guardar(0);">
            </div>
          </form>
          <span class="textoformulario">        </span></td>
        <td width="167">
          <div align="center">
            <input type="button" name="Submit2" value="Procesar" onClick="JavaScript:Guardar(1);">
          </div>
         </td>
      </tr>
    </table>    
    <span class="textoformulario">    </span>    <p align="center">&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<script>
document.getElementById("cant_serial").value=1;
</script>
<%
Desconectar();
%>