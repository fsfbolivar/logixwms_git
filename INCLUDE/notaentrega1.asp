
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Nota de entrega de solicitud</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="INCLUDE/estilos2.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {
	font-size: 16px;
	color: #000080;
}
.style2 {font-size: 12px}
-->
</style>
<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
</head>


<body>
<table width="100%"  border="0" cellspacing="1" cellpadding="1">
  <tr>
    <td width="49%">&nbsp;</td>
    <td width="51%" rowspan="4" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td class="textonegrita style1"><img src="images/logo.png" width="257" height="53"></td>
  </tr>
  <tr>
    <td class="texto style2">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="tituloblanco" >NOTA DE ENTREGA </td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td width="19%" class="textonegrita">Nota de entrega numero:</td>
        <td width="30%" class="texto"><%=NroEntrega%></td>
        <td width="13%" class="textonegrita">&nbsp;</td>
        <td width="38%" class="texto"></td>
      </tr>
      <tr>
        <td class="textonegrita">Fecha:</td>
        <td class="texto"><script>document.write(laFecha());</script></td>
        <td class="textonegrita">Solicitud:</td>
        <td class="texto">
       
        </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2" ><table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td class="textonegrita">Consultor:</td>
        <td>&nbsp;</td>
        <td class="textonegrita">&nbsp;</td>
        <td class='texto'>
       
        </td>
        <td class="textonegrita">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td colspan="6">&nbsp;</td>
        </tr>
    </table>
    <hr size="1" noshade></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td width="8%" class="textonegrita">RIF:</td>
        <td width="41%" class="texto"></td>
        <td width="11%" class="textonegrita">&nbsp;</td>
        <td width="40%" class="texto"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td width="28%" class="textonegrita">Direcci&oacute;n de entrega </td>
        <td width="72%" class="texto">
        
       </td>
      </tr>
      <tr>
        <td class="textonegrita">Persona autorizada </td>
        <td class="texto"></td>
      </tr>
      <tr>
        <td class="textonegrita">Cargo / Tel&eacute;fono / Correo electr&oacute;nico:</td>
        <td class="texto">> </td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><hr size="1" noshade></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="1" cellpadding="1" bgcolor='black' >
      <tr>
        <td colspan="6" class="tituloblanco">Equipos</td>
        </tr>
      <tr class="titulo_subgrupo">
        <td>Marca y modelo </td>
        <td>Serial</td>
        <td>N&uacute;mero</td>
        <td>Fecha procesado</td>
        <td>Cuenta</td>
        <td>Precio referencial</td>
      </tr>
     
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><hr size="1" noshade></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="1" cellpadding="1" >
      <tr>
        <td width="15%" class="textonegrita">Forma de entrega </td>
        <td width="19%" class="texto"></td>
        <td width="8%" class="textonegrita">Courrier</td>
        <td width="18%" class="texto"></td>
        <td width="15%" class="textonegrita">Gu&iacute;a</td>
        <td width="25%" class="texto"></td>
      </tr>
      
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td class="textonegrita">Observaciones</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="2"><table width="100%"  border="0" cellspacing="1" cellpadding="1">
      <tr>
        <td width="48%" class="textonegrita">Nombre y apellido del receptor</td>
        <td width="52%" class="textonegrita">C&eacute;dula de identidad del receptor </td>
      </tr>
      <tr>
        <td class="texto"><input name="textfield" type="text" size="40"></td>
        <td><span class="texto">
          <input name="textfield2" type="text" size="40">
        </span></td>
      </tr>
      <tr>
        <td class="textonegrita">Cargo y/o tel&eacute;fono del receptor</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td><span class="texto">
          <input name="textfield3" type="text" size="40">
        </span></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita"><b>NOTA: ESTE DOCUMENTO NO ES UNA FACTURA</b></td>
  </tr>
</table>
<script>
window.resizeBy(800,600);
if(window.confirm("�Desea imprimir la nota de entrega?"))
	window.print();
</script>
</body>
</html>