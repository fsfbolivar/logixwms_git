  function ValidaFecha(Formato,campo,tipo)
  {
	var retorno=true;
	var valor=document.getElementById(campo).value;

	
	if (valor.length==0) 
		return(true);
   	if (valor.length<10) 
   	{
		alert('Ha colocado una fecha inv�lida');
		document.getElementById(campo).value="";
		return(false);
   	}

	var posDD = Formato.indexOf("DD");
	var posMM = Formato.indexOf("MM");
	var posYYYY = Formato.indexOf("YYYY");

    	var myDD   	= parseInt(valor.substring(posDD,posDD+2),10);		// extrae dia
    	var myMM   	= parseInt(valor.substring(posMM,posMM+2),10); 		// extrae mes
    	var myYYYY	= parseInt(valor.substring(posYYYY,posYYYY+4),10);	// extrae a�o

    if ( (isNaN(myDD)) || (isNaN(myMM)) || ( isNaN(myYYYY)) ) 
	retorno=false;

    if (myYYYY < 1000) 
	retorno=false;
    if ( (myMM > 12) || (myMM <= 0) ) 
	retorno=false;

if(!retorno)
{
	alert('Ha colocado una fecha inv�lida');
	document.getElementById(campo).value="";
	
	return(false);
}
else
{

	if(tipo=="-1")
		return(true);
	
	var fecha=new Date();
	if(fecha.getFullYear()>myYYYY)
	{
		retorno=false;
	}
	else
	{
		if(fecha.getFullYear()==myYYYY && (fecha.getMonth()+1)>myMM)
		{
			retorno=false;
		}
		else
		{
			if(fecha.getFullYear()==myYYYY && (fecha.getMonth()+1)==myMM && fecha.getDate()>myDD)
			{
				retorno=false;
			}
		}
	}
	if(!retorno)
	{
		alert('Ha colocado una fecha menor a la fecha del dia');
		document.getElementById(campo).value="";
		return(false);			
	}
	else
		return (true);	
}
    
  }
function ValidaHora(Campo)
{
try{
	var txt="Ha colocado una hora inv�lida. Por favor use el formato HH:MM AM/PM";
	var valor=document.getElementById(Campo).value;
	if(valor=="") return(true);
	if(valor.length!=8) 
	{
		alert(txt);
		document.getElementById(Campo).value="";
		document.getElementById(Campo).focus();
		return(false);
	}
	if(valor.indexOf(":")<=0)
	{
		alert(txt);
		document.getElementById(Campo).value="";
		document.getElementById(Campo).focus();		
		return(false);
	}
	var hh=valor.substring(0,valor.indexOf(":"));
	var mm=valor.substring(valor.indexOf(":")+1,valor.indexOf(" "));
	var am=valor.substring(valor.indexOf(" ")+1);
	if(hh.indexOf(" ")>0)
		while(hh.indexOf(" ")>0)
			hh=hh.replace(" ","");

	if(mm.indexOf(" ")>0)
		while(mm.indexOf(" ")>0)
			mm=mm.replace(" ","");

	if(am.indexOf(" ")>0)
		while(am.indexOf(" ")>0)
			am=am.replace(" ","");

		
	if(parseFloat(hh)>12 || parseFloat(hh)<=0 || parseFloat(mm)<0 || parseFloat(mm)>59 || (am.toUpperCase()!="AM" &&  am.toUpperCase()!="PM") )
	{
		alert(txt)
		document.getElementById(Campo).value="";
		document.getElementById(Campo).focus();		
		return(false);
	}
}
catch(e)
{
alert(e);
}
}