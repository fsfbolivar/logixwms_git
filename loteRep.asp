 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();

var ret="";
if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var nb_clte = "";
var nro_lote = "";
var fdesde = "";
var fhasta = "";
if (Request.Form.Count > 0)
{
	nb_clte = Request.Form("nb_clte")+"";
	nro_lote = Request.Form("nro_lote")+"";
	fdesde = Request.Form("fdesde")+"";
	fhasta = Request.Form("fhasta")+"";
}

%>
<html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" type="text/JavaScript" src="include/calendar1.js"></script>
<script language="JavaScript" type="text/JavaScript" src="include/Validaciones.js"></script>
<script language="JavaScript">
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms["FRM_DESK"].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
function BusqDesk()
{
	if(document.getElementById("fdesde").value==""
		&& document.getElementById("fhasta").value==""
		&& document.getElementById("nro_lote").value==""
		&& document.getElementById("nb_clte").value=="")
	{
		alert("Debe indicar todos los campos para realizar la busqueda");
		return(false);
	}
	else
	{
		document.getElementById("FRM_DESK").submit();
	}
}
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a), <%=Request.Cookies("usr")("nombre")%> </td>
  </tr>
  <tr>
    <td width="115"></td>
    <td width="631"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
	
	<form id="FRM_DESK" name="FRM_DESK" action="loteRep.asp" method="post">
    <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="5" class="tituloblanco"><div align="center">B�squeda de lotes aprobados</div></td>
        </tr>
        <tr class="textoformulario">
		  <td>Nro de Lote</td>
		  <td>Cliente</td>
          <td>Fecha de pedido(desde)</td>
          <td>Fecha de pedido(hasta)</td>
          <td>&nbsp;</td>
        </tr>
		<tr><!-- la fila con las entradas y calendarios... asi como con el boton de submit -->
		  <td class="textoformulario"><input name="nro_lote" id="nro_lote" type="text" class="texto" size="15" maxlength="15"></td>
		  <td class="textoformulario"><input type='text' class='texto' id='nb_clte' name='nb_clte' size='15' maxlength='60'></td>
		  <td class="textoformulario"><!-- la primera pareja entrada/calendario -->
		   <table><tr>
		     <td class="textoformulario"><input name="fdesde" id="fdesde" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fdesde","-1");'></td>
			 <td class="textoformulario"><A HREF="JavaScript:IniciarCalendario('fdesde');"><img border='0' src='images/cal.gif'></A></td>
		   </tr></table>
		  </td>
		  <td class="textoformulario"><!-- la segunda pareja entrada&calendario -->
		   <table><tr>
		     <td class="textoformulario"><input name="fhasta" id="fhasta" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fhasta","-1");'></td>
			 <td class="textoformulario"><A HREF='JavaScript:IniciarCalendario("fhasta");'><img border='0' src='images/cal.gif'></A></td>
		   </tr></table>
		  </td><!-- el boton de submit -->
		  <td width="15%" class="textoformulario"><input name="Submit" onClick="JavaScript:BusqDesk();" type="button" class="texto" value="Buscar">
		  </td>
		</tr>
    </table>
	</form>
	
	<%
		strSQL="select id_lote,id_ord_gp,fe_apr_lot,nb_clte from t_sant_ord";
		strSQL+=" where id_lote is not null and id_lote<>''";
		strSQL+=" and ID_TPO_ORD = 0 and ID_ST_ORD <> 4";
		if (nb_clte.length > 0)
		{
			strSQL += " and NB_CLTE like '%"+nb_clte+"%'";
		}
		if (nro_lote.length > 0)
		{
			strSQL+=" and ID_LOTE='"+nro_lote+"'";
		}
		if ((fdesde.length <= 0) && (fhasta.length <= 0)
			&& (nro_lote.length <= 0) && (nb_clte.length <= 0))
		{
			strSQL += " and FE_APR_LOT >= CONVERT(VARCHAR(12),DATEADD(Day, -1, GETDATE()))";
		    %><p class="textonegrita">Pedidos aprobados en el ultimo d�a</p><%
		}
		else
		{
			%><p class="textonegrita">Pedidos aprobados<%
			if (fdesde.length > 0)
			{
				strSQL += " and CONVERT(VARCHAR(12),FE_APR_LOT,112) >= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fdesde+"',103),112)";
				%> posteriores al <%=fdesde%><%
				if (fhasta.length > 0)
				{
					%> y <%
				}
			}
			if (fhasta.length > 0)
			{
				strSQL += " and CONVERT(VARCHAR(12),FE_APR_LOT,112) <= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fhasta+"',103),112)";
				%> anteriores al <%=fhasta%><%
			}
			%></p><%
		}
		Rs=Ejecutar(strSQL,2);
	%>
	<!--<%=strSQL%>-->
	<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
		<tr class="tituloblanco">
			<td>Lote</td>
			<td>Pedido</td>
			<td>Fecha</td>
			<td>Cliente</td>
		</tr>
		<%
			if(Rs){
				while(!Rs.EOF){
		%>
		<tr class="textoformulario" height="20px;">
			<td><%=Rs("ID_LOTE")%></td>
			<td><%=Rs("ID_ORD_GP")%></td>
			<td><%=Rs("FE_APR_LOT")%></td>
			<td><%=Rs("NB_CLTE")%></td>
		</tr>
		<%
					Rs.MoveNext();
				}
				Rs.Close();
			}else{
		%>
		<tr class="textoformulario">
			<td class="texto_rojo" colspan="2"><center>No hay lotes autorizados</center></td>
		</tr>
		<%
			}
		%>
	</table>

	
    </td>
   </tr>
 </table>

</body>
</html>