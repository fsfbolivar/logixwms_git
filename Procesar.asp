  <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<% Response.Expires=0; %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var strObs="";

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var cantSol=0;
var cantEmb=0;
var cantDif=0;
var txOrd="";
var tpo=0;
var nbCliente="";
var dirClte="";
var tpoOrd="";
var telOrd="";
var ciuOrd="";
var tpoAcc=parseInt(Request.QueryString("tpo"));
strObs=unescape(Request.QueryString("obs"));

if(tpoAcc==0)//Solo guarda
{
	strSQL="EXEC SP_SANT_ACT_ORD "+idOrd+",1,'"+strObs+"'";
	Ejecutar(strSQL,1);
	Desconectar();
	Response.Redirect("ordenventa.asp?idord="+idOrd+"&e=1");
}
else
{
strSQL="EXEC SP_SANT_ACT_ORD "+idOrd+",2,'"+strObs+"'";
var ret=Ejecutar(strSQL,1);
if(parseFloat(ret.split(',')[0])!=0)
{
	Response.Redirect("ordenventa.asp?idord="+idOrd+"&e=1&msg="+ret.split(',')[1]);
}
else
{
//Response.Write(ret);

//Response.Write(strSQL);
        strSQL="SELECT CIU_ENT, DIR_ENT,TEL_CLTE1, TEL_CLTE2, ID_ST_ORD, ID_ORD, ID_ORD_GP, CASE ID_TPO_ORD WHEN 0 THEN 'Venta' ELSE 'Consignaci�n' END ID_TPO_ORD, ";
		strSQL+=" NB_CLTE, CONVERT(VARCHAR(12),FE_ING_ORD,103) FE_ING_ORD, 'Por embalar' ST";
		strSQL+=" FROM T_SANT_ORD WHERE ID_ORD="+idOrd;

Rs=Ejecutar(strSQL,2);

txOrd=Rs("ID_ORD_GP").Value;
nbCliente=Rs("NB_CLTE").Value;
dirClte=Rs("DIR_ENT").Value;
tpoOrd=Rs("ID_TPO_ORD").Value;
telOrd=Rs("TEL_CLTE1").Value+" "+Rs("TEL_CLTE2").Value;
ciuOrd=Rs("CIU_ENT").Value
Rs.Close();
}

%>

 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Excel()
{
	document.getElementById("Q").submit();
	//window.open("ReportesDespExcel.asp","impetiqueta","width=700,height=500,modal=yes,status=no, menubar=no, scroll=1, scrollbars=1");
}
function ImpEtiqueta(tipo)
{
if(window.confirm("�Est� seguro que desea imprimir la guia de empaque?"))
{
	var idOrd=document.getElementById("ID_ORD").value;
	window.open("ImpProcesar.asp?tipo="+tipo+"&idOrd="+idOrd,"impetiqueta","width=800,height=400,modal=yes,status=no, menubar=no, scroll=1, scrollbars=1");
}
}
</script>
<input type='hidden' name='ID_ORD' id='ID_ORD' value='<%=idOrd%>'>

<table width="97%" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="116"></td>
    <td width="788"></td>
  </tr>
  <tr>
    <td height="605" valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
      <table width="758"  border="0" cellpadding="2" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="2" class="tituloblanco"><div align="center">Datos de la orden </div></td>
        </tr>
        <tr class="textoformulario">
          <td width="15%" class="textoformulario">Nro orden venta</td>
          <td><%=txOrd%>
          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td><%=nbCliente%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Direcci&oacute;n</td>
          <td><%=dirClte%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Ciudad</td>
          <td><%=ciuOrd%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Tipo</td>
          <td class="textoformulario"><%=tpoOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td height="24" bgcolor="#0000FF" class="textoformulario">Telefonos</td>
          <td class="textoformulario"><%=telOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">&nbsp;</td>
          <td class="textoformulario">&nbsp;</td>
        </tr>
	</table>
	<p> <div align="center">
              <input type="button" name="Submit22" value="Exp Excel guia de empaque detallada" onClick="JavaScript:Excel();">
            </div>  </p>
      <br>    
      <table width="758" border="0" cellpadding="2" cellspacing="1" class="tablamenu">
        <tr class="">
          <td colspan="3" class="tituloblanco">Datos del pedido </td>
        </tr>
        <tr class="titulo_subgrupo">
          <td width="144" class="">C&oacute;digo del producto </td>
          <td width="137">Descripci&oacute;n</td>
          <td width="144">Cantidad embalada </td>
        </tr>
        <%
        strSQL="SELECT A.SER_PROD,A.ID_PROD, A.DESC_PROD, A.CANT_ORD, SUM(ISNULL(B.CANT_PROD,0)) EMB ";
		strSQL+=" FROM T_SANT_DET_ORD A LEFT JOIN T_SANT_DET_CAJA_ORD B ";
		strSQL+=" ON A.ID_ORD=B.ID_ORD AND A.ID_PROD=B.ID_PROD ";
		strSQL+=" WHERE A.ID_ORD="+idOrd;
		strSQL+=" GROUP BY A.SER_PROD, A.ID_PROD, A.DESC_PROD, A.CANT_ORD";
		Rs=Ejecutar(strSQL,2);
		while(!Rs.EOF)
		{
        %>        
        <tr class="textoformulario">
          <td class="textoformulario"><div align="center"><%=Rs("ID_PROD")%></div></td>
          <td><div align="center"><%=Rs("DESC_PROD")%> </div></td>
          <td><div align="center" id="CANT_EMB_<%=Rs("SER_PROD")%>" name="CANT_EMB_<%=Rs("SER_PROD")%>"><%=Rs("EMB")%></div></td>
        </tr>
       <%
       cantSol+=parseFloat(Rs("CANT_ORD"));
       cantEmb+=parseFloat(Rs("EMB"));
       Rs.MoveNext();
       }
       Rs.Close();
       %>
        <tr class="titulo_subgrupo">
          <td class="">&nbsp;</td>
          <td><div align="right">Total </div></td>
          <td><div align="center"  id="CANT_EMB" name="CANT_EMB"><%=cantEmb%></div></td>
        </tr>
      </table>
      <p align="center">&nbsp;</p>      
      <div align="center">
        <table width="20%"  border="0" cellspacing="1" cellpadding="1">
          <tr>
		  
            <td><div align="center">
              <input type="button" name="Submit22" value="Imprimir guia de empaque detallada" onClick="JavaScript:ImpEtiqueta(0);">
            </div></td>
            <td><div align="center">
              <input type="button" name="Submit22" value="Imprimir guia de empaque total" onClick="JavaScript:ImpEtiqueta(1);">
            </div></td>            
            <td><div align="center">
              <input type="button" name="Submit2" value="Aceptar" onClick="JavaScript:document.location.href='desk.asp';">
            </div></td>
          </tr>
		 
        </table>
      </div>     
    </td>
  </tr>
  <FORM ID='Q' NAME='Q' ACTION='guiaDetalladaExcel.asp?idOrd=<%=idOrd%>&tipo=<%=tpo%>' METHOD='POST' TARGET='_BLANK'>
  </FORM>
</table>

</body>
</html>

<%

Desconectar();
}
%>