
 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var nroOrd="";
var nbClte="";
var fdesde="";
var fhasta="";
var fdesde2="";
var fhasta2="";
var st="";
var consig=0;
if(Request.Form.Count>0)
{
	nroOrd=Request.Form("nro_ord1")+"";
	nbClte=Request.Form("nb_clte")+"";
	fdesde=Request.Form("fdesde")+"";
	fhasta=Request.Form("fhasta")+"";
	fdesde2=Request.Form("fdesde2")+"";
	fhasta2=Request.Form("fhasta2")+"";	
	st=Request.Form("st_dev")+"";
	consig=Request.Form("sta")+"";
}


%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.Estilo1 {font-size: 12px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Excel()
{
	document.getElementById("Q").submit();
	//window.open("ReportesDespExcel.asp","impetiqueta","width=700,height=500,modal=yes,status=no, menubar=no, scroll=1, scrollbars=1");
}
function Ordenar(tipo,dir,lugar)
{
	document.getElementById("BUSQ_ORD").action+="?tipo="+tipo+"&dir="+dir+"&lug="+lugar;
	document.getElementById("BUSQ_ORD").submit();
}
function BusqDesk()
{
if( document.getElementById("sta").options.selectedIndex==0 && document.getElementById("nro_ord1").value=="" && document.getElementById("nb_clte").value=="" && document.getElementById("fdesde").value=="" && document.getElementById("fhasta").value=="" && document.getElementById("fdesde2").value=="" && document.getElementById("fhasta2").value=="")
{
	alert("Debe indicar alguno de los campos para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_DESK").submit();
}
}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
</script>
<table width="774" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          <p>&nbsp;</p></td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="107"></td>
    <td width="641"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
	                <FORM ID='FRM_DESK' NAME='FRM_DESK' action='ReportesDevolucionesProc.asp' METHOD='POST'>
        <tr>
          <td colspan="6" class="tituloblanco"><div align="center">Busqueda</div></td>
        </tr>

        <tr class="textoformulario">
          <td width="180" height="19" class="textoformulario">Nro devolución</td>
          <td width="33">&nbsp;</td>
          <td width="202"><input name="nro_ord1"  id="nro_ord1" type="text" class="texto"></td>
          <td width="31">&nbsp;</td>
          <td colspan="2" bgcolor="#999999"><div align="center"><a href="deskGest.asp" class="Estilo1">Pedidos Con Diferencias</a>
          </div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Cliente </td>
          <td>&nbsp;</td>
          <td><input name="nb_clte" id="nb_clte" type="text" class="texto"></td>
          <td>&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Fecha de orden (desde) </td>
          <td>&nbsp;</td>
          <td><input name="fdesde" id="fdesde" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fdesde","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fdesde");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td width="170">&nbsp;</td>
          <td width="18" >&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Fecha de orden (hasta) </td>
          <td>&nbsp;</td>
          <td><input name="fhasta" id="fhasta" type="text" class="texto" size="15" maxlength="10" onChange='JavaScript:ValidaFecha("DD/MM/YYYY","fhasta","-1");'>
              <A HREF='JavaScript:IniciarCalendario("fhasta");'><img border='0' src='images/cal.gif'></A></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td >&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="">Fecha recepci&oacute;n almac&eacute;n (desde)</td>
          <td>&nbsp;</td>
          <td>              <A HREF='JavaScript:IniciarCalendario("fdesde2");'>
            <input name="fdesde2" id="fdesde2" type="text" class="texto" size="15" maxlength="10" onChange='JavaScript:ValidaFecha("DD/MM/YYYY","fdesde2","-1");'>
            <img border='0' src='images/cal.gif'></A></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td >&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="">Fecha recepci&oacute;n almac&eacute;n (hasta)</td>
          <td>&nbsp;</td>
          <td><input name="fhasta2" id="fhasta2" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fhasta2","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fhasta2");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td>&nbsp;</td>
          <td >&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="">&iquest;Enviada a consignaci&oacute;n? </td>
          <td>&nbsp;</td>
          <td><select name="sta"  id="sta" class="textonegrita">
			<option/>
            <option value="1">Si</option>
            <option value="0">No</option>
          </select>
		  
		  </td>
          <td>&nbsp;</td>
          <td><div align="right">
            <input name="button" type="button" class="textonegrita" value="Buscar" onClick="JavaScript:BusqDesk();">
          </div></td>
          <td >&nbsp;</td>
        </tr>
        </form>
        
	</table>
      <table width="100%"  border="0" cellpadding="3" cellspacing="0">
        <tr class="">
          <td height="9"><p>&nbsp;</p>
            <table width="100%"  border="0" cellpadding="1" cellspacing="1" class="tablamenu">
              <tr>
                <td colspan="13" class="tituloblanco">Reporte</td>
              </tr>
              <tr class="titulo_subgrupo">
                <td WIDTH='55' height="32">Status</td>
                <td WIDTH='163'>&iquest;En consignaci&oacute;n? </td>
                <td WIDTH='163'>Fecha registro</td>
                <td WIDTH='75'>Fecha recepción almacén</td>
                <td width="72">Nro devoluci&oacute;n</td>
                <td width="71">Cliente</td>
                <td width="73">Cant cajas indica cliente</td>
                <td width="79">Cant ejemp indica cliente</td>
                <td width="87">Cant ejemp recibidas</td>
                <td width="37">Observaciones</td>
              </tr>
              <%


			
				if(Request.Form.Count>0)
				{
				strSQL="SELECT (SELECT COUNT(DISTINCT ID_PROD) FROM T_SANT_DET_ORD WHERE ID_ORD=A.ID_ORD) PROD,"
strSQL+="(SELECT SUM(CANT_ORD) FROM T_SANT_DET_ORD WHERE ID_ORD=A.ID_ORD) CANTIDAD,B.NB_USR,"
strSQL+="CASE A.ID_ST_ORD WHEN 5 THEN 'Eliminada' WHEN 0 THEN 'Nueva' WHEN 2 THEN 'Embalada' WHEN 3 THEN 'Despachada'WHEN 1 THEN 'En proceso' END STATUS,a.id_ord,A.NB_CLTE,A.ID_ORD_GP,"
strSQL+="CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD,CONVERT(VARCHAR(12),A.FE_DESP_ORD,103)"
strSQL+=" FE_DESP_ORD,A.CI_Transp,C.CI_Transp,C.NB_Transp,AP_Transp" 
strSQL+=" FROM T_SANT_ORD A,T_SANT_USR B,T_SANT_TRANSP C  WHERE A.CI_Transp=C.CI_Transp and A.ID_CHEQ=B.ID_USR and ID_ST_ORD in ('3')";

				strSQL="SELECT * FROM V_SANT_REP_DEV WHERE 1=1 ";
				
				if(nroOrd.length>0)
				{
					strSQL+=" AND COD_DEV='"+nroOrd+"'" ;
				}
				if(nbClte.length>0)
				{
					strSQL+=" AND NB_CLTE_DEV LIKE '%"+nbClte+"%'" ;
				}
				if(st.length>0)
				{
					strSQL+=" AND ID_ST=3";
				}
				
					if(fdesde.length>0)
					{
						strSQL+=" AND CONVERT(VARCHAR(12),FE_REG,112) >= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fdesde+"',103),112)" ;
					}
					if(fhasta.length>0)
					{
						strSQL+=" AND CONVERT(VARCHAR(12),FE_REG,112) <= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fhasta+"',103),112)" ;
					}

					if(fdesde2.length>0)
					{
						strSQL+=" AND CONVERT(VARCHAR(12),FE_REC_ALM,112) >= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fdesde2+"',103),112)" ;
					}
					if(fhasta2.length>0)
					{
						strSQL+=" AND CONVERT(VARCHAR(12),FE_REC_ALM,112) <= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fhasta2+"',103),112)" ;
					}
					
				if(consig.length>0)
				{
					strSQL+=" AND IN_CONSIG="+consig;
				}			
				
				strSQL+=" ORDER BY NB_CLTE_DEV, FE_REG";
				//Response.Write(strSQL);
				//Response.Flush();
				
				Rs=Ejecutar(strSQL,2,0,1);
				
				while(!Rs.EOF)
				{
						%>
					<tr class="textoformulario">
					  <td><%=Rs("NB_ST")%></td>
					  <td><%
						if(parseFloat(Rs("IN_CONSIG").Value)==0)
							Response.Write("No");
						else
							Response.Write("Si");
					  %></td>
					  <td><%=Rs("FECHA_REG") %></td>
					  <td><%=Rs("FE_REC_ALM")%></td>
					  <td><a class="textoformulario" href="OrdenDev.asp?idord=<%=Rs("ID_DEV")%>&proc=1&cs=<%=Rs("IN_CONSIG")%>"><%=Rs("COD_DEV")%></a></td>					  
					  <td><%=Rs("NB_CLTE_DEV")%></td>
					  <td><%=Rs("CANT_CAJA")%></td>
					  <td><%=Rs("CANTIDAD_PRE")%></td>
					  <td><%=Rs("CANTIDAD")%></td>
					  <td><%=Rs("OBS")%></td>
					</tr>
				<%
				
				
				Rs.MoveNext();
				}
				Rs.Close();
				

				}
           
              %>
            </table>            
            
        </tr>
      </table>
</td>
  </tr>
</table>
</body>
</html>


<%

Desconectar();
%>