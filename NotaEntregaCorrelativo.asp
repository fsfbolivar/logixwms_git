<%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%
//Se valida que aun la session sea valida
ValidarSession();


Session("DEV")="";

if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");

//Recuperar o guardar el correlativo de nota de entrega
if (Request.Form.Count > 0) {
	strSQL="UPDATE T_SANT_NE_CFG SET NEXT_ID_NE='" + (Request.Form("CORRELATIVO")+"") + "', LAST_NE_DATE = GETDATE()";
	Rs=Ejecutar(strSQL,2);
}

strSQL="SELECT NEXT_ID_NE 'NEXT_ID_NE', LAST_NE_DATE 'LAST_NE_DATE' FROM T_SANT_NE_CFG";
//strSQL="SELECT 1+3 'NEXT_ID_NE', GETDATE() 'LAST_NE_DATE'"
Rs=Ejecutar(strSQL,2);
next_id_ne = "";
last_ne_date = "";
if (Rs && !Rs.EOF) {
	next_id_ne = Rs("NEXT_ID_NE");
	last_ne_date = Rs("LAST_NE_DATE");
}

%>
<html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a), <%=Request.Cookies("usr")("nombre")%> </td>
  </tr>
  <tr>
    <td width="115"></td>
    <td width="631"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>

      <form id="FRM_DESK" name="FRM_DESK" action='NotaEntregaCorrelativo.asp' method="post">
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="6" class="tituloblanco"><div align="center">Establecer el correlativo de la Nota de Entrega</div></td>
        </tr>
        <tr class="textoformulario">
		  <td width="20%" class="textoformulario">Proximo correlativo</td>
          <td><input value='<%=next_id_ne%>' type='text' class='texto' id='CORRELATIVO' name='CORRELATIVO' size='15' maxlength='60'></td>
          <td>&nbsp;</td>
		  <td width="20%" class="textoformulario">Fecha ultimo uso</td>
		  <td><%if (last_ne_date != "") Response.Write(last_ne_date);
				else Response.Write("&nbsp;");%></td>
        </tr>
        <tr class="">
          <td class="textoformulario">&nbsp;</td>
          <td class="textoformulario">&nbsp;</td>
          <td class="textoformulario">&nbsp;</td>
          <td class="textoformulario">&nbsp;</td>
          <td class="textoformulario"><input name="Submit" type="submit" class="texto" value="Actualizar Correlativo"></td>
        </tr>
	  </table>
      </form>

