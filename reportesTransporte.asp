<%@ Language="JavaScript" %>


<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var nroOrd="";
var nbClte="";
var fdesde="";
var fhasta="";
var nbtransp="";
var citransp="";
var ncajas="";
var obs="";

if(Request.Form.Count>0)
{
	nroOrd=Request.Form("nro_ord1")+"";
	nbClte=Request.Form("nb_clte")+"";
	fdesde=Request.Form("fdesde")+"";
	fhasta=Request.Form("fhasta")+"";
	nbtransp=Request.Form("nb_transp")+"";
	citransp=Request.Form("ci_transp")+"";
}


          
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">


 <script language="javascript" type="text/javascript">
function Asig()
{
var forma=document.getElementById("form3");
var sol=document.getElementsByName("SOLIC");
var hayCheck=false;
var strch="";

for(var i=0;i<sol.length && !hayCheck;i++)
{
	if(sol.item(i).checked)
	{
		hayCheck=true;
	}			
}

if(!hayCheck)
{
	alert("Indique al menos 1 solicitud a asignar al transportista");
	return(false);
}
else
{
	if(document.getElementById("Asignar").selectedIndex==0)
	{
		alert("Indique el transportista");
		return(false);
	}
	else
	{
		
		forma.submit();
	}
}

}
 
 </script>
<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Ordenar(tipo,dir,lugar)
{
	document.getElementById("BUSQ_ORD").action+="?tipo="+tipo+"&dir="+dir+"&lug="+lugar;
	document.getElementById("BUSQ_ORD").submit();
}
function BusqDesk()
{
if(document.getElementById("nro_ord1").value=="" && document.getElementById("nb_clte").value=="" && document.getElementById("fdesde").value=="" && document.getElementById("fhasta").value=="")
{
	alert("Debe indicar alguno de los campos para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_DESK").submit();
}
}
function alerta()
{
alert("�Est� seguro que desea asignar este Transportista?");

}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
function BusqDesk2()
{
if(document.getElementById("ci_transp").value=="" && document.getElementById("nb_transp").value=="")
{
	alert("Debe indicar alguno de los campos de transportista para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_TRANSP").submit();
}
}
			
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          <p>&nbsp;</p></td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="115"></td>
    <td width="631"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
      <table width="102%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="9" class="tituloblanco"><div align="center">Busqueda</div></td>
        </tr>
				
              <FORM ID='FRM_DESK' NAME='FRM_DESK' action='reportesTransporte.asp?bsq=1' METHOD='POST'>
        <tr class="textoformulario">
          <td width="210" class="textoformulario">Nro Pedido </td>
          <td width="208">&nbsp;</td>
          <td width="208"><input name="nro_ord1"  id="nro_ord1" type="text" class="texto"></td>
          <td width="33">&nbsp;</td>
          <td colspan="5">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Ciudad</td>
          <td>&nbsp;</td>
          <td><input name="nb_clte" id="nb_clte" type="text" class="texto"></td>
          <td>&nbsp;</td>
          <td colspan="5">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Fecha de la orden (desde) </td>
          <td>&nbsp;</td>
          <td><input name="fdesde" id="fdesde" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fdesde","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fdesde");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td width="76">&nbsp;</td>
          <td width="101" colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="">Fecha de la orden (hasta)</td>
          <td>&nbsp;</td>
          <td><input name="fhasta" id="fhasta" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fhasta","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fhasta");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td><input name="button" type="button" class="textonegrita" value="Buscar" onClick="JavaScript:BusqDesk();"></td>
          <td colspan="2">&nbsp;</td>
        </tr>
        </form>
		
	</table>
	<%
	tiempo=new Date();
	Response.Write(tiempo)
	%>
      <table width="100%"  border="0" cellpadding="3" cellspacing="0">
        <tr class="">
          <td height="9"><p>&nbsp;</p>
          <form name="form3" method="post" action="CrearGuia.asp?id=2">
            <table width="100%"  border="0" cellpadding="1" cellspacing="1" class="tablamenu">
              <tr>
                <td colspan="11" class="tituloblanco">Reporte</td>
              </tr>
              <tr class="titulo_subgrupo">
                <td width='60'>Seleccione</td>
                <td width='249'>Cliente</td>
                <td width="72">Nro Pedido </td>
                <td width="73">Fecha orden </td>
                <td width="88">Ciudad</td>
				 <td width='238'>Direcci&oacute;n</td>
				 <td width='50'>Status</td>
				 <td width='20'>Cajas</td>
				 <td width='20'>Zona</td>
				 <td width='30'>Comentario 1</td>
				 <td width='30'>Comentario 2</td>
              </tr>
              <%
	
				 strSQL="SELECT 1 TIPOX,'Impreso' STATUS, a.id_ord, A.NB_CLTE, A.ID_ORD_GP,A.CIU_ENT,A.DIR_ENT, CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD,  CONVERT(VARCHAR(12),A.FE_DESP_ORD,103) FE_DESP_ORD, (SELECT COUNT(*) FROM T_SANT_CAJA_ORD WITH(NOLOCK) WHERE ID_ORD=A.ID_ORD) AS CAJAS, ISNULL(UPSZONE, ' ') UPSZONE, ISNULL(COMENT1, ' ') AS COMENT1, ISNULL(COMENT2, ' ') AS COMENT2 ";
				strSQL+=" FROM T_SANT_ORD A WITH (NOLOCK)		WHERE ID_ST_ORD = 7";
				//Response.Write(strSQL); //Response.Flush(); Response.End();

			  if(Request.Form.Count>0)
              {
			 
			  		
				if(Request.QueryString("bsq")=="1")
				{
			  			 nroOrd=Request.Form("nro_ord1")+"";
						 nbClte=Request.Form("nb_clte")+"";
						 fdesde=Request.Form("fdesde")+"";
						 fhasta=Request.Form("fhasta")+"";
						 nbtransp=Request.Form("nb_transp")+"";
						 citransp=Request.Form("ci_transp")+"";
													 
              		if(nroOrd.length>0)
					{
						strSQL+=" AND A.ID_ORD_GP='"+nroOrd+"'" ;
					}
						if(nbClte.length>0)
					{
						strSQL+=" AND A.CIU_ENT LIKE '%"+nbClte+"%'" ;
					}
					
				
					if(fdesde.length>0)
					{
						strSQL+=" AND CONVERT(VARCHAR(12),A.FE_ING_ORD,112) >= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fdesde+"',103),112)" ;
					}
					if(fhasta.length>0)
					{
						strSQL+=" AND CONVERT(VARCHAR(12),A.FE_ING_ORD,112) <= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fhasta+"',103),112)" ;
					}
				 }
				}
				strSQL+=" UNION SELECT 0,'Nueva',  ID_DEV, NB_CLTE_DEV,COD_DEV, '', DIR_DEV,CONVERT(VARCHAR(10),FE_REG,103) ,NULL, (SELECT COUNT(*) FROM T_SANT_CAJA_DEV WITH(NOLOCK) WHERE ID_DEV=X.ID_DEV) AS CAJAS, ' ' UPSZONE, ' ' AS COMENT1, ' ' AS COMENT2 FROM T_SANT_DEV x WITH(NOLOCK) WHERE IN_ATC=1 AND ID_ST=1 AND ID_GUIA IS NULL";

              		if(nroOrd.length>0)
					{
						strSQL+=" AND COD_DEV='"+nroOrd+"'" ;
					}
					
				
					if(fdesde.length>0)
					{
						strSQL+=" AND CONVERT(VARCHAR(12),FE_REG,112) >= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fdesde+"',103),112)" ;
					}
					if(fhasta.length>0)
					{
						strSQL+=" AND CONVERT(VARCHAR(12),FE_REG,112) <= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fhasta+"',103),112)" ;
					}
					
				strSQL+=" ORDER BY A.CIU_ENT, A.ID_ORD_GP";
				//Response.Write("<hr/>");
				//Response.Write(strSQL);
				Response.Flush(); //Response.End();
				Rs=Ejecutar(strSQL,2);
				if (Rs.EOF)
				{%>
				<tr class="texto_rojo">
          		<td colspan="9" class="textoformulario">No hay Pedidos pendientes</td>
        		</tr><%		  
				}
				
				while(!Rs.EOF)
				{
				%> 
              <tr class="textoformulario">
                <td>
                  <div align="center">
        		      
			     <input  class="texto" type='CHECKBOX' id='SOLIC' name='SOLIC' value='<%=Rs("ID_ORD")%>;<%=Rs("TIPOX")%>'>
				  </div></td>
                <td class="texoformulario"><%=Rs("NB_CLTE") %></a></td>
                <td><%=Rs("ID_ORD_GP")%></td>
                <td><%=Rs("FE_ING_ORD")%></td>
                <td><%=Rs("CIU_ENT")%></td>
				<td><%=Rs("DIR_ENT")%></td>
				<td><%=Rs("STATUS")%></td>
				<td><%=Rs("CAJAS")%></td>
				<td><%=Rs("UPSZONE")%></td>
				<td><%=Rs("COMENT1")%></td>
				<td><%=Rs("COMENT2")%></td>
              </tr>
              <%
				Rs.MoveNext();
				}
				Rs.Close();				 
              %>
            </table>
            <%
		tiempo=new Date();
		Response.Write(tiempo)
	%>
            <p>&nbsp;</p>
            <table width="100%" border="0" cellpadding="1" cellspacing="0" class="textoformulario">
              <tr class="textoformulario">
                <td colspan="6">Asignar Transportista</td>
                <td width="206"></td>
                <td width="253"><label>
                  <%
			  strSQL="SELECT *  FROM T_Sant_Transp WITH(NOLOCK) WHERE Status_Transp = 1 and IN_ST=1 ORDER BY NB_Transp";
				
				Rs2=Ejecutar(strSQL,2);
				
				
				%>
                  <select name="asig_transp">
                    <option></option>
                    <%while(!Rs2.EOF)
				{
				Response.Write("<option value='"+ Rs2("CI_Transp").Value +"'>"+ Rs2("NB_Transp").Value +"&nbsp;"+ Rs2("AP_Transp").Value +"</option>");
				Rs2.MoveNext();
			}
			Rs2.Close();
		
              %>
                  </select>
                  </label>                </td>
                <td width="360"><label>
				
                  <input type="submit" name="Asignar"  id="Asignar" value="Asignar" onClick="JavaScript:Asig();">
                </label>                </td>
              </tr>
            </table>
           </form>
           <%
	tiempo=new Date();
	Response.Write(tiempo)
	%>
      </table>
     
   
</body>
</html>


<%

Desconectar();
%>