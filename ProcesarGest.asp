  <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<% Response.Expires=0; %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var strObs="";

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var cantSol=0;
var cantEmb=0;
var cantDif=0;
var txOrd="";
var tpo=0;
var nbCliente="";
var dirClte="";
var tpoOrd="";
var telOrd="";
var ciuOrd="";
var feEmb="";
var feAtc="";
var nbUsrAtc="";
var tpoAcc=parseInt(Request.QueryString("tpo"));
strObs=unescape(Request.QueryString("obs"));


strSQL="EXEC SP_SANT_GEST_ORD "+idOrd+","+intIdVend+",'"+strObs+"',"+tpoAcc;
Ejecutar(strSQL,1);
	
if(tpoAcc==0)//Solo guarda
{
	Desconectar();
	Response.Redirect("ordenventagest.asp?idord="+idOrd+"&e=0");
}
else
{
	//Response.Write(strSQL);
	strSQL="SELECT CONVERT(VARCHAR(12),A.FE_USR_ATC,103) FE_USR_ATC , B.NB_USR, A.CIU_ENT, A.DIR_ENT,A.TEL_CLTE1, A.TEL_CLTE2, A.ID_ST_ORD, A.ID_ORD, A.ID_ORD_GP, CASE A.ID_TPO_ORD WHEN 0 THEN 'Venta' ELSE 'Consignaci�n' END ID_TPO_ORD, ";
	strSQL+=" A.NB_CLTE, CONVERT(VARCHAR(12),A.FE_EMB_ORD,103) FE_EMB_ORD, CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD, 'Por embalar' ST, isnull(A.OBS,'') obs";
	strSQL+=" FROM T_SANT_ORD A , T_SANT_USR B WHERE A.ID_ORD="+idOrd+"  AND A.ID_USR_ATC=B.ID_USR ";

	Rs=Ejecutar(strSQL,2);

	feEmb=Rs("FE_EMB_ORD").Value
	txOrd=Rs("ID_ORD_GP").Value;
	nbCliente=Rs("NB_CLTE").Value;
	dirClte=Rs("DIR_ENT").Value;
	tpoOrd=Rs("ID_TPO_ORD").Value;
	telOrd=Rs("TEL_CLTE1").Value+" "+Rs("TEL_CLTE2").Value;
	ciuOrd=Rs("CIU_ENT").Value+"";
	txObs=Rs("OBS").Value+"";
	nbUsrAtc=Rs("NB_USR").Value+"";
	feAtc=Rs("FE_USR_ATC").Value+"";

	Rs.Close();


%>

 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Excel()
{
	document.getElementById("Q").submit();
	//window.open("ReportesDespExcel.asp","impetiqueta","width=700,height=500,modal=yes,status=no, menubar=no, scroll=1, scrollbars=1");
}
function ImpEtiqueta(tipo)
{
if(window.confirm("�Est� seguro que desea imprimir la guia de empaque?"))
{
	var idOrd=document.getElementById("ID_ORD").value;
	window.open("ImpProcesar.asp?tipo="+tipo+"&idOrd="+idOrd,"impetiqueta","width=800,height=400,modal=yes,status=no, menubar=no, scroll=1, scrollbars=1");
}
}
</script>
<input type='hidden' name='ID_ORD' id='ID_ORD' value='<%=idOrd%>'>

<table width="97%" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="116"></td>
    <td width="788"></td>
  </tr>
  <tr>
    <td height="605" valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
      <table width="758"  border="0" cellpadding="2" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="2" class="tituloblanco"><div align="center">Datos de la orden </div></td>
        </tr>
        <tr class="textoformulario">
          <td width="15%" class="textoformulario">Nro orden venta</td>
          <td><%=txOrd%>
          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td><%=nbCliente%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Direcci&oacute;n</td>
          <td><%=dirClte%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Ciudad</td>
          <td><%=ciuOrd%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Tipo</td>
          <td class="textoformulario"><%=tpoOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td height="24" bgcolor="#0000FF" class="textoformulario">Telefonos</td>
          <td class="textoformulario"><%=telOrd%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Fecha embalaje</td>
          <td class="textoformulario"><%=feEmb%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Visualizado primera vez por </td>
          <td class="textoformulario"><%=nbUsrAtc%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Fecha visualizaci&oacute;n </td>
          <td class="textoformulario"><%=feAtc%></td>
        </tr>
        <tr class="textoformulario">
          <td bgcolor="#0000FF" class="textoformulario">Fecha cierre </td>
          <td class="textoformulario">&nbsp;</td>        
         </TR>
	</table>
<br>   
      <table width="758" border="0" cellpadding="2" cellspacing="1" class="tablamenu">
        <tr class="">
          <td colspan="3" class="tituloblanco">Datos del pedido </td>
        </tr>
        <tr class="titulo_subgrupo">
          <td width="144" class="">C&oacute;digo del producto </td>
          <td width="137">Descripci&oacute;n</td>
          <td width="144">Cantidad embalada </td>
        </tr>
        <%
        strSQL="SELECT A.SER_PROD,A.ID_PROD, A.DESC_PROD, A.CANT_ORD, SUM(ISNULL(B.CANT_PROD,0)) EMB ";
		strSQL+=" FROM T_SANT_DET_ORD A LEFT JOIN T_SANT_DET_CAJA_ORD B ";
		strSQL+=" ON A.ID_ORD=B.ID_ORD AND A.ID_PROD=B.ID_PROD ";
		strSQL+=" WHERE A.ID_ORD="+idOrd;
		strSQL+=" GROUP BY A.SER_PROD, A.ID_PROD, A.DESC_PROD, A.CANT_ORD";
		Rs=Ejecutar(strSQL,2);
		while(!Rs.EOF)
		{
        %>        
        <tr class="textoformulario">
          <td class="textoformulario"><div align="center"><%=Rs("ID_PROD")%></div></td>
          <td><div align="center"><%=Rs("DESC_PROD")%> </div></td>
          <td><div align="center" id="CANT_EMB_<%=Rs("SER_PROD")%>" name="CANT_EMB_<%=Rs("SER_PROD")%>"><%=Rs("EMB")%></div></td>
        </tr>
       <%
       cantSol+=parseFloat(Rs("CANT_ORD"));
       cantEmb+=parseFloat(Rs("EMB"));
       Rs.MoveNext();
       }
       Rs.Close();
       %>
        <tr class="titulo_subgrupo">
          <td class="">&nbsp;</td>
          <td><div align="right">Total </div></td>
          <td><div align="center"  id="CANT_EMB" name="CANT_EMB"><%=cantEmb%></div></td>
        </tr>
      </table>
      <p align="center">&nbsp;</p>  
      <table width="100%"  border="0" cellpadding="2" cellspacing="1" class="tablamenu">
        <tr class="tituloblanco">
          <td colspan="3" class="tituloblanco">Datos de la gesti&oacute;n </td>
        </tr>
        <tr class="titulo_subgrupo">
          <td width="23%" class="titulo_subgrupo"><div align="center">Usuario</div></td>
          <td width="20%"><div align="center">Fecha de gesti&oacute;n </div></td>
          <td width="57%"><div align="center">Observaciones</div></td>
        </tr>
        <%
        strSQL="SELECT CONVERT(VARCHAR(10), A.FE_GEST, 103) FE_GEST, A.OBS_GEST, B.NB_USR FROM T_SANT_USR B, T_SANT_GEST_ORD A WHERE A.ID_USR_ATC=B.ID_USR AND A.ID_ORD="+idOrd+" ORDER BY FE_GEST";
        Rs=Ejecutar(strSQL,2);
        while(!Rs.EOF)
        {
        %>
        <tr class="textoformulario">
          <td><%=Rs("NB_USR")%></td>
          <td><%=Rs("FE_GEST")%></td>
          <td><%=Rs("OBS_GEST")%></td>
        </tr>
        <%
			Rs.MoveNext();
        }
        Rs.Close();
        %>

      </table>      
      <div align="center">
        <table width="20%"  border="0" cellspacing="1" cellpadding="1">
          <tr>
		     <td><div align="center">
              <input type="button" name="Submit2" value="Aceptar" onClick="JavaScript:document.location.href='deskGest.asp';">
            </div></td>
          </tr>
		 
        </table>
      </div>     
    </td>
  </tr>
  <FORM ID='Q' NAME='Q' ACTION='guiaDetalladaExcel.asp?idOrd=<%=idOrd%>&tipo=<%=tpo%>' METHOD='POST' TARGET='_BLANK'>
  </FORM>
</table>

</body>
</html>

<%

Desconectar();
}
%>