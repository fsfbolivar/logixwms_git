<%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var cantSol=0;
var cantEmb=0;
var cantDif=0;
var txOrd="";
var nbCliente="";
var dirClte="";
var tpoOrd="";
var telOrd="";
var ciuOrd="";

        strSQL="SELECT CIU_ENT, DIR_ENT,TEL_CLTE1, TEL_CLTE2, ID_ST_ORD, ID_ORD, ID_ORD_GP, CASE ID_TPO_ORD WHEN 0 THEN 'Venta' ELSE 'Consignación' END ID_TPO_ORD, ";
		strSQL+=" NB_CLTE, CONVERT(VARCHAR(12),FE_ING_ORD,103) FE_ING_ORD, 'Por embalar' ST";
		strSQL+=" FROM T_SANT_ORD WHERE ID_ORD="+idOrd;

Rs=Ejecutar(strSQL,2);

txOrd=Rs("ID_ORD_GP").Value;
nbCliente=Rs("NB_CLTE").Value;
dirClte=Rs("DIR_ENT").Value;
tpoOrd=Rs("ID_TPO_ORD").Value;
telOrd=Rs("TEL_CLTE1").Value+" "+Rs("TEL_CLTE2").Value;
ciuOrd=Rs("CIU_ENT").Value
Rs.Close();

var idCaja=Request.QueryString("idcaja")+"";
%>

 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
.style2 {font-weight: bold}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
</script>
<input type='hidden' name='ID_ORD' id='ID_ORD' value='<%=idOrd%>'>

<table width="334" border="0" align="center" cellpadding="1" cellspacing="0">
  <tr>
    <td height="57"><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="411" colspan="6" class="textonegrita style1" ><div align="center">
          <p><font size=5> Caja <%=idCaja%> </font></p>
          </div></td>
      </tr>
	</td>
    </table>
  </tr>
  <tr>
    <td>
      <%sql="Select A.ID_CHEQ,B.ID_USR,B.NB_USR,CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD from T_SANT_ORD A,T_SANT_USR B where A.ID_ORD='"+idOrd+"' and A.ID_CHEQ = B.ID_USR"
	Rs2=Ejecutar(sql,2);
	%>
      <p align="center" class="textoformulario"><strong>Chequeador:
      <%Response.Write(Rs2("NB_USR"));%> 
	  
	  Fecha:<%=Rs2("FE_ING_ORD")%>
      </strong></p>
	 
	  
	    <% Rs2.Close();%>
    </td>
  </tr>
  <tr>
    <td height="104" valign="top" nowrap>
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td colspan="2" class="tituloblanco"><div align="center">Datos del pedido </div></td>
        </tr>
        <tr class="textoformulario">
          <td width="24%" class="textoformulario">Nro pedido </td>
          <td width="76%"><%=txOrd%>
          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td><%=nbCliente%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario ">Direcci&oacute;n</td>
          <td><%=dirClte%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>
        <tr class="textoformulario">
          <td height="20" class="textoformulario ">Ciudad</td>
          <td><%=ciuOrd%>
          <div align="center"></div>          <div align="center"></div>          <div align="center"></div></td>
        </tr>

	</table>
</td>
  </tr>
</table>

</body>
</html>
<script>window.print();</script>
<%
Desconectar();
%>