<%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%  
/****************************************************************************
PAGINA FUNCIONAL.
FINALIDAD:		Permite la busqueda y seleccin de un cliente
FECHA:			16/03/2006
DESARROLLADO:	Dario Carnelutti - SITVEN, C.A.
*****************************************************************************/

//Se valida que aun la session sea valida
ValidarSession();

Session("creando")=0;

if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");
	
//declaracion de variables	
var boBusqueda=false;
var boChkCliente=false;
var strNbCliente=new String();
var strRifCliente=new String();
var strSQL=new String();
var intIdVend=0;
var idUsr=new Number();
intIdVend=Request.Cookies("usr")("id");
var cargo=parseFloat(Request.Cookies("usr")("cr"));

if(Request.Form.Count>0)
{
	//Captura de los datos para la busqueda
	boBusqueda=true;
	strNbCliente=Request.Form("NB_CLIE")+"";
	if(Request.Form("RIF_CLIE")+""!="undefined")
		strRifCliente=Request.Form("RIF_CLIE")+"";
	else
		strRifCliente="";
	boChkCliente=(Request.Form("CHK_CLIE")=="T");
}
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Grupo SANTILLANA - LOGISTICA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" type="text/JavaScript">
function BusquedaCliente()
{
	if(document.getElementById("NB_CLIE").value=="" && document.getElementById("RIF_CLIE").value=="")
		alert("Error:\n - Debe indicar el nombre o el RIF del cliente");
	else
		document.getElementById("BUSQ_CLIE").submit();
}
 
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a), <%=Request.Cookies("usr")("nombre")%> </td>
  </tr>
  <tr>
    <td width="125"></td>
    <td width="621"></td>
  </tr>
  <tr>
    <td valign="top">
        <!-- #include file="include/menu.asp"//-->
	<p>&nbsp;</p>      </td>
    <td valign="top" nowrap><div align="left">
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">


        <tr bgcolor="#6699CC" class="subtitulo">
          <td colspan="7" class="tituloblanco"><div align="center">Busqueda</div></td>
          </tr>
<form id='BUSQ_CLIE' action="IniProy.asp" method='POST'>          
        <tr bgcolor="#E7E7E7" class="subtitulo">
          <td width="20%" bgcolor="#E7E7E7" class="textoformulario">Nombre de cliente </td>
          <td width="19%"><span class="b">
            <input id="NB_CLIE" name="NB_CLIE" type="text" class="texto" size="20" maxlength="20">
          </span></td>
          <td colspan="3" class="textoformulario">
           </td>
          <td width="9%" bgcolor="#E7E7E7">&nbsp;        </td>
          <td width="11%" bgcolor="#E7E7E7"><input name="button" type="button" class="textonegrita" id="button" onClick="JavaScript:BusquedaCliente();" value="Buscar"></td>
        </tr>
</form>        		
</table>
<br><br>
          <%
          if(boBusqueda){ 
          %>
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">		  
        <tr bgcolor="#6699CC" class="tituloblanco">
			<td><div align="left">RIF</div></td>
          <td colspan=3><div align="left">Cliente</div></td>
          <td colspan=3><div align="left">Tel�fono</div></td>
          </tr>
			<%
			//Se arma el query de busqueda
			
			strSQL="SELECT LTRIM(RTRIM(CUSTNMBR)) CUSTNMBR, LTRIM(RTRIM(CUSTNAME)) CUSTNAME, PHONE1 FROM rm00101 WHERE CUSTNAME LIKE '%"+strNbCliente+"%'";
			
			//Response.Write(strSQL);
			Rs=Ejecutar(strSQL,2,0,1);
			if(Rs.EOF)
			{%>
					<tr bgcolor="#E7E7E7" class="textonegrita">
						<td colspan="7" class="textoformulario">No se han encontrado clientes</a></td>
					</tr>
			<%	
			}
			else
			{
				while(!Rs.EOF)
				{
				%>          
					<tr bgcolor="#E7E7E7" class="textonegrita">
						<td class="textoformulario"><a href="regdev.asp?id_cli=<%=escape(Rs("CUSTNAME"))%>&ph=<%=Rs("phone1")%>&rif=<%=Rs("CUSTNMBR")%>"><%=Rs("CUSTNMBR")%></a></td>
						<td colspan=3 class="textoformulario"><a href="regdev.asp?id_cli=<%=escape(Rs("CUSTNAME"))%>&ph=<%=Rs("phone1")%>&rif=<%=Rs("CUSTNMBR")%>"><%=Rs("custname")%></a></td>
						<td colspan=3 class="textoformulario"><a href="regdev.asp?id_cli=<%=escape(Rs("CUSTNAME"))%>&ph=<%=Rs("phone1")%>&rif=<%=Rs("CUSTNMBR")%>"><%=Rs("phone1")%></a></td>
					</tr>
				<%
				Rs.MoveNext
				}
			}
			Rs.Close();
        }%>
      </table>
    </div></td>
  </tr>
</table>
</body>
</html>
<%
Desconectar();
%>