<%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<% Response.Expires=0; %>

<%
//Se valida que aun la session sea valida
ValidarSession();


	if(Conectar()!=0)
		Response.Redirect("index.asp?error=1");
	
	var mensaje = "";
	if(Request.Form("caja")+""!="undefined"){
		var cajas =  (Request.Form("caja")+"").split(",");
		var cantidades =  (Request.Form("n")+"").split(",");
		for(var i=0; i<cajas.length; i++){
			strSQL="update t_sant_det_caja_ord set cant_prod="+cantidades[i]+" where id_caja="+cajas[i]+" and id_ord="+Request.QueryString("idord")+" and id_prod='"+Request.QueryString("idprod")+"'";
			//Response.Write(strSQL+"<br/>");
			Ejecutar(strSQL,1);
			mensaje = "Cantidades asignadas correctamente";
		}
	}
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>GRUPO SANTILLANA</title>
		<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
	</head>
	
	<body style="margin:0px 0px 0px 0px;">
		<form name="fcambiar" action="cambiarcantidad.asp?idord=<%=Request.QueryString("idord")%>&idprod=<%=Request.QueryString("idprod")%>" method="post" style="text-align:right;">
			<table width="100%" border="0" cellpadding="2" cellspacing="1" class="tablamenu">
				<tr>
					<td colspan="4" class="tituloblanco">Cambiar cantidades</td>
				</tr>
				<tr class="titulo_subgrupo">
					<td>Caja</td>
					<td>C&oacute;digo</td>
					<td>Producto</td>
					<td>Cantidad</td>
				</tr>
				<%
					strSQL="select a.id_caja, a.id_prod, b.itemdesc, a.cant_prod from t_sant_det_caja_ord as a inner join iv00101 as b on a.id_prod=b.itemnmbr where a.id_ord="+Request.QueryString("idord")+" and a.id_prod='"+Request.QueryString("idprod")+"' order by a.id_caja";
					//Response.Write(strSQL); Response.Flush(); Response.End();
					Rs=Ejecutar(strSQL,2);
					while(!Rs.EOF){
				%>
				<tr class="textoformulario">
					<td><%=Rs("id_caja")%><input type="hidden" name="caja" id="caja" value="<%=Rs("id_caja")%>" /></td>
					<td><%=Rs("id_prod")%></td>
					<td><%=Rs("itemdesc")%></td>
					<td><input class="textonegrita" size="4" type="text" name="n" value="<%=Rs("cant_prod")%>" /></td>
				</tr>
				<%
						Rs.MoveNext();
					}
					Rs.Close();
				%>
			</table>
			<div class="texto_verde" align="center"><%=mensaje%></div>
			<input class="textonegrita" type="submit" name="guardar" id="guardar" value="Guardar" onclick="javascript: alert('Esta a punto de modificar la cantidad de ejemplares por caja del pedido que seleccion�'); return(confirm('�Esta seguro de querer asignar las cantidades?'));" />
			<%
				if(mensaje!=""){
			%>
			<script language="javascript">
				window.opener.location.reload();
			</script>
			<%
				}
			%>
		</form>
	</body>
</html>
<%
	Desconectar();
%>