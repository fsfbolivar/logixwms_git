 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var tipo=Request.QueryString("tipo")+"";
var texto=(parseFloat(tipo)==0?" orden ":" devolución ");
var idg=Request.QueryString("idg")+"";
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<script>
window.resizeTo(800,500);

function Agregar()
{
var hay=false

if(document.getElementById("nbCliente").value=="" || document.getElementById("bultos").value=="" || document.getElementById("ejemp").value=="" || document.getElementById("destino").value=="" || document.getElementById("direccion").value=="" || document.getElementById("obs").value==""   )
{
	alert("Debe indicar todos los datos solicitados");
}
else
{
	document.getElementById("solicitudes").submit();
}
}
</script>

<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<div align="center">
</div>
<p align="center">
<%
if(Request.QueryString("msg")=="1")
{
	Response.Write(" <span class='texto_verde'>El memo ha sido agregado a la guia de carga</span>");
	Response.Write("<script>window.opener.location.reload();</script>");
}
%>
</p>

<div align="center">
  <table width="90%"  border="0" cellpadding="2" cellspacing="1" class="tablamenu">
  <form id='solicitudes' name='solicitudes' action='AgregarMemoF.asp?idg=<%=Request.QueryString("idg")%>&tipo=<%=Request.QueryString("tipo")%>' method='POST'>
    <tr class="tituloblanco">
      <td colspan="2">Datos del memo</td>
    </tr>
    <tr class="textoformulario">
      <td width="20%" class="textoformulario">Cliente</td>
      <td width="51%"><input name="nbCliente" id="nbCliente" type="text" class="textonegritaFB" size="40"></td>
      </tr>
    <tr class="textoformulario">
      <td class="textoformulario">Bultos</td>
      <td class="textoformulario"><input name="bultos" id="bultos" type="text" class="textonegritaFB"></td>
    </tr>
    <tr class="textoformulario">
      <td class="textoformulario">Ejemplares</td>
      <td class="textoformulario"><input name="ejemp" id="ejemp" type="text" class="textonegritaFB"></td>
    </tr>
    <tr class="textoformulario">
      <td class="textoformulario">Destino</td>
      <td class="textoformulario"><input name="destino" id="destino" type="text" class="textonegritaFB" size="40"></td>
    </tr>
    <tr class="textoformulario">
      <td class="textoformulario">Direcci&oacute;n</td>
      <td class="textoformulario"><textarea name="direccion" id="direccion" cols="45" rows="4" class="textonegritaFB"></textarea></td>
    </tr>
    <tr class="textoformulario">
      <td class="textoformulario">Comentarios</td>
      <td class="textoformulario"><textarea name="obs" id="obs" cols="45" rows="4" class="textonegritaFB"></textarea></td>
    </tr>
    </form>
  </table>
</div>
<p align="center">
  <input name="Submit2" type="button" class="textonegrita" value="Agregar" onClick="JavaScript:Agregar();">
</p>

</body>
</html>

<%
Desconectar();
%>