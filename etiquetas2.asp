
 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var nroOrd="";
var nbClte="";
var fdesde="";
var fhasta="";

if(Request.Form.Count>0)
{
	nroOrd=Request.Form("nro_ord1")+"";
	nbClte=Request.Form("nb_clte")+"";
	fdesde=Request.Form("fdesde")+"";
	fhasta=Request.Form("fhasta")+"";
}
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function ImpEtiqueta(idOrd, idCaja)
{
if(window.confirm("�Est� seguro que desea imprimir la etiqueta?"))
{
	window.open("ImpEtiqueta2.asp?idOrd="+idOrd+"&idcaja="+idCaja,"impetiqueta","width=700,height=500,modal=yes,status=no, menubar=no");
}
}

function BusqDesk()
{
if(document.getElementById("nro_ord1").value=="" && document.getElementById("nb_clte").value=="" && document.getElementById("fdesde").value=="" && document.getElementById("fhasta").value=="")
{
	alert("Debe indicar alguno de los campos para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_DESK").submit();
}
}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          <p>&nbsp;</p></td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="115"></td>
    <td width="631"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
    
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
      <FORM ID='FRM_DESK' NAME='FRM_DESK' action='etiquetas2.asp' METHOD='POST'>
        <tr>
          <td colspan="6" class="tituloblanco"><div align="center">Busqueda</div></td>
        </tr>
        <tr class="textoformulario">
          <td width="210" class="textoformulario">Nro pedido </td>
          <td width="208">&nbsp;</td>
          <td width="208"><input name="nro_ord1"  id="nro_ord1" type="text" class="texto"></td>
          <td width="33">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td>&nbsp;</td>
          <td><input name="nb_clte" id="nb_clte" type="text" class="texto"></td>
          <td>&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Fecha del pedido (desde) </td>
          <td>&nbsp;</td>
          <td><input name="fdesde" id="fdesde" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fdesde","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fdesde");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td width="76">&nbsp;</td>
          <td width="101" colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="">Fecha del pedido(hasta)</td>
          <td>&nbsp;</td>
          <td><input name="fhasta" id="fhasta" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fhasta","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fhasta");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td><input name="button" type="button" class="textonegrita" value="Buscar" onClick="JavaScript:BusqDesk();"></td>
          <td colspan="2">&nbsp;</td>
        </tr>
        </form>
	</table>
      <table width="100%"  border="0" cellpadding="3" cellspacing="0">
        <tr class="">
          <td height="9"><p>&nbsp;</p>
          <table width="100%"  border="0" cellpadding="1" cellspacing="1" class="tablamenu">
              <tr>
                <td colspan="5" class="tituloblanco">Pedidos</td>
              </tr>
              <tr class="titulo_subgrupo">
                <td width="24%">Cliente</td>
                <td width="24%"><div align="center">Nro pedido </div></td>
                <td width="15%"><div align="center">Fecha pedido </div></td>
                <td width="12%"><div align="center">Caja</div></td>
                <td width="25%"><div align="center"></div></td>
              </tr>
              <%
              if(Request.Form.Count>0)
              {
				strSQL="SELECT A.ID_ORD, A.NB_CLTE, A.ID_ORD_GP, CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD, B.ID_CAJA ";
				strSQL+=" FROM T_SANT_ORD A, T_SANT_CAJA_ORD B ";
				strSQL+=" WHERE A.ID_ORD=B.ID_ORD	";
				strSQL+=" AND B.ID_ST_CAJA=1 ";
				if(nroOrd.length>0)
				{
					strSQL+=" AND A.ID_ORD_GP='"+nroOrd+"'" ;
				}
				if(nbClte.length>0)
				{
					strSQL+=" AND A.NB_CLTE LIKE '%"+nbClte+"%'" ;
				}
				if(fdesde.length>0)
				{
					strSQL+=" AND CONVERT(VARCHAR(12),a.FE_ING_ORD,112) >= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fdesde+"',103),112) ";
				}
				if(fhasta.length>0)
				{
					strSQL+=" AND CONVERT(VARCHAR(12),a.FE_ING_ORD,112) <= CONVERT(VARCHAR(12),CONVERT(DATETIME,'"+fhasta+"',103),112) ";
				}
				strSQL+=" ORDER BY A.NB_CLTE, A.FE_ING_ORD, A.ID_ORD_GP, B.ID_CAJA";
				
              Rs=Ejecutar(strSQL,2);
              while(!Rs.EOF)
              {
              %>
              <tr class="textoformulario">
                <td><%=Rs("NB_CLTE")%></td>
                <td><%=Rs("ID_ORD_GP")%></td>
                <td><%=Rs("FE_ING_ORD")%></td>
                <td><%=Rs("ID_CAJA")%></td>
                <td><INPUT TYPE='BUTTON' VALUE='Imprimir' ONCLICK='JavaScript:ImpEtiqueta(<%=Rs("ID_ORD")%>,<%=Rs("ID_CAJA")%>);' CLASS='TEXTONEGRITA'></td>
              </tr>
              <%
              Rs.MoveNext();
              }
              Rs.Close();
              }
              
              %>
            </table>          <p>&nbsp;</p></td>
        </tr>
 

      </table>
</td>
  </tr>
</table>
</body>
</html>



<%

Desconectar();
%>