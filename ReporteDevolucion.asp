<%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->

<%
	//Se valida que aun la session sea valida
	ValidarSession();
	
	
	if(Conectar()!=0)
		Response.Redirect("index.asp?error=1");
		
	var strSQL="SELECT DISTINCT ISNULL(B.CUSTNAME,' ') AS NB_CLTE_DEV, ISNULL(B.CUSTNMBR,' ') AS RIF_CLTE_DEV, ISNULL(B.CNTCPRSN,' ') AS CNTCPRSN, ISNULL(A.TEL_CLTE,' ') AS TEL_CLTE, ISNULL(A.DIR_DEV,' ') AS DIR_DEV, ISNULL(A.CANT_CAJA,' ') AS CANT_CAJA FROM T_SANT_DEV AS A WITH (NOLOCK) LEFT JOIN RM00101 AS B WITH (NOLOCK) ON (A.RIF_CLTE_DEV=B.CUSTNMBR OR A.NB_CLTE_DEV=CUSTNAME) WHERE A.ID_DEV="+Request.QueryString("id_dev")+"";
	//Response.Write(strSQL); //Response.Flush(); Response.End();
	var Rs=Ejecutar(strSQL,2);
	var nbCliente = Rs(0)+"";
	var rifCliente = Rs(1)+"";
	var perContacto = Rs(2)+"";
	var telCliente = Rs(3)+"";
	var dirCliente = Rs(4)+"";
	var nCajas = Rs(5)+"";
	Rs.Close();
	
%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Solicitud de devoluci&oacute;n</title>
		<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css" media="all">
	</head>
		
	<body>
	<%
	
	%>
	<table width="726" border="0" align="center" cellpadding="5" cellspacing="2">
			<!--CABECERA DEL REPORTE -->
			<tr>
				<td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td class="textonegrita" ><p><img src="images/logo.png" width="257" height="53"></p></td>
							<td width="30%" class="textonegrita" align='right' style="font-size:13px;">
								<% 
									var fecha = new Date();
									Response.Write((fecha.getDate()<10?"0"+fecha.getDate():fecha.getDate()+"")+"/"+((fecha.getMonth()+1)<10?"0"+(fecha.getMonth()+1):(fecha.getMonth()+1)+"")+"/"+fecha.getFullYear()+"");
								%>
							</td>
						</tr>
						<tr>
							<td colspan="2" class="textonegrita" style="font-weight:bold; font-size:18px; font-variant:small-caps; text-align:center; height:30px;">Devoluci&oacute;n de Cliente</td>
						</tr>
					</table>
				</td>
			</tr>
			<!-- CUERPO -->
			<tr>
				<td height="162" valign="top">
				<table width="100%" border="0" cellpadding="0" cellspacing="4" bordercolor="#FFFFFF" style="font-family:Arial, Helvetica, sans-serif; font-size:12px;">
                  <tr>
                    <td colspan="10" height="25" class="textoformulariocentrado" style="font-size:12px;">Informaci&oacute;n a ser llenada por ATC </td>
                  </tr>
                  <tr>
                    <td width="87">Cliente</td>
                    <td colspan="4" style="border:2px solid #000000;"><%=nbCliente%></td>
                    <td width="87">R.I.F.:</td>
                    <td colspan="4" style="border:2px solid #000000;"><%=rifCliente%></td>
                  </tr>
                  <tr>
                    <td colspan="2">C&oacute;digo del cliente </td>
                    <td colspan="3" style="border:2px solid #000000;">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td width="87">&nbsp;</td>
                    <td width="37">&nbsp;</td>
                    <td width="37">&nbsp;</td>
                    <td width="37">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="2">Persona de contacto </td>
                    <td colspan="3" style="border:2px solid #000000;"><%=perContacto%></td>
                    <td>Tel&eacute;fono</td>
                    <td colspan="4" style="border:2px solid #000000;"><%=telCliente%></td>
                  </tr>
                  <tr>
                    <td colspan="3">Direcci&oacute;n de retiro de la mercanc&iacute;a </td>
                    <td colspan="7">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="82" colspan="10" valign="top" style="border:2px solid #000000;"><%=dirCliente%></td>
                  </tr>
                  
                  <tr>
                    <td colspan="3">Atendido en el Dpto. de ATC por: </td>
                    <td colspan="7" style="border:2px solid #000000;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">Tipo de venta</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Venta en firme 
                    <input type="checkbox" name="checkbox" value="checkbox"  /></td>
                    <td colspan="2">Consignaci&oacute;n
                    <input type="checkbox" name="checkbox2" value="checkbox" /></td>
                    <td colspan="2">Promoci&oacute;n
                    <input type="checkbox" name="checkbox22" value="checkbox" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">L&iacute;nea de negocio </td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Texto:</td>
                    <td width="87"><input type="checkbox" name="checkbox222" value="checkbox" /></td>
                    <td colspan="2">Richmond:</td>
                    <td><input type="checkbox" name="checkbox2223" value="checkbox" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="2">Ediciones generales:</td>
                    <td><input type="checkbox" name="checkbox2222" value="checkbox" /></td>
                    <td colspan="2">Alfaguara infantil:</td>
                    <td><input type="checkbox" name="checkbox22232" value="checkbox" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="25" colspan="10" class="textoformulariocentrado" style="font-size:12px;">Informaci&oacute;n a ser llenada por el cliente </td>
                  </tr>
                  <tr>
                    <td colspan="2">Cajas Santillana:</td>
                    <td width="87">S&iacute;
                    <input type="checkbox" name="checkbox222322" value="checkbox" /></td>
                    <td>No
                    <input type="checkbox" name="checkbox2223222" value="checkbox" /></td>
                    <td width="87">&nbsp;</td>
                    <td colspan="2" rowspan="2" valign="middle"><div align="center">N&deg; de bultos retirados </div></td>
                    <td colspan="3" rowspan="2" valign="middle" style="border:2px solid #000000; font-size:12px" align="center"><%=nCajas%></td>
                  </tr>
                  <tr>
                    <td colspan="3">Motivo devoluci&oacute;n:</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td height="140" colspan="10" valign="top" style="border:2px solid #000000;">&nbsp;</td>
                  </tr>
                  
                  <tr>
                    <td colspan="4">&iquest;Desea revisar la devoluci&oacute;n en el almac&eacute;n? </td>
                    <td>S&iacute;
                      <input type="checkbox" name="checkbox2223223" value="checkbox" /></td>
                    <td>No
                    <input type="checkbox" name="checkbox22232222" value="checkbox" /></td>
                    <td><div align="right">Fecha:</div></td>
                    <td colspan="3" style="border:2px solid #000000;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="4">&iquest;Existe un reclamo para la devoluci&oacute;n?</td>
                    <td>S&iacute;
                      <input type="checkbox" name="checkbox22232232" value="checkbox" /></td>
                    <td>No
                    <input type="checkbox" name="checkbox222322222" value="checkbox" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td colspan="2"><div align="right">N&deg; de Reclamo: </div></td>
                    <td colspan="2" style="border:2px solid #000000;">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="4">Nombre del transportista que retir&oacute; la devoluci&oacute;n: </td>
                    <td colspan="6" style="border:2px solid #000000;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="10">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="3" style="border-bottom:2px solid #000000;">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="5" style="border-bottom:2px solid #000000;">&nbsp;</td>
                  </tr>
                  <tr>
                    <td colspan="3"><div align="center">Atendido por (Cliente) </div></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="5"><div align="center">Recibido en Log&iacute;stica por </div></td>
                  </tr>
                  <tr>
                    <td colspan="3" style="border-bottom:2px solid #000000;">C.I.:</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td width="87">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                </table></td>
			</tr>
		</table>
	
	</body>
	<script language="javascript">
		window.print();
	</script>
</html>
<%
Desconectar();
%>