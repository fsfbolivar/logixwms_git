/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Servlets;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 *
 * @author postgres
 */
public class ExportToPDF extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        // TODO code application logic here
        JasperReport jasperReport;
        JasperPrint  jasperPrint;

        try
        {
            //Conectandose a la base de datos
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            String url = "jdbc:jtds:sqlserver://172.20.1.5/SNTD2";
            Connection conn = DriverManager.getConnection(url, "sa", "sql0103");

            HashMap parametros = new HashMap();

            //1-Compilar
            jasperReport = JasperCompileManager.compileReport("/opt/nota_entrega/NotaEntrega.jrxml");
            //2-Llenar
            jasperPrint  = JasperFillManager.fillReport(jasperReport, parametros, conn);
            //3-Exportar
            JasperExportManager.exportReportToPdfFile(jasperPrint, "/opt/nota_entrega/NotaEntrega.pdf");

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        ServletOutputStream  out = response.getOutputStream ();
    //---------------------------------------------------------------
    // Set the output data's mime type
    //---------------------------------------------------------------
        response.setContentType( "application/pdf" );  // MIME type for pdf doc
    //---------------------------------------------------------------
    // create an input stream from fileURL
    //---------------------------------------------------------------
        String fileURL = "file://///opt/nota_entrega/NotaEntrega.pdf";
    //------------------------------------------------------------
    // Content-disposition header - don't open in browser and
    // set the "Save As..." filename.
    // *There is reportedly a bug in IE4.0 which  ignores this...
    //------------------------------------------------------------
        //response.setHeader("Content-disposition","attachment; filename=" + "Example.pdf" );
    //-----------------------------------------------------------------
    // PROXY_HOST and PROXY_PORT should be your proxy host and port
    // that will let you go through the firewall without authentication.
    // Otherwise set the system properties and use URLConnection.getInputStream().
    //-----------------------------------------------------------------
        BufferedInputStream  bis = null;
        BufferedOutputStream bos = null;
        try {

            URL url = new URL(fileURL);
            //URL url = new URL( "http", PROXY_HOST, Integer.parseInt(PROXY_PORT), fileURL);
            // Use Buffered Stream for reading/writing.
            bis = new BufferedInputStream(url.openStream());
            bos = new BufferedOutputStream(out);
            byte[] buff = new byte[2048];
            int bytesRead;
            // Simple read/write loop.
            while(-1 != (bytesRead = bis.read(buff, 0, buff.length))) {
                bos.write(buff, 0, bytesRead);
            }
        } catch(final MalformedURLException e) {
            System.out.println ( "MalformedURLException." );
            throw e;
        } catch(final IOException e) {
            System.out.println ( "IOException." );
            throw e;
        } finally {
            if (bis != null)
                bis.close();
            if (bos != null)
                bos.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
