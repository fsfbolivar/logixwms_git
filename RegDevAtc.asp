 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<% Response.CacheControl = "no-cache" %>
<% Response.AddHeader("Pragma", "no-cache"); %>
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
var idOrd=Request.QueryString("idord")+"";
var edit=Request.QueryString("e")+"";
var id_cli=Request.QueryString("id_cli")+"";
var ph=Request.QueryString("ph")+"";
Session("hay")=0;
var idDev=0;

%>

<html>
<head>
<title>Grupo SANTILLANA - LOGISTICA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>
<body>
<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Registrar()
{
if(document.getElementById("txCliente").value=="")
{
	alert("Debe indicar el nombre del cliente");
	return(false);
}
if(document.getElementById("cant").value=="")
{
	alert("Debe indicar la cantidad de cajas devueltas");
	return(false);
}
document.getElementById('dev').submit();

}
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          <p>&nbsp;</p></td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="108"></td>
    <td width="701"></td>
  </tr>
  <tr>
    <td height="605" valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
      <table width="100%"  border="0" cellpadding="2" cellspacing="0" class="tabla_borde_negro">
      <form id='dev' name='dev' method='post' action='regdevf.asp'>
        <tr>
          <td colspan="2" class="tituloblanco"><div align="center">Datos de la devoluci&oacute;n </div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td>
            <div align="left">
              <INPUT TYPE='hidden' NAME='txCliente' ID='txCliente'  value='<%=id_cli%>'><%=unescape(id_cli)%>
              
            </div></td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Tel�fono del cliente </td>
          <td>
            <div align="left">
              <INPUT TYPE='hidden' NAME='phCliente' ID='phCliente'  value='<%=ph%>'><%=unescape(ph)%>
              
            </div></td>
        </tr>        
        
        <tr class="textoformulario">
          <td class="textoformulario ">Cantidad cajas </td>
          <td>
            <div align="left">
              <INPUT TYPE='TEXT' NAME='cant' ID='cant' >
            </div></td>
        </tr>
        </form>
	</table>
      
      <p><br>
    

</p>
      <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tabla_borde_negro">
        <tr>
          <td class="tituloblanco">Producto a ingresar </td>
        </tr>
        <tr class="textoformulario">
          <td width="30%">
            <div align="center">
              <input name="serial" id="serial" onKeyUp="JavaScript:IngresoSerial(this.value);" type="text" class="textonegrita" size="20" maxlength="13">   
            [Buscar producto] 
          </div></td>
        </tr>
        <tr class="textoformulario">
          <td width="30%">&nbsp;</td>
        </tr>
      </table>

      <br>
      <table width="758" border="0" cellpadding="2" cellspacing="1" class="tablamenu" id="tabla_seriales">
        <tr class="">
          <td colspan="4" class="tituloblanco">Detalle de la devoluci&oacute;n </td>
        </tr>
        <tr class="titulo_subgrupo">
          <td width="172" class=""><div align="center">C&oacute;digo del producto </div></td>
          <td width="384"><div align="center">Descripci&oacute;n</div></td>
          <td width="57"><div align="center">Cantidad </div></td>
        </tr>
        <%
			strSQL="SELECT LTRIM(RTRIM(C.ITEMNMBR)) ITEMNMBR, C.ITEMDESC , A.CANT_PROD, ST_PROD, A.SER_PROD ,CASE A.ST_PROD WHEN 1 THEN 'No da&ntilde;ado' else 'Da&ntilde;ado' END ST FROM T_SANT_DET_CAJA_DEV A, T_SANT_CAJA_DEV B, IV00101 C ";
			strSQL+=" WHERE B.ID_DEV="+idDev+" AND A.ID_CAJA=B.ID_CAJA AND A.ID_PROD=C.ITEMNMBR AND A.ID_DEV=B.ID_DEV";
			//Response.Write(strSQL);
			Rs=Ejecutar(strSQL,2);
			var f=0;
			var cant=0;
			while(!Rs.EOF)
			{
			var desc=Rs("ITEMDESC").Value+"";
			desc=desc.replace("'","");
			 %>
        <tr class="textoformulario">
          <td class="textoformulario"><%=Rs("ITEMNMBR")%></td>
          <td><div align="left" ><%=Rs("ITEMDESC")%></div></td>
          <td><div align="right" >
            <input name="textfield" type="text" class="textonegrita" size="4" maxlength="5" value="<%=Rs("CANT_PROD")%>">
            <%=Rs("CANT_PROD")%></div></td>
          <%="<script>"%> IniciarSerial('<%=Rs("ser_prod")%>', parseFloat(<%=Rs("ST_PROD")%>),<%=f%>,'<%=desc%>',<%=Rs("CANT_PROD")%>,'<%=Rs("ITEMNMBR").Value%>'); <%="</script>"%> 
        </tr>
        <%
			cant+=parseFloat(Rs("CANT_PROD"));
			Rs.MoveNext();
			
			f++;
		   }
       Rs.Close();

       %>
        <tr class="titulo_subgrupo">
          <td class="">&nbsp;</td>
          <td><div align="right">Total </div></td>
          <td><div align="right" id="CANT_SOL" name="CANT_SOL"><%=cant%></div></td>
        </tr>
      </table>
      <p>&nbsp;</p>
    <p>&nbsp;</p>
    <table width="324" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td height="41">       
        <span class="textoformulario">        </span><span class="textoformulario">
		</span>
          <form name="form1" method="post" action="">
            <div align="center">            </div>
          </form>
          <span class="textoformulario">        </span>
          <div align="center">
            <input type="button" name="Submit2" value="Registrar" onClick="JavaScript:Registrar();" class='textonegrita'>
</div></td>
        </tr>
    </table>    
    <span class="textoformulario">    </span>    <p align="center">&nbsp;</p></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
<%
Desconectar();
%>