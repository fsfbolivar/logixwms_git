 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%
//Se valida que aun la session sea válida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var intCargo=parseInt(Request.Cookies("usr")("cr"));
var intIdVend=Request.Cookies("usr")("id");
  
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>
<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Ordenar(tipo,dir,lugar)
{
	document.getElementById("BUSQ_ORD").action+="?tipo="+tipo+"&dir="+dir+"&lug="+lugar;
	document.getElementById("BUSQ_ORD").submit();
}
function BusqDesk()
{
if(document.getElementById("codigoLibro2").value=="" && document.getElementById("codigoBarra2").value=="" )
{
	alert("Debe indicar alguno de los campos para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_DESK").submit();
}
}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
function ImpOrd()
{
var ordenes=document.getElementsByName("IMPORD");
var hay=false;
for(var i=0;i<ordenes.length && !hay;i++)
{
	if(ordenes.item(i).checked)
		hay=true;
}
if(!hay)
	alert("Indique las ordenes que desea imprimir");
else
	document.getElementById("ordenes").submit();

}
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>          </td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a), <%=Request.Cookies("usr")("nombre")%> </td>
  </tr>
  <tr>
    <td width="115"></td>
    <td width="631"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
	<div align="center" class="textonegrita"><%=Session("mensaje")%></div>
	<table width="100%" height="99"  border="0" align="center" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
      <tr>
        <td colspan="6" class="tituloblanco"><div align="center">Registro de Libro </div></td>
      </tr>
      <form id="FRM_DESK" name="FRM_DESK" action="Cargar_LB.asp" method="post">
        <tr class="titulo_subgrupo">
          <td width="12%" >  Cod Libro:</td>
          <td width="13%" > Cod Barra </td>
          <td width="51%" > Descripcion</td>
          <td width="10%" > Clase</td>
          <td width="14%" > Cantidad</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario"><input name="CodigoLibro" type="text" size="12"></td>
          <td class="textoformulario"><input name="CodigoBarra" type="text" size="13"></td>
          <td class="textoformulario"><input name="Descripcion" type="text" size="50"></td>
          <td class="textoformulario"><input name="Clase" type="text" size="10"></td>
          <td class="textoformulario"><input name="Cantidad" type="text" size="7"></td>
        </tr>
        <tr class="textoformulario">
          <td height="22" colspan="5" class="textoformulario"><div align="center"><input type="submit" name="Submit" value="Guardar" class="textonegrita"></div></td>
        </tr>
      </form>
	  </table>
	  <table><tr><td>&nbsp;</td></tr></table>
	 <form action="Registro_Libro.asp?bsq=1" name="FRM_DESK" id="FRM_DESK" method="post">
	   <table width="100%" border="0" cellspacing="0" cellpadding="1" class="tabla_borde_negro">
      <tr>
        <td colspan="5" class="tituloblanco"><div align="center">Buscar</div></td>
        </tr>
      <tr class="textoformulario">
        <td width="64">Cod Libro </td>
        <td width="151"><label>
          <input type="text" name="codigoLibro2" id="codigoLibro2">
        </label></td>
        <td width="96">Cod de Barra </td>
        <td width="156"><input type="text" name="codigoBarra2" id="codigoBarra2"></td>
        <td width="159"><label>
          <input type="submit" name="Submit2" value="Buscar" class="textonegrita" onClick="JavaScript:BusqDesk();">
        </label></td>
      </tr>
	  
    </table>
	</form>
	
      <table width="100%" height="63"  border="0" align="center" cellpadding="1" cellspacing="1" class="tablamenu">
        <tr>
          <td height="14" colspan="6" class="tituloblanco"><div align="center">Libros Nuevos </div></td>
        </tr>
        <tr class="titulo_subgrupo" >
          <td width="11%" height="16">Cod Libro </td>
		  <td width="45%" height="16">Descripcion</td>
		  <td width="8%" height="16">Clase</td>
		  <td width="9%" height="16" >Cantidad</td>
		  <td height="16"  colspan="2">Estatus</td>
		</tr>
		<%
			strSQL="SELECT * FROM T_SANT_LIBROS_NUEVOS";
			
			if (Request.Form.Count>0){
			
				if(Request.QueryString("bsq")=="1")
			{
				var codigoLibro2=Request.Form("codigoLibro2")+"";
				var codigoBarra2=Request.Form("codigoBarra2")+"";
				
				
				if(codigoLibro2.length>0)
				{
					strSQL+=" WHERE COD_LIBRO = '"+codigoLibro2+"'  ORDER BY COD_LIBRO ";
				}
				if(codigoBarra2.length>0)
				{
					strSQL+=" WHERE COD_BARRA = '"+codigoBarra2+"'  ORDER BY COD_BARRA";
				}
				
			}
		}
		Response.Flush();
		
		Rs=Ejecutar(strSQL,2);
		if(Rs.EOF)
		{%>        
        <tr class="textoformulario">
          <td height="14" colspan="2" class="textoformulario">No hay pedidos pendientes</td>
          <td class="textoformulario">&nbsp;</td>
          <td class="textoformulario">&nbsp;</td>
          <td class="textoformulario">&nbsp;</td>
          <td class="textoformulario">&nbsp;</td>
        </tr>
		<%}%>
		<%while(!Rs.EOF){%>
        <tr class="textoformulario">
          <td height="14" class="textoformulario"><%=Rs("COD_LIBRO")%></td>
          <td width="45%" class="textoformulario"><%=Rs("DESCRIPCION")%></td>
          <td width="8%" class="textoformulario"><%=Rs("CLASE")%></td>
          <td class="textoformulario"><%=Rs("CANTIDAD")%></td>
          <td width="12%" class="opcionmenu"><a href="ver_libroNuevo.asp?id=<%=Rs("COD_LIBRO")%>">Modificar</td>
          <td width="15%" class="opcionmenu"><a href="Eliminar_LB.asp?id=<%=Rs("COD_LIBRO")%>">Eliminar</a></td>
        </tr>
        <%
		Rs.MoveNext();
		}
		Rs.Close();
		%>
      </table>
      <p>&nbsp;</p>
      <p class="textonegrita">&nbsp;</p>
      <p>&nbsp;</p>
    <p>&nbsp;</p></td>
  </tr>
</table>

</body>
</html>
<%
Session("mensaje")="";
Desconectar();
%>