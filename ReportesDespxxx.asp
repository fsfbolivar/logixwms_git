
 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var nroOrd="";
var nbClte="";
var fdesde="";
var fhasta="";

if(Request.Form.Count>0)
{
	nroOrd=Request.Form("nro_ord1")+"";
	nbClte=Request.Form("nb_clte")+"";
	fdesde=Request.Form("fdesde")+"";
	fhasta=Request.Form("fhasta")+"";
}
%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Excel()
{
	document.getElementById("Q").submit();
	//window.open("ReportesDespExcel.asp","impetiqueta","width=700,height=500,modal=yes,status=no, menubar=no, scroll=1, scrollbars=1");
}
function Ordenar(tipo,dir,lugar)
{
	document.getElementById("BUSQ_ORD").action+="?tipo="+tipo+"&dir="+dir+"&lug="+lugar;
	document.getElementById("BUSQ_ORD").submit();
}
function BusqDesk()
{
if(document.getElementById("nro_ord1").value=="" && document.getElementById("nb_clte").value=="" && document.getElementById("fdesde").value=="" && document.getElementById("fhasta").value=="")
{
	alert("Debe indicar alguno de los campos para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_DESK").submit();
}
}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
</script>
<table width="758" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          <p>&nbsp;</p></td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="115"></td>
    <td width="631"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
	                <FORM ID='FRM_DESK' NAME='FRM_DESK' action='ReportesDesp.asp' METHOD='POST'>
        <tr>
          <td colspan="6" class="tituloblanco"><div align="center">Busqueda</div></td>
        </tr>

        <tr class="textoformulario">
          <td width="143" class="textoformulario">Nro pedido </td>
          <td width="128">&nbsp;</td>
          <td width="172"><input name="nro_ord1"  id="nro_ord1" type="text" class="texto"></td>
          <td width="19">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Nombre del cliente </td>
          <td>&nbsp;</td>
          <td><input name="nb_clte" id="nb_clte" type="text" class="texto"></td>
          <td>&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Fecha del pedido (desde) </td>
          <td>&nbsp;</td>
          <td><input name="fdesde" id="fdesde" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fdesde","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fdesde");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td width="82">&nbsp;</td>
          <td width="80" >&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="">Fecha del pedido (hasta)</td>
          <td>&nbsp;</td>
          <td><input name="fhasta" id="fhasta" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fhasta","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fhasta");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td><input name="button" type="button" class="textonegrita" value="Buscar" onclick="JavaScript:BusqDesk();"></td>
          <td ><input name="button2" type="button" class="textonegrita" value="Exp a Excel" onclick="JavaScript:Excel();"></td>
        </tr>
        </form>
        
	</table>
      <table width="100%"  border="0" cellpadding="3" cellspacing="0">
        <tr class="">
          <td height="9"><p>&nbsp;</p>
            <table width="100%"  border="0" cellpadding="1" cellspacing="1" class="tablamenu">
              <tr>
                <td colspan="8" class="tituloblanco">Reporte</td>
              </tr>
              <tr class="titulo_subgrupo">
                <td WIDTH='44'>Status</td>
                <td WIDTH='298'>Cliente</td>
                <td WIDTH='49'>Usuario</td>
                <td width="95">Nro pedido </td>
                <td width="65">Fecha orden </td>
                <td width="14"><p>Fecha status</p>                </td>
                <td width="14"><p>Dias</p>                </td>
                <td width="15">Titulos</td>
                <td width="30">Ejemplares</td>
              </tr>
              <%

              
				strSQL="SELECT (SELECT COUNT(DISTINCT ID_PROD) FROM T_SANT_DET_ORD WHERE ID_ORD=A.ID_ORD) PROD,(SELECT SUM(CANT_ORD) FROM T_SANT_DET_ORD WHERE ID_ORD=A.ID_ORD) CANTIDAD, B.NB_USR,CASE A.ID_ST_ORD WHEN 5 THEN 'Eliminada' WHEN 0 THEN 'Nueva' WHEN 2 THEN 'Embalada' WHEN 3 THEN 'Despachada' WHEN 1 THEN 'En proceso' END STATUS, a.id_ord, A.NB_CLTE, A.ID_ORD_GP, CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD,  CASE A.ID_ST_ORD WHEN 2 THEN  CONVERT(VARCHAR(12),A.FE_EMB_ORD,103) WHEN 3 THEN CONVERT(VARCHAR(12),A.FE_DESP_ORD,103) END FE_ST , DATEDIFF(D,A.FE_ING_ORD,CASE A.ID_ST_ORD WHEN 2 THEN  A.FE_EMB_ORD WHEN 3 THEN A.FE_DESP_ORD END) DIAS ";
				strSQL+=" FROM T_SANT_ORD A	, T_SANT_USR B	WHERE A.ID_CHEQ=B.ID_USR ";
				if(Request.Form.Count>0)
				{
				if(nroOrd.length>0)
				{
					strSQL+=" AND A.ID_ORD_GP='"+nroOrd+"'" ;
				}
				if(nbClte.length>0)
				{
					strSQL+=" AND A.NB_CLTE LIKE '%"+nbClte+"%'" ;
				}
				if(fdesde.length>0)
				{
					strSQL+=" AND CONVERT(VARCHAR(12),A.FE_ING_ORD,103) >= '"+fdesde+"'" ;
				}
				if(fhasta.length>0)
				{
					strSQL+=" AND CONVERT(VARCHAR(12),A.FE_ING_ORD,103) <= '"+fhasta+"'" ;
				}
				}
				strSQL+=" ORDER BY B.NB_USR,NB_CLTE, FE_ING_ORD";
				//Response.Write(strSQL);
				Response.Flush();
				Rs=Ejecutar(strSQL,2);
				while(!Rs.EOF)
				{
				%>
					<tr class="textoformulario">
					  <td><%=Rs("STATUS")%></td>
					  <td><a href='Procesardet.asp?idord=<%=Rs("ID_ORD")%>' class='texoformulario'><%=Rs("NB_CLTE") %></a></td>
					  <td><%=Rs("NB_USR")%></td>
					  <td><%=Rs("ID_ORD_GP")%></td>
					  <td><%=Rs("FE_ING_ORD")%></td>
					  <td><%=Rs("FE_ST")%></td>
					  <td align='right'><%=Rs("DIAS")%></td>
				      <td><%=Rs("PROD")%></td>
				      <td><%=Rs("CANTIDAD")%></td>
					</tr>
				<%
				Rs.MoveNext();
				}
				Rs.Close();
            var strSQL2=strSQL;
              %>
            </table>            
            <p>&nbsp;</p>
			<%
			strSQL="";
			strSQL="SELECT B.ID_ST,B.NB_ST,COUNT(a.id_ord) cta";
			strSQL+=" FROM 	T_SANT_ORD A right JOIN T_SANT_ST_ORD B ";
			strSQL+=" ON A.ID_ST_ORD=B.ID_ST ";
			
				if(nroOrd.length>0)
				{
					strSQL+=" AND A.ID_ORD_GP='"+nroOrd+"'" ;
				}
				if(nbClte.length>0)
				{
					strSQL+=" AND A.NB_CLTE LIKE '%"+nbClte+"%'" ;
				}
				if(fdesde.length>0)
				{
					strSQL+=" AND CONVERT(VARCHAR(12),A.FE_ING_ORD,103) >= '"+fdesde+"'" ;
				}
				if(fhasta.length>0)
				{
					strSQL+=" AND CONVERT(VARCHAR(12),A.FE_ING_ORD,103) <= '"+fhasta+"'" ;
				}
			strSQL+=" GROUP BY B.ID_ST,NB_ST ORDER BY ID_ST ";
			Rs=Ejecutar(strSQL,2);
			%>
            <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="tabla_borde_negro">
              <tr>
                <td class="tituloblanco">Total de pedidos</td>
              </tr>			
            <%
            while(!Rs.EOF)
			{%>
              <tr>
                <td class="textonegrita"><p class="textoformulario"><%=Rs("NB_ST")%>: <%=Rs("CTA")%></p>                </td>
              </tr>
             <%
             Rs.MoveNext();
             }
             Rs.Close();
             %>
            </table>            
            <p>&nbsp;</p></td>
        </tr>
            <FORM ID='Q' NAME='Q' ACTION='ReportesDespExcel.asp' METHOD='POST' TARGET='_BLANK'>
            <INPUT TYPE='HIDDEN' NAME='QUERY' ID='QUERY' VALUE='<%=escape(strSQL2)%>'>
           <INPUT TYPE='HIDDEN' NAME='QUERY2' ID='QUERY2' VALUE='<%=escape(strSQL)%>'>
			</FORM>
      </table>
</td>
  </tr>
</table>
</body>
</html>


<%

Desconectar();
%>