
 <%@ Language="JavaScript" %>
<!-- #include file="include/incdb.asp" //-->
<!-- #include file="include/sess.asp" //-->
<%
//Se valida que aun la session sea valida
ValidarSession();


if(Conectar()!=0)
	Response.Redirect("index.asp?error=1");

var nroOrd="";
var nbClte="";
var fdesde="";
var fhasta="";

if(Request.Form.Count>0)
{
	nroOrd=Request.Form("nro_ord1")+"";
	nbClte=Request.Form("nb_clte")+"";
	nb_guia=Request.Form("nb_guia")+"";
	fdesde=Request.Form("fdesde")+"";
	fhasta=Request.Form("fhasta")+"";
}


%>
 <html>
<head>
<title>GRUPO SANTILLANA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">



<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.style1 {font-size: 16px}
-->
</style>
<link href="INCLUDE/estilos.css" rel="stylesheet" type="text/css">

</head>

<body>

<script language="JavaScript" type="text/JavaScript" src="include/fecha.js"></script>
<script language="JavaScript" src="include/calendar1.js"></script><!-- Date only with year scrolling -->
<script language="JavaScript" src="include/Validaciones.js"></script>
<script>
function Excel()
{
	document.getElementById("Q").submit();
}
function Ordenar(tipo,dir,lugar)
{
	document.getElementById("BUSQ_ORD").action+="?tipo="+tipo+"&dir="+dir+"&lug="+lugar;
	document.getElementById("BUSQ_ORD").submit();
}
function BusqDesk()
{
if(document.getElementById("nro_ord1").value=="" && document.getElementById("nb_clte").value=="" && document.getElementById("fdesde").value=="" && document.getElementById("fhasta").value=="" && document.getElementById("nb_guia").value=="")
{
	alert("Debe indicar alguno de los campos para realizar la busqueda");
	return(false);
}
else
{
	document.getElementById("FRM_DESK").submit();
}
}
function IniciarCalendario(nbCampo)
{
	var cal1 = new calendar1(document.forms['FRM_DESK'].elements[nbCampo]);
	cal1.year_scroll = true;
	cal1.time_comp = false;
	cal1.popup();
}
</script>
<table width="1033" border="0" align="center" cellpadding="5" cellspacing="2">
  <tr>
    <td colspan="2"><table width="758" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td colspan="6" class="textonegrita style1" ><p><img src="images/logo.png" width="257" height="53"></p>
          <p>&nbsp;</p></td>
      </tr>

    </table></td>
  </tr>
  <tr>
    <td colspan="2" class="textonegrita">Bienvenido(a),  <%=Request.Cookies("usr")("nombre")%>  </td>
  </tr>
  <tr>
    <td width="107"></td>
    <td width="900"></td>
  </tr>
  <tr>
    <td valign="top">
    <!-- #include file="include/menu.asp"//-->
    <p>&nbsp;</p>      </td>
    <td valign="top" nowrap>
      <table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tabla_borde_negro">
	                <FORM ID='FRM_DESK' NAME='FRM_DESK' action='ReportesT.asp' METHOD='POST'>
        <tr>
          <td colspan="6" class="tituloblanco"><div align="center">Busqueda</div></td>
        </tr>

        <tr class="textoformulario">
          <td width="143" height="19" class="textoformulario">Nro Pedido </td>
          <td width="128">&nbsp;</td>
          <td width="172"><input name="nro_ord1"  id="nro_ord1" type="text" class="texto"></td>
          <td width="19">&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Cedula del Transportista </td>
          <td>&nbsp;</td>
          <td><input name="nb_clte" id="nb_clte" type="text" class="texto"></td>
          <td>&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Gu&iacute;a de transporte </td>
          <td>&nbsp;</td>
          <td><input name="nb_guia" id="nb_guia" type="text" class="texto"></td>
          <td>&nbsp;</td>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="textoformulario">Fecha del pedido (desde) </td>
          <td>&nbsp;</td>
          <td><input name="fdesde" id="fdesde" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fdesde","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fdesde");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td width="82">&nbsp;</td>
          <td width="80" >&nbsp;</td>
        </tr>
        <tr class="textoformulario">
          <td class="">Fecha del pedido (hasta)</td>
          <td>&nbsp;</td>
          <td><input name="fhasta" id="fhasta" type="text" class="texto" size="15" maxlength="10" ONCHANGE='JavaScript:ValidaFecha("DD/MM/YYYY","fhasta","-1");'>
          <A HREF='JavaScript:IniciarCalendario("fhasta");'><img border='0' src='images/cal.gif'></A></td><td>&nbsp;</td>
          <td><input name="button" type="button" class="textonegrita" value="Buscar" onClick="JavaScript:BusqDesk();"></td>
          <td >&nbsp;</td>
        </tr>
        </form>
	</table>
      <table width="100%"  border="0" cellpadding="3" cellspacing="0">
        <tr class="">
          <td height="9"><p>&nbsp;</p>
            <table width="100%"  border="0" cellpadding="1" cellspacing="1" class="tablamenu">
              <tr>
                <td colspan="11" class="tituloblanco">Reporte</td>
              </tr>
              <tr class="titulo_subgrupo">
                <td WIDTH='55' height="32">Status</td>
                <td WIDTH='163'>Cliente</td>
                <td WIDTH='75'>Usuario</td>
                <td width="72">Nro pedido </td>
                <td width="71">Fecha orden </td>
                <td width="73">Fecha despachada </td>
                <td width="79">CI transportista </td>
                <td width="87">Nombre transportista </td>
                <td width="83">Apellido transportista</td>
                <td width="37">Titulos</td>
                <td width="65">Ejemplares</td>
              </tr>
              <%


			
				if(Request.Form.Count>0)
				{
				strSQL="";
				
				strSQL="SELECT DISTINCT (SELECT COUNT(DISTINCT ID_PROD) FROM T_SANT_DET_ORD WHERE ID_ORD=A.ID_ORD) PROD, (SELECT SUM(CANT_ORD) FROM T_SANT_DET_ORD WHERE ID_ORD=A.ID_ORD) CANTIDAD, D.NB_USR, CASE A.ID_ST_ORD WHEN 5 THEN 'Eliminada' WHEN 0 THEN 'Nueva' WHEN 2 THEN 'Embalada' WHEN 3 THEN 'Despachada'WHEN 1 THEN 'En proceso' END STATUS, A.ID_ORD, A.NB_CLTE, A.ID_ORD_GP, CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD, CONVERT(VARCHAR(12),A.FE_DESP_ORD,103) FE_DESP_ORD, C.CI_Transp, C.NB_TRANSP, C.AP_TRANSP FROM T_SANT_GUIA_CARGA AS B INNER JOIN T_SANT_ORD AS A ON B.ID_GUIA_CARGA=A.ID_GUIA INNER JOIN T_SANT_TRANSP AS C ON B.ID_TRANSP=C.CI_TRANSP INNER JOIN T_SANT_USR AS D ON B.ID_USR=D.ID_USR WHERE A.ID_ST_ORD IN ('3')"
					
//				strSQL+="SELECT (SELECT COUNT(DISTINCT ID_PROD) FROM T_SANT_DET_ORD WHERE ID_ORD=A.ID_ORD) PROD,"
//				strSQL+="(SELECT SUM(CANT_ORD) FROM T_SANT_DET_ORD WHERE ID_ORD=A.ID_ORD) CANTIDAD,B.NB_USR,"
//				strSQL+="CASE A.ID_ST_ORD WHEN 5 THEN 'Eliminada' WHEN 0 THEN 'Nueva' WHEN 2 THEN 'Embalada' WHEN 3 THEN 'Despachada'WHEN 1 THEN 'En proceso' END STATUS,a.id_ord,A.NB_CLTE,A.ID_ORD_GP,"
//				strSQL+="CONVERT(VARCHAR(12),A.FE_ING_ORD,103) FE_ING_ORD,CONVERT(VARCHAR(12),A.FE_DESP_ORD,103)"
//				strSQL+=" FE_DESP_ORD,A.CI_Transp,C.CI_Transp,C.NB_Transp,AP_Transp" 
//				strSQL+=" FROM T_SANT_GUIA_CARGA D, T_SANT_ORD A,T_SANT_USR B,T_SANT_TRANSP C  WHERE D.ID_GUIA_CARGA=A.ID_GUIA AND C.CI_TRANSP=D.ID_TRANSP and A.ID_CHEQ=B.ID_USR and ID_ST_ORD in ('3')";

				if(nroOrd.length>0)
				{
					strSQL+=" AND A.ID_ORD_GP='"+nroOrd+"'" ;
				}
				if(nbClte.length>0)
				{
					strSQL+=" AND B.ID_TRANSP = '"+nbClte+"'" ;
				}
				if(nb_guia.length>0){
					strSQL+=" AND B.ID_GUIA_CARGA = "+nb_guia+"";
				}
				if(fdesde.length>0)
				{
					strSQL+=" AND A.FE_ING_ORD >= CONVERT(DATETIME,'"+fdesde+"',103)" ;
				}
				if(fhasta.length>0)
				{
					strSQL+=" AND A.FE_ING_ORD <= CONVERT(DATETIME,'"+fhasta+"',103)" ;
				}
								
				strSQL+=" ORDER BY A.NB_CLTE, A.FE_ING_ORD";
				
				//Response.Write(strSQL); 
				Response.Flush(); 
				//Response.End();
				Rs=Ejecutar(strSQL,2);
				
				while(!Rs.EOF)
				{
						%>
					<tr class="textoformulario">
					  <td><%=Rs("STATUS")%></td>
					  <td><a href="GuiaCarga.asp?SOLIC=<%=Rs("CI_Transp")%>&ID=<%=Rs("FE_DESP_ORD")%>"><%=Rs("NB_CLTE") %></td>
					  <td><%=Rs("NB_USR")%></td>
					  <td><%=Rs("ID_ORD_GP")%></td>
					  <td><%=Rs("FE_ING_ORD")%></td>
					  <td><%=Rs("FE_DESP_ORD")%></td>
					  <td><%=Rs("CI_Transp")%></td>
					  <td><%=Rs("NB_Transp")%></td>
					  <td><%=Rs("AP_Transp")%></td>
				      <td><%=Rs("PROD")%></td>
				      <td><%=Rs("CANTIDAD")%></td>
			        </tr>
				<%
				
				
				Rs.MoveNext();
				}
				Rs.Close();
				
				 var strSQL2=strSQL;
				}
           
              %>
            </table>            
            
        </tr>
            <FORM ID='Q' NAME='Q' ACTION='ReportesDespExcel.asp' METHOD='POST' TARGET='_BLANK'>
            <INPUT TYPE='HIDDEN' NAME='QUERY' ID='QUERY' VALUE='<%//=escape(strSQL2)%>'>
           
			</FORM>
      </table>
</td>
  </tr>
</table>
</body>
</html>


<%

Desconectar();
%>